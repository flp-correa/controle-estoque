Yahto = Yahto || {};

Yahto.MaskEnderecoLocalizacao = (function() {
	
	function MaskEnderecoLocalizacao() {
		this.inputEnderecoLocalizacao = $('.js-endereco-localizacao');
		this.blocado = $('.js-blocado');
	}
	
	MaskEnderecoLocalizacao.prototype.iniciar = function() {
		this.blocado.on('switchChange.bootstrapSwitch',onAlterarMascaraEndereco.bind(this));
		
		onAlterarMascaraEndereco.call(this, null, Boolean(this.blocado.val()));
	}
	
	function onAlterarMascaraEndereco(event, state) {
		if (state) {
			this.inputEnderecoLocalizacao.mask('00-00-000');
		} else {
			this.inputEnderecoLocalizacao.mask('00-00-000-00-0');
		}
		
		if (event != null) {
			this.inputEnderecoLocalizacao.val('');
		}
	}
	
	return MaskEnderecoLocalizacao;
	
}());

$(function(){
	var maskEnderecoLocalizacao = new Yahto.MaskEnderecoLocalizacao();
	maskEnderecoLocalizacao.iniciar();
});