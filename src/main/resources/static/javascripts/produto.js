Yahto = Yahto || {};

Yahto.Produto = (function(){
	
	function Produto() {
		this.possuiEstoque = $('.js-possui-estoque');
		this.vende = $('.js-vende');
		this.lancheOuPrato = $('.js-lanche-ou-prato');
		this.localFixo = $('.js-local-fixo');
		this.enviarCompra = $('.js-enviar-compra');
		this.containerLocalFixo = $('.js-container-local-fixo');
		this.containerLocalizacao = $('.js-container-localizacao');
		this.containerIngredientes = $('.js-container-ingredientes');
		this.containerLancheOuPrato = $('.js-container-lanche-ou-prato');
		this.containerQuiloCozinha = $('.js-container-quilo-cozinha');
		this.containerEnviarCompra = $('.js-container-enviar-compra');
		this.containerValorUnitario = $('.js-container-valor-unitario');
		this.containerQtdEstoque = $('.js-container-qtd-estoque');
		this.localizacoes = $('#localizacoes');
		this.jsColExclusaoLocalizacao = '.js-col-exclusao-localizacao';
	}
	
	Produto.prototype.iniciar = function() {
		this.localFixo.on('switchChange.bootstrapSwitch',onLocalFixo.bind(this));
		this.vende.on('switchChange.bootstrapSwitch',onVende.bind(this));
		this.possuiEstoque.on('switchChange.bootstrapSwitch',onPossuiEstoque.bind(this));
		this.lancheOuPrato.on('switchChange.bootstrapSwitch',onLancheOuPrato.bind(this));
		
		$('#quantidadeEstoque').val($('#quantidadeEstoque').val().replace(',000', ''));
		$('#valorUnitario').val(Yahto.formatarMoeda($('#valorUnitario').val()));
	}
	
	function onVende(event, state) {
		if (state) {			
			this.containerValorUnitario.removeClass('hidden');
		} else {
			this.containerValorUnitario.addClass('hidden');
		}
	}
	
	function onLocalFixo(event, state) {
		if (state) {			
			this.localizacoes.removeClass('hidden');
			$(this.jsColExclusaoLocalizacao).removeClass('hidden');
		} else {
			this.localizacoes.addClass('hidden');
			$(this.jsColExclusaoLocalizacao).addClass('hidden');
		}
	}
	
	function onLancheOuPrato(event, state) {
		this.containerIngredientes.toggleClass('hidden', !state);
		this.containerQuiloCozinha.toggleClass('hidden', !state);
		this.containerQuiloCozinha.find('[type=checkbox]').bootstrapSwitch('state', false);
	}
	
	function onPossuiEstoque(event, state) {
		this.containerLocalizacao.toggleClass('hidden', !state);
		this.containerLocalFixo.toggleClass('hidden', !state);
		this.containerEnviarCompra.toggleClass('hidden', !state);
		this.containerQtdEstoque.toggleClass('hidden', !state);
		this.containerLancheOuPrato.toggleClass('hidden', state);
		
		this.localFixo.bootstrapSwitch('state', state);
		this.enviarCompra.bootstrapSwitch('state', false);
		this.lancheOuPrato.bootstrapSwitch('state', false);
		this.vende.bootstrapSwitch('state', true);
		this.vende.bootstrapSwitch('readonly', !state);
	}
	
	return Produto;
}());

$(function(){
	var produto = new Yahto.Produto();
	produto.iniciar();
});