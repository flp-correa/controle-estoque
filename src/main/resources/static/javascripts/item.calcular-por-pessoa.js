Yahto.ItemCalcularPorPessoa = (function(){
	
	function ItemCalcularPorPessoa(tabelaItens, vendaTipoPagamento, venda) {
		this.tabelaItens = tabelaItens;
		this.vendaTipoPagamento = vendaTipoPagamento; 
		this.venda = venda;
		this.linkCalculoItensPorPessoa = $('.js-link-calculo-itens-por-pessoa');
		this.confirmarBtn = $('.js-confirmar-calc-itens-btn');
		this.resetBtn = $('.js-reset-calc-itens-btn');
		this.totalCalcItensInput = $('#totalCalcItens');
		this.qtdPessoasInput = $('#qtdPessoas');
		this.totalPorPessoaInput = $('#totalPorPessoa');
		this.totalPagoInput = $('#totalPago');
		
		this.itensSelecionados = null;
		this.itensPagos = null;
	}
	
	ItemCalcularPorPessoa.prototype.iniciar = function() {
		this.linkCalculoItensPorPessoa.on('click', onLinkClicado.bind(this));
		$('.js-selecao-itens').on('click', onSelecaoClicado.bind(this));
		$('.js-container-porcent-pago').find('input').on('keyup', onKeyupPorcentagem.bind(this));
		$('.js-container-porcent-pago').find('input').on('blur', function(){$('.js-nome-produto-input').focus();});
		this.confirmarBtn.on('click', onConfirmarClicado.bind(this));
		this.resetBtn.on('click', onResetClicado.bind(this));
		this.qtdPessoasInput.on('keyup', onCalcularValores.bind(this));
		
		if (this.tabelaItens != null) {
			this.tabelaItens.on('tabela-itens-atualizada', onTabelaItensAtualizada.bind(this));
		}
		
		this.itensSelecionados = new Array();
		this.itensPagos = new Array();
	}
	
	function onLinkClicado() {
		this.linkCalculoItensPorPessoa.closest('fieldset').find('.row').toggleClass('hidden');
	}
	
	function onTabelaItensAtualizada() {
		var maskMoney = new Yahto.MaskMoney();
	    maskMoney.enable();
		
		$('.js-selecao-itens').unbind('click');
		$('.js-container-porcent-pago').unbind('click');
		$('.js-selecao-itens').on('click', onSelecaoClicado.bind(this));
		$('.js-container-porcent-pago').find('input').on('keyup', onKeyupPorcentagem.bind(this));
		$('.js-container-porcent-pago').find('input').on('blur', function(){$('.js-nome-produto-input').focus();});
		$('.js-container-porcent-pago').find('input').maskNumber({ decimal: ',', thousands: '.' });
		
		this.itensSelecionados = new Array();
		this.confirmarBtn.attr('disabled', true);
		this.resetBtn.attr('disabled', true);
		
		resetItensPagos.call(this);
		resetValores.call(this);
	}
	
	function onSelecaoClicado(evento) {
		var containerPorcentagemItem = $(evento.currentTarget).closest('.js-tabela-item').find('.js-container-porcent-pago');
		if ($(evento.currentTarget).is(':checked')) {
			this.itensSelecionados.push(new ItemPedidoVenda($(evento.currentTarget).data('codigo'), 
															$(evento.currentTarget).data('valor-item'),
															containerPorcentagemItem.find('input').val(),
															this.itensPagos));
			containerPorcentagemItem.removeClass('hidden');
		} else {
			this.itensSelecionados = this.itensSelecionados.filter(value => value.codigo != $(evento.currentTarget).data('codigo'));
		}
		containerPorcentagemItem.toggleClass('hidden', !$(evento.currentTarget).is(':checked'));
		if (this.itensSelecionados.length > 0) {
			var totalValores = this.itensSelecionados.reduce(( total, i ) => total + (i.valor * i.porcentPagar / 100), 0);
			this.totalCalcItensInput.val(Yahto.formatarMoeda(totalValores));
		} else {
			this.totalCalcItensInput.val('0,00');
		}
		this.confirmarBtn.attr('disabled', this.itensSelecionados.length < 1);
		this.resetBtn.attr('disabled', this.itensSelecionados.length < 1);
		onCalcularValores.call(this);
	}
	
	function onKeyupPorcentagem(evento) {
		var porcentInput = $(evento.currentTarget);
		var codigoItem = $(evento.currentTarget).closest('.js-tabela-item').find('.js-selecao-itens').data('codigo');
		
		var itemExiste = this.itensPagos.find(i => i.codigo == codigoItem);
		var porcentPagar = itemExiste == undefined ? 100 : itemExiste.porcentPagar;
		
		if (porcentInput.val().trim() == '' || numeral(porcentInput.val()) > porcentPagar) {
			porcentInput.val('0,00');
		}
		
		this.itensSelecionados.forEach(function(e) {
			if (e.codigo == codigoItem) {
				e.porcentPagar = numeral(porcentInput.val()).value();
			}
		});
		
		var totalValores = this.itensSelecionados.reduce(( total, i ) => total + (i.valor * i.porcentPagar / 100), 0);
		this.totalCalcItensInput.val(Yahto.formatarMoeda(totalValores));
		onCalcularValores.call(this);
	}
	
	function onConfirmarClicado(evento) {
		if (this.venda != null) {			
			atualizarDinheiroOuCartaoVenda.call(this, evento);
		}
		
		$.map($('.js-tabela-item'), function(e) {
			if ($(e).find('.js-selecao-itens').is(':checked') && !$(e).find('.js-selecao-itens').prop('disabled')) {
				var porcentPagar = 100;
				var itemPagoAgora = this.itensSelecionados.find(i => i.codigo == $(e).find('.js-selecao-itens').data('codigo'));
				
				var itemPagoExiste = this.itensPagos.find(i => i.codigo == itemPagoAgora.codigo);
				if (itemPagoExiste == undefined) {					
					itemPagoAgora.porcentPago = itemPagoAgora.porcentPagar;
					itemPagoAgora.porcentPagar = 100 - itemPagoAgora.porcentPago;
					porcentPagar = itemPagoAgora.porcentPagar;
					this.itensPagos.push(itemPagoAgora);
				} else {
					itemPagoExiste.porcentPago += itemPagoAgora.porcentPagar;
					itemPagoExiste.porcentPagar = 100 - itemPagoExiste.porcentPago;
					porcentPagar = itemPagoExiste.porcentPagar;
				}
				
				if (porcentPagar < 0.02) {
					$(e).find('.js-selecao-itens').attr('disabled', true);
					$(e).removeClass('y-tabela-item__pendente');
					$(e).addClass('y-tabela-item__calculado');
					$(e).find('.js-container-porcent-pago input').val(100);
				} else {
					$(e).addClass('y-tabela-item__pendente');
					$(e).find('.js-selecao-itens').prop('checked', false);
					$(e).find('.js-container-porcent-pago input').val(Yahto.formatarMoeda(porcentPagar));
				}
				$(e).find('.js-container-porcent-pago').addClass('hidden');
			}
		}.bind(this));
		
		this.itensSelecionados = new Array();
		this.confirmarBtn.attr('disabled', true);
		resetValores.call(this);
		
		this.totalPagoInput.val(Yahto.formatarMoeda(this.itensPagos.reduce(( total, i ) => total + (i.valor * i.porcentPago / 100), 0)));
	}
	
	function atualizarDinheiroOuCartaoVenda(evento) {
		if ($(evento.currentTarget).data('confirmar-calc-itens') == 'dinheiro') {
			var totalDinheiro = numeral(this.venda.getValorDinheiro()) + numeral(this.totalPorPessoaInput.val());
			this.venda.setValorDinheiro(Yahto.formatarMoeda(totalDinheiro));
		} else if ($(evento.currentTarget).data('confirmar-calc-itens') == 'cartao') {
			var totalCartao = numeral(this.venda.getValorCartao()) + numeral(this.totalPorPessoaInput.val());
			this.venda.setValorCartao(Yahto.formatarMoeda(totalCartao));
		}
		
		if (numeral(this.venda.getValorDinheiro()) != 0 && numeral(this.venda.getValorCartao()) == 0) {
			this.vendaTipoPagamento.setTipoPagamento('input[id=id_DINHEIRO]');
		} else if (numeral(this.venda.getValorDinheiro()) == 0 && numeral(this.venda.getValorCartao()) != 0) {
			this.vendaTipoPagamento.setTipoPagamento('input[id=id_CARTAO]');
		} else {
			this.vendaTipoPagamento.setTipoPagamento('input[id=id_DINHEIRO_E_CARTAO]');
		}
	}
	
	function onResetClicado() {
		this.itensSelecionados = new Array();
		$.map($('.js-tabela-item'), function(e) {
			$(e).find('.js-selecao-itens').attr('disabled', false);
			$(e).find('.js-selecao-itens').prop('checked', false);
			$(e).removeClass('y-tabela-item__calculado');
			$(e).removeClass('y-tabela-item__pendente');
		});
		this.confirmarBtn.attr('disabled', true);
		this.resetBtn.attr('disabled', true);
		
		if (this.venda != null) {			
			this.venda.setValorDinheiro('0,00');
			this.venda.setValorCartao('0,00');
			this.vendaTipoPagamento.setTipoPagamento('input[id=id_DINHEIRO]');
		}
		
		resetItensPagos.call(this);
		resetValores.call(this);
	}
	
	function resetItensPagos() {
		this.itensPagos = new Array();
		$.map($('.js-container-porcent-pago'), function(e) {
			$(e).find('input').val(100);
			$(e).addClass('hidden');
		});
	}
	
	function onCalcularValores() {
		if (this.qtdPessoasInput.val() <= 0) {
			this.qtdPessoasInput.val(1);
		}
		this.totalPorPessoaInput.val(Yahto.formatarMoeda(numeral(this.totalCalcItensInput.val()) / numeral(this.qtdPessoasInput.val())));
	}
	
	function resetValores() {
		this.totalCalcItensInput.val('0,00');
		this.qtdPessoasInput.val(1);
		this.totalPorPessoaInput.val('0,00');
		this.totalPagoInput.val('0,00');
	}
	
	function ItemPedidoVenda(codigo, valor, porcentPagar) {
		this.codigo = codigo;
		this.valor = numeral(valor).value();
		this.porcentPagar = numeral(porcentPagar).value();
		this.porcentPago = 0;
	}
	
	return ItemCalcularPorPessoa;
}());