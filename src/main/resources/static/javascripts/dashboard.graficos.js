var Yahto = Yahto || {};

Yahto.GraficoFaturamentoPorMes = (function () {
	
	function GraficoFaturamentoPorMes() {
		this.ctx = $('#graficoFaturamentosPorMes')[0].getContext('2d');
	}
	
	GraficoFaturamentoPorMes.prototype.iniciar = function() {
		$.ajax({
			url: $('.js-base-url').data('base-url') + '/vendas/totalFaturamentoPorMes',
			method: 'GET', 
			success: onDadosRecebidos.bind(this)
		});
	}
	
	function onDadosRecebidos(faturamentoPorMes) {
		var meses = [];
		var valores = [];
		
		faturamentoPorMes.forEach(function(obj) {
			meses.unshift(obj.mes);
			valores.unshift(obj.total);
		});
		
		var graficoFaturamentosPorMes = new Chart(this.ctx, {
			type: 'bar',
			data: {
				labels: meses,
				datasets: [{
					label: 'Faturamento por mês',
					data: valores,
					backgroundColor: 'rgba(26,179,148,0.5)',
					borderColor: 'rgba(26,179,148,1)',
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true,
							callback: function (value) {
	                            return numeral(value).format('$ 0,0')
	                        },
	                        fontColor: "black"
						}
					}],
					xAxes: [{
						ticks: {
							fontColor: "black"
						}
					}]
				},
				tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var value = Yahto.formatarMoeda(data.datasets[0].data[tooltipItem.index]);
							return 'R$ ' + value;
		                }
					}
				},
			}
		});		
	}
	
	return GraficoFaturamentoPorMes;
})();


Yahto.GraficoVendaPorMes = (function() {
	
	function GraficoVendaPorMes() {
		this.ctx = $('#graficoVendasPorMes')[0].getContext('2d');
	}
	
	GraficoVendaPorMes.prototype.iniciar = function() {
		$.ajax({
			url: $('.js-base-url').data('base-url') + '/vendas/totalPorMes',
			method: 'GET', 
			success: onDadosRecebidos.bind(this)
		});
	}
	
	function onDadosRecebidos(vendaMes) {
		var meses = [];
		var valores = [];
		vendaMes.forEach(function(obj) {
			meses.unshift(obj.mes);
			valores.unshift(obj.total);
		});
		
		var graficoVendasPorMes = new Chart(this.ctx, {
		    type: 'line',
		    data: {
		    	labels: meses,
		    	datasets: [{
		    		label: 'Vendas por mês',
		    		backgroundColor: "rgba(26,179,148,0.5)",
	                pointBorderColor: "rgba(26,179,148,1)",
	                pointBackgroundColor: "#fff",
	                data: valores
		    	}]
		    },
		});
	}
	
	return GraficoVendaPorMes;
	
}());


$(function() {
	var faturamentoPorMes = new Yahto.GraficoFaturamentoPorMes();
	faturamentoPorMes.iniciar();
	
	var graficoVendaPorMes = new Yahto.GraficoVendaPorMes();
	graficoVendaPorMes.iniciar();
});