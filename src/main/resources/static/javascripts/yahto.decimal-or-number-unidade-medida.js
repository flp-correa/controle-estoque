Yahto.DecimalOrNumberUnidadeMedida = (function() {
	
	function DecimalOrNumberUnidadeMedida(sigla, camposFormatar) {
		this.sigla = sigla;
		this.camposFormatar = camposFormatar;
		this.decimalOrNumber = $('.js-decimal-or-number');
	}
	
	DecimalOrNumberUnidadeMedida.prototype.iniciar = function() {
		if (this.sigla == 'Kg' || this.sigla == 'g') {
			formatarCampos.call(this, false);
		} else {
			formatarCampos.call(this, true);
		}
		var maskMoney = new Yahto.MaskMoney();
	    maskMoney.enable();
	}
	
	function formatarCampos(ehNumber) {
		for (var i = 0; i < this.camposFormatar.length; i++) {
			if (ehNumber) {
				this.camposFormatar[i].removeClass('js-decimal');
				this.camposFormatar[i].addClass('js-number'); 
			} else {
				this.camposFormatar[i].removeClass('js-number');
				this.camposFormatar[i].addClass('js-decimal');
			}
		}
	}
	
	return DecimalOrNumberUnidadeMedida;
	
}());