Yahto = Yahto || {};

Yahto.PesquisaRapidaProdutoMvtoEstoque = (function() {
	
	function PesquisaRapidaProdutoMvtoEstoque(movimentoEstoque) {
		this.movimentoEstoque = movimentoEstoque;
		this.pesquisaRapidaProdutosMvtoEstoqueModal = $('#pesquisaRapidaProdutosMvtoEstoque');
		this.nomeInput = $('#descricaoProdutoModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-produtos-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-produtos-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaProdutosMvtoEstoque');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-produto').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-produto');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
		
		this.containerBuscarPorFornecedor = $('.js-container-buscar-por-fornecedor');
		this.codigoFornecedor = $('#codigoFornecedor');
		this.checkboxBuscarPorFornecedor = $('#buscarPorFornecedor');
		
		this.tipoMovimento = '';
		
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	PesquisaRapidaProdutoMvtoEstoque.prototype.iniciar = function() {
		this.movimentoEstoque.on('tipo-movimento-selecionado', onTipoMovimentoSelecionado.bind(this));
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaProdutosMvtoEstoqueModal.on('shown.bs.modal', onModalShow.bind(this));
		
		Handlebars.registerHelper('json', function(obj) {
		    return JSON.stringify(obj);
		});
	}
	
	PesquisaRapidaProdutoMvtoEstoque.prototype.getEmitter = function() {
		return this.emitter;
	}
	
	function onTipoMovimentoSelecionado(evento, tipoMovimento, limparTela) {
		this.tipoMovimento = tipoMovimento;
	}
	
	function onModalShow() {
		this.containerTabelaPesquisa.addClass('hidden');
		this.nomeInput.focus();
		if (buscarPorFornecedor.call(this)) {
			onPesquisaRapidaClicado.call(this);
		}
	}
	
	function buscarPorFornecedor() {
		if (this.tipoMovimento == 'ENTRADA' && this.codigoFornecedor.val().trim() != '') {
			this.containerBuscarPorFornecedor.removeClass('hidden');
			this.checkboxBuscarPorFornecedor.prop('checked', true);
			return true;
		}
		this.containerBuscarPorFornecedor.addClass('hidden');
		this.checkboxBuscarPorFornecedor.prop('checked', false);
		return false;
	}
	
	function onPesquisaRapidaClicado(event) {
		this.containerTabelaPesquisa.removeClass('hidden');
		if (event != undefined) {
			event.preventDefault();
		}
		
		$.ajax({
			url: this.pesquisaRapidaProdutosMvtoEstoqueModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				descricao: this.nomeInput.val(),
				buscarPorFornecedor: this.checkboxBuscarPorFornecedor.is(':checked'),
				codigoFornecedor: this.codigoFornecedor.val(),
				tipoMovimento: this.tipoMovimento
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaProdutosMvtoEstoqueModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaProdutoMovtoEstoquePesquisaRapida = new Yahto.TabelaProdutoMovtoEstoquePesquisaRapida(
				this.movimentoEstoque, this.tipoMovimento, this.emitter, this.pesquisaRapidaProdutosMvtoEstoqueModal);
		tabelaProdutoMovtoEstoquePesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaProdutoMvtoEstoque;
	
}());

Yahto.TabelaProdutoMovtoEstoquePesquisaRapida = (function() {
	
	function TabelaProdutoMovtoEstoquePesquisaRapida(movimentoEstoque, tipoMovimento, emitterPesqProdMvtoEstoq, modal) {
		this.movimentoEstoque = movimentoEstoque;
		this.tipoMovimento = tipoMovimento;
		this.emitterPesqProdMvtoEstoq = emitterPesqProdMvtoEstoq;
		this.modalProduto = modal;
		this.produto = $('.js-produto-pesquisa-rapida');
	}
	
	TabelaProdutoMovtoEstoquePesquisaRapida.prototype.iniciar = function() {
		this.produto.on('click', onProdutoSelecionado.bind(this));
	}
	
	function onProdutoSelecionado(evento) {
		this.modalProduto.modal('hide');
		
		var produtoSelecionado = $(evento.currentTarget);
		var familiaUnidadeMedida = produtoSelecionado.data('familia-unidade-medida');
		var localizacao = produtoSelecionado.data('localizacao');
		
		$('#codigoProduto').val(produtoSelecionado.data('codigo-produto'));
		$('#descricaoProduto').val(produtoSelecionado.data('descricao-produto'));
		$('#localFixo').val(produtoSelecionado.data('local-fixo-produto'));
		$('#familiaUnidadeMedida').val(familiaUnidadeMedida);
		
		if (localizacao != null) {
			new Yahto.exibirCamposLocalizacaoMovtoEstoque(localizacao, true);
			
			$('.js-input-group').removeClass('input-group');
			$('.js-input-group').find('span').addClass('hidden');
			
			var objectEvent = { codigoEmpresa: localizacao.deposito.empresa.codigo, familiaUnidadeMedida: familiaUnidadeMedida };
			
			this.emitterPesqProdMvtoEstoq.trigger('produto-selecionado', objectEvent);
			
			$('.js-container-localizacao-transferencia').find('button').attr('disabled', this.tipoMovimento != 'TRANSFERENCIA');
			
			new Yahto.exibirCamposLocalizacaoMovtoEstoque(null, false);
			if (this.tipoMovimento == 'TRANSFERENCIA') {
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setEmitterPesqProdMvtoEstoq(null);
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setFamiliaUnidadeMedida(null);
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setCodigoProduto(produtoSelecionado.data('codigo-produto'));
			}
		} else {
			$('.js-input-group').addClass('input-group');
			$('.js-input-group').find('span').removeClass('hidden');
			$('#unidadeMedida').empty();
			$('#unidadeMedida').attr('disabled', true);
			$('.js-container-localizacao').find('button').attr('disabled', false);
			
			new Yahto.exibirCamposLocalizacaoMovtoEstoque(null, true);

			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setEmitterPesqProdMvtoEstoq(this.emitterPesqProdMvtoEstoq);
			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setFamiliaUnidadeMedida(familiaUnidadeMedida);
			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setCodigoProduto(null);
		}
		
	}
	
	return TabelaProdutoMovtoEstoquePesquisaRapida;
	
}());