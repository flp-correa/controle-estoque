Yahto = Yahto || {};

Yahto.PesquisaRapidaFornecedor = (function() {
	
	function PesquisaRapidaFornecedor() {
		this.pesquisaRapidaFornecedoresModal = $('#pesquisaRapidaFornecedores');
		this.nomeInput = $('#nomeEmpresarialFornecModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-fornecedores-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-fornecedores-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaFornecedores');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-fornecedor').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-fornecedor');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaFornecedor.prototype.iniciar = function() {
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaFornecedoresModal.on('shown.bs.modal', onModalShow.bind(this));
	}
	
	function onModalShow() {
		this.nomeInput.focus();
	}
	
	function onPesquisaRapidaClicado(event) {
		event.preventDefault();
		
		$.ajax({
			url: this.pesquisaRapidaFornecedoresModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				nomeEmpresarial: this.nomeInput.val()
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaFornecedoresModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaFornecedorPesquisaRapida = new Yahto.TabelaFornecedorPesquisaRapida(this.pesquisaRapidaFornecedoresModal);
		tabelaFornecedorPesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaFornecedor;
	
}());

Yahto.TabelaFornecedorPesquisaRapida = (function() {
	
	function TabelaFornecedorPesquisaRapida(modal) {
		this.modalFornecedor = modal;
		this.fornecedor = $('.js-fornecedor-pesquisa-rapida');
	}
	
	TabelaFornecedorPesquisaRapida.prototype.iniciar = function() {
		this.fornecedor.on('click', onFornecedorSelecionado.bind(this));
	}
	
	function onFornecedorSelecionado(evento) {
		this.modalFornecedor.modal('hide');
		
		var contabilidadeSelecionado = $(evento.currentTarget);
		$('#codigoFornecedor').val(contabilidadeSelecionado.data('codigo'));
		$('#cpfOuCnpjFornecedor').val(contabilidadeSelecionado.data('cpf-ou-cnpj'));
		$('#descricaoFornecedor').val(contabilidadeSelecionado.data('nome-empresarial'));
	}
	
	return TabelaFornecedorPesquisaRapida;
	
}());

$(function() {
	var pesquisaRapidaFornecedor = new Yahto.PesquisaRapidaFornecedor();
	pesquisaRapidaFornecedor.iniciar();
});