Yahto.TabelaItensVenda = (function() {
	
	function TabelaItensVenda(autocomplete) {
		this.autocomplete = autocomplete;
		this.tabelaProdutosContainer = $('.js-tabela-produtos-container');
		this.uuid = $('#uuid').val();
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	TabelaItensVenda.prototype.iniciar = function() {
		this.autocomplete.on('item-selecionado', onItemSelecionado.bind(this));
		
		bindQuantidade.call(this);
		bindValorUnitario.call(this);
		bindTabelaItem.call(this);
	}
	
	TabelaItensVenda.prototype.valorTotal = function() {
		return this.tabelaProdutosContainer.data('valor');
	}
	
	function onItemSelecionado(evento, item) {
		var resposta = $.ajax({
			url: 'item',
			method: 'POST',
			data: {
				codigoProduto: item.codigo,
				uuid: this.uuid
			}
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onItemAtualizadoNoServidor(html) {
		this.tabelaProdutosContainer.html(html);
		
		bindQuantidade.call(this);
		bindValorUnitario.call(this);
		
		var tabelaItem = bindTabelaItem.call(this); 
		this.emitter.trigger('tabela-itens-atualizada', tabelaItem.data('valor-total'));
	}
	
	function onQuantidadeItemAlterado(evento) {
		var input = $(evento.target);
		var quantidade = input.val();
		
		if (quantidade <= 0) {
			input.val(1);
			quantidade = 1;
		}
		
		var codigoProduto = input.data('codigo-produto');
		
		var resposta = $.ajax({
			url: 'item/' + codigoProduto,
			method: 'PUT',
			data: {
				quantidade: quantidade,
				uuid: this.uuid
			}
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onValorUnitarioItemAlterado(evento) {
		var input = $(evento.target);
		var valorUnitario = input.val();
		
		if (valorUnitario < 0) {
			input.val(0);
			valorUnitario = 0;
		}
		
		var codigoProduto = input.data('codigo-produto');
		
		var resposta = $.ajax({
			url: 'item/valor-unitario/' + codigoProduto,
			method: 'PUT',
			data: {
				valorUnitario: valorUnitario,
				uuid: this.uuid
			}
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onDoubleClick(evento) {
		$(this).toggleClass('solicitando-exclusao');
	}
	
	function onExclusaoItemClick(evento) {
		var codigoItem = $(evento.target).data('codigo-item-pedido-venda');
		var resposta = $.ajax({
			url: 'item/' + this.uuid + '/' + codigoItem,
			method: 'DELETE'
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function bindQuantidade() {
		var quantidadeItemInput = $('.js-tabela-produto-quantidade-item');
		quantidadeItemInput.on('blur', onQuantidadeItemAlterado.bind(this));
		quantidadeItemInput.maskNumber({ integer: true, thousands: '.' });
	}
	
	function bindValorUnitario() {
		var valorUnitarioInput = $('.js-tabela-produto-valor-unitario');
		valorUnitarioInput.on('blur', onValorUnitarioItemAlterado.bind(this));
		valorUnitarioInput.maskNumber({ decimal: ',', thousands: '.' });
	}
	
	function bindTabelaItem() {
		//if ($('.js-inputHiddenDetalhes').val() == 'false') {			
			var tabelaItem = $('.js-tabela-item');
			tabelaItem.on('dblclick', onDoubleClick);
			$('.js-exclusao-item-btn').on('click', onExclusaoItemClick.bind(this));
			return tabelaItem;
		//}
	}
	
	return TabelaItensVenda;
	
}());