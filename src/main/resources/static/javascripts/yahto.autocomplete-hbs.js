var Yahto = Yahto || {};

Yahto.AutocompleteHbs = (function() {
	
	function AutocompleteHbs(campoShowTabela, campoInput, template, campoSelecionado) {
		this.campoShowTabela = campoShowTabela;
		this.nomeInput = campoInput;
		this.template = Handlebars.compile(template.html());
		this.registroSelecionado = campoSelecionado;
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	AutocompleteHbs.prototype.iniciar = function() {
		options = {
				url: function(campoPesquisa) {
					return this.nomeInput.data('url') + '?campoPesquisa=' + campoPesquisa;
				}.bind(this),
				getValue: this.campoShowTabela,
				minCharNumber: 3,
				requestDelay: 300,
				adjustWidth: false,
				ajaxSettings: {
					contentType: 'application/json'
				},
				list: {
					onChooseEvent: onRegistroSelecionado.bind(this)
				},
				template: {
					type: 'custom',
					method: template.bind(this)
				}
		}
		
		function template(descricao, parametro) {
			return this.template(parametro);
		}
		
		this.nomeInput.easyAutocomplete(options);
	}
	
	function onRegistroSelecionado() {
		this.emitter.trigger(this.registroSelecionado, this.nomeInput.getSelectedItemData());
		this.nomeInput.val('');
		this.nomeInput.focus();
	}
	
	return AutocompleteHbs;
	
}());