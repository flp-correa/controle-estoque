Yahto = Yahto || {};

Yahto.TabelaItensPedidoVenda = (function() {
	
	function TabelaItensPedidoVenda(autocomplete, selecaoIngredienteAdicional) {
		this.autocomplete = autocomplete;
		this.selecaoIngredienteAdicional = selecaoIngredienteAdicional;
		this.tabelaProdutosContainer = $('.js-tabela-produtos-container');
		this.uuid = $('#uuid').val();
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	TabelaItensPedidoVenda.prototype.iniciar = function() {
		this.autocomplete.on('item-selecionado', onItemSelecionado.bind(this));
		this.selecaoIngredienteAdicional.on('ingredientes-selecionados', enviarItemPedidoVenda.bind(this));
		
		bindQuantidade.call(this);
		bindValorUnitario.call(this);
		bindTabelaItem.call(this);
	}
	
	TabelaItensPedidoVenda.prototype.valorTotal = function() {
		return this.tabelaProdutosContainer.data('valor');
	}
	
	function onItemSelecionado(evento, produto) {
		var itemPedidoVenda = {
			codigo: Yahto.getRandomInt(-100000, -1),
			produto: produto
		}
		
		if (itemPedidoVenda.produto.enviarCozinha) {
			this.selecaoIngredienteAdicional.abrirModal(itemPedidoVenda);
		} else {
			enviarItemPedidoVenda.call(this, '', itemPedidoVenda);
		}
	}
	
	function enviarItemPedidoVenda(evento, itemPedidoVenda) {
		itemPedidoVenda.uuid = this.uuid;
		var resposta = $.ajax({
			url: 'item',
			contentType: 'application/json',
			method: 'POST',
			data: JSON.stringify(itemPedidoVenda)
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onItemAtualizadoNoServidor(html) {
		this.tabelaProdutosContainer.html(html);
		
		bindQuantidade.call(this);
		bindValorUnitario.call(this);
		
		var tabelaItem = bindTabelaItem.call(this); 
		this.emitter.trigger('tabela-itens-atualizada', tabelaItem.data('valor-total'));
		
		$('input').keydown( function(e) {
			var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
			if(key == 13) {
				e.preventDefault();
				var inputs = $(this).closest('body').find(':input:visible');
				inputs.eq( inputs.index(this)+ 1 ).focus();
			}
		});
	}
	
	function onQuantidadeItemAlterado(evento) {
		var input = $(evento.target);
		var quantidade = input.val();
		
		if (quantidade <= 0) {
			input.val(1);
			quantidade = 1;
		}
		
		var codigoProduto = input.data('codigo-produto');
		
		var resposta = $.ajax({
			url: 'item/quantidade/' + codigoProduto,
			method: 'PUT',
			data: {
				quantidade: quantidade,
				uuid: this.uuid
			}
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onValorUnitarioItemAlterado(evento) {
		var input = $(evento.target);
		var valorUnitario = input.val();
		
		if (valorUnitario < 0 || valorUnitario.trim() == '') {
			input.val(0);
			valorUnitario = 0;
		}
		
		var codigoItem = input.data('codigo-item-pedido-venda');
		
		var resposta = $.ajax({
			url: 'item/valor-unitario',
			method: 'PUT',
			data: {
				codigoItem: codigoItem,
				valorUnitario: valorUnitario,
				uuid: this.uuid
			}
		});
		$('.js-nome-produto-input').focus();
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onDoubleClick(evento) {
		$(this).toggleClass('solicitando-exclusao');
	}
	
	function onExclusaoItemClick(evento) {
		var codigoItem = $(evento.target).data('codigo-item-pedido-venda');
		var resposta = $.ajax({
			url: 'item/' + this.uuid + '/' + codigoItem,
			method: 'DELETE'
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onEdicaoItemClick(evento) {
		var codigoItem = $(evento.target).data('codigo-item-pedido-venda');
		$.ajax({
			url: 'item/' + this.uuid + '/' + codigoItem,
			method: 'GET',
			success: function(itemPedidoVenda) {
				this.selecaoIngredienteAdicional.abrirModal(itemPedidoVenda);
			}.bind(this)
		});
	}
	
	function onStatusCozinhaItemClick(evento) {
		evento.preventDefault();
		var codigos = [$(evento.target).data('codigo-item-pedido-venda')];
		$.ajax({
			url: 'status-cozinha',
			method: 'PUT',
			data: {
				uuid: this.uuid,
				codigos: codigos,
				statusCozinha: 'ENTREGUE'
			},
			success: function(e) {
				$(evento.currentTarget).removeClass('btn-primary');
				$(evento.currentTarget).addClass('btn-success');
				$(evento.currentTarget).attr('disabled', true);
			}
		});
	}
	
	function bindQuantidade() {
		var quantidadeItemInput = $('.js-tabela-produto-quantidade-item');
		quantidadeItemInput.on('blur', onQuantidadeItemAlterado.bind(this));
		quantidadeItemInput.maskNumber({ integer: true, thousands: '.' });
	}
	
	function bindValorUnitario() {
		var valorUnitarioInput = $('.js-tabela-produto-valor-unitario');
		valorUnitarioInput.on('blur', onValorUnitarioItemAlterado.bind(this));
		valorUnitarioInput.maskNumber({ decimal: ',', thousands: '.' });
		
		var ehPorQk = $.map(valorUnitarioInput, function(c) {
			if (Yahto.recuperarValor($(c).val()) == 0) {
				$(c).focus().val('');
				return true;
			}
		});
		
		if (ehPorQk.length == 0) {
			$('.js-nome-produto-input').focus();
		}
	}
	
	function bindTabelaItem() {
		var tabelaItem = $('.js-tabela-item');
		tabelaItem.on('dblclick', onDoubleClick);
		$('.js-exclusao-item-btn').on('click', onExclusaoItemClick.bind(this));
		$('.js-editar-lanche-ou-prato').on('click', onEdicaoItemClick.bind(this));
		$('.js-btn-status-cozinha-item').on('click', onStatusCozinhaItemClick.bind(this));
		return tabelaItem;
	}
	
	return TabelaItensPedidoVenda;
	
}());


Yahto.SelecaoIngredienteAdicional = (function(){
	
	function SelecaoIngredienteAdicional() {
		this.selecaoIngredienteModal = $('#selecaoIngredientesItem');
		this.tituloSelecaoIngrediente = $('.js-titulo-selecao-ingrediente');
		this.form = this.selecaoIngredienteModal.find('form');
		this.url = this.form.attr('action');
		this.tabelaIngredientesContainer = $('.js-tabela-ingredientes-container');
		this.tabelaAdicionaisContainer = $('.js-tabela-adicionais-container');
		this.confirmarBtn = $('.js-confirmar-btn');
		this.cancelarBtn= $('.js-cancelar-btn');
		this.containerMensagemErro = $('.js-mensagem-erro-selecao-ingrediente');
		this.admLogado = $('.js-adm-logado');
		
		this.itemPedidoVenda = null;
		this.multiSelecaoIngrediente = null;
		this.multiSelecaoAdicional = null;
		
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	SelecaoIngredienteAdicional.prototype.iniciar = function() {
		this.selecaoIngredienteModal.height($(window).height())
		
		this.form.on('submit', function(event) { event.preventDefault() });
		this.confirmarBtn.on('click', onConfirmarBtn.bind(this));
		this.cancelarBtn.on('click', onFecharModal.bind(this));
		
	}
	
	SelecaoIngredienteAdicional.prototype.abrirModal = function(itemPedidoVenda) {
		this.itemPedidoVenda = itemPedidoVenda;
		
		carregarIngredientesDoServidor.call(this);
		carregarAdicionaisDoServidor.call(this);
		
		this.tituloSelecaoIngrediente.text(itemPedidoVenda.produto.descricao);
		this.selecaoIngredienteModal.modal("show");
		
		if ( (this.itemPedidoVenda.statusCozinha == 'ENTREGUE' || this.itemPedidoVenda.statusCozinha == 'PRONTO') &&
				this.admLogado.val() != 'true') {
			this.confirmarBtn.addClass('hidden');
		} else {
			this.confirmarBtn.removeClass('hidden');
		}
	}
	
	function carregarIngredientesDoServidor() {
		var resposta = $.ajax({
			url: 'ingredientes',
			method: 'GET',
			data: {
				uuid: this.itemPedidoVenda.uuid,
				codigoItemPedidoVenda: this.itemPedidoVenda.codigo,
				codigoProduto: this.itemPedidoVenda.produto.codigo
			}
		});
		
		resposta.done(onPesquisaIngredienteConcluida.bind(this));	
	}
	
	function onPesquisaIngredienteConcluida(html) {
		this.tabelaIngredientesContainer.html(html);
		
		this.multiSelecaoIngrediente = new Yahto.MultiSelecaoIngrediente($('.js-selecao-ingrediente'), $('.js-selecao-todos-ingredientes'));
		this.multiSelecaoIngrediente.iniciar();
	}
	
	function carregarAdicionaisDoServidor() {
		var resposta = $.ajax({
			url: 'adicionais',
			method: 'GET',
			data: {
				uuid: this.itemPedidoVenda.uuid,
				codigoItemPedidoVenda: this.itemPedidoVenda.codigo
			}
		});
		
		resposta.done(onPesquisaAdicionalConcluida.bind(this));
	}
	
	function onPesquisaAdicionalConcluida(html) {
		this.tabelaAdicionaisContainer.html(html);
		
		this.multiSelecaoAdicional = new Yahto.MultiSelecaoIngrediente($('.js-selecao-adicional'), $('.js-selecao-todos-adicionais'));
		this.multiSelecaoAdicional.iniciar();
	}
	
	function onConfirmarBtn(evento) {
		this.itemPedidoVenda.ingredientes = this.multiSelecaoIngrediente.getIngredientesSelecionados();
		
		if (itemPedidoVendaValido.call(this)) {			
			this.itemPedidoVenda.adicionais = this.multiSelecaoAdicional.getIngredientesSelecionados();
			this.emitter.trigger('ingredientes-selecionados', this.itemPedidoVenda);
			this.selecaoIngredienteModal.modal('hide');
		}
	}
	
	function itemPedidoVendaValido() {
		if (this.itemPedidoVenda.produto.enviarCozinha && this.itemPedidoVenda.ingredientes.length == 0) {
			this.containerMensagemErro.html('<span>Adicione pelo menos um ingrediente</span>');
			this.containerMensagemErro.removeClass('hidden');
			return false;
		} else {
			this.containerMensagemErro.addClass('hidden');
			return true;
		}
	}
	
	function onFecharModal(evento) {
		this.selecaoIngredienteModal.modal('hide');
	}
	
	return SelecaoIngredienteAdicional;
}());

Yahto.MultiSelecaoIngrediente = (function() {
	
	function MultiSelecaoIngrediente(selecaoCheckbox, selecaoTodosCheckbox) {
		this.selecaoCheckbox = selecaoCheckbox;
		this.selecaoTodosCheckbox = selecaoTodosCheckbox;
	}
	
	MultiSelecaoIngrediente.prototype.iniciar = function() {
		this.selecaoTodosCheckbox.on('click', onSelecaoTodosClicado.bind(this));
		this.selecaoCheckbox.on('click', onSelecaoClicado.bind(this));
		
		onSelecaoClicado.call(this);
	}
	
	MultiSelecaoIngrediente.prototype.getIngredientesSelecionados = function() {
		var checkBoxSelecionados = this.selecaoCheckbox;//.filter(':checked');
		var ingredientesOuAdicionais = $.map(checkBoxSelecionados, function(c) {
			ingredienteOuAdicional = {
					codigo: $(c).data('codigo'),
					nome: $(c).data('nome'),
					selecionado: $(c).is(':checked')
			} 
			if ($(c).data('valor-unitario') != undefined) {
				ingredienteOuAdicional.valorUnitario = $(c).data('valor-unitario');
			}
			return ingredienteOuAdicional;
		});
		return ingredientesOuAdicionais;
	}
	
	function onSelecaoTodosClicado() {
		var situacao = this.selecaoTodosCheckbox.prop('checked');
		this.selecaoCheckbox.prop('checked', situacao);
	}
	
	function onSelecaoClicado() {
		var selecaoCheckboxChecados = this.selecaoCheckbox.filter(':checked');
		this.selecaoTodosCheckbox.prop('checked', selecaoCheckboxChecados.length >= this.selecaoCheckbox.length);
	}
	
	return MultiSelecaoIngrediente;
	
}());