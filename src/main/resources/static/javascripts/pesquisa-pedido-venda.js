Yahto = Yahto || {};

Yahto.PesquisaPedidoVenda = (function() {
	
	function PesquisaPedidoVenda() {
		this.finalizarPedidoVenda = $('.js-finalizar');
	}
	
	PesquisaPedidoVenda.prototype.iniciar = function() {
		this.finalizarPedidoVenda.on('click', onFinalizarPedidoVenda.bind(this));
		bindTabelaPedidosVendas.call(this);
	}
	
	function onFinalizarPedidoVenda(evento) {
		var url = $(evento.currentTarget).data('url');
		var urlVenda = $('.js-url-venda').data('url');
		
		$.ajax({
			url: url,
			method: 'POST',
			success: function(codigo) {
				location.href = urlVenda + codigo;
			}
		});
	}
	
	function bindTabelaPedidosVendas() {
		var tabelaPedidosVendas = $('.js-tabela-pedidos-vendas');
		tabelaPedidosVendas.on('dblclick', onDoubleClick.bind(this));
		return tabelaPedidosVendas;
	}
	
	function onDoubleClick(evento) {
		var url = $(evento.currentTarget).data('url');
		location.href = url;
	}
	
	return PesquisaPedidoVenda;
	
}());

$(function(){
	var pesquisaPedidoVenda = new Yahto.PesquisaPedidoVenda();
	pesquisaPedidoVenda.iniciar();
});