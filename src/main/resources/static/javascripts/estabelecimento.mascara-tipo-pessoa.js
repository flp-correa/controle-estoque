var Yahto = Yahto || {};

Yahto.EstabelecimentoMascaraTipoPessoa = (function() {
	
	function EstabelecimentoMascaraTipoPessoa() {
		this.radioTipoPessoa = $('.js-radio-tipo-pessoa');
		this.labelCpfCnpj = $('[for=cpfOuCnpj]');
		this.inputCpfCnpj = $('#cpfOuCnpj');
		this.containerUnidadeEmpresa = $('.js-container-unidade-empresa');
		this.containerCpfOuCnpj = $('.js-container-cpf-ou-cnpj');
		this.containerNomeFantasia = $('.js-container-nome-fantasia');
		this.containerNomeEmpresarial = $('.js-container-nome-empresarial');
	}
	
	EstabelecimentoMascaraTipoPessoa.prototype.iniciar = function() {
		this.radioTipoPessoa.on('change', onTipoPessoaAlterado.bind(this));
		var tipoPessoaSelecionada = this.radioTipoPessoa.filter(':checked')[0];
		if (tipoPessoaSelecionada) {
			aplicarMascara.call(this, $(tipoPessoaSelecionada));
		}
	}
	
	function onTipoPessoaAlterado(evento) {
		var tipoPessoaSelecionada = $(evento.currentTarget);
		aplicarMascara.call(this, tipoPessoaSelecionada);
		this.inputCpfCnpj.val('');
	}
	
	function aplicarMascara(tipoPessoaSelecionada) {
		this.labelCpfCnpj.text(tipoPessoaSelecionada.data('documento'));
		this.inputCpfCnpj.mask(tipoPessoaSelecionada.data('mascara'));
		if ($('.js-inputHiddenDetalhes').val() == 'false') {
			this.inputCpfCnpj.removeAttr('disabled');
		}
		mostrarContainerPorTipoPessoa.call(this, tipoPessoaSelecionada.data('documento'));
	}
	
	function mostrarContainerPorTipoPessoa(pessoa) {
		if (pessoa == 'CNPJ') {
			this.containerUnidadeEmpresa.removeClass('hidden');
			this.containerNomeFantasia.removeClass('hidden');
			this.containerCpfOuCnpj.removeClass('col-lg-9');
			this.containerCpfOuCnpj.addClass('col-lg-6');
			this.containerNomeEmpresarial.removeClass('col-lg-12');
			this.containerNomeEmpresarial.addClass('col-lg-6');
			this.containerNomeEmpresarial.find('label').text('Nome empresarial');
		} else {
			this.containerUnidadeEmpresa.addClass('hidden');
			this.containerNomeFantasia.addClass('hidden');
			this.containerCpfOuCnpj.removeClass('col-lg-6');
			this.containerCpfOuCnpj.addClass('col-lg-9');
			this.containerNomeEmpresarial.removeClass('col-lg-6');
			this.containerNomeEmpresarial.addClass('col-lg-12');
			this.containerNomeEmpresarial.find('label').text('Nome');
		}
	}
	
	return EstabelecimentoMascaraTipoPessoa;
	
}());

$(function() {
	var estabelecimentoMascaraTipoPessoa = new Yahto.EstabelecimentoMascaraTipoPessoa();
	estabelecimentoMascaraTipoPessoa.iniciar();
});