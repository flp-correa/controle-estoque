Yahto.TabelaAbstrataDeObjects = (function () {
	
	function TabelaAbstrataDeObjects(autocomplete, tabelaAbstrataDeObjectsContainer, campoEventObjectSelecionado,
				jsExclusaoObject, urlObject, dataObject) {
		
		this.autocomplete = autocomplete;
		this.tabelaAbstrataDeObjectsContainer = tabelaAbstrataDeObjectsContainer;
		this.campoEventObjectSelecionado = campoEventObjectSelecionado;
		this.jsExclusaoObject = jsExclusaoObject;
		this.urlObject = urlObject;
		this.dataObject = dataObject;
		this.uuid = $('#uuid').val();
	}
	
	TabelaAbstrataDeObjects.prototype.iniciar = function() {
		this.autocomplete.on(this.campoEventObjectSelecionado, onObjectSelecionado.bind(this));
		$(this.jsExclusaoObject).on('click', onExclusaoObject.bind(this));
	}
	
	function onObjectSelecionado(evento, object) {
		var resposta = $.ajax({
			url: this.urlObject,
			method: 'POST',
			data: {
				codigoObject: object.codigo,
				uuid: this.uuid
			}
		});
		
		resposta.done(onObjectAtualizadaNoServidor.bind(this));
	}
	
	function onObjectAtualizadaNoServidor(html) {
		this.tabelaAbstrataDeObjectsContainer.html(html);	
		$(this.jsExclusaoObject).on('click', onExclusaoObject.bind(this));
	}
	
	function onExclusaoObject(evento) {
		var codigoObject = $(evento.target).data(this.dataObject);
		var resposta = $.ajax({
			url: this.urlObject + '/' + this.uuid + '/' + codigoObject,
			method: 'DELETE'
		});
		
		resposta.done(onObjectAtualizadaNoServidor.bind(this));
	}
	
	return TabelaAbstrataDeObjects;
	
}());