Yahto = Yahto || {};

Yahto.Localizacao = (function() {
	
	function Localizacao() {
		this.siglaUnidadeMedida = $('.js-sigla-unidade-medida');
		this.unidadeMedida = $('.js-unidade-medida');
		this.estoqueMinimo = $('#estoqueMinimo');
		this.estoqueMaximo = $('#estoqueMaximo');
	}
	
	Localizacao.prototype.iniciar = function() {
		this.unidadeMedida.on('change', onUnidadeMedidaSelecinada.bind(this));
		
		if (this.unidadeMedida.find(':selected').data('sigla') != '') {
			onUnidadeMedidaSelecinada.call(this);
		}
	}
	
	function onUnidadeMedidaSelecinada(evento) {
		var sigla = this.unidadeMedida.find(':selected').data('sigla');
		
		if (sigla == undefined) {
			sigla = $('#tipoUnidadeMedida').val();
		}
		
		if (sigla != '') {
			this.estoqueMinimo.attr('readonly', false);
			this.estoqueMaximo.attr('readonly', false);
		} else {
			this.estoqueMinimo.attr('readonly', true);
			this.estoqueMaximo.attr('readonly', true);
			this.estoqueMinimo.val('');
			this.estoqueMaximo.val('');
		}
		this.siglaUnidadeMedida.html(sigla);
	}
	
	
	
	return Localizacao;
	
}());

$(function(){
	var localizacao = new Yahto.Localizacao();
	localizacao.iniciar();
});