Yahto = Yahto || {};

Yahto.PesquisaPedidoVendaCozinha = (function() {
	
	function PesquisaPedidoVendaCozinha(socketJS) {
		this.socketJS = socketJS;
		this.containerPesquisaCozinha = $('.js-container-pesquisa-cozinha');
	}
	
	PesquisaPedidoVendaCozinha.prototype.iniciar = function() {
		this.socketJS.on('pedido-salvo', buscarItens.bind(this));
		$('.js-botoes-panel').on('click', onBotoesPanelClicado.bind(this));
		$('.js-botoes-table').on('click', onBotoesTableClicado.bind(this));
		
		$('.js-sidebar-toggle').trigger( "click" );
	}
	
	function buscarItens(evento, codigoPedidoVenda) {
		var resposta = $.ajax({
			url: 'buscar-cozinha/' + codigoPedidoVenda,
			method: 'GET'
		});
		
		resposta.done(onItemAtualizadoNoServidor.bind(this));
	}
	
	function onItemAtualizadoNoServidor(html) {
		if (html.search('form-group') > -1) {
			this.containerPesquisaCozinha.append(html);
			$('.js-botoes-panel').unbind('click');
			$('.js-botoes-table').unbind('click');
			$('.js-botoes-panel').on('click', onBotoesPanelClicado.bind(this));
			$('.js-botoes-table').on('click', onBotoesTableClicado.bind(this));
		}
	}
	
	function onBotoesPanelClicado(evento) {
		var botaoClicado = $(evento.target).val();
		
		if (botaoClicado == 'FECHAR') {
			$(evento.currentTarget).closest('div .js-pesquisa-pedido-venda-cozinha').remove();
		} else {			
			var uuid = $(evento.target).data('uuid');
			var codigos = getCodigosItensPedidoVenda.call(this, uuid);
			$.ajax({
				url: this.containerPesquisaCozinha.data('url'),
				method: 'PUT',
				data: {
					codigos: codigos,
					statusCozinha: botaoClicado
				},
				success: onBotaoPainelAtualizado.bind(this, evento),
				error: onErroEnviarDados.bind(this, evento)
			});
		}
	}
	
	function onBotoesTableClicado(evento) {
		var botaoClicado = $(evento.target).val();
		var codigos = [$(evento.currentTarget).closest('div .js-itens-pedido-venda').data('codigo-item')];
		
		if (botaoClicado == 'FECHAR') {
			var uuidPedidoVenda = $(evento.currentTarget).closest('div .js-itens-pedido-venda').data('uuid-pedido-venda');
			if (getQuantidadeItensPedidoVenda.call(this, uuidPedidoVenda) < 2) {
				$(evento.currentTarget).closest('div .js-pesquisa-pedido-venda-cozinha').remove();
			} else {
				$(evento.currentTarget).closest('div .table-responsive').remove();
			}
		} else {			
			$.ajax({
				url: this.containerPesquisaCozinha.data('url'),
				method: 'PUT',
				data: {
					codigos: codigos,
					statusCozinha: botaoClicado
				},
				success: onBotaoTableAtualizado.bind(this, evento),
				error: onErroEnviarDados.bind(this, evento)
			});
		}
	}
	
	function onErroEnviarDados(evento) {
		$(evento.currentTarget).closest('div .js-pesquisa-pedido-venda-cozinha').find('.js-msg-erro-enviar-dados').removeClass('hidden');
	}
	
	function onBotaoPainelAtualizado(evento) {
		var botaoPanel = $(evento.currentTarget);
		if (botaoPanel.val() == 'INICIADO') {
			botaoPanel.closest('div .panel').removeClass('panel-info');
			botaoPanel.closest('div .panel').addClass('panel-primary');
			botaoPanel.removeClass('btn-primary');
			botaoPanel.addClass('btn-success');
			botaoPanel.text('Pronto');
			botaoPanel.val('PRONTO');
			botaoPanel.closest('div .panel').find('.js-botoes-table').text('Pronto');
			botaoPanel.closest('div .panel').find('.js-botoes-table').removeClass('btn-primary');
			botaoPanel.closest('div .panel').find('.js-botoes-table').addClass('btn-success');
			botaoPanel.closest('div .panel').find('.js-botoes-table').val('PRONTO');
			botaoPanel.closest('div .panel').find('.js-botoes-table').removeClass('hidden');
		} else {
			setAparenciaPainelStatusFechar(botaoPanel.closest('div .panel'));
			setAparenciaFecharTodosItens(botaoPanel.closest('div .panel').find('.js-botoes-panel').data('uuid'));
		}
		$('.js-msg-erro-enviar-dados').addClass('hidden');
	}
	
	function onBotaoTableAtualizado(evento) {
		var botaoTable = $(evento.currentTarget);
		botaoTable.removeClass('btn-success');
		botaoTable.addClass('btn-primary');
		botaoTable.val('FECHAR');
		botaoTable.text('Fechar');
		
		var uuidPedidoVenda = botaoTable.closest('div .js-itens-pedido-venda').data('uuid-pedido-venda');
		if (getStatusCozinhaItensPedidoVenda.call(this, uuidPedidoVenda).includes(false)) {
			botaoTable.closest('div .panel').removeClass('panel-primary');
			botaoTable.closest('div .panel').addClass('panel-warning');
		} else {
			setAparenciaPainelStatusFechar(botaoTable.closest('div .panel'));
		}
	}
	
	function setAparenciaPainelStatusFechar(painel) {
		painel.removeClass('panel-primary panel-warning');
		painel.addClass('panel-success');
		painel.find('.js-botoes-panel').removeClass('btn-success');
		painel.find('.js-botoes-panel').addClass('btn-primary');
		painel.find('.js-botoes-panel').val('FECHAR');
		painel.find('.js-botoes-panel').text('Fechar');
	}
	
	function setAparenciaFecharTodosItens(uuidPedidoVenda) {
		$.map($('.js-itens-pedido-venda'), function(c) {
			if ($(c).data('uuid-pedido-venda') == uuidPedidoVenda) {
				$(c).find('.js-botoes-table').removeClass('btn-success');
				$(c).find('.js-botoes-table').addClass('btn-primary');
				$(c).find('.js-botoes-table').val('FECHAR');
				$(c).find('.js-botoes-table').text('Fechar');
			}
		});
	}
	
	function getCodigosItensPedidoVenda(uuidPedidoVenda) {
		var codigos = $.map($('.js-itens-pedido-venda'), function(c) {
			if ($(c).data('uuid-pedido-venda') == uuidPedidoVenda && $(c).find('.js-botoes-table').val() != 'FECHAR') {
				return $(c).data('codigo-item');
			}
		});
		return codigos;
	}
	
	function getStatusCozinhaItensPedidoVenda(uuidPedidoVenda) {
		var ehTodosFechar = $.map($('.js-itens-pedido-venda'), function(c) {
			if ($(c).data('uuid-pedido-venda') == uuidPedidoVenda) {
				return $(c).find('.js-botoes-table').val() == 'FECHAR';
			}
		});
		return ehTodosFechar;
	}
	
	function getQuantidadeItensPedidoVenda(uuidPedidoVenda) {
		var codigos = $.map($('.js-itens-pedido-venda'), function(c) {
			if ($(c).data('uuid-pedido-venda') == uuidPedidoVenda) {
				return $(c).data('codigo-item');
			}
		});
		return codigos.length;
	}
	
	return PesquisaPedidoVendaCozinha;
	
}());

Yahto.SocketJSPesquisaPedidoVendaCozinha = (function() {
	
	function SocketJSPesquisaPedidoVendaCozinha() {
		this.tenantid = $('#tenantid');
		this.stompClient = null;
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	SocketJSPesquisaPedidoVendaCozinha.prototype.iniciar = function() {
		connect.call(this);
		
		window.onbeforeunload = function(e) {
			disconnect.call(this);
			return null;
		}.bind(this);
		
		setInterval(function() {
			disconnect.call(this);
			this.emitter.trigger('pedido-salvo', 0);
			setTimeout(function() {
				connect.call(this);
			}.bind(this), 1200);
		}.bind(this), 1200000);
	}
	
	function connect() {
	    var socket = new SockJS('/websocket-yahto');
	    this.stompClient = Stomp.over(socket);
	    this.stompClient.heartbeat.outgoing = 0;
	    this.stompClient.heartbeat.incoming = 0;
	    this.stompClient.connect({}, function (frame) {
	        this.stompClient.subscribe('/enviar-cozinha/' + this.tenantid.val(), function (greeting) {
	        	this.emitter.trigger('pedido-salvo', greeting.body);
	        }.bind(this));
	    }.bind(this));
	}
	
	function disconnect() {
	    if (this.stompClient !== null) {
	        this.stompClient.disconnect();
	    }
	    console.log("Disconnected");
	}
	
	return SocketJSPesquisaPedidoVendaCozinha;
	
}());

$(function(){
	var socketJSPesquisaPedidoVendaCozinha = new Yahto.SocketJSPesquisaPedidoVendaCozinha();
	socketJSPesquisaPedidoVendaCozinha.iniciar();

	var pesquisaPedidoVendaCozinha = new Yahto.PesquisaPedidoVendaCozinha(socketJSPesquisaPedidoVendaCozinha);
	pesquisaPedidoVendaCozinha.iniciar();
});