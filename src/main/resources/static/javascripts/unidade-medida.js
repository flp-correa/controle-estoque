Yahto = Yahto || {};

Yahto.UnidadeMedida = (function(){
	
	function UnidadeMedida() {
		this.unidadeMedida = $('.js-unidade-medida');
		this.quantidade = $('.js-quantidade');
	}
	
	UnidadeMedida.prototype.iniciar = function() {
		this.unidadeMedida.on('change', onUnidadeMedidaSelecinada.bind(this));
		
		if (this.unidadeMedida.val().trim() != '') {
			onUnidadeMedidaSelecinada.call(this);
		}
	}
	
	function onUnidadeMedidaSelecinada(evento) {
		var leitura = true;
		if (evento != undefined) {
			this.quantidade.find('input').val('');
		}
		this.quantidade.removeClass('hidden');
		
		switch (this.unidadeMedida.val()) {
		case 'UNID':
			this.quantidade.find('input').val('1');
			break;
		case 'DUZIA':
			this.quantidade.find('input').val('12');
			break;
		case 'KG':
		case 'GRAMAS':
			this.quantidade.addClass('hidden');
			this.quantidade.find('input').val('');
			break;
		default:
			leitura = $('.js-inputHiddenDetalhes').val() == 'true';
			break;
		}
		
		this.quantidade.find('input').attr('readonly', leitura);
	}
	
	return UnidadeMedida;
}());

$(function(){
	var unidadeMedida = new Yahto.UnidadeMedida();
	unidadeMedida.iniciar();
});