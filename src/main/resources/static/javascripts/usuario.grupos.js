Yahto = Yahto || {};

Yahto.UsuarioGrupos = (function(){
	
	function UsuarioGrupos() {
		this.checkBoxSelecionados = $('.js-grupos');
		this.checkBoxAdministrador = $('input[id=grupos1]');
		this.checkBoxOperacional = $('input[id=grupos2]');
	}
	
	UsuarioGrupos.prototype.iniciar = function() {
		this.checkBoxSelecionados.on('click', onSelecaoClicado.bind(this));
	}
	
	function onSelecaoClicado(evento) {
		this.checkBoxAdministrador.prop('checked', false);
		this.checkBoxOperacional.prop('checked', false);

		if ($(evento.currentTarget).data('grupo') == 'Administrador') {
			this.checkBoxAdministrador.prop('checked', true);
		} else {
			this.checkBoxOperacional.prop('checked', true);			
		}
	}
	
	return UsuarioGrupos;
}());

$(function(){
	var usuarioGrupos = new Yahto.UsuarioGrupos();
	usuarioGrupos.iniciar();
});