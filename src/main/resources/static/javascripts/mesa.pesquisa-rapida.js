Yahto = Yahto || {};

Yahto.PesquisaRapidaMesa = (function() {
	
	function PesquisaRapidaMesa() {
		this.pesquisaRapidaMesasModal = $('#pesquisaRapidaMesas');
		this.numeroMesaInput = $('#numeroMesaModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-mesas-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-mesas-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaMesas');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-mesa').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-mesa');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaMesa.prototype.iniciar = function() {
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaMesasModal.on('shown.bs.modal', onModalShow.bind(this));
	}
	
	function onModalShow() {
		this.numeroMesaInput.focus();
	}
	
	function onPesquisaRapidaClicado(event) {
		event.preventDefault();
		
		$.ajax({
			url: this.pesquisaRapidaMesasModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				nrMesa: this.numeroMesaInput.val() 
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaMesasModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaMesaPesquisaRapida = new Yahto.TabelaMesaPesquisaRapida(this.pesquisaRapidaMesasModal);
		tabelaMesaPesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaMesa;
	
}());

Yahto.TabelaMesaPesquisaRapida = (function() {
	
	function TabelaMesaPesquisaRapida(modal) {
		this.modalMesa = modal;
		this.mesa = $('.js-mesa-pesquisa-rapida');
	}
	
	TabelaMesaPesquisaRapida.prototype.iniciar = function() {
		this.mesa.on('click', onMesaSelecionado.bind(this));
	}
	
	function onMesaSelecionado(evento) {
		this.modalMesa.modal('hide');
		
		var mesaSelecionada = $(evento.currentTarget);
		$('#codigoMesa').val(mesaSelecionada.data('codigo'));
		$('#nrMesa').val(mesaSelecionada.data('nr-mesa'));
		$('.js-container-mesa-selecionada').find('h1').text('Mesa - ' + mesaSelecionada.data('nr-mesa'));
	}
	
	return TabelaMesaPesquisaRapida;
	
}());

$(function() {
	var pesquisaRapidaMesa = new Yahto.PesquisaRapidaMesa();
	pesquisaRapidaMesa.iniciar();
});