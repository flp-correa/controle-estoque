Yahto.TabelaIngredientes = (function () {
	
	function TabelaIngredientes(autocomplete) {
		this.autocomplete = autocomplete;
		this.tabelaIngredientesContainer = $('.js-tabela-ingredientes-container');
		this.uuid = $('#uuid').val();
	}
	
	TabelaIngredientes.prototype.iniciar = function() {
		this.autocomplete.on('ingrediente-selecionado', onIngredienteSelecionada.bind(this));
		$('.js-exclusao-ingrediente').on('click', onExclusaoIngrediente.bind(this));
	}
	
	function onIngredienteSelecionada(evento, ingrediente) {
		var resposta = $.ajax({
			url: 'ingrediente',
			method: 'POST',
			data: {
				codigoIngrediente: ingrediente.codigo,
				uuid: this.uuid
			}
		});
		
		resposta.done(onIngredienteAtualizadaNoServidor.bind(this));
	}
	
	function onIngredienteAtualizadaNoServidor(html) {
		this.tabelaIngredientesContainer.html(html);	
		$('.js-exclusao-ingrediente').on('click', onExclusaoIngrediente.bind(this));
	}
	
	function onExclusaoIngrediente(evento) {
		var codigoIngrediente = $(evento.target).data('codigo-ingrediente');
		var resposta = $.ajax({
			url: 'ingrediente/' + this.uuid + '/' + codigoIngrediente,
			method: 'DELETE'
		});
		
		resposta.done(onIngredienteAtualizadaNoServidor.bind(this));
	}
	
	return TabelaIngredientes;
	
}());

$(function() {	
	var autocompleteIngrediente = new Yahto.Autocomplete($('.js-nome-ingrediente-input'), 'nome', 'ingrediente-selecionado');
	autocompleteIngrediente.iniciar();
	
	var tabelaIngredientes = new Yahto.TabelaIngredientes(autocompleteIngrediente);
	tabelaIngredientes.iniciar();
});