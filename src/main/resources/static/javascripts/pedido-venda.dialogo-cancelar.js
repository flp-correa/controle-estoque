Yahto = Yahto || {};

Yahto.DialogoCancelar = (function() {
	
	function DialogoCancelar() {
		this.cancelarBtn = $('.js-cancelar-btn')
	}
	
	DialogoCancelar.prototype.iniciar = function() {
		this.cancelarBtn.on('click', onCancelarClicado.bind(this));
	}
	
	function onCancelarClicado(evento) {
		evento.preventDefault();
		var botaoClicado = $(evento.currentTarget);
		var url = botaoClicado.data('url');
		var objeto = botaoClicado.data('objeto');
		
		swal({
			title: 'Tem certeza?',
			text: 'Cancelar pedido de venda ' + objeto + '? Você não poderá recuperar depois.',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Sim, Cancelar agora!',
			closeOnConfirm: false
		}, onCancelarConfirmado.bind(this, url));
	}
	
	function onCancelarConfirmado(url) {
		$.ajax({
			url: url,
			method: 'POST',
			success: function() {
				window.location.reload();
			}
		});
	}
	
	return DialogoCancelar;
	
}());

$(function() {
	var dialogoCancelar = new Yahto.DialogoCancelar();
	dialogoCancelar.iniciar();
});
