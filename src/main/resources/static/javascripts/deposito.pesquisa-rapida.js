Yahto = Yahto || {};

Yahto.PesquisaRapidaDeposito = (function() {
	
	function PesquisaRapidaDeposito() {
		this.pesquisaRapidaDepositosModal = $('#pesquisaRapidaDepositos');
		this.nomeInput = $('#descricaoModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-depositos-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-depositos-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaDepositos');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-deposito').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-deposito');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaDeposito.prototype.iniciar = function() {
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaDepositosModal.on('shown.bs.modal', onModalShow.bind(this));
	}
	
	function onModalShow() {
		this.pesquisaRapidaBtn.click();
		this.nomeInput.focus();
	}
	
	function onPesquisaRapidaClicado(event) {
		event.preventDefault();
		
		$.ajax({
			url: this.pesquisaRapidaDepositosModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				descricao: this.nomeInput.val()
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaDepositosModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaDepositoPesquisaRapida = new Yahto.TabelaDepositoPesquisaRapida(this.pesquisaRapidaDepositosModal);
		tabelaDepositoPesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaDeposito;
	
}());

Yahto.TabelaDepositoPesquisaRapida = (function() {
	
	function TabelaDepositoPesquisaRapida(modal) {
		this.modalDeposito = modal;
		this.deposito = $('.js-deposito-pesquisa-rapida');
	}
	
	TabelaDepositoPesquisaRapida.prototype.iniciar = function() {
		this.deposito.on('click', onDepositoSelecionado.bind(this));
	}
	
	function onDepositoSelecionado(evento) {
		this.modalDeposito.modal('hide');
		
		var depositoSelecionado = $(evento.currentTarget);
		$('#codigoEmpresa').val(depositoSelecionado.data('codigo-empresa'));
		$('#cnpjEmpresa').val(depositoSelecionado.data('cnpj'));
		$('#descricaoEmpresa').val(depositoSelecionado.data('nome-empresarial'));
		$('#codigoDeposito').val(depositoSelecionado.data('codigo-deposito'));
		$('#descricaoDeposito').val(depositoSelecionado.data('descricao'));
	}
	
	return TabelaDepositoPesquisaRapida;
	
}());

$(function() {
	var pesquisaRapidaDeposito = new Yahto.PesquisaRapidaDeposito();
	pesquisaRapidaDeposito.iniciar();
});