Yahto = Yahto || {};

Yahto.MultiSelecao = (function() {
	
	function MultiSelecao() {
		this.situacaoBtn = $('.js-situacao-btn');
		this.selecaoCheckbox = $('.js-selecao');
		this.selecaoTodosCheckbox = $('.js-selecao-todos');
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	MultiSelecao.prototype.iniciar = function() {
		this.situacaoBtn.on('click', onStatusBtnClicado.bind(this));
		this.selecaoTodosCheckbox.on('click', onSelecaoTodosClicado.bind(this));
		this.selecaoCheckbox.on('click', onSelecaoClicado.bind(this));
	}
	
	function onStatusBtnClicado(event) {
		var botaoClicado = $(event.currentTarget);
		var situacao = botaoClicado.data('situacao');
		var url = botaoClicado.data('url');
		
		var checkBoxSelecionados = this.selecaoCheckbox.filter(':checked');
		var codigos = $.map(checkBoxSelecionados, function(c) {
			return $(c).data('codigo');
		});
		
		if (codigos.length > 0) {
			$.ajax({
				url: url,
				method: 'PUT',
				data: {
					codigos: codigos,
					situacao: situacao
				}, 
				success: function() {
					window.location.reload();
				}
			});
			
		}
	}
	
	function onSelecaoTodosClicado() {
		var situacao = this.selecaoTodosCheckbox.prop('checked');
		this.selecaoCheckbox.prop('checked', situacao);
		situacaoBotaoAcao.call(this, situacao);
		this.emitter.trigger('multiselecao-clicado', 'todos');
	}
	
	function onSelecaoClicado() {
		var selecaoCheckboxChecados = this.selecaoCheckbox.filter(':checked');
		this.selecaoTodosCheckbox.prop('checked', selecaoCheckboxChecados.length >= this.selecaoCheckbox.length);
		situacaoBotaoAcao.call(this, selecaoCheckboxChecados.length);
		this.emitter.trigger('multiselecao-clicado', 'unico');
	}
	
	function situacaoBotaoAcao(ativar) {
		ativar ? this.situacaoBtn.removeClass('disabled') : this.situacaoBtn.addClass('disabled');
	}
	
	return MultiSelecao;
	
}());