var Yahto = Yahto || {};

Yahto.MaskMoney = (function() {
	
	function MaskMoney() {
		this.decimal = $('.js-decimal');
		this.plain = $('.js-plain');
		this.number = $('.js-number');
	}
	
	MaskMoney.prototype.enable = function() {
		this.decimal.maskNumber({ decimal: ',', thousands: '.' });
		this.plain.maskNumber({ integer: true, thousands: '.' });
		this.number.maskNumber({ integer: true, thousands: '' });
	}
	
	return MaskMoney;
	
}());

Yahto.MaskGramas = (function() {
	
	function MaskGramas() {
		this.gramas = $('.js-gramas');
	}
	
	MaskGramas.prototype.enable = function() {
		this.gramas.maskMoney({ decimal: ',', thousands: '.', precision: 3 });
	}
	
	return MaskGramas;
	
}());

Yahto.MaskPhoneNumber = (function() {
	
	function MaskPhoneNumber() {
		this.inputPhoneNumber = $('.js-phone-number');
	}
	
	MaskPhoneNumber.prototype.enable = function() {
		var maskBehavior = function (val) {
		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		};
		
		var options = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskBehavior.apply({}, arguments), options);
		    }
		};
		
		this.inputPhoneNumber.mask(maskBehavior, options);
	}
	return MaskPhoneNumber;
	
}());

Yahto.BootstrapSwitch = (function() {
	
	function BootstrapSwitch() {
		this.situacaoCadastro = $('.js-situacao');
	}
	
	BootstrapSwitch.prototype.iniciar = function() {
		this.situacaoCadastro.bootstrapSwitch();
	}
	
	return BootstrapSwitch;
	
}());

Yahto.MascaraCpfCnpj = (function() {
	
	function MascaraCpfCnpj() {
		this.inputCnpj = $('.js-mascara-cnpj');
		this.inputCpf = $('.js-mascara-cpf');
	}
	
	MascaraCpfCnpj.prototype.iniciar = function() {
		this.inputCnpj.mask('00.000.000/0000-00');
		this.inputCpf.mask('000.000.000-00');
	}
	
	return MascaraCpfCnpj;
	
}());

Yahto.MaskCep = (function() {
	
	function MaskCep() {
		this.inputCep = $('.js-cep');
	}
	
	MaskCep.prototype.enable = function() {
		this.inputCep.mask('00.000-000');
	}
	
	return MaskCep;
	
}());

Yahto.MaskDate = (function() {
	
	function MaskDate() {
		this.inputDate = $('.js-date');
	}
	
	MaskDate.prototype.enable = function() {
		this.inputDate.mask('00/00/0000');
		this.inputDate.datepicker({
			orientation: 'bottom',
			language: 'pt-BR',
			todayHighlight: true,
			autoclose: true
		});
	}
	
	return MaskDate;
	
}());

Yahto.MaskHour = (function() {
	
	function MaskHour() {
		this.inputHour = $('.js-hour');
	}
	
	MaskHour.prototype.enable = function() {
		var mask = function (val) {
		    val = val.split(":");
		    return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
		}

		pattern = {
		    onKeyPress: function(val, e, field, options) {
		        field.mask(mask.apply({}, arguments), options);
		    },
		    translation: {
		        'H': { pattern: /[0-2]/, optional: false },
		        'Z': { pattern: /[0-3]/, optional: false },
		        'M': { pattern: /[0-5]/, optional: false }
		    },
		    placeholder: 'hh:mm'
		};
		
		this.inputHour.mask(mask, pattern);
	}
	
	return MaskHour;
	
}());

Yahto.Security = (function() {
	
	function Security() {
		this.token = $('input[name=_csrf]').val();
		this.header = $('input[name=_csrf_header]').val();
	}
	
	Security.prototype.enable = function() {
		$(document).ajaxSend(function(event, jqxhr, settings) {
			jqxhr.setRequestHeader(this.header, this.token);
		}.bind(this));
	}
	
	return Security;
	
}());

numeral.language('pt-br');

Yahto.formatarMoeda = function(valor) {
	return numeral(valor).format('0,0.00');
}

Yahto.recuperarValor = function(valorFormatado) {
	return numeral().unformat(valorFormatado);
}

Yahto.getRandomInt = function(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

$(function() {
	var security = new Yahto.Security();
    security.enable();
    	
    var maskMoney = new Yahto.MaskMoney();
    maskMoney.enable();
    
    var maskGramas = new Yahto.MaskGramas();
    maskGramas.enable();
    	
    var maskPhoneNumber = new Yahto.MaskPhoneNumber();
    maskPhoneNumber.enable();
    	
    var situacaoCadastro = new Yahto.BootstrapSwitch();
    situacaoCadastro.iniciar();
    	
    var mascaraCpfCnpj = new Yahto.MascaraCpfCnpj();
    mascaraCpfCnpj.iniciar();
    	
    var maskCep = new Yahto.MaskCep();
    maskCep.enable();
    	
    var maskDate = new Yahto.MaskDate();
    maskDate.enable();
    
    var maskHour = new Yahto.MaskHour();
	maskHour.enable();
	
	$('input').attr('autocomplete','off');
	
	$('input').keydown( function(e) {
		var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		if(key == 13) {
			e.preventDefault();
			var inputs = $(this).closest('body').find(':input:visible');
			inputs.eq( inputs.index(this)+ 1 ).focus();
		}
	});
});