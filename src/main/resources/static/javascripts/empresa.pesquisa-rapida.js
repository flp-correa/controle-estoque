Yahto = Yahto || {};

Yahto.PesquisaRapidaEmpresa = (function() {
	
	function PesquisaRapidaEmpresa() {
		this.pesquisaRapidaEmpresasModal = $('#pesquisaRapidaEmpresas');
		this.nomeInput = $('#nomeEmpresarialEmpresaModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-empresas-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-empresas-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaEmpresas');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-empresa').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-empresa');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaEmpresa.prototype.iniciar = function() {
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaEmpresasModal.on('shown.bs.modal', onModalShow.bind(this));
	}
	
	function onModalShow() {
		this.pesquisaRapidaBtn.click();
		this.nomeInput.focus();
	}
	
	function onPesquisaRapidaClicado(event) {
		event.preventDefault();
		
		if ($('#codigoEmpresaDeposito').val() != undefined) {
			buscarParaMovtoEstoque.call(this);
		} else {
			buscarParaDepositoEmpresa.call(this);
		}
	}
	
	function buscarParaMovtoEstoque() {
		$.ajax({
			url: this.pesquisaRapidaEmpresasModal.find('form').attr('action') + '/movto-estoque',
			method: 'GET',
			contentType: 'application/json',
			data: {
				cnpjOuNomeEmpresarial: this.nomeInput.val() 
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function buscarParaDepositoEmpresa() {
		$.ajax({
			url: this.pesquisaRapidaEmpresasModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				nomeEmpresarial: this.nomeInput.val()
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaEmpresasModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaEmpresaPesquisaRapida = new Yahto.TabelaEmpresaPesquisaRapida(this.pesquisaRapidaEmpresasModal);
		tabelaEmpresaPesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaEmpresa;
	
}());

Yahto.TabelaEmpresaPesquisaRapida = (function() {
	
	function TabelaEmpresaPesquisaRapida(modal) {
		this.modalEmpresa = modal;
		this.empresa = $('.js-empresa-pesquisa-rapida');
	}
	
	TabelaEmpresaPesquisaRapida.prototype.iniciar = function() {
		this.empresa.on('click', onEmpresaSelecionado.bind(this));
	}
	
	function onEmpresaSelecionado(evento) {
		this.modalEmpresa.modal('hide');
		
		var empresaSelecionada = $(evento.currentTarget);
		$('#codigoEmpresa').val(empresaSelecionada.data('codigo'));
		$('#cnpjEmpresa').val(empresaSelecionada.data('cnpj'));
		$('#descricaoEmpresa').val(empresaSelecionada.data('nome-empresarial'));
	}
	
	return TabelaEmpresaPesquisaRapida;
	
}());

$(function() {
	var pesquisaRapidaEmpresa = new Yahto.PesquisaRapidaEmpresa();
	pesquisaRapidaEmpresa.iniciar();
});