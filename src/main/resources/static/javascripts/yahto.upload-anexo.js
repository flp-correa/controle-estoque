var Yahto = Yahto || {};

Yahto.UploadAnexos = (function () {
	
	function UploadAnexos(uploadDrop, uploadSelect, imgLoading, uuid, urlAnexo) {
		this.uuid = uuid;
		this.urlAnexo = urlAnexo;
		
		this.uploadDrop = uploadDrop;
		this.uploadSelect = uploadSelect;
		this.imgLoading = imgLoading;
		
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	UploadAnexos.prototype.iniciar = function() {
		var settings = {
				type: 'json',
				filelimit: 1,
				allow: '*.(pdf)',
				action: this.urlAnexo,
				beforeSend: adicionarCsrfToken,
				complete: onUploadCompleto.bind(this),
				loadstart: onLoadStart.bind(this)
		};
		
		UIkit.uploadSelect(this.uploadSelect, settings);
		UIkit.uploadDrop(this.uploadDrop, settings);
	}
	
	function onUploadCompleto(resposta) {
		this.emitter.trigger('upload-completo', resposta);
		this.imgLoading.addClass('hidden');
	}
	
	function onLoadStart() {
		this.imgLoading.removeClass('hidden');
	}
	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		var header = $('input[name=_csrf_header]').val();
		xhr.setRequestHeader(header, token);
	}
	
	return UploadAnexos;
})();