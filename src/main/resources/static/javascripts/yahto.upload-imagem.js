var Yahto = Yahto || {};

Yahto.UploadImagem = (function() {
	
	function UploadImagem(inputNomeImagem) {
		this.inputNomeImagem = inputNomeImagem;
		this.inputContentType = $('input[name=contentType]');
		this.inputUrlImagem = $('input[name=urlImagem]'); 
		this.htmlImagemHbsTemplate = $('#imagem-hbs').html();
		this.template = Handlebars.compile(this.htmlImagemHbsTemplate);
		this.containerImagem = $('.js-container-imagem');
		this.uploadDrop = $('#upload-drop');
		this.inputDetalhes = $('.js-inputHiddenDetalhes');
		
		this.imgLoading = $('.js-img-loading-image');
	}
	
	UploadImagem.prototype.iniciar = function () {
		var settings = {
				type: 'json',
				filelimit: 1,
				allow: '*.(jpg|jpeg|png)',
				action: this.containerImagem.data('url-arquivos'),
				beforeSend: adicionarCsrfToken,
				complete: onUploadCompleto.bind(this),
				loadstart: onLoadStart.bind(this)
		};
		
		UIkit.uploadSelect($('#upload-select'), settings);
		UIkit.uploadDrop(this.uploadDrop, settings);
		
		if (this.inputNomeImagem.val()) {
			onUploadCompleto.call(this, { 
				nome: this.inputNomeImagem.val(), 
				contentType: this.inputContentType.val(), 
				url: this.inputUrlImagem.val()
			});
		}
	}
	
	function onUploadCompleto(resposta) {
		this.inputNomeImagem.val(resposta.nome);
		this.inputContentType.val(resposta.contentType);
		this.inputUrlImagem.val(resposta.url);
		
		this.uploadDrop.addClass('hidden');
		var htmlImagemHbs = this.template({url: resposta.url});
		this.containerImagem.append(htmlImagemHbs);
		
		if (this.inputDetalhes.val() == 'true') {
			$('.js-remove-logo').addClass('hidden');
		} else {
			$('.js-remove-logo').on('click', onRemoverImagem.bind(this));
		}
		
		this.imgLoading.addClass('hidden');
	}
	
	function onRemoverImagem() {
		$('.js-imagem-hbs').remove();
		this.uploadDrop.removeClass('hidden');
		this.inputNomeImagem.val('');
		this.inputContentType.val('');
	}
	
	function onLoadStart() {
		this.imgLoading.removeClass('hidden');
	}
	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		var header = $('input[name=_csrf_header]').val();
		xhr.setRequestHeader(header, token);
	}
	
	return UploadImagem;
	
})();