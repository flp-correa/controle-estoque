Yahto = Yahto || {};

Yahto.Usuario = (function(){
	
	function Usuario() {
		this.containerDeposito = $('.js-container-deposito');
		this.checkBoxSelecionados = $('.js-grupos');
		this.checkBoxOperacional = $('input[id=grupos3]');
	}
	
	Usuario.prototype.iniciar = function() {
		this.checkBoxSelecionados.on('click', onSelecaoClicado.bind(this));
		
		window.onload = function() {
			onSelecaoClicado.call(this, $('.js-grupos'));
		}.bind(this)
	}
	
	function onSelecaoClicado(evento) {
		var codigoGrupo = $('input:checked').val();
		this.containerDeposito.toggleClass('hidden', codigoGrupo != '2' && codigoGrupo != '3' || $('.js-inputHiddenDetalhes').val() == 'true');
	}
	
	return Usuario;
}());

$(function(){
	var usuario = new Yahto.Usuario();
	usuario.iniciar();
});