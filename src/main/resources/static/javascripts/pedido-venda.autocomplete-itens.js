Yahto = Yahto || {};

Yahto.AutocompleteItensPedidoVenda = (function() {
	
	function AutocompleteItensPedidoVenda() {
		this.nomeInput = $('.js-nome-produto-input');
		var htmlTemplateAutocompleteItensPedidoVenda = $('#template-autocomplete-produto-venda').html();
		this.template = Handlebars.compile(htmlTemplateAutocompleteItensPedidoVenda);
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	AutocompleteItensPedidoVenda.prototype.iniciar = function() {
		var options = {
			url: function(descricao) {
				return this.nomeInput.data('url') + '?descricao=' + descricao;
			}.bind(this),
			getValue: 'descricao',
			minCharNumber: 3,
			requestDelay: 300,
			ajaxSettings: {
				contentType: 'application/json'
			},
			template: {
				type: 'custom',
				method: template.bind(this)
			},
			list: {
				maxNumberOfElements: 20,
				onChooseEvent: onItemSelecionado.bind(this)
			}
		};
		
		this.nomeInput.easyAutocomplete(options);
	}
	
	function onItemSelecionado() {
		this.emitter.trigger('item-selecionado', this.nomeInput.getSelectedItemData());
		this.nomeInput.val('');
		this.nomeInput.focus();
	}
	
	function template(nome, produto) {
		produto.valorFormatado = Yahto.formatarMoeda(produto.valorUnitario);
		return this.template(produto);
	}
	
	return AutocompleteItensPedidoVenda
	
}());