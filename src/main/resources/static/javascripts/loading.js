var Yahto = Yahto || {};

Yahto.Loading = (function() {
	
	function Loading() {
		this.modalLoading = $('#modalLoading');
	}
	
	Loading.prototype.iniciar = function() {
		this.modalLoading.modal("show");
		
		window.onload = function() {
			this.modalLoading.modal('hide');
		}.call(this)
	}
	
	return Loading;
	
}());

$(function() {
	var loading = new Yahto.Loading();
	loading.iniciar();
});