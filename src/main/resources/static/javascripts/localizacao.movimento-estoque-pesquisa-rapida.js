Yahto = Yahto || {};

Yahto.PesquisaRapidaLocalizacaoMvtoEstoque = (function() {
	
	function PesquisaRapidaLocalizacaoMvtoEstoque() {
		this.emitterPesqProdMvtoEstoq = null;
		this.familiaUnidadeMedida = null;
		this.codigoProduto = null;
		this.pesquisaRapidaLocalizacoesMvtoEstoqueModal = $('#pesquisaRapidaLocalizacoesMvtoEstoque');
		this.nomeInput = $('#descricaoLocalizacaoModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-localizacoes-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-localizacoes-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaLocalizacoesMvtoEstoque');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-localizacao').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-localizacao');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaLocalizacaoMvtoEstoque.prototype.iniciar = function() {
		this.pesquisaRapidaBtn.on('click', onPesquisaRapidaClicado.bind(this));
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaLocalizacoesMvtoEstoqueModal.on('shown.bs.modal', onModalShow.bind(this));
		
		Handlebars.registerHelper('json', function(obj) {
		    return JSON.stringify(obj);
		});
	}
	
	PesquisaRapidaLocalizacaoMvtoEstoque.prototype.setEmitterPesqProdMvtoEstoq = function(emitterPesqProdMvtoEstoq) {
		this.emitterPesqProdMvtoEstoq = emitterPesqProdMvtoEstoq;
	}
	
	PesquisaRapidaLocalizacaoMvtoEstoque.prototype.setFamiliaUnidadeMedida = function(familiaUnidadeMedida) {
		this.familiaUnidadeMedida = familiaUnidadeMedida;
	}
	
	PesquisaRapidaLocalizacaoMvtoEstoque.prototype.setCodigoProduto = function(codigoProduto) {
		this.codigoProduto = codigoProduto;
	}
	
	function onModalShow() {
		this.nomeInput.focus();
		onPesquisaRapidaClicado.call(this);
	}
	
	function onPesquisaRapidaClicado(event) {
		if (event != undefined) {
			event.preventDefault();
		}
		
		$.ajax({
			url: this.pesquisaRapidaLocalizacoesMvtoEstoqueModal.find('form').attr('action'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				descricao: this.nomeInput.val(),
				codigoProduto: this.codigoProduto,
				codigoLocalizacao: $('#codigoLocalizacao').val()
			},
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaLocalizacoesMvtoEstoqueModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaLocalizacaoMovtoEstoquePesquisaRapida = new Yahto.TabelaLocalizacaoMovtoEstoquePesquisaRapida(
				this.codigoProduto, this.emitterPesqProdMvtoEstoq, this.familiaUnidadeMedida, this.pesquisaRapidaLocalizacoesMvtoEstoqueModal);
		tabelaLocalizacaoMovtoEstoquePesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaLocalizacaoMvtoEstoque;
	
}());

Yahto.TabelaLocalizacaoMovtoEstoquePesquisaRapida = (function() {
	
	function TabelaLocalizacaoMovtoEstoquePesquisaRapida(codigoProduto, emitterPesqProdMvtoEstoq, familiaUnidadeMedida, modal) {
		this.codigoProduto = codigoProduto;
		this.emitterPesqProdMvtoEstoq = emitterPesqProdMvtoEstoq;
		this.familiaUnidadeMedida = familiaUnidadeMedida;
		this.modalLocalizacao = modal;
		this.localizacao = $('.js-localizacao-pesquisa-rapida');
	}
	
	TabelaLocalizacaoMovtoEstoquePesquisaRapida.prototype.iniciar = function() {
		this.localizacao.on('click', onLocalizacaoSelecionado.bind(this));
	}
	
	function onLocalizacaoSelecionado(evento) {
		this.modalLocalizacao.modal('hide');
		
		var localizacaoSelecionado = $(evento.currentTarget);
		var deposito = localizacaoSelecionado.data('deposito');

		var localizacao = { 
				codigo: localizacaoSelecionado.data('codigo-localizacao'),
				endereco: localizacaoSelecionado.data('endereco-localizacao'),
				descricao: localizacaoSelecionado.data('descricao-localizacao'),
				deposito: deposito
		}
		
		if (this.codigoProduto == null) {
			new Yahto.exibirCamposLocalizacaoMovtoEstoque(localizacao, true);
			
			var objectEvent = { codigoEmpresa: deposito.empresa.codigo, familiaUnidadeMedida: this.familiaUnidadeMedida };
			
			this.emitterPesqProdMvtoEstoq.trigger('produto-selecionado', objectEvent);
		} else {
			new Yahto.exibirCamposLocalizacaoMovtoEstoque(localizacao, false);
		}
		
	}
	
	return TabelaLocalizacaoMovtoEstoquePesquisaRapida;
	
}());

Yahto.exibirCamposLocalizacaoMovtoEstoque = function(localizacao, principal) {
	if (localizacao == null) {
		localizacao = getLocalizacaoVazia();
	}
	
	if (principal) {
		$('#codigoLocalizacao').val(localizacao.codigo);
		$('#enderecoLocalizacao').val(localizacao.endereco);
		$('#descricaoLocalizacao').val(localizacao.descricao);
		
		$('#codigoDeposito').val(localizacao.deposito.codigo);
		$('#descricaoDeposito').val(localizacao.deposito.descricao);
		
		$('#codigoEmpresaDeposito').val(localizacao.deposito.empresa.codigo);
		$('#cnpjEmpresaDeposito').val(localizacao.deposito.empresa.cpfOuCnpj);
		$('#nomeEmpresarialEmpresaDeposito').val(localizacao.deposito.empresa.nomeEmpresarial);
	} 
	if (!principal || localizacao.codigo == '') {
		$('#codigoLocalizacaoTransferencia').val(localizacao.codigo);
		$('#enderecoLocalizacaoTransferencia').val(localizacao.endereco);
		$('#descricaoLocalizacaoTransferencia').val(localizacao.descricao);
		
		$('#codigoDepositoTransferencia').val(localizacao.deposito.codigo);
		$('#descricaoDepositoTransferencia').val(localizacao.deposito.descricao);
		
		$('#codigoEmpresaDepositoTransferencia').val(localizacao.deposito.empresa.codigo);
		$('#cnpjEmpresaDepositoTransferencia').val(localizacao.deposito.empresa.cpfOuCnpj);
		$('#nomeEmpresarialEmpresaDepositoTransferencia').val(localizacao.deposito.empresa.nomeEmpresarial);
	}
	
	function getLocalizacaoVazia() {
		var localizacao = {
				codigo: '',
				endereco: '',
				descricao: '',
				deposito: {
						codigo: '',
						descricao: '',
						empresa: {
							codigo: '',
							cpfOuCnpj: '',
							nomeEmpresarial: ''
						}
				}
		}
		return localizacao;
	}
}