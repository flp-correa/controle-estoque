Yahto.Venda = (function() {
	
	function Venda(tabelaItens) {
		this.tabelaItens = tabelaItens;
		this.valorTotalBox = $('.js-valor-total-box');
		this.valorFreteInput = $('#valorFrete');
		this.valorDescontoInput = $('#valorDesconto');
		this.valorAcrescimoInput = $('#valorAcrescimo');
		this.valorDinheiroInput = $('#valorDinheiro');
		this.valorCartaoInput = $('#valorCartao');
		this.valorTrocoInput = $('#valorTroco');
		this.valorTotalInput = $('#valorTotal');
		this.valorTotalBoxContainer = $('.js-valor-total-box-container');
		
		this.valorTotalItens = this.tabelaItens.valorTotal();
		this.valorFrete = this.valorFreteInput.data('valor');
		this.valorDesconto = this.valorDescontoInput.data('valor');
		this.valorAcrescimo = this.valorAcrescimoInput.data('valor');
	}
	
	Venda.prototype.iniciar = function() {
		this.tabelaItens.on('tabela-itens-atualizada', onTabelaItensAtualizada.bind(this));
		this.valorFreteInput.on('keyup', onValorFreteAlterado.bind(this));
		this.valorDescontoInput.on('keyup', onValorDescontoAlterado.bind(this));
		this.valorAcrescimoInput.on('keyup', onValorAcrescimoAlterado.bind(this));
		this.valorDinheiroInput.on('keyup', onValorDinheiroAlterado.bind(this));
		
		this.tabelaItens.on('tabela-itens-atualizada', onValoresAlterados.bind(this));
		this.valorFreteInput.on('keyup', onValoresAlterados.bind(this));
		this.valorDescontoInput.on('keyup', onValoresAlterados.bind(this));
		this.valorAcrescimoInput.on('keyup', onValoresAlterados.bind(this));
		
		this.valorFreteInput.val(Yahto.formatarMoeda(this.valorFreteInput.val()));
		this.valorDescontoInput.val(Yahto.formatarMoeda(this.valorDescontoInput.val()));
		this.valorAcrescimoInput.val(Yahto.formatarMoeda(this.valorAcrescimoInput.val()));
		this.valorDinheiroInput.val(Yahto.formatarMoeda(this.valorDinheiroInput.val()));
		this.valorCartaoInput.val(Yahto.formatarMoeda(this.valorCartaoInput.val()));
		this.valorTrocoInput.val(Yahto.formatarMoeda(this.valorTrocoInput.val()));
		this.valorTotalInput.val(Yahto.formatarMoeda(this.valorTotalInput.val()));
		
		onValoresAlterados.call(this);
	}
	
	Venda.prototype.setValorDinheiro = function(valorDinheiro) {
		this.valorDinheiroInput.val(valorDinheiro);
	}
	
	Venda.prototype.getValorDinheiro = function() {
		return this.valorDinheiroInput.val();
	}
	
	Venda.prototype.setValorCartao = function(valorCartao) {
		this.valorCartaoInput.val(valorCartao);
	}
	
	Venda.prototype.getValorCartao = function() {
		return this.valorCartaoInput.val();
	}
	
	function onTabelaItensAtualizada(evento, valorTotalItens) {
		this.valorTotalItens = valorTotalItens == null ? 0 : valorTotalItens;
	}
	
	function onValorFreteAlterado(evento) {
		this.valorFrete = Yahto.recuperarValor($(evento.target).val());
	}
	
	function onValorDescontoAlterado(evento) {
		this.valorDesconto = Yahto.recuperarValor($(evento.target).val());
	}
	
	function onValorAcrescimoAlterado(evento) {
		this.valorAcrescimo = Yahto.recuperarValor($(evento.target).val());
	}
	
	function onValorDinheiroAlterado(evento) {
		var valorDinheiro = Yahto.recuperarValor(this.valorDinheiroInput.val());
		var valorTroco = numeral(valorDinheiro) - numeral(this.valorTotalInput.val());
		if (valorTroco >= 0) {
			this.valorTrocoInput.val(Yahto.formatarMoeda(valorTroco));
		} else {
			this.valorTrocoInput.val(Yahto.formatarMoeda(0));
		}
	}
	
	function onValoresAlterados() {
		var valorTotal = numeral(this.valorTotalItens) + numeral(this.valorFrete) + numeral(this.valorAcrescimo) - numeral(this.valorDesconto);
		this.valorTotalBox.html(Yahto.formatarMoeda(valorTotal));
		this.valorTotalInput.val(Yahto.formatarMoeda(valorTotal));
		
		this.valorTotalBoxContainer.toggleClass('negativo', valorTotal < 0);
		
		onValorDinheiroAlterado.call(this);
	}
	
	return Venda;
	
}());

Yahto.VendaTipoPagamento = (function(){
	
	function VendaTipoPagamento() {
		this.containerValorCartao = $('.js-container-valor-cartao');
		this.containerValorDinheiro = $('.js-container-valor-dinheiro');
		this.containerValorTroco = $('.js-container-valor-troco');
	}
	
	VendaTipoPagamento.prototype.iniciar = function() {
		$('.js-tipos-pagamentos').on('click', onSelecaoClicado.bind(this));
	}
	
	VendaTipoPagamento.prototype.setTipoPagamento = function(tipoPagamento) {
		$(tipoPagamento).prop('checked', true);
		onSelecaoClicado.call(this);
	}
	
	function onSelecaoClicado(evento) {
		if ($('input:checked').val() == 'DINHEIRO') {
			this.containerValorCartao.addClass('hidden');
			this.containerValorDinheiro.removeClass('hidden');
			this.containerValorTroco.removeClass('hidden');
		} else {
			this.containerValorCartao.removeClass('hidden');
			this.containerValorTroco.addClass('hidden');
			if ($('input:checked').val() == 'CARTAO') {
				this.containerValorDinheiro.addClass('hidden');
			} else {
				this.containerValorDinheiro.removeClass('hidden');
			}
		}
	}
	
	return VendaTipoPagamento;
}());

$(function() {
	
	var autocompleteItensVenda = new Yahto.AutocompleteItensVenda();
	autocompleteItensVenda.iniciar();
	
	var tabelaItensVenda = new Yahto.TabelaItensVenda(autocompleteItensVenda);
	tabelaItensVenda.iniciar();
	
	var venda = new Yahto.Venda(tabelaItensVenda);
	venda.iniciar();
	
	var vendaTipoPagamento = new Yahto.VendaTipoPagamento();
	vendaTipoPagamento.iniciar();
	
	var itemVendaCalcularPorPessoa = new Yahto.ItemCalcularPorPessoa(null, vendaTipoPagamento, venda);
	itemVendaCalcularPorPessoa.iniciar();
});