Yahto = Yahto || {};

Yahto.PedidoVenda = (function() {
	
	function PedidoVenda(tabelaItens) {
		this.tabelaItens = tabelaItens;
		this.valorTotalBox = $('.js-valor-total-box');
		this.valorFreteInput = $('#valorFrete');
		this.valorTotalBoxContainer = $('.js-valor-total-box-container');
		
		this.valorTotalItens = this.tabelaItens.valorTotal();
		this.valorFrete = this.valorFreteInput.data('valor');
	}
	
	PedidoVenda.prototype.iniciar = function() {
		this.tabelaItens.on('tabela-itens-atualizada', onTabelaItensAtualizada.bind(this));
		this.valorFreteInput.on('keyup', onValorFreteAlterado.bind(this));
		
		this.tabelaItens.on('tabela-itens-atualizada', onValoresAlterados.bind(this));
		this.valorFreteInput.on('keyup', onValoresAlterados.bind(this));
		
		this.valorFreteInput.val(Yahto.formatarMoeda(this.valorFreteInput.val()));
		
		onValoresAlterados.call(this);
	}
	
	function onTabelaItensAtualizada(evento, valorTotalItens) {
		this.valorTotalItens = valorTotalItens == null ? 0 : valorTotalItens;
	}
	
	function onValorFreteAlterado(evento) {
		this.valorFrete = Yahto.recuperarValor($(evento.target).val());
	}
	
	function onValoresAlterados() {
		var valorTotal = numeral(this.valorTotalItens) + numeral(this.valorFrete);
		this.valorTotalBox.html(Yahto.formatarMoeda(valorTotal));
		
		this.valorTotalBoxContainer.toggleClass('negativo', valorTotal < 0);
	}
	
	return PedidoVenda;
	
}());

Yahto.TipoPedidoVenda = (function(){
	
	function TipoPedidoVenda() {
		this.tipoPedidoVenda = $('#tipoPedidoVenda');
		this.botaoTipoPedidoVenda = $('.js-botao-tipo-pedido-venda');
		this.botaoTipoPedidoCliente = $('.js-botao-tipo-pedido-cliente');
		this.botaoTipoPedidoMesa = $('.js-botao-tipo-pedido-mesa');
		this.containerMesas = $('.js-container-mesas');
		this.containerCliente = $('.js-container-cliente');
		this.botaoMesa = $('.js-botao-mesa');
		this.containerMesaSelecionada = $('.js-container-mesa-selecionada');
		this.codigoMesa = $('#codigoMesa');
		this.containerProdutos = $('.js-container-produtos');
		this.novoPedidoVenda = $('#novoPedidoVenda');
	}
	
	TipoPedidoVenda.prototype.iniciar = function() {
		this.botaoTipoPedidoVenda.on('click', onBotaoTipoPedidoVendaSelecionado.bind(this));
		this.botaoMesa.on('click', onBotaoMesaSelecionada.bind(this));
		
		iniciandoCadastroPedidoVenda.call(this);
	}
	
	function iniciandoCadastroPedidoVenda() {
		if (this.novoPedidoVenda.val() == 'true' && this.codigoMesa.val() == '') {			
			if (this.tipoPedidoVenda.val() == 'PEDIDO_MESA') {
				this.botaoTipoPedidoMesa.trigger( "click" );
			} else {
				this.botaoTipoPedidoCliente.trigger( "click" );
			}
		} else {
			mostrarContainersPorTipoPedidoVenda.call(this, this.tipoPedidoVenda.val());
		}
	}
	
	function onBotaoTipoPedidoVendaSelecionado(evento) {
		var tipoPedidoVenda = $(evento.target).data('tipo-pedido-venda');
		this.tipoPedidoVenda.val(tipoPedidoVenda);
		this.codigoMesa.val('');
		this.containerMesaSelecionada.addClass('hidden');
		
		mostrarContainersPorTipoPedidoVenda.call(this, tipoPedidoVenda);
	}
	
	function mostrarContainersPorTipoPedidoVenda(tipoPedidoVenda) {
		this.containerMesas.addClass('hidden');
		this.containerCliente.addClass('hidden');
		this.containerProdutos.removeClass('hidden');
		
		if (tipoPedidoVenda == 'PEDIDO_CLIENTE') {				
			this.botaoTipoPedidoCliente.removeClass('btn-default');
			this.botaoTipoPedidoCliente.addClass('btn-success');
			this.botaoTipoPedidoCliente.addClass('active');
			this.botaoTipoPedidoMesa.removeClass('btn-success');
			this.botaoTipoPedidoMesa.removeClass('active');
			this.botaoTipoPedidoMesa.addClass('btn-default');
		
			this.containerCliente.removeClass('hidden');
		} else {
			this.botaoMesa.removeClass('hidden');
			this.botaoTipoPedidoMesa.removeClass('btn-default');
			this.botaoTipoPedidoMesa.addClass('btn-success');
			this.botaoTipoPedidoMesa.addClass('active');
			this.botaoTipoPedidoCliente.removeClass('btn-success');
			this.botaoTipoPedidoCliente.removeClass('active');
			this.botaoTipoPedidoCliente.addClass('btn-default');
			
			if (this.codigoMesa.val() == '') {
				this.containerMesas.removeClass('hidden');
				this.containerProdutos.addClass('hidden');
			} else {
				this.containerMesaSelecionada.removeClass('hidden');
			} 
		}
		
	}
	
	function onBotaoMesaSelecionada(evento) {
		var codigoMesa = $(evento.target).data('codigo-mesa-selecionada');
		var nrMesa = $(evento.target).data('nr-mesa-selecionada');
		
		this.containerMesas.addClass('hidden');
		this.containerMesaSelecionada.removeClass('hidden');
		this.containerProdutos.removeClass('hidden');
		this.codigoMesa.val(codigoMesa);
		$('#nrMesa').val(nrMesa);
		this.containerMesaSelecionada.find('h1').text('Mesa - ' + nrMesa);
		$('.js-nome-produto-input').focus();
	}
	
	return TipoPedidoVenda;
}());

$(function(){
	var autocompleteItensPedidoVenda = new Yahto.AutocompleteItensPedidoVenda();
	autocompleteItensPedidoVenda.iniciar();
	
	var selecaoIngredienteAdicional = new Yahto.SelecaoIngredienteAdicional();
	selecaoIngredienteAdicional.iniciar();
	
	var tabelaItensPedidoVenda = new Yahto.TabelaItensPedidoVenda(autocompleteItensPedidoVenda, selecaoIngredienteAdicional);
	tabelaItensPedidoVenda.iniciar();
	
	var pedidoVenda = new Yahto.PedidoVenda(tabelaItensPedidoVenda);
	pedidoVenda.iniciar();
	
	var itemPedidoVendaCalcularPorPessoa = new Yahto.ItemCalcularPorPessoa(tabelaItensPedidoVenda, null, null);
	itemPedidoVendaCalcularPorPessoa.iniciar();
	
	var tipoPedidoVenda = new Yahto.TipoPedidoVenda();
	tipoPedidoVenda.iniciar();
});