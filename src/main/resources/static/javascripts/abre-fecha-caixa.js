Yahto = Yahto || {};

Yahto.AbreFechaCaixa = (function() {
	
	function AbreFechaCaixa() {
		this.checkboxRetirarFechar = $('.js-retirar-fechar');
		this.totalDinheiro = $('#totalDinheiro');
		this.totalCartao = $('#totalCartao');
		this.valorSangria = $('#valorSangria');
		this.valorEntrada = $('#valorEntrada');
		this.valorSangriaOriginal = $('#valorSangriaOriginal');
		this.valorEntradaOriginal = $('#valorEntradaOriginal');
		this.valorTotal = $('#valorTotal');
		this.dataHoraInicio = $('.js-data-hora-inicio');
		this.containersFinalizar = $('.js-container-finalizar');
	}
	
	AbreFechaCaixa.prototype.iniciar = function() {
		this.checkboxRetirarFechar.on('click', onBuscarValoresBtn.bind(this));
		
		$('#valorInicioDinheiro').val(Yahto.formatarMoeda($('#valorInicioDinheiro').val()));
		$('#valorInicioCartao').val(Yahto.formatarMoeda($('#valorInicioCartao').val()));
		this.totalDinheiro.val(Yahto.formatarMoeda(this.totalDinheiro.val()));
		this.totalCartao.val(Yahto.formatarMoeda(this.totalCartao.val()));
		this.valorSangria.val(Yahto.formatarMoeda(this.valorSangria.val()));
		this.valorEntrada.val(Yahto.formatarMoeda(this.valorEntrada.val()));
		this.valorSangriaOriginal.val(Yahto.formatarMoeda(this.valorSangriaOriginal.val()));
		this.valorEntradaOriginal.val(Yahto.formatarMoeda(this.valorEntradaOriginal.val()));
		this.valorTotal.val(Yahto.formatarMoeda(this.valorTotal.val()));
		
		window.onload = function() {
			onBuscarValoresBtn.call(this, $('.js-retirar-fechar'));
		}.bind(this)
	}
	
	function onBuscarValoresBtn(evento) {
		var opcao = $('input:checked').val();
		
		if (opcao == 'RETIRAR_DINHEIRO') {
			this.containersFinalizar.addClass('hidden');
			$('.js-container-sangria').removeClass('hidden');
			$('.js-container-entrada').addClass('hidden');
			this.valorSangria.removeAttr('readonly');
		} else if (opcao == 'ENTRAR_DINHEIRO') {
			this.containersFinalizar.addClass('hidden');
			$('.js-container-sangria').addClass('hidden');
			$('.js-container-entrada').removeClass('hidden');
			this.valorEntrada.removeAttr('readonly');
		} else if (opcao == 'FECHAR_CAIXA') {
			this.valorSangria.val(this.valorSangriaOriginal.val());
			this.valorEntrada.val(this.valorEntradaOriginal.val());
			$.ajax({
				url: $('.js-retirar-fechar').data('url'),
				method: 'GET',
				data: {
					dataHoraInicio: this.dataHoraInicio.val()
				},
				success: onPesquisaConcluida.bind(this)
			});
		}
		$('.js-salvar-btn').removeClass('hidden');
	}
	
	function onPesquisaConcluida(evento) {
		this.totalDinheiro.val(Yahto.formatarMoeda(
				evento.totalDinheiro + numeral($('#valorInicioDinheiro').val()) - numeral(this.valorSangria.val()) + numeral(this.valorEntrada.val())));
		this.totalCartao.val(Yahto.formatarMoeda(evento.totalCartao + numeral($('#valorInicioCartao').val()) ));
		this.valorTotal.val(Yahto.formatarMoeda(evento.valorTotal + 
				numeral($('#valorInicioDinheiro').val()) + numeral($('#valorInicioCartao').val()) - numeral(this.valorSangria.val()) + numeral(this.valorEntrada.val()) ));
		
		this.containersFinalizar.removeClass('hidden');
		this.valorSangria.attr('readonly', true);
		this.valorEntrada.attr('readonly', true);
	}
	
	return AbreFechaCaixa;
	
}());

$(function(){
	var abreFechaCaixa = new Yahto.AbreFechaCaixa();
	abreFechaCaixa.iniciar();
});