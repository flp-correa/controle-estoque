Yahto = Yahto || {};

Yahto.MovimentoEstoque = (function(){
	
	function MovimentoEstoque() {
		this.tipoMovimento = $('#tipoMovimento');
		this.botaoTipoMovimento = $('.js-botao-tipo-movimento');
		this.novoMvtoEstoque = $('#novoMvtoEstoque');
		this.destinoSaidaEstoque = $('#destinoSaidaEstoque');
		this.botoesDeOpcoesMvtoEstoq = [$('.js-botao-entrada'), $('.js-botao-transferencia'), $('.js-botao-saida')]
		this.containerFornecedor = $('.js-container-fornecedor');
		this.containerProduto = $('.js-container-produto');
		this.containerLocalizacao = $('.js-container-localizacao');
		this.containerEmpresaSaida = $('.js-container-empresa-saida');
		this.containerVenda = $('.js-container-venda');
		this.containerLocalizacaoTransferencia = $('.js-container-localizacao-transferencia');
		this.containerValores = $('.js-container-valores');
		
		this.pesquisaRapidaLocalizacaoMvtoEstoque = null;
		
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	MovimentoEstoque.prototype.iniciar = function() {
		this.botaoTipoMovimento.on('click', onBotaoTipoMovimentoSelecionado.bind(this));
		
		this.pesquisaRapidaLocalizacaoMvtoEstoque = new Yahto.PesquisaRapidaLocalizacaoMvtoEstoque();
		this.pesquisaRapidaLocalizacaoMvtoEstoque.iniciar();
		
		if (this.tipoMovimento.val().trim() != '') {
			habilitarContainers.call(this);
			habilitaBotaoAcionadoEDesabilitaOsOutros.call(this);
			
			setTimeout(function() {
				this.emitter.trigger('tipo-movimento-selecionado', [this.tipoMovimento.val(), false]);
			}.bind(this), 1000);
		}
	}
	
	MovimentoEstoque.prototype.getPesquisaRapidaLocalizacaoMvtoEstoque = function() {
		return this.pesquisaRapidaLocalizacaoMvtoEstoque;
	}
	
	function onBotaoTipoMovimentoSelecionado(evento) {
		var tipoMovimento = $(evento.target).data('tipo-movimento');
		this.tipoMovimento.val(tipoMovimento);
		
		new Yahto.limparCamposMovtoEstoque();
		setTimeout(function() {
			this.emitter.trigger('tipo-movimento-selecionado', [tipoMovimento, true]);
		}.bind(this), 200);
		
		habilitaBotaoAcionadoEDesabilitaOsOutros.call(this)
		habilitarContainers.call(this);
		
	}
	
	function habilitaBotaoAcionadoEDesabilitaOsOutros() {		
		var btnHabilitado = getArrayBooleanTipoMovimento(this.tipoMovimento.val());
		for (var i = 0; i < this.botoesDeOpcoesMvtoEstoq.length; i++) {			
			if (btnHabilitado[i]) {				
				this.botoesDeOpcoesMvtoEstoq[i].removeClass('btn-default');
				this.botoesDeOpcoesMvtoEstoq[i].addClass('btn-success');
				this.botoesDeOpcoesMvtoEstoq[i].addClass('active');
			} else {
				this.botoesDeOpcoesMvtoEstoq[i].removeClass('btn-success');
				this.botoesDeOpcoesMvtoEstoq[i].removeClass('active');
				this.botoesDeOpcoesMvtoEstoq[i].addClass('btn-default');
			}
		}
	}
	
	function habilitarContainers() {
		var btnHabilitado = getArrayBooleanTipoMovimento(this.tipoMovimento.val());
		
		this.containerFornecedor.addClass('hidden');
		this.containerEmpresaSaida.addClass('hidden');
		this.containerVenda.addClass('hidden');
		
		this.containerValores.removeClass('hidden');
		this.containerLocalizacaoTransferencia.addClass('hidden');
		
		this.containerProduto.removeClass('hidden');
		this.containerLocalizacao.removeClass('hidden');
		
		if (btnHabilitado[0]) {			
			this.containerFornecedor.removeClass('hidden');
		} else if (btnHabilitado[1]) {
			this.containerLocalizacaoTransferencia.removeClass('hidden');
			this.containerValores.addClass('hidden');
		} else {
			if (this.novoMvtoEstoque.val() == 'true' || this.destinoSaidaEstoque.val() == 'UNIDADE_PROPRIA') {				
				this.destinoSaidaEstoque.val('UNIDADE_PROPRIA');
				this.containerEmpresaSaida.removeClass('hidden');
			} else {
				this.containerVenda.removeClass('hidden');
			}
		}
	}
	
	function getArrayBooleanTipoMovimento(tipoMovimento) {
		return [tipoMovimento == 'ENTRADA', tipoMovimento == 'TRANSFERENCIA', tipoMovimento == 'SAIDA']
	}
	
	return MovimentoEstoque;
}());

Yahto.ComboboxUnidadeMedidaMovtoEstoque = (function(){
	
	function ComboboxUnidadeMedidaMovtoEstoque(movimentoEstoque, pesquisaRapidaProdutoMvtoEstoque) {
		this.movimentoEstoque = movimentoEstoque;
		this.pesquisaRapidaProdutoMvtoEstoque = pesquisaRapidaProdutoMvtoEstoque;
		
		this.comboboxUnidadeMedida = $('#unidadeMedida');
		this.siglaQuantidade = $('.js-sigla-quantidade');
		this.containerQuantidade = $('.js-container-quantidade');
		this.containerQuilograma = $('.js-container-quilograma');
		this.quantidade = $('#quantidade');
		this.quilograma = $('#quilograma');
		this.valorUnitario = $('#valorUnitario');
		this.valorTotal = $('#valorTotal');
		this.familiaUnidadeMedida = '';
		this.tipoMovimento = '';
	}
	
	ComboboxUnidadeMedidaMovtoEstoque.prototype.iniciar = function() {
		this.comboboxUnidadeMedida.on('change', onUnidadeMedidaSelecinada.bind(this));
		this.pesquisaRapidaProdutoMvtoEstoque.on('produto-selecionado', onProdutoSelecionado.bind(this));
		this.movimentoEstoque.on('tipo-movimento-selecionado', onTipoMovimentoSelecionado.bind(this));
		this.quantidade.on('keyup', onValorOuQuantidadeAlterado.bind(this));
		this.quilograma.on('keyup', onQuantidadeQuilogramaAlterado.bind(this));
		this.valorUnitario.on('keyup', onValorOuQuantidadeAlterado.bind(this));
		
		rotinaIniciar.call(this);
	}
	
	function rotinaIniciar() {
		this.comboboxUnidadeMedida.attr('disabled', $('#codigoLocalizacao').val() == '' || $('.js-inputHiddenDetalhes').val() == 'true');
		
		if ($('#codigoProduto').val() != '' && $('#codigoLocalizacao').val() == '' && this.movimentoEstoque != 'TRANSFERENCIA') {			
			$('.js-input-group').addClass('input-group');
			$('.js-input-group').find('span').removeClass('hidden');
			
			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setEmitterPesqProdMvtoEstoq(this.pesquisaRapidaProdutoMvtoEstoque.getEmitter());
			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setFamiliaUnidadeMedida($('#familiaUnidadeMedida').val());
			this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setCodigoProduto(null);
		} else {
			$('.js-input-group').removeClass('input-group');
			$('.js-input-group').find('span').addClass('hidden');
		}
	}
	
	function onValorOuQuantidadeAlterado(evento) {
		if (this.tipoMovimento != 'TRANSFERENCIA' && this.quantidade.val().trim() != '' && this.valorUnitario.val().trim() != '') {
			var valorTotal = numeral(this.quantidade.val().replace('.', '')) * numeral(this.valorUnitario.val());
			this.valorTotal.val(Yahto.formatarMoeda(valorTotal));
		}
	}
	
	function onTipoMovimentoSelecionado(event, tipoMovimento, limparTela) {
		this.tipoMovimento = tipoMovimento;
		
		if (!limparTela) {
			onUnidadeMedidaSelecinada.call(this, '');
			
			if ($('#codigoProduto').val() != '' && $('#codigoLocalizacao').val() != '' && this.tipoMovimento == 'TRANSFERENCIA') {
				$('.js-container-localizacao-transferencia').find('button').attr('disabled', false);
				
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setEmitterPesqProdMvtoEstoq(null);
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setFamiliaUnidadeMedida(null);
				this.movimentoEstoque.getPesquisaRapidaLocalizacaoMvtoEstoque().setCodigoProduto($('#codigoProduto').val());
			}
		}
	}
	
	function onQuantidadeQuilogramaAlterado() {
		this.quantidade.val(this.quilograma.val());
		onValorOuQuantidadeAlterado.call(this);
	}
	
	function onProdutoSelecionado(event, objectEvent) {
		$.ajax({
			url: this.comboboxUnidadeMedida.data('url-unidade-medida'),
			method: 'GET',
			contentType: 'application/json',
			data: {
				familiaUnidadeMedida: objectEvent.familiaUnidadeMedida
			},
			success: onCarregarComboboxUnidadeMedida.bind(this)
		});
	}
	
	function onCarregarComboboxUnidadeMedida(unidadesMedida) {
		this.comboboxUnidadeMedida.empty();
		this.comboboxUnidadeMedida.append('<option value="">Selecione a unidade de medida</option>');
		unidadesMedida.forEach(function(element) {
			  this.comboboxUnidadeMedida.append('<option value=' + element.codigo + ' data-sigla=' + element.tipoUnidadeMedida + '>' +
					  element.descricaoComQuantidade + '</option>');
		}.bind(this));
		
		this.comboboxUnidadeMedida.attr('disabled', false);
		this.quantidade.attr('readonly', true);
		this.valorUnitario.attr('readonly', true);
		limparValores.call(this, false);
	}
	
	function onUnidadeMedidaSelecinada(evento) {
		var sigla = this.comboboxUnidadeMedida.find(':selected').data('sigla');
		this.containerQuilograma.addClass('hidden');
		this.containerQuantidade.addClass('hidden');

		if (sigla == 'KG') {			
			this.containerQuilograma.removeClass('hidden');
			this.quilograma.attr('readonly', false);
			this.quantidade.attr('readonly', true);
		} else {			
			this.containerQuantidade.removeClass('hidden');
			this.quantidade.attr('readonly', false);
			this.quilograma.attr('readonly', true);
			this.quantidade.val(this.quantidade.val().replace(',000', ''));
		}
		
		this.valorUnitario.attr('readonly', false);
		this.siglaQuantidade.html(sigla);
		
		this.valorUnitario.val(Yahto.formatarMoeda(this.valorUnitario.val()));
		this.valorTotal.val(Yahto.formatarMoeda(this.valorTotal.val()));
		
		var maskMoney = new Yahto.MaskMoney();
	    maskMoney.enable();
	    var maskGramas = new Yahto.MaskGramas();
	    maskGramas.enable();
	}
	
	function limparValores() {
		this.quantidade.val('');
		this.quilograma.val('');
		this.valorUnitario.val('');
		this.valorTotal.val('');
		this.siglaQuantidade.html('');
	}
	
	return ComboboxUnidadeMedidaMovtoEstoque;
	
}());

/*Yahto.ContainerDestinoSaida = (function(){
	
	function ContainerDestinoSaida(movimentoEstoque, pesquisaRapidaProdutoMvtoEstoque) {
		this.movimentoEstoque = movimentoEstoque;
		this.pesquisaRapidaProdutoMvtoEstoque = pesquisaRapidaProdutoMvtoEstoque;
		
		this.containerDestinoEstoque = $('.js-container-destino-estoque-saida');
		this.radioButtonDestinoSaidaEstoque = $('.js-destino-estoque-saida');
		this.containerVenda = $('.js-container-venda');
		this.containerEmpresa = $('.js-container-empresa-saida');
		
		this.codigoEmpresaDeposito = $('#codigoEmpresaDeposito');
		this.destinoSaida = $('#destinoSaidaEstoque');
		this.tipoMovimento = '';
	}
	
	ContainerDestinoSaida.prototype.iniciar = function() {
		this.movimentoEstoque.on('tipo-movimento-selecionado', onTipoMovimentoSelecionado.bind(this));
		this.pesquisaRapidaProdutoMvtoEstoque.on('produto-selecionado', onProdutoSelecionado.bind(this));
		this.radioButtonDestinoSaidaEstoque.on('change', onDestinoSaidaSelecionado.bind(this));
	}
	
	function onTipoMovimentoSelecionado(event, tipoMovimento, limparTela) {
		console.log(limparTela);
		this.tipoMovimento = tipoMovimento;
		//if (!limparTela) {
			habilitarContainerDestinoSaida.call(this);
		//}
	}
	
	function onProdutoSelecionado(event, objectEvent) {
		this.codigoEmpresaDeposito.val(objectEvent.codigoEmpresa);
		habilitarContainerDestinoSaida.call(this);
	}
	
	function onDestinoSaidaSelecionado(evento) {
		this.destinoSaida.val($(evento.currentTarget).data('destino-saida'));
		habilitarContainerDestinoSaida.call(this);
	}
	
	function habilitarContainerDestinoSaida() {
		this.containerDestinoEstoque.addClass('hidden');
		this.containerVenda.addClass('hidden');
		this.containerEmpresa.addClass('hidden');
		
		if (this.tipoMovimento == 'SAIDA' && this.codigoEmpresaDeposito.val() != '') {
			this.containerDestinoEstoque.removeClass('hidden');
			
			if (this.destinoSaida.val() == 'UNIDADE_PROPRIA') {
				this.containerEmpresa.removeClass('hidden');
			} else if (this.destinoSaida.val() == 'VENDA') {
				this.containerVenda.removeClass('hidden');
			}
		}
		
	}
	
	return ContainerDestinoSaida;
}());*/

Yahto.limparCamposMovtoEstoque = function() {
	$('#codigoFornecedor').val('');
	$('#cpfOuCnpjFornecedor').val('');
	$('#descricaoFornecedor').val('');
	$('#codigoProduto').val('');
	$('#descricaoProduto').val('');
	$('#unidadeMedida').empty();
	$('#unidadeMedida').append('<option value="">Selecione a unidade de medida</option>');
	$('#unidadeMedida').attr('disabled', true);
	$('#codigoEmpresa').val('');
	$('#cnpjEmpresa').val('');
	$('#descricaoEmpresa').val('');
	$('#codigoVenda').val('');
	$('.js-sigla-quantidade').val('');
	$('#quantidade').val('');
	$('#quilograma').val('');
	$('#valorUnitario').val('');
	$('#valorTotal').val('');
	$('#observacao').val('');
	
	$('.js-container-localizacao').find('button').attr('disabled', true);
	$('.js-container-localizacao-transferencia').find('button').attr('disabled', true);
	new Yahto.exibirCamposLocalizacaoMovtoEstoque(null, true);
}

$(function(){
	var movimentoEstoque = new Yahto.MovimentoEstoque();
	movimentoEstoque.iniciar();
	
	var pesquisaRapidaProdutoMvtoEstoque = new Yahto.PesquisaRapidaProdutoMvtoEstoque(movimentoEstoque);
	pesquisaRapidaProdutoMvtoEstoque.iniciar();
	
	var comboboxUnidadeMedidaMovtoEstoque = new Yahto.ComboboxUnidadeMedidaMovtoEstoque(movimentoEstoque, pesquisaRapidaProdutoMvtoEstoque);
	comboboxUnidadeMedidaMovtoEstoque.iniciar();
	
	//var containerDestinoSaida = new Yahto.ContainerDestinoSaida(movimentoEstoque, pesquisaRapidaProdutoMvtoEstoque);
	//containerDestinoSaida.iniciar();
});