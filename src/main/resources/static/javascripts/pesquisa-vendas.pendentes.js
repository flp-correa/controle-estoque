Yahto = Yahto || {};

Yahto.PesquisaVendasPendentes = (function() {
	
	function PesquisaVendasPendentes(multiselecao) {
		this.multiselecao = multiselecao;
		this.total = $('#total');
	}
	
	PesquisaVendasPendentes.prototype.iniciar = function() {
		this.multiselecao.on('multiselecao-clicado', onSelecaoClicado.bind(this));
		onSelecaoClicado.call(this);
	}
	
	function onSelecaoClicado(evento) {
		this.total.val('0,00');
		
		var vendasPendentes = $.map($('.js-selecao'), function(e) {
			if ($(e).is(':checked')) {
				return numeral($(e).data('valor-total'));
			}
		});
		this.total.val(Yahto.formatarMoeda(vendasPendentes.reduce(( total, i ) => total + i, 0)));
	}
	
	return PesquisaVendasPendentes;
	
}());

$(function() {
	var multiSelecao = new Yahto.MultiSelecao();
	multiSelecao.iniciar();
	
	var vendasPendentes = new Yahto.PesquisaVendasPendentes(multiSelecao);
	vendasPendentes.iniciar();
});