Yahto = Yahto || {};

Yahto.AutocompleteItensVenda = (function() {
	
	function AutocompleteItensVenda() {
		this.nomeInput = $('.js-nome-produto-input');
		var htmlTemplateAutocompleteItensVenda = $('#template-autocomplete-produto-venda').html();
		this.template = Handlebars.compile(htmlTemplateAutocompleteItensVenda);
		this.emitter = $({});
		this.on = this.emitter.on.bind(this.emitter);
	}
	
	AutocompleteItensVenda.prototype.iniciar = function() {
		var options = {
			url: function(descricao) {
				return this.nomeInput.data('url') + '?descricao=' + descricao;
			}.bind(this),
			getValue: 'descricao',
			minCharNumber: 3,
			requestDelay: 300,
			ajaxSettings: {
				contentType: 'application/json'
			},
			template: {
				type: 'custom',
				method: template.bind(this)
			},
			list: {
				onChooseEvent: onItemSelecionado.bind(this)
			}
		};
		
		this.nomeInput.easyAutocomplete(options);
	}
	
	function onItemSelecionado() {
		this.emitter.trigger('item-selecionado', this.nomeInput.getSelectedItemData());
		this.nomeInput.val('');
		this.nomeInput.focus();
	}
	
	function template(nome, produto) {
		produto.valorFormatado = Yahto.formatarMoeda(produto.valorUnitario);
		return this.template(produto);
	}
	
	return AutocompleteItensVenda
	
}());