Yahto.TabelaLocalizacoes = (function () {
	
	function TabelaLocalizacoes(autocomplete) {
		this.autocomplete = autocomplete;
		this.tabelaLocalizacoesContainer = $('.js-tabela-localizacoes-container');
		this.uuid = $('#uuid').val();
	}
	
	TabelaLocalizacoes.prototype.iniciar = function() {
		this.autocomplete.on('localizacao-selecionada', onLocalizacaoSelecionada.bind(this));
		$('.js-exclusao-localizacao').on('click', onExclusaoLocalizacao.bind(this));
	}
	
	function onLocalizacaoSelecionada(evento, localizacao) {
		var resposta = $.ajax({
			url: 'localizacao',
			method: 'POST',
			data: {
				codigoLocalizacao: localizacao.codigo,
				uuid: this.uuid
			}
		});
		
		resposta.done(onLocalizacaoAtualizadaNoServidor.bind(this));
	}
	
	function onLocalizacaoAtualizadaNoServidor(html) {
		this.tabelaLocalizacoesContainer.html(html);	
		$('.js-exclusao-localizacao').on('click', onExclusaoLocalizacao.bind(this));
	}
	
	function onExclusaoLocalizacao(evento) {
		var codigoLocalizacao = $(evento.target).data('codigo-localizacao');
		
		$.ajax({
			url: 'localizacao/validar-exclusao',
			contentType: 'application/json',
			method: 'DELETE',
			data: JSON.stringify({codigo: $('.js-codigo-produto').val(),
								descricao: $('.js-descricao-produto').val(),
								tipoUnidadeMedida: $('#tipoUnidadeMedida').val(),
								localizacao: {codigo: codigoLocalizacao}}),
			success: onPodeExcluirLocalizacao.bind(this, codigoLocalizacao),
			error: onErroExcluirLocalizacao.bind(this)
		});
	}
	
	function onPodeExcluirLocalizacao(codigoLocalizacao) {
		var resposta = $.ajax({
			url: 'localizacao/' + this.uuid + '/' + codigoLocalizacao,
			method: 'DELETE'
		});
		
		resposta.done(onLocalizacaoAtualizadaNoServidor.bind(this));
	}
	
	function onErroExcluirLocalizacao(e) {
		swal('Ops!', e.responseText, 'error');
	}
	
	return TabelaLocalizacoes;
	
}());

$(function() {		
	var autocompleteLocalizacao = new Yahto.AutocompleteHbs('descricao', $('.js-endereco-ou-descricao-localizacao-input'), 
			$('#template-autocomplete-localizacao'), 'localizacao-selecionada');
	autocompleteLocalizacao.iniciar();
	
	var tabelaLocalizacoes = new Yahto.TabelaLocalizacoes(autocompleteLocalizacao);
	tabelaLocalizacoes.iniciar();
});