CREATE TABLE item_ped_venda_adic (
    codigo_item_ped_venda BIGINT(20) NOT NULL,
    codigo_adicional BIGINT(20) NOT NULL,
    PRIMARY KEY (codigo_item_ped_venda, codigo_adicional),
    FOREIGN KEY (codigo_item_ped_venda) REFERENCES item_pedido_venda(codigo),
    FOREIGN KEY (codigo_adicional) REFERENCES adicional(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE item_ped_venda_ingred (
    codigo_item_ped_venda BIGINT(20) NOT NULL,
    codigo_ingrediente BIGINT(20) NOT NULL,
    PRIMARY KEY (codigo_item_ped_venda, codigo_ingrediente),
    FOREIGN KEY (codigo_item_ped_venda) REFERENCES item_pedido_venda(codigo),
    FOREIGN KEY (codigo_ingrediente) REFERENCES ingrediente(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
