CREATE TABLE venda (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,    
    data_hora_criacao DATETIME NOT NULL,
    valor_acrescimo DECIMAL(10,3),
    valor_desconto DECIMAL(10,3),
    valor_frete DECIMAL(10,3),
    valor_total DECIMAL(10,3) NOT NULL,
    status VARCHAR(30) NOT NULL,
    observacao VARCHAR(200),
    data_hora_entrega DATETIME,
    codigo_cliente BIGINT(20),
    codigo_mesa BIGINT(20),
    codigo_usuario BIGINT(20) NOT NULL,
    codigo_pedido_venda BIGINT(20),
    FOREIGN KEY (codigo_cliente) REFERENCES cliente(codigo),
    FOREIGN KEY (codigo_mesa) REFERENCES mesa(codigo),
    FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo),
    FOREIGN KEY (codigo_pedido_venda) REFERENCES pedido_venda(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE item_venda (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,    
    quantidade INTEGER NOT NULL,
    valor_unitario DECIMAL(10,3) NOT NULL,
    codigo_produto BIGINT(20) NOT NULL,
    codigo_venda BIGINT(20),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo),
    FOREIGN KEY (codigo_venda) REFERENCES venda(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO permissao VALUES (21, 'ROLE_PESQUISAR_VENDA');
INSERT INTO permissao VALUES (22, 'ROLE_CADASTRAR_VENDA');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 21);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 21);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 22);
