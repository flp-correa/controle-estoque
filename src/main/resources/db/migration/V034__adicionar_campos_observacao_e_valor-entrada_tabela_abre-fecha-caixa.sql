ALTER TABLE abre_fecha_caixa
    ADD valor_entrada DECIMAL(10,3),
    ADD observacao VARCHAR(300);

UPDATE abre_fecha_caixa SET valor_entrada=0.000 WHERE valor_entrada IS NULL;
