CREATE TABLE movimento_estoque (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    codigo_produto BIGINT(20) NOT NULL,
    codigo_fornecedor BIGINT(20),
    codigo_empresa BIGINT(20),
    codigo_usuario BIGINT(20) NOT NULL,
    codigo_unidade_medida BIGINT(20) NOT NULL,
    tipo_movimento VARCHAR(15) NOT NULL,
    quantidade DECIMAL(10,3) NOT NULL,
    valor_unitario DECIMAL(10,3),
    data_hora_cadastro DATETIME  NOT NULL,
    lote VARCHAR(15),
    validade_lote DATE,
    observacao VARCHAR(200),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo),
    FOREIGN KEY (codigo_fornecedor) REFERENCES fornecedor(codigo),
    FOREIGN KEY (codigo_empresa) REFERENCES empresa(codigo),
    FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo),
    FOREIGN KEY (codigo_unidade_medida) REFERENCES unidade_medida(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE saldo_estoque (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    codigo_movto_estoque BIGINT(20) NOT NULL,
    codigo_localizacao BIGINT(20) NOT NULL,
    saldo_estoque DECIMAL(10,3) NOT NULL,
    saldo_localizacao DECIMAL(10,3) NOT NULL,  
    FOREIGN KEY (codigo_movto_estoque) REFERENCES movimento_estoque(codigo),
    FOREIGN KEY (codigo_localizacao) REFERENCES localizacao(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
