CREATE TABLE ingrediente (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE produto_ingrediente (
    codigo_produto BIGINT(20) NOT NULL,
    codigo_ingrediente BIGINT(20) NOT NULL,
    PRIMARY KEY (codigo_produto, codigo_ingrediente),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo),
    FOREIGN KEY (codigo_ingrediente) REFERENCES ingrediente(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
