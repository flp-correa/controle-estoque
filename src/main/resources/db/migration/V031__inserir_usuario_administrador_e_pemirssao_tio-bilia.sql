INSERT INTO usuario (tenant_id, cpf, nome, email, telefone, situacao, data_nascimento, data_cadastro, senha) VALUES
('tio-bilia', '37771039006', 'Fernando', 'fernando_tio-bilia@gmail.com', '0000000000', 1, '1984-07-01', sysdate(),
	'$2a$10$g.wT4R0Wnfel1jc/k84OXuwZE02BlACSLfWy6TycGPvvEKvIm86SG');

INSERT INTO usuario_grupo (codigo_usuario, codigo_grupo) VALUES (
	(SELECT codigo FROM usuario WHERE email = 'fernando_tio-bilia@gmail.com'), 1);
