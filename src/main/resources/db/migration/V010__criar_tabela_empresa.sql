CREATE TABLE empresa (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    possui_estoque BOOLEAN NOT NULL,
    cpf_cnpj VARCHAR(30),
    tipo_pessoa VARCHAR(15) NOT NULL,
    unidade CHAR(6),    
    nome_empresarial VARCHAR(100) NOT NULL,
    nome_fantasia VARCHAR(100),
    email  VARCHAR(50),
    telefone1 VARCHAR(50),
    telefone2 VARCHAR(50),
    celular VARCHAR(50),
    fax VARCHAR(50),
    cep VARCHAR(50),
	logradouro VARCHAR(100),
	numero VARCHAR(50),
	complemento VARCHAR(100),
	bairro VARCHAR(100),
    observacao VARCHAR(300),
    data_cadastro DATE,
	codigo_cidade BIGINT(20),
    FOREIGN KEY (codigo_cidade) REFERENCES cidade(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


