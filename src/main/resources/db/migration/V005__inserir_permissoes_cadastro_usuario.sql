INSERT INTO permissao VALUES (2, 'ROLE_PESQUISAR_USUARIO');
INSERT INTO permissao VALUES (3, 'ROLE_CADASTRAR_USUARIO');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 2);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 3);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 2);

-- grupo 3 - Garçom
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 2);

-- grupo 4 - Chapeiro/Cozinha
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (4, 2);
