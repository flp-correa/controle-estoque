ALTER TABLE movimento_estoque
    ADD codigo_venda BIGINT(20) AFTER codigo_empresa,
    ADD FOREIGN KEY (codigo_venda) REFERENCES venda(codigo);
