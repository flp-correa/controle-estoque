ALTER TABLE produto
    ADD por_quilo BOOLEAN DEFAULT false AFTER lanche_prato,
    ADD enviar_cozinha BOOLEAN DEFAULT false AFTER por_quilo;

UPDATE produto SET enviar_cozinha=1 WHERE lanche_prato=1;
