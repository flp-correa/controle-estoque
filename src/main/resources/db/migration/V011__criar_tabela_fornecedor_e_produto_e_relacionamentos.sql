CREATE TABLE fornecedor (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    cpf_cnpj VARCHAR(30),
    tipo_pessoa VARCHAR(15) NOT NULL,
    unidade CHAR(6),
    nome_empresarial VARCHAR(150) NOT NULL,
    nome_fantasia VARCHAR(150),
    carregar_produtos BOOLEAN,  
    email  VARCHAR(50),
    condicao_pagamento VARCHAR(150),    
    telefone1 VARCHAR(50),
    telefone2 VARCHAR(50),
    celular VARCHAR(50),
    fax VARCHAR(50),
    cep VARCHAR(50),
	logradouro VARCHAR(100),
	numero VARCHAR(50),
	complemento VARCHAR(100),
	bairro VARCHAR(100),
    observacao VARCHAR(300),
    data_cadastro DATE,
	codigo_cidade BIGINT(20),
    FOREIGN KEY (codigo_cidade) REFERENCES cidade(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE produto (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    descricao VARCHAR(100) NOT NULL,
    unidade_medida VARCHAR(50) NOT NULL,
    vende BOOLEAN NOT NULL,
    lanche_prato BOOLEAN NOT NULL,
    possui_estoque BOOLEAN NOT NULL,
    local_fixo BOOLEAN NOT NULL,
    valor_unitario DECIMAL(10,3) NOT NULL,
    observacao VARCHAR(200),
    foto VARCHAR(100),
    content_type VARCHAR(100),
    data_cadastro DATE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE fornecedor_produto (
    codigo_fornecedor BIGINT(20) NOT NULL,
    codigo_produto BIGINT(20) NOT NULL,
    PRIMARY KEY (codigo_fornecedor, codigo_produto),
    FOREIGN KEY (codigo_fornecedor) REFERENCES fornecedor(codigo),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


