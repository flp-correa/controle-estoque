CREATE TABLE deposito (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    codigo_empresa BIGINT(20) NOT NULL,
    descricao VARCHAR(100)  NOT NULL,    
    observacao VARCHAR(200),
    situacao BOOLEAN DEFAULT true,
    data_cadastro DATE NOT NULL,
    FOREIGN KEY (codigo_empresa) REFERENCES empresa(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
