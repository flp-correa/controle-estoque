CREATE TABLE abre_fecha_caixa (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,    
    data_hora_inicio DATETIME NOT NULL,
    data_hora_fim DATETIME,
    valor_inicio_din DECIMAL(10,3),
    valor_inicio_car DECIMAL(10,3),
    total_dinheiro DECIMAL(10,3),
    total_cartao DECIMAL(10,3),
    valor_total DECIMAL(10,3),
    valor_sangria DECIMAL(10,3)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
