ALTER TABLE produto
    ADD enviar_compra BOOLEAN AFTER enviar_cozinha;

UPDATE produto SET enviar_compra=0 WHERE enviar_compra IS NULL;
