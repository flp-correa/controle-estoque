CREATE TABLE unidade_medida (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    familia VARCHAR(50) NOT NULL,
    unidade_medida VARCHAR(50) NOT NULL,
    quantidade DECIMAL(10,3),
    observacao VARCHAR(200),
    situacao BOOLEAN DEFAULT true,
    data_cadastro DATE NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
