CREATE TABLE pedido_venda (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,    
    data_hora_criacao DATETIME NOT NULL,
    data_hora_fim DATETIME,
    valor_frete DECIMAL(10,3),
    valor_total DECIMAL(10,3) NOT NULL,
    status VARCHAR(30) NOT NULL,
    data_hora_entrega DATETIME,
    observacao VARCHAR(200),
    codigo_cliente BIGINT(20),
    codigo_mesa BIGINT(20),
    codigo_usuario BIGINT(20) NOT NULL,
    FOREIGN KEY (codigo_cliente) REFERENCES cliente(codigo),
    FOREIGN KEY (codigo_mesa) REFERENCES mesa(codigo),
    FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE item_pedido_venda (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,    
    quantidade INTEGER NOT NULL,
    valor_unitario DECIMAL(10,3) NOT NULL,
    status_cozinha VARCHAR(30),
    codigo_produto BIGINT(20) NOT NULL,
    codigo_pedido_venda BIGINT(20),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo),
    FOREIGN KEY (codigo_pedido_venda) REFERENCES pedido_venda(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO permissao VALUES (16, 'ROLE_PESQUISAR_PEDIDO_VENDA');
INSERT INTO permissao VALUES (17, 'ROLE_CADASTRAR_PEDIDO_VENDA');

INSERT INTO permissao VALUES (18, 'ROLE_OPERADOR_CAIXA');
INSERT INTO permissao VALUES (19, 'ROLE_GARCOM');
INSERT INTO permissao VALUES (20, 'ROLE_CHAPEIRO_COZINHA');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 16);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 16);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 17);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 18);

-- grupo 3 - Garçom
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 17);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 19);

-- grupo 3 - Chapeiro/Cozinha
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (4, 20);
