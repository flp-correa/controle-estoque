CREATE TABLE localizacao (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    codigo_deposito BIGINT(20) NOT NULL,
    codigo_produto BIGINT(20),
    unidade_medida VARCHAR(50) NOT NULL,
    estoque_minimo DECIMAL(10,3) NOT NULL,
    estoque_maximo DECIMAL(10,3) NOT NULL,
    blocado BOOLEAN NOT NULL,  
    endereco VARCHAR(10) NOT NULL,
    descricao VARCHAR(100)  NOT NULL,    
    observacao VARCHAR(200),
    situacao BOOLEAN DEFAULT true,
    data_cadastro DATE NOT NULL,
    FOREIGN KEY (codigo_deposito) REFERENCES deposito(codigo),
    FOREIGN KEY (codigo_produto) REFERENCES produto(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
