INSERT INTO permissao VALUES (6, 'ROLE_PESQUISAR_PRODUTO');
INSERT INTO permissao VALUES (7, 'ROLE_CADASTRAR_PRODUTO');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 6);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 7);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 6);

-- grupo 3 - Garçom
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 6);

-- grupo 4 - Chapeiro/Cozinha
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (4, 6);
