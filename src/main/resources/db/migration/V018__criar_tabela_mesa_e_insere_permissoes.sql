CREATE TABLE mesa (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    nr_mesa CHAR(3) NOT NULL,
	codigo_deposito BIGINT(20),
    data_cadastro DATE,
    FOREIGN KEY (codigo_deposito) REFERENCES deposito(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO permissao VALUES (14, 'ROLE_PESQUISAR_MESA');
INSERT INTO permissao VALUES (15, 'ROLE_CADASTRAR_MESA');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 14);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 15);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 14);

-- grupo 3 - Garçom
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 14);
