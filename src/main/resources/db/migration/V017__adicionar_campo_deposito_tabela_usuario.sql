ALTER TABLE usuario
    ADD codigo_deposito BIGINT(20),
    ADD FOREIGN KEY (codigo_deposito) REFERENCES deposito(codigo);
