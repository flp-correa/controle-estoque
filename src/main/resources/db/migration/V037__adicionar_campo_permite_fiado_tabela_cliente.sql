ALTER TABLE cliente
    ADD permite_fiado BOOLEAN AFTER nome_empresarial;

UPDATE cliente SET permite_fiado=0 WHERE permite_fiado IS NULL;
