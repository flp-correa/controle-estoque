CREATE TABLE cliente (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    cpf_cnpj VARCHAR(30),
    tipo_pessoa VARCHAR(15) NOT NULL,
    unidade CHAR(6),
    nome_empresarial VARCHAR(150),
    nome_fantasia VARCHAR(150),
    email  VARCHAR(50),
    condicao_pagamento VARCHAR(150),    
    telefone1 VARCHAR(50),
    telefone2 VARCHAR(50),
    celular VARCHAR(50),
    fax VARCHAR(50),
    cep VARCHAR(50),
	logradouro VARCHAR(200),
	numero VARCHAR(50),
	complemento VARCHAR(200),
	bairro VARCHAR(200),
    observacao VARCHAR(300),
    data_cadastro DATE,
	codigo_cidade BIGINT(20),
    FOREIGN KEY (codigo_cidade) REFERENCES cidade(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO permissao VALUES (12, 'ROLE_PESQUISAR_CLIENTE');
INSERT INTO permissao VALUES (13, 'ROLE_CADASTRAR_CLIENTE');

-- grupo 1 - administrador 
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 12);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (1, 13);

-- grupo 2 - Operador de caixa
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 12);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (2, 13);

-- grupo 3 - Garçom
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 12);
INSERT INTO grupo_permissao (codigo_grupo, codigo_permissao) VALUES (3, 13);
