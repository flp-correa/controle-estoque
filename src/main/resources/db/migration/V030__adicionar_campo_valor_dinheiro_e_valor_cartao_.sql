ALTER TABLE venda
    ADD valor_dinheiro DECIMAL(10,3) AFTER data_hora_criacao,
    ADD valor_cartao DECIMAL(10,3) AFTER valor_dinheiro;
    
