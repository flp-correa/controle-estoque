package br.com.yahtosistemas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.helper.usuario.UsuariosQueries;

public interface Usuarios extends JpaRepository<Usuario, Long>, UsuariosQueries {
	
	public List<Usuario> findByEmailOrCodigo(String email, Long codigo);

}
