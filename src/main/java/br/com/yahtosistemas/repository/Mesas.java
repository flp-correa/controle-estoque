package br.com.yahtosistemas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.repository.helper.mesa.MesasQueries;

public interface Mesas extends JpaRepository<Mesa, Long>, MesasQueries {

	public Optional<Mesa> findByNrMesaAndDeposito(String nrMesa, Deposito deposito);
	
	public List<Mesa> findByDepositoOrderByNrMesaAsc(Deposito deposito);
	
	public List<Mesa> findByNrMesaContainingAndDeposito(String nrMesa, Deposito deposito);
}
