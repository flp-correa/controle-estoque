package br.com.yahtosistemas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.repository.helper.saldoEstoque.SaldosEstoqueQueries;

@Repository
public interface SaldosEstoque extends JpaRepository<SaldoEstoque, Long>, SaldosEstoqueQueries {

	public List<SaldoEstoque> findByMovimentoEstoque(MovimentoEstoque movimentoEstoque);	
}
