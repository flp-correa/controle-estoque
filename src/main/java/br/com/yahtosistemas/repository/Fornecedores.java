package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.repository.helper.fornecedor.FornecedoresQueries;

public interface Fornecedores extends JpaRepository<Fornecedor, Long>, FornecedoresQueries {

}
