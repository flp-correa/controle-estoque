package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Grupo;

public interface Grupos extends JpaRepository<Grupo, Long> {

}
