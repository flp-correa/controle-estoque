package br.com.yahtosistemas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.repository.helper.ingrediente.IngredientesQueries;

public interface Ingredientes extends JpaRepository<Ingrediente, Long>, IngredientesQueries {

	public Ingrediente findByCodigo(Long codigo);
	
	public Optional<Ingrediente> findByNomeIgnoreCase(String nome);

	public List<Ingrediente> findByTenantIdAndNomeContaining(String tenantId, String campoPesquisa);
}
