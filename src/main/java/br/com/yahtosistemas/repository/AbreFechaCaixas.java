package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.repository.helper.abreFechaCaixa.AbreFechaCaixasQueries;

public interface AbreFechaCaixas extends JpaRepository<AbreFechaCaixa, Long>, AbreFechaCaixasQueries {

	public AbreFechaCaixa findByTenantIdAndDataHoraFimIsNull(String tenantId);
}
