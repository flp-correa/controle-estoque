package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.repository.helper.cliente.ClientesQueries;

public interface Clientes extends JpaRepository<Cliente, Long>, ClientesQueries {

	public Cliente findByTenantIdAndNomeEmpresarial(String tenantId, String nome);
}
