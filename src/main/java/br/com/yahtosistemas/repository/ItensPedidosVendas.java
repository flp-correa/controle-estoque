package br.com.yahtosistemas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.repository.helper.itemPedidoVenda.ItensPedidosVendasQueries;

public interface ItensPedidosVendas extends JpaRepository<ItemPedidoVenda, Long>, ItensPedidosVendasQueries {

	public List<ItemPedidoVenda> findByCodigoIn(Long[] codigos);
	
	public List<ItemPedidoVenda> findByPedidoVenda(PedidoVenda pedidoVenda);
}
