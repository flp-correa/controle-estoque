package br.com.yahtosistemas.repository.filter;

import br.com.yahtosistemas.model.Deposito;

public class MesaFilter {

	private Long codigo;
	private String nrMesa;
	private Deposito deposito;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNrMesa() {
		return nrMesa;
	}
	public void setNrMesa(String nrMesa) {
		this.nrMesa = nrMesa;
	}
	public Deposito getDeposito() {
		return deposito;
	}
	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}
}
