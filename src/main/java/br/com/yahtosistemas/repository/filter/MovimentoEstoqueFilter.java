package br.com.yahtosistemas.repository.filter;

import java.time.LocalDate;

import br.com.yahtosistemas.model.Produto;

public class MovimentoEstoqueFilter {

	private Long codigo;
	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Produto produto;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}
	public LocalDate getDataFim() {
		return dataFim;
	}
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
