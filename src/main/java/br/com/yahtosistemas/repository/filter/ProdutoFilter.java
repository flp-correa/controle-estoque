package br.com.yahtosistemas.repository.filter;

public class ProdutoFilter {

	private String descricao;
	private Boolean estoqueMinimo = Boolean.FALSE;
	private Boolean vendedor = Boolean.FALSE;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getEstoqueMinimo() {
		return estoqueMinimo;
	}

	public void setEstoqueMinimo(Boolean estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}

	public Boolean getVendedor() {
		return vendedor;
	}

	public void setVendedor(Boolean vendedor) {
		this.vendedor = vendedor;
	}
}
