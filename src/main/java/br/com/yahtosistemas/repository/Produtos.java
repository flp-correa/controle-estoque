package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.helper.produto.ProdutosQueries;

@Repository
public interface Produtos extends JpaRepository<Produto, Long>, ProdutosQueries {

	public Produto findByCodigo(Long codigo);
}
