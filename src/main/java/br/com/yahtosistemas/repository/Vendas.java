package br.com.yahtosistemas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.repository.helper.venda.VendasQueries;

public interface Vendas extends JpaRepository<Venda, Long>, VendasQueries {
	
	public List<Venda> findByCodigoIn(Long[] codigos);

	public List<Venda> findByTenantIdAndStatusAndCliente_NomeEmpresarialContaining(String tenantId, StatusVenda status, String nome);
}
