package br.com.yahtosistemas.repository.listener;

import javax.persistence.PostLoad;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.storage.FotoStorage;

public class ProdutoEntityListener {

	@PostLoad
	public void postLoad(final Produto produto) {
		FotoStorage fotoStorage = ControleEstoqueApplication.getBean(FotoStorage.class);
		produto.setUrlFoto(fotoStorage.getUrl(produto.getFotoOuMock()));
		produto.setUrlThumbnailFoto(fotoStorage.getUrl(FotoStorage.THUMBNAIL_PREFIX + produto.getFotoOuMock()));
	}
}