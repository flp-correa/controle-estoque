package br.com.yahtosistemas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.helper.localizacao.LocalizacoesQueries;

@Repository
public interface Localizacoes extends JpaRepository<Localizacao, Long>, LocalizacoesQueries {

	public Localizacao findByCodigo(Long codigo);
	
	public Localizacao findByDepositoAndProduto(Deposito deposito, Produto produto);
	
	public List<Localizacao> findByProduto(Produto produto);
	
	public List<Localizacao> findByCodigoIn(long[] localizacoesArray);
}
