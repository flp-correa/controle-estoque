package br.com.yahtosistemas.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Adicional;
import br.com.yahtosistemas.repository.helper.adicional.AdicionaisQueries;

public interface Adicionais extends JpaRepository<Adicional, Long>, AdicionaisQueries {

	public Optional<Adicional> findByNomeIgnoreCase(String nome);
	
	public List<Adicional> findByTenantId(String tenantId);
}
