package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.Estado;

public interface Estados extends JpaRepository<Estado, Long> {

}
