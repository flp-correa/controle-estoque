package br.com.yahtosistemas.repository.helper.ingrediente;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.model.ProdutoIngrediente;
import br.com.yahtosistemas.repository.filter.IngredienteFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class IngredientesImpl implements IngredientesQueries {

	@PersistenceContext
	private EntityManager manager;

	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public Page<Ingrediente> filtrar(IngredienteFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Ingrediente.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public List<Ingrediente> listaIngredientesPorProduto(Long codigoProduto) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Ingrediente.class);
		DetachedCriteria dc = DetachedCriteria.forClass(ProdutoIngrediente.class);
		dc.add(Restrictions.eq("id.produto.codigo", codigoProduto)); 
		dc.setProjection(Projections.property("id.ingrediente"));
		criteria.add(Restrictions.and(Subqueries.propertyIn("codigo", dc)));
		
		return criteria.list();
	}

	@SuppressWarnings("deprecation")
	private Long total(IngredienteFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Ingrediente.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	private void adicionarFiltro(IngredienteFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getNome())) {
				criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
			}
		}
	}
}
