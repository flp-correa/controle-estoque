package br.com.yahtosistemas.repository.helper.adicional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.model.Adicional;
import br.com.yahtosistemas.repository.filter.AdicionalFilter;

public interface AdicionaisQueries {

	public Page<Adicional> filtrar(AdicionalFilter filtro, Pageable pageable);
}
