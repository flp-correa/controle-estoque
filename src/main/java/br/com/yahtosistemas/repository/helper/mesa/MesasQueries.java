package br.com.yahtosistemas.repository.helper.mesa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.repository.filter.MesaFilter;

public interface MesasQueries {

	public Page<Mesa> filtrar(MesaFilter filtro, Pageable pageable);
	
}
