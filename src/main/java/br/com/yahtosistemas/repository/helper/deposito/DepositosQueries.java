package br.com.yahtosistemas.repository.helper.deposito;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.dto.DepositoDTO;
import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.repository.filter.DepositoFilter;

public interface DepositosQueries {

	@Transactional(readOnly = true)
	public Page<Deposito> filtrar(DepositoFilter filtro, Pageable pageable);
	
	@Transactional(readOnly = true)
	public List<DepositoDTO> porDescricaoPesquisaRapida(String descricao);
	
}
