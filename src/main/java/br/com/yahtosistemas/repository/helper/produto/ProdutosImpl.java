package br.com.yahtosistemas.repository.helper.produto;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.ProdutoDTO;
import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.repository.filter.ProdutoFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;
import br.com.yahtosistemas.storage.FotoStorage;

public class ProdutosImpl implements ProdutosQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Autowired
	private SaldosEstoque saldosEstoque;
	
	@Autowired
	private MovimentacoesEstoque movimentacoesEstoque;
	
	@Autowired
	private FotoStorage fotoStorage;
	
	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<Produto> filtrar(ProdutoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Produto.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		List<Produto> produtos = criteria.list();
		produtos.forEach((p)->{
			if (p.getPossuiEstoque()) {				
				BigDecimal saldoTotalEstoque = saldosEstoque.saldoTotalEstoqueProduto(p);
				p.setQuantidadeEstoque(saldoTotalEstoque);
				p.setValorTotalEstoque(movimentacoesEstoque.valorUnitarioUltimoMovtoEntradaEstoqueProduto(p)
						.multiply(saldoTotalEstoque, MathContext.DECIMAL128));
			}
		});
		
		return new PageImpl<>(produtos, pageable, total(filtro));
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<ProdutoDTO> buscarPorDescricaoEPossuiEstoque(String campoPesquisa) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(p.codigo, p.descricao, p.valorUnitario, p.tipoUnidadeMedida) " 
					+ "from Produto AS p where possuiEstoque IS TRUE and lower(descricao) like lower(:campoPesquisa)";
		
		return manager.createQuery(jpql, ProdutoDTO.class)
			.setParameter("campoPesquisa", "%" + campoPesquisa + "%")
			.getResultList();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<ProdutoDTO> buscarPorDescricaoEDepositoOuNaoPossuiEstoque(String campoPesquisa, Deposito deposito) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(p.codigo, p.descricao, p.valorUnitario, p.foto, p.lancheOuPrato, p.porQuilo, p.enviarCozinha) " 
				+ "from Produto AS p LEFT OUTER JOIN p.localizacoes l where "
				+ "p.vende IS TRUE and lower(p.descricao) like lower(:campoPesquisa) and (p.possuiEstoque IS FALSE or l.deposito = :deposito)";
	
		List<ProdutoDTO> produtosFiltrados = manager.createQuery(jpql, ProdutoDTO.class)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%")
				.setParameter("deposito", deposito)
				.getResultList();
		
		produtosFiltrados.forEach(c -> c.setUrlThumbnailFoto(fotoStorage.getUrl(FotoStorage.THUMBNAIL_PREFIX + c.getFoto())));
			
		return produtosFiltrados;
	}

	@Transactional(readOnly = true)
	@Override
	public List<ProdutoDTO> porDescricaoPesquisaRapidaCadastroMovtoEstoque(String campoPesquisa, Long codigoFornecedor, TipoMovimentoEstoque tipoMovimento) {
		if (tipoMovimento.equals(TipoMovimentoEstoque.TRANSFERENCIA) || tipoMovimento.equals(TipoMovimentoEstoque.SAIDA)) {
			return buscarPesquisaRapidaMovimentoEstoque(campoPesquisa, "");
		} else if (codigoFornecedor == null) {
			return buscarPesquisaRapidaMovimentoEstoque(campoPesquisa);
		} else {
			return buscarPesquisaRapidaMovimentoEstoque(campoPesquisa, codigoFornecedor);
		}
	}
	
	@Transactional(readOnly = true)
	@Override
	public ProdutoDTO buscarApenasTipoUnidadeMedidaPorCodigoProduto(Long codigo) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(codigo, tipoUnidadeMedida) " 
				+ "from Produto where codigo = :codigo";
	
		return manager.createQuery(jpql, ProdutoDTO.class)
			.setParameter("codigo", codigo).getSingleResult();
	}
	
	@Transactional(readOnly = true)
	@Override
	public ProdutoDTO buscarProdutoPorCodigoLocalizacao(Long codigoLocalizacao) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(l.produto.codigo) " 
				+ "from Localizacao AS l where l.codigo = :codigoLocalizacao";
		
		return manager.createQuery(jpql, ProdutoDTO.class)
				.setParameter("codigoLocalizacao", codigoLocalizacao)
				.setMaxResults(1)
				.getSingleResult();
	}
	
	private List<ProdutoDTO> buscarPesquisaRapidaMovimentoEstoque(String campoPesquisa) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(p.codigo, p.descricao, p.tipoUnidadeMedida, p.localFixo, l) " 
				+ "from Produto AS p LEFT OUTER JOIN p.localizacoes l where p.possuiEstoque IS TRUE and lower(p.descricao) like lower(:campoPesquisa)";
		
		return manager.createQuery(jpql, ProdutoDTO.class)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%").getResultList();
	}
	
	private List<ProdutoDTO> buscarPesquisaRapidaMovimentoEstoque(String campoPesquisa, String string) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(p.codigo, p.descricao, p.tipoUnidadeMedida, p.localFixo, l) " 
				+ "from Produto AS p RIGHT OUTER JOIN p.localizacoes l where p.possuiEstoque IS TRUE and lower(p.descricao) like lower(:campoPesquisa)";
		
		return manager.createQuery(jpql, ProdutoDTO.class)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%").getResultList();
	}
	
	private List<ProdutoDTO> buscarPesquisaRapidaMovimentoEstoque(String campoPesquisa, Long codigoFornecedor) {
		String jpql = "select new br.com.yahtosistemas.dto.ProdutoDTO(p.codigo, p.descricao, p.tipoUnidadeMedida, p.localFixo, l) " 
				+ "from Fornecedor AS f INNER JOIN f.produtos p LEFT JOIN p.localizacoes l "
				+ "where f.codigo = :codigoFornecedor and p.possuiEstoque IS TRUE and lower(p.descricao) like lower(:campoPesquisa)";
		
		return manager.createQuery(jpql, ProdutoDTO.class)
				.setParameter("codigoFornecedor", codigoFornecedor)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%").getResultList();
	}
	
	private Long total(ProdutoFilter filtro) {
		@SuppressWarnings("deprecation")
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Produto.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(ProdutoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				criteria.add(Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE));
			}
			
			if (filtro.getVendedor()) {
				criteria.add(Restrictions.eq("vende", Boolean.TRUE));
			}
			
			if (filtro.getEstoqueMinimo()) {
				DetachedCriteria dcm = DetachedCriteria.forClass(MovimentoEstoque.class, "me");
				dcm.setProjection(Projections.max("codigo"));
				dcm.add(Property.forName("me.produto").eqProperty("me2.produto"));
				
				Criteria movimentacoesEstoqueCriteria = criteria.createCriteria("movimentacoesEstoque", "me2");
				movimentacoesEstoqueCriteria.add(Property.forName("codigo").eq(dcm));

				DetachedCriteria dcs = DetachedCriteria.forClass(SaldoEstoque.class, "se");
				dcs.setProjection(Projections.max("codigo"));
				dcs.add(Property.forName("se.movimentoEstoque").eqProperty("se2.movimentoEstoque"));
				
				Criteria saldosEstoqueCriteria = movimentacoesEstoqueCriteria.createCriteria("saldosEstoque", "se2");
				saldosEstoqueCriteria.add(Property.forName("codigo").eq(dcs));
				
				criteria.createAlias("localizacoes", "l", JoinType.LEFT_OUTER_JOIN);
				criteria.add(Restrictions.geProperty("l.estoqueMinimo", "se2.saldoEstoque"));
			}
		}
	}
}
