package br.com.yahtosistemas.repository.helper.itemPedidoVenda;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.dto.ItemPedidoVendaDTO;
import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.StatusItemCozinha;

public class ItensPedidosVendasImpl implements ItensPedidosVendasQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(readOnly = true)
	@Override
	public ItemPedidoVendaDTO buscarStatusCozinhaItemPedidoVenda(Long codigo) {
		String jpql = "select new br.com.yahtosistemas.dto.ItemPedidoVendaDTO(codigo, statusCozinha) " 
				+ "from ItemPedidoVenda where codigo = :codigo";
	
		return manager.createQuery(jpql, ItemPedidoVendaDTO.class)
				.setParameter("codigo", codigo)
				.getSingleResult();
	}

/*	@Transactional(readOnly = true)
	@Override
	public List<ItemPedidoVendaDTO> buscarStatusCozinhaItemPedidoVendaPorPedidoVenda(Long codigoPedidoVenda) {
		String jpql = "select new br.com.yahtosistemas.dto.ItemPedidoVendaDTO(i.codigo, i.statusCozinha) " 
				+ "from ItemPedidoVenda AS i where i.pedidoVenda.codigo = :codigo";
	
		return manager.createQuery(jpql, ItemPedidoVendaDTO.class)
				.setParameter("codigo", codigoPedidoVenda)
				.getResultList();
	}
*/
	@Transactional(readOnly = false)
	@Override
	public void salvarStatusItemCozinha(Long[] codigos, StatusItemCozinha statusCozinha) {
		CriteriaUpdate<ItemPedidoVenda> criteria = manager.getCriteriaBuilder().createCriteriaUpdate(ItemPedidoVenda.class);
		Root<ItemPedidoVenda> root = criteria.from(ItemPedidoVenda.class);
		criteria.set(root.get("statusCozinha"), statusCozinha);
		criteria.where(root.get("codigo").in(Arrays.asList(codigos)));
		manager.createQuery(criteria).executeUpdate();
	}

	@Transactional(readOnly = false)
	@Override
	public void salvarLoadCozinhaItem(List<Long> codigos, Boolean loadCozinha) {
		CriteriaUpdate<ItemPedidoVenda> criteria = manager.getCriteriaBuilder().createCriteriaUpdate(ItemPedidoVenda.class);
		Root<ItemPedidoVenda> root = criteria.from(ItemPedidoVenda.class);
		criteria.set(root.get("loadCozinha"), loadCozinha);
		criteria.where(root.get("codigo").in(codigos));
		manager.createQuery(criteria).executeUpdate();
	}


}
