package br.com.yahtosistemas.repository.helper.abreFechaCaixa;

import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.repository.filter.AbreFechaCaixaFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class AbreFechaCaixasImpl implements AbreFechaCaixasQueries {

	@PersistenceContext
	private EntityManager manager;

	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public Page<AbreFechaCaixa> filtrar(AbreFechaCaixaFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(AbreFechaCaixa.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	@SuppressWarnings("deprecation")
	private Long total(AbreFechaCaixaFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(AbreFechaCaixa.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	private void adicionarFiltro(AbreFechaCaixaFilter filtro, Criteria criteria) {
		if (filtro != null) {
			criteria.add(Restrictions.isNotNull("dataHoraFim"));
			if (filtro.getDataInicio() != null) {
				criteria.add(Restrictions.ge("dataHoraInicio", LocalDateTime.of(filtro.getDataInicio(), LocalTime.of(0, 0))));
			}
			if (filtro.getDataFim() != null) {
				criteria.add(Restrictions.le("dataHoraInicio", LocalDateTime.of(filtro.getDataInicio(), LocalTime.of(23, 59))));
			}
		}
	}
}
