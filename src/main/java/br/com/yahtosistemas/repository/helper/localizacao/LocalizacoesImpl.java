package br.com.yahtosistemas.repository.helper.localizacao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.LocalizacaoDTO;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.repository.filter.LocalizacaoFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class LocalizacoesImpl implements LocalizacoesQueries {

	@PersistenceContext
	private EntityManager manager;

	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public Page<Localizacao> filtrar(LocalizacaoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Localizacao.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<LocalizacaoDTO> buscarPorEnderecoOuDescricao(String campoPesquisa, Long codigoProduto, Long codigoLocalizacao) {
		if (codigoProduto == null) {
			return buscarLocalizacao(campoPesquisa);
		} else {
			return buscarLocalizacao(campoPesquisa, codigoProduto, codigoLocalizacao);
		}
	}
	
	@Transactional(readOnly = true)
	@Override
	public LocalizacaoDTO getLocalizacaoDTO(String endereco) {
		String jpql = "select new br.com.yahtosistemas.dto.LocalizacaoDTO(codigo, blocado, endereco) " 
				+ "from Localizacao where endereco = :endereco";
		
		try {
			return manager.createQuery(jpql, LocalizacaoDTO.class).setParameter("endereco", endereco).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	private List<LocalizacaoDTO> buscarLocalizacao(String campoPesquisa) {
		String jpql = "select new br.com.yahtosistemas.dto.LocalizacaoDTO(l.codigo, l.blocado, l.endereco, l.descricao, l.deposito) " 
				+ "from Localizacao AS l where l.produto IS NULL and ( l.endereco like :campoPesquisa or lower(l.descricao) like lower(:campoPesquisa) )";
		
		return manager.createQuery(jpql, LocalizacaoDTO.class)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%").getResultList();
	}
	
	private List<LocalizacaoDTO> buscarLocalizacao(String campoPesquisa, Long codigoProduto, Long codigoLocalizacao) {
		String jpql = "select new br.com.yahtosistemas.dto.LocalizacaoDTO(l.codigo, l.blocado, l.endereco, l.descricao, l.deposito) " 
				+ "from Localizacao AS l where l.codigo != :codigoLocalizacao and l.produto.codigo = :codigoProduto and"
				+ " ( l.endereco like :campoPesquisa or lower(l.descricao) like lower(:campoPesquisa) )";
		
		return manager.createQuery(jpql, LocalizacaoDTO.class)
				.setParameter("campoPesquisa", "%" + campoPesquisa + "%")
				.setParameter("codigoLocalizacao", codigoLocalizacao)
				.setParameter("codigoProduto", codigoProduto)
				.getResultList();
	}

	@SuppressWarnings("deprecation")
	private Long total(LocalizacaoFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Localizacao.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(LocalizacaoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				criteria.add(Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE));
			}
		}
	}
}
