package br.com.yahtosistemas.repository.helper.cliente;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.Estabelecimento;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class ClientesImpl implements ClientesQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<Cliente> filtrar(EstabelecimentoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cliente.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltroCliente(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, totalCliente(filtro));
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<EstabelecimentoDTO> porCnpjOuNomeEmpresarial(String campoPesquisa) {
		String jpql = "select new br.com.yahtosistemas.dto.EstabelecimentoDTO(codigo, cpfOuCnpj, nomeEmpresarial, tipoPessoa) " 
				+ "from Cliente where lower(cpfOuCnpj) like lower(:campoPesquisa) or lower(nomeEmpresarial) like lower(:campoPesquisa)";
	
		List<EstabelecimentoDTO> fornecedoresFiltrados = manager.createQuery(jpql, EstabelecimentoDTO.class)
			.setParameter("campoPesquisa", "%" + campoPesquisa + "%")
			.getResultList();
	return fornecedoresFiltrados;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Optional<Estabelecimento> seJaExisteMatrizCadastrada(String substring) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cliente.class);
		ProjectionList pl = Projections.projectionList (); 
		pl.add (Projections.property ("cpfOuCnpj"), "cpfOuCnpj"); 
		pl.add (Projections.property ("unidadeEmpresa"), "unidadeEmpresa"); 
		criteria.setProjection (pl);
		criteria.add(Restrictions.like("cpfOuCnpj", substring, MatchMode.START));
		criteria.add(Restrictions.eq("unidadeEmpresa", UnidadeEstabelecimento.MATRIZ));
		criteria.setResultTransformer(Transformers.aliasToBean(Cliente.class));
		return ( (List<Estabelecimento>) criteria.list() ).stream().findFirst();
	}
	
	@Transactional(readOnly = true)
	@Override
	public EstabelecimentoDTO seJaExisteCnpjCadastrado(String cpfOuCnpj) {
		String jpql = "select new br.com.yahtosistemas.dto.EstabelecimentoDTO(cpfOuCnpj) " 
				+ "from Cliente where cpfOuCnpj = :cpfOuCnpj";
	
		try {
			return manager.createQuery(jpql, EstabelecimentoDTO.class).setParameter("cpfOuCnpj", cpfOuCnpj).getSingleResult();			
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	private Long totalCliente(EstabelecimentoFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cliente.class);
		adicionarFiltroCliente(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltroCliente(EstabelecimentoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNomeEmpresarial())) {
				criteria.add(Restrictions.ilike("nomeEmpresarial", filtro.getNomeEmpresarial(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getCpfOuCnpj())) {
				criteria.add(Restrictions.ilike("cpfOuCnpj", filtro.getCpfOuCnpj(), MatchMode.ANYWHERE));
			}
		}
	}
}
