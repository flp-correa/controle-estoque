package br.com.yahtosistemas.repository.helper.produto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.dto.ProdutoDTO;
import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.filter.ProdutoFilter;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;

public interface ProdutosQueries {

	public Page<Produto> filtrar(ProdutoFilter filtro, Pageable pageable);
	
	public List<ProdutoDTO> buscarPorDescricaoEPossuiEstoque(String campoPesquisa);
	
	public List<ProdutoDTO> buscarPorDescricaoEDepositoOuNaoPossuiEstoque(String campoPesquisa, Deposito deposito);
	
	public List<ProdutoDTO> porDescricaoPesquisaRapidaCadastroMovtoEstoque(String descricao, Long codigoFornecedor, TipoMovimentoEstoque tipoMovimento);
	
	public ProdutoDTO buscarApenasTipoUnidadeMedidaPorCodigoProduto(Long codigo);
	
	public ProdutoDTO buscarProdutoPorCodigoLocalizacao(Long codigoLocalizacao);
}
