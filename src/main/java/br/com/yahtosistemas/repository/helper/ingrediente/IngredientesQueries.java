package br.com.yahtosistemas.repository.helper.ingrediente;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.repository.filter.IngredienteFilter;

public interface IngredientesQueries {

	public Page<Ingrediente> filtrar(IngredienteFilter filtro, Pageable pageable);
	
	public List<Ingrediente> listaIngredientesPorProduto(Long codigoProduto);
}
