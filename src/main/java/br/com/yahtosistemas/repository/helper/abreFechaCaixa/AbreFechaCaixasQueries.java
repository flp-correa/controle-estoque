package br.com.yahtosistemas.repository.helper.abreFechaCaixa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.repository.filter.AbreFechaCaixaFilter;

public interface AbreFechaCaixasQueries {

	public Page<AbreFechaCaixa> filtrar(AbreFechaCaixaFilter filtro, Pageable pageable);
}
