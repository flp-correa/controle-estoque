package br.com.yahtosistemas.repository.helper.movimentoEstoque;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.MovimentoEstoqueDTO;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.filter.MovimentoEstoqueFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class MovimentacoesEstoqueImpl implements MovimentacoesEstoqueQueries {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;

	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<MovimentoEstoque> filtrar(MovimentoEstoqueFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(MovimentoEstoque.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@Transactional(readOnly = true)
	@Override
	public MovimentoEstoqueDTO buscarUltimaMovimentacaoPorProduto(Produto produto) {
		String jpql = "select new br.com.yahtosistemas.dto.MovimentoEstoqueDTO(m.codigo, m.produto) " 
				+ "from MovimentoEstoque AS m where m.produto = :produto ORDER BY m.codigo DESC";
	
		try {
			MovimentoEstoqueDTO usuariosFiltrados = manager.createQuery(jpql, MovimentoEstoqueDTO.class)
					.setParameter("produto", produto)
					.getSingleResult();			
			return usuariosFiltrados;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@Override
	public BigDecimal valorUnitarioUltimoMovtoEntradaEstoqueProduto(Produto produto) {
		try {
			String jpql = "select (m.valorUnitario / u.quantidade) " 
					+ "from MovimentoEstoque AS m INNER JOIN m.unidadeMedida AS u where m.produto = :produto and m.tipoMovimento = 'ENTRADA' ORDER BY m.codigo DESC";
			
			Optional<BigDecimal> optional = Optional.ofNullable(manager.createQuery(jpql, BigDecimal.class)
					.setParameter("produto", produto)
					.setMaxResults(1).getSingleResult());
			
			return optional.orElse(BigDecimal.ZERO);
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}
	
	private Long total(MovimentoEstoqueFilter filtro) {
		@SuppressWarnings("deprecation")
		Criteria criteria = manager.unwrap(Session.class).createCriteria(MovimentoEstoque.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(MovimentoEstoqueFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			if (!StringUtils.isEmpty(filtro.getDataInicio())) {
				LocalDateTime dataInicio = LocalDateTime.of(filtro.getDataInicio(), LocalTime.of(0, 0));
				criteria.add(Restrictions.ge("dataHoraCadastro", dataInicio));
			} 
			if (!StringUtils.isEmpty(filtro.getDataFim())) {
				LocalDateTime dataFim = LocalDateTime.of(filtro.getDataFim(), LocalTime.of(23, 59));
				criteria.add(Restrictions.le("dataHoraCadastro", dataFim));
			}
			if (filtro.getProduto() != null && !StringUtils.isEmpty(filtro.getProduto().getDescricao())) {
				DetachedCriteria dc = DetachedCriteria.forClass(Produto.class);
				dc.add(Restrictions.ilike("descricao", filtro.getProduto().getDescricao(), MatchMode.ANYWHERE));
				dc.setProjection(Projections.property("codigo"));
				
				criteria.add(Restrictions.and(Subqueries.propertyIn("produto.codigo", dc)));
			}
		}
	}
}
