package br.com.yahtosistemas.repository.helper.empresa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.repository.helper.EstabelecimentosQueries;

public interface EmpresasQueries extends EstabelecimentosQueries {

	public Page<Empresa> filtrar(EstabelecimentoFilter filtro, Pageable pageable);
	
	public List<EstabelecimentoDTO> porCnpjOuNomeEmpresarialEPossuiEstoque(String nomeEmpresarial);
}
