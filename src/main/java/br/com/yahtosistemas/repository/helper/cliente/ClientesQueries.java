package br.com.yahtosistemas.repository.helper.cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.repository.helper.EstabelecimentosQueries;

public interface ClientesQueries extends EstabelecimentosQueries {

	public Page<Cliente> filtrar(EstabelecimentoFilter filtro, Pageable pageable);
	
}
