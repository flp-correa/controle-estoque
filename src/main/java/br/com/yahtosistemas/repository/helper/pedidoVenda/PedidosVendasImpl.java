package br.com.yahtosistemas.repository.helper.pedidoVenda;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.PedidoVendaDTO;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusItemCozinha;
import br.com.yahtosistemas.model.StatusPedidoVenda;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.filter.PedidoVendaFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class PedidosVendasImpl implements PedidosVendasQueries {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true)
	@Override
	public Page<PedidoVenda> filtrar(PedidoVendaFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PedidoVenda.class);
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	@SuppressWarnings("deprecation")
	@Transactional(readOnly = true)
	@Override
	public PedidoVenda buscarComItens(Long codigo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PedidoVenda.class);
		criteria.createAlias("itens", "i", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("codigo", codigo));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		PedidoVenda pedidoVenda = (PedidoVenda) criteria.uniqueResult();
		pedidoVenda.getItens().forEach((i) -> {
			Hibernate.initialize(i.getIngredientes());
			Hibernate.initialize(i.getAdicionais());
			Hibernate.initialize(i.getProduto().getIngredientes());
		});
		
		return pedidoVenda;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public List<PedidoVenda> buscarPedidosParaCozinha(Long codigoPedidoVenda) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PedidoVenda.class);
		criteria.createAlias("itens", "i", JoinType.LEFT_OUTER_JOIN);
		
		if (codigoPedidoVenda == null) {
			criteria.add(Restrictions.or(
					Restrictions.eq("i.statusCozinha", StatusItemCozinha.NAO_INICIADO), Restrictions.eq("i.statusCozinha", StatusItemCozinha.INICIADO)));
			criteria.add(Restrictions.not(Restrictions.eq("status", StatusPedidoVenda.CANCELADO)));
		} else if (codigoPedidoVenda == 0) {
			criteria.add(Restrictions.or(
					Restrictions.eq("i.statusCozinha", StatusItemCozinha.NAO_INICIADO), Restrictions.eq("i.statusCozinha", StatusItemCozinha.INICIADO)));
			criteria.add(Restrictions.not(Restrictions.eq("status", StatusPedidoVenda.CANCELADO)));
			criteria.add(Restrictions.eq("i.loadCozinha", Boolean.FALSE));
		} else {
			criteria.add(Restrictions.eq("codigo", codigoPedidoVenda));
			criteria.add(Restrictions.eq("i.loadCozinha", Boolean.FALSE));
		}
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<PedidoVenda> pedidosVendas = (List<PedidoVenda>) criteria.list();
		pedidosVendas.forEach(p -> p.getItens().forEach((i) -> {
			Hibernate.initialize(i.getIngredientes());
			Hibernate.initialize(i.getAdicionais());
			Hibernate.initialize(i.getProduto().getIngredientes());
		}));
		
		pedidosVendas.removeIf(p -> !p.getItens().stream().filter(i -> i.getProduto().getEnviarCozinha()).findFirst().isPresent());
		pedidosVendas.forEach(p -> p.getItens().removeIf(i -> !i.getProduto().getEnviarCozinha()));
		return pedidosVendas;
	}
	
	@Transactional(readOnly = false)
	@Override
	public void salvarStatusPedidoVenda(Long codigo, StatusPedidoVenda status) {
		CriteriaUpdate<PedidoVenda> criteria = manager.getCriteriaBuilder().createCriteriaUpdate(PedidoVenda.class);
		Root<PedidoVenda> root = criteria.from(PedidoVenda.class);
		criteria.set(root.get("status"), status);
		criteria.set(root.get("dataHoraFim"), LocalDateTime.now());
		criteria.where(manager.getCriteriaBuilder().equal(root.get("codigo"), codigo));
		manager.createQuery(criteria).executeUpdate();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<PedidoVendaDTO> buscarPedidosPorGarcom(Usuario usuario) {
		String jpql = "select new br.com.yahtosistemas.dto.PedidoVendaDTO(p.codigo, p.mesa.nrMesa, p.cliente.nomeEmpresarial, p.valorTotal, "
				+ "(select COUNT(*) > 0 FROM ItemPedidoVenda AS ipv where ipv.pedidoVenda = p and ipv.statusCozinha = 'PRONTO') )" 
				+ "from PedidoVenda AS p LEFT OUTER JOIN p.mesa LEFT OUTER JOIN p.cliente where p.usuario = :usuario and p.status = 'EM_ANDAMENTO'";
	
		return manager.createQuery(jpql, PedidoVendaDTO.class)
				.setParameter("usuario", usuario)
				.getResultList();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<PedidoVendaDTO> buscarTodosPedidosParaGarcom() {
		String jpql = "select new br.com.yahtosistemas.dto.PedidoVendaDTO(p.codigo, p.mesa.nrMesa, p.cliente.nomeEmpresarial, p.valorTotal, "
				+ "(select COUNT(*) > 0 FROM ItemPedidoVenda AS ipv where ipv.pedidoVenda = p and ipv.statusCozinha = 'PRONTO') )" 
				+ "from PedidoVenda AS p LEFT OUTER JOIN p.mesa LEFT OUTER JOIN p.cliente where p.status = 'EM_ANDAMENTO' ORDER BY p.mesa.nrMesa ASC";
	
		return manager.createQuery(jpql, PedidoVendaDTO.class)
				.getResultList();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<PedidoVendaDTO> buscarPainelPedidos() {
		String jpql = "select new br.com.yahtosistemas.dto.PedidoVendaDTO(p.codigo, p.mesa.nrMesa, "
				+ "p.cliente.nomeEmpresarial, p.valorTotal, p.dataHoraCriacao, p.observacao) " 
				+ "from PedidoVenda AS p LEFT OUTER JOIN p.mesa LEFT OUTER JOIN p.cliente where p.status = 'EM_ANDAMENTO' ORDER BY p.mesa.nrMesa ASC";
	
		return manager.createQuery(jpql, PedidoVendaDTO.class)
				.getResultList();
	}
	
	@Transactional(readOnly = true)
	@Override
	public PedidoVendaDTO buscarStatusPedidoVenda(Long codigo) {
		String jpql = "select new br.com.yahtosistemas.dto.PedidoVendaDTO(codigo, status) " 
				+ "from PedidoVenda where codigo = :codigo";
	
		return manager.createQuery(jpql, PedidoVendaDTO.class)
				.setParameter("codigo", codigo)
				.getSingleResult();
	}
	
	@Transactional(readOnly = true)
	@Override
	public Optional<StatusPedidoVenda> seExistePedidoVendaEmAndamento() {
		return manager
				.createQuery("select p.status from PedidoVenda AS p where p.status = 'EM_ANDAMENTO' ORDER BY p.codigo DESC", StatusPedidoVenda.class)
				.setMaxResults(1).getResultList().stream().findFirst();
	}
	
	@SuppressWarnings("deprecation")
	private Long total(PedidoVendaFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PedidoVenda.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(PedidoVendaFilter filtro, Criteria criteria) {
		criteria.createAlias("cliente", "c", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("mesa", "m", JoinType.LEFT_OUTER_JOIN);
		
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (filtro.getStatus() != null) {
				criteria.add(Restrictions.eq("status", filtro.getStatus()));
			}
			
			if (filtro.getDesde() != null) {
				LocalDateTime desde = LocalDateTime.of(filtro.getDesde(), LocalTime.of(0, 0));
				criteria.add(Restrictions.ge("dataHoraCriacao", desde));
			}
			
			if (filtro.getAte() != null) {
				LocalDateTime ate = LocalDateTime.of(filtro.getAte(), LocalTime.of(23, 59));
				criteria.add(Restrictions.le("dataHoraCriacao", ate));
			}
			
			if (filtro.getValorMinimo() != null) {
				criteria.add(Restrictions.ge("valorTotal", filtro.getValorMinimo()));
			}
			
			if (filtro.getValorMaximo() != null) {
				criteria.add(Restrictions.le("valorTotal", filtro.getValorMaximo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNomeCliente())) {
				criteria.add(Restrictions.ilike("c.nome", filtro.getNomeCliente(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getCpfOuCnpjCliente())) {
				criteria.add(Restrictions.eq("c.cpfOuCnpj", TipoPessoa.removerFormatacao(filtro.getCpfOuCnpjCliente())));
			}
			
			if (!StringUtils.isEmpty(filtro.getNrMesa())) {
				criteria.add(Restrictions.eq("m.nrMesa", filtro.getNrMesa()));
			}
		}
	}
}
