package br.com.yahtosistemas.repository.helper.pedidoVenda;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.dto.PedidoVendaDTO;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusPedidoVenda;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.filter.PedidoVendaFilter;

public interface PedidosVendasQueries {

public Page<PedidoVenda> filtrar(PedidoVendaFilter filtro, Pageable pageable);
	
	public PedidoVenda buscarComItens(Long codigo);
	
	public void salvarStatusPedidoVenda(Long codigo, StatusPedidoVenda status);
	
	public List<PedidoVendaDTO> buscarPedidosPorGarcom(Usuario usuario);
	
	public List<PedidoVendaDTO> buscarTodosPedidosParaGarcom();
	
	public List<PedidoVendaDTO> buscarPainelPedidos();
	
	public List<PedidoVenda> buscarPedidosParaCozinha(Long codigoPedidoVenda);
	
	public PedidoVendaDTO buscarStatusPedidoVenda(Long codigo);
	
	public Optional<StatusPedidoVenda> seExistePedidoVendaEmAndamento();
}
