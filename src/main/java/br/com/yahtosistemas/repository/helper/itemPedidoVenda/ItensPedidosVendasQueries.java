package br.com.yahtosistemas.repository.helper.itemPedidoVenda;

import java.util.List;

import br.com.yahtosistemas.dto.ItemPedidoVendaDTO;
import br.com.yahtosistemas.model.StatusItemCozinha;

public interface ItensPedidosVendasQueries {

	public ItemPedidoVendaDTO buscarStatusCozinhaItemPedidoVenda(Long codigo);
	
	//public List<ItemPedidoVendaDTO> buscarStatusCozinhaItemPedidoVendaPorPedidoVenda(Long codigoPedidoVenda);
	
	public void salvarStatusItemCozinha(Long[] codigo, StatusItemCozinha status);
	
	public void salvarLoadCozinhaItem(List<Long> codigo, Boolean loadCozinha);
}
