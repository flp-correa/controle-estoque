package br.com.yahtosistemas.repository.helper;

import java.util.List;
import java.util.Optional;

import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Estabelecimento;

public interface EstabelecimentosQueries {

	public List<EstabelecimentoDTO> porCnpjOuNomeEmpresarial(String nomeEmpresarial);
	
	public Optional<Estabelecimento> seJaExisteMatrizCadastrada(String substring);
	
	public EstabelecimentoDTO seJaExisteCnpjCadastrado(String cnpj);
}
