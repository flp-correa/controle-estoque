package br.com.yahtosistemas.repository.helper.venda;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.dto.FaturamentoPorMes;
import br.com.yahtosistemas.dto.VendaDTO;
import br.com.yahtosistemas.dto.VendaMes;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.repository.filter.VendaFilter;

public interface VendasQueries {

	public Page<Venda> filtrar(VendaFilter filtro, Pageable pageable);
	public Venda buscarComItens(Long codigo);
	public Long codigoVendaPorPedidoVenda(Long codigo);
	public Optional<StatusVenda> seExisteVendaEmAndamento();
	public VendaDTO buscarValoresFecharCaixa(LocalDateTime dataHoraInicio);
	
	public BigDecimal valorTotalNoAno();
	public BigDecimal valorTotalNoMes();
	public BigDecimal valorTicketMedioNoAno();
	
	public List<VendaMes> totalPorMes(String tenantId);
	public List<FaturamentoPorMes> totalFaturamentoPorMes(String tenantId);
}
