package br.com.yahtosistemas.repository.helper.localizacao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.dto.LocalizacaoDTO;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.repository.filter.LocalizacaoFilter;

public interface LocalizacoesQueries {

	public Page<Localizacao> filtrar(LocalizacaoFilter filtro, Pageable pageable);

	public List<LocalizacaoDTO> buscarPorEnderecoOuDescricao(String campoPesquisa, Long codigoProduto, Long codigoLocalizacao);
	
	public LocalizacaoDTO getLocalizacaoDTO(String endereco);
	
}
