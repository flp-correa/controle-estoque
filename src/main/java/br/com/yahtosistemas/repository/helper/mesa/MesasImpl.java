package br.com.yahtosistemas.repository.helper.mesa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.repository.filter.MesaFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class MesasImpl implements MesasQueries {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;

	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<Mesa> filtrar(MesaFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Mesa.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	@SuppressWarnings("deprecation")
	private Long total(MesaFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Mesa.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(MesaFilter filtro, Criteria criteria) {
		criteria.createAlias("deposito", "d");
		
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNrMesa())) {
				criteria.add(Restrictions.ilike("nrMesa", filtro.getNrMesa(), MatchMode.ANYWHERE));
			}
			
			if (filtro.getDeposito() != null) {
				if (!StringUtils.isEmpty(filtro.getDeposito().getCodigo())) {
					criteria.add(Restrictions.eq("d.codigo", filtro.getDeposito().getCodigo()));
				}
				if (!StringUtils.isEmpty(filtro.getDeposito().getDescricao())) {
					criteria.add(Restrictions.ilike("d.descricao", filtro.getDeposito().getDescricao(), MatchMode.ANYWHERE));
				}
			}
		}
	}
}
