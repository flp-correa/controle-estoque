package br.com.yahtosistemas.repository.helper.saldoEstoque;

import java.math.BigDecimal;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;

public class SaldosEstoqueImpl implements SaldosEstoqueQueries {
	
	@PersistenceContext
	private EntityManager manager;

	@Transactional(readOnly = true)
	@Override
	public BigDecimal saldoLocalizacaoEstoqueProtudo(Produto produto, Localizacao localizacao) {
		try {
			String jpql = "select s.saldoLocalizacao " 
					+ "from SaldoEstoque AS s LEFT JOIN s.movimentoEstoque m LEFT JOIN s.localizacao l "
					+ "where m.produto = :produto and l = :localizacao ORDER BY m.codigo DESC, s.codigo DESC";
			
			Optional<BigDecimal> optional = Optional.ofNullable(manager.createQuery(jpql, BigDecimal.class)
					.setParameter("produto", produto)
					.setParameter("localizacao", localizacao)
					.setMaxResults(1).getSingleResult());
			
			return optional.orElse(BigDecimal.ZERO);
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}

	@Transactional(readOnly = true)
	@Override
	public BigDecimal saldoTotalEstoqueProduto(Produto produto) {
		try {
			String jpql = "select s.saldoEstoque " 
					+ "from SaldoEstoque AS s LEFT JOIN s.movimentoEstoque m where m.produto = :produto ORDER BY m.codigo DESC, s.codigo DESC";
			
			Optional<BigDecimal> optional = Optional.ofNullable(manager.createQuery(jpql, BigDecimal.class)
					.setParameter("produto", produto)
					.setMaxResults(1).getSingleResult());
			
			return optional.orElse(BigDecimal.ZERO);
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
	}

}
