package br.com.yahtosistemas.repository.helper.movimentoEstoque;

import java.math.BigDecimal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.dto.MovimentoEstoqueDTO;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.filter.MovimentoEstoqueFilter;

public interface MovimentacoesEstoqueQueries {
	
	public Page<MovimentoEstoque> filtrar(MovimentoEstoqueFilter filtro, Pageable pageable);

	@Transactional(readOnly = true)
	public MovimentoEstoqueDTO buscarUltimaMovimentacaoPorProduto(Produto produto);
	
	public BigDecimal valorUnitarioUltimoMovtoEntradaEstoqueProduto(Produto produto);
}
