package br.com.yahtosistemas.repository.helper.fornecedor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.repository.helper.EstabelecimentosQueries;

public interface FornecedoresQueries extends EstabelecimentosQueries {

	@Transactional(readOnly = true)
	public Page<Fornecedor> filtrar(EstabelecimentoFilter filtro, Pageable pageable);
	
}
