package br.com.yahtosistemas.repository.helper.saldoEstoque;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;

public interface SaldosEstoqueQueries {

	public BigDecimal saldoLocalizacaoEstoqueProtudo(Produto produto, Localizacao localizacao);
	
	public BigDecimal saldoTotalEstoqueProduto(Produto produto);
}
