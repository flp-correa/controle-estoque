package br.com.yahtosistemas.repository.helper.deposito;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.DepositoDTO;
import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.repository.filter.DepositoFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class DepositosImpl implements DepositosQueries {

	@PersistenceContext
	private EntityManager manager;

	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Transactional(readOnly = true)
	@Override
	public Page<Deposito> filtrar(DepositoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Deposito.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}

	@Transactional(readOnly = true)
	@Override
	public List<DepositoDTO> porDescricaoPesquisaRapida(String descricao) {
		String jpql = "select new br.com.yahtosistemas.dto.DepositoDTO(d.codigo, d.descricao, d.empresa) " 
				+ "from Deposito AS d where lower(descricao) like lower(:descricao)";
		
		List<DepositoDTO> depositosFiltrados = manager.createQuery(jpql, DepositoDTO.class)
			.setParameter("descricao", "%" + descricao + "%")
			.getResultList();
		return depositosFiltrados;
	}
	
	@SuppressWarnings("deprecation")
	private Long total(DepositoFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Deposito.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(DepositoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				criteria.add(Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE));
			}
		}
	}
}
