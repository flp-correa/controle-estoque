package br.com.yahtosistemas.repository.helper.venda;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.FaturamentoPorMes;
import br.com.yahtosistemas.dto.VendaDTO;
import br.com.yahtosistemas.dto.VendaMes;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.repository.filter.VendaFilter;
import br.com.yahtosistemas.repository.paginacao.PaginacaoUtil;

public class VendasImpl implements VendasQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Transactional(readOnly = true)
	@Override
	public Page<Venda> filtrar(VendaFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Venda.class);
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@SuppressWarnings("deprecation")
	@Transactional(readOnly = true)
	@Override
	public Venda buscarComItens(Long codigo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Venda.class);
		criteria.createAlias("itens", "i", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("codigo", codigo));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (Venda) criteria.uniqueResult();
	}
	
	@Transactional(readOnly = true)
	@Override
	public Long codigoVendaPorPedidoVenda(Long codigoPedidoVenda) {
		String jpql = "select v.codigo " 
				+ "from Venda AS v where v.pedidoVenda.codigo = :codigoPedidoVenda";
	
		try {
			return manager.createQuery(jpql, Long.class).setParameter("codigoPedidoVenda", codigoPedidoVenda).getSingleResult();			
		} catch (NoResultException e) {
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	@Override
	public Optional<StatusVenda> seExisteVendaEmAndamento() {
		return manager
				.createQuery("select v.status from Venda AS v where v.status = 'EM_ANDAMENTO' ORDER BY v.codigo DESC", StatusVenda.class)
				.setMaxResults(1).getResultList().stream().findFirst();
	}
	
	@Transactional(readOnly = true)
	@Override
	public VendaDTO buscarValoresFecharCaixa(LocalDateTime dataHoraInicio) {
		String jpql = "select new br.com.yahtosistemas.dto.VendaDTO(sum(v.valorDinheiro), sum(v.valorCartao), "
				+ "sum(v.valorDinheiro) + sum(v.valorCartao) )" 
				+ "from Venda AS v where v.dataHoraCriacao > :dataHoraInicio and v.status = 'FINALIZADA'";
	
		return manager.createQuery(jpql, VendaDTO.class)
				.setParameter("dataHoraInicio", dataHoraInicio)
				.getSingleResult();
	}
	
	@Transactional(readOnly = true)
	@Override
	public BigDecimal valorTotalNoAno() {
		Optional<BigDecimal> optional = Optional.ofNullable(
				manager.createQuery("select sum(valorTotal) from Venda where year(dataHoraCriacao) = :ano and status = :status", BigDecimal.class)
					.setParameter("ano", Year.now().getValue())
					.setParameter("status", StatusVenda.FINALIZADA)
					.getSingleResult());
		return optional.orElse(BigDecimal.ZERO);
	}
	
	@Transactional(readOnly = true)
	@Override
	public BigDecimal valorTotalNoMes() {
		Optional<BigDecimal> optional = Optional.ofNullable(
				manager.createQuery("select sum(valorTotal) from Venda where year(dataHoraCriacao) = :ano and month(dataHoraCriacao) = :mes and status = :status", BigDecimal.class)
					.setParameter("ano", Year.now().getValue())
					.setParameter("mes", MonthDay.now().getMonthValue())
					.setParameter("status", StatusVenda.FINALIZADA)
					.getSingleResult());
		return optional.orElse(BigDecimal.ZERO);
	}
	
	@Transactional(readOnly = true)
	@Override
	public BigDecimal valorTicketMedioNoAno() {
		Optional<BigDecimal> optional = Optional.ofNullable(
				manager.createQuery("select sum(valorTotal)/count(*) from Venda where year(dataHoraCriacao) = :ano and status = :status", BigDecimal.class)
					.setParameter("ano", Year.now().getValue())
					.setParameter("status", StatusVenda.FINALIZADA)
					.getSingleResult());
		return optional.orElse(BigDecimal.ZERO);
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	@Override
	public List<VendaMes> totalPorMes(String tenantId) {
		List<VendaMes> vendasMes = manager.createNamedQuery("Vendas.totalPorMes")
				.setParameter(1, tenantId)
				.getResultList();
		
		LocalDate hoje = LocalDate.now();
		for (int i = 1; i <= 6; i++) {
			String mesIdeal = String.format("%d/%02d", hoje.getYear(), hoje.getMonthValue());
			
			boolean possuiMes = vendasMes.stream().filter(v -> v.getMes().equals(mesIdeal)).findAny().isPresent();
			if (!possuiMes) {
				vendasMes.add(i - 1, new VendaMes(mesIdeal, 0));
			}
			
			hoje = hoje.minusMonths(1);
		}
		
		return vendasMes;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<FaturamentoPorMes> totalFaturamentoPorMes(String tenantId) {
		List<FaturamentoPorMes> faturamentosMes =  manager.createNamedQuery("Vendas.totalFaturamentoPorMes")
				.setParameter(1, tenantId)
				.getResultList();
		
		LocalDate hoje = LocalDate.now();
		for (int i = 1; i <= 6; i++) {
			String mesIdeal = String.format("%d/%02d", hoje.getYear(), hoje.getMonthValue());
			
			boolean possuiMes = faturamentosMes.stream().filter(v -> v.getMes().equals(mesIdeal)).findAny().isPresent();
			if (!possuiMes) {
				faturamentosMes.add(i - 1, new FaturamentoPorMes(mesIdeal, BigDecimal.ZERO));
			}
			
			hoje = hoje.minusMonths(1);
		}
		
		return faturamentosMes;
	}
	
	@SuppressWarnings("deprecation")
	private Long total(VendaFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Venda.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(VendaFilter filtro, Criteria criteria) {
		criteria.createAlias("cliente", "c", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("mesa", "m", JoinType.LEFT_OUTER_JOIN);
		
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCodigo())) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			}
			
			if (filtro.getStatus() != null) {
				criteria.add(Restrictions.eq("status", filtro.getStatus()));
			}
			
			if (filtro.getDesde() != null) {
				LocalDateTime desde = LocalDateTime.of(filtro.getDesde(), LocalTime.of(0, 0));
				criteria.add(Restrictions.ge("dataHoraCriacao", desde));
			}
			
			if (filtro.getAte() != null) {
				LocalDateTime ate = LocalDateTime.of(filtro.getAte(), LocalTime.of(23, 59));
				criteria.add(Restrictions.le("dataHoraCriacao", ate));
			}
			
			if (filtro.getValorMinimo() != null) {
				criteria.add(Restrictions.ge("valorTotal", filtro.getValorMinimo()));
			}
			
			if (filtro.getValorMaximo() != null) {
				criteria.add(Restrictions.le("valorTotal", filtro.getValorMaximo()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNomeCliente())) {
				criteria.add(Restrictions.ilike("c.nome", filtro.getNomeCliente(), MatchMode.ANYWHERE));
			}
			
			if (!StringUtils.isEmpty(filtro.getCpfOuCnpjCliente())) {
				criteria.add(Restrictions.eq("c.cpfOuCnpj", TipoPessoa.removerFormatacao(filtro.getCpfOuCnpjCliente())));
			}
			
			if (!StringUtils.isEmpty(filtro.getNrMesa())) {
				criteria.add(Restrictions.eq("m.nrMesa", filtro.getNrMesa()));
			}
		}
	}
}
