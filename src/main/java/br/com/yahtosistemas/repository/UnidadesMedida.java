package br.com.yahtosistemas.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.repository.helper.unidadeMedida.UnidadesMedidasQueries;
import br.com.yahtosistemas.service.unidadeMedida.FamiliaUnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

public interface UnidadesMedida extends JpaRepository<UnidadeMedida, Long>, UnidadesMedidasQueries {

	public List<UnidadeMedida> findByCodigoIn(Long[] codigos);
	
	public Optional<UnidadeMedida> findByTipoUnidadeMedidaAndQuantidade(TipoUnidadeMedida tipoUnidadeMedida, BigDecimal quantidade);
	
	public List<UnidadeMedida> findByTenantIdAndFamilia(String tenantId, FamiliaUnidadeMedida familiaUnidadeMedida);
	
	public UnidadeMedida findByTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida);
}
