package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.repository.helper.movimentoEstoque.MovimentacoesEstoqueQueries;

@Repository
public interface MovimentacoesEstoque extends JpaRepository<MovimentoEstoque, Long>, MovimentacoesEstoqueQueries {

}
