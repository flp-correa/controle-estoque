package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.repository.helper.deposito.DepositosQueries;

@Repository
public interface Depositos extends JpaRepository<Deposito, Long>, DepositosQueries {

}
