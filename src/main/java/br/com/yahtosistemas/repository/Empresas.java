package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.repository.helper.empresa.EmpresasQueries;

@Repository
public interface Empresas extends JpaRepository<Empresa, Long>, EmpresasQueries {

}
