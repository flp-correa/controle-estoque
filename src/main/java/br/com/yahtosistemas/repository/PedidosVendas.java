package br.com.yahtosistemas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.repository.helper.pedidoVenda.PedidosVendasQueries;

public interface PedidosVendas extends JpaRepository<PedidoVenda, Long>, PedidosVendasQueries {

}
