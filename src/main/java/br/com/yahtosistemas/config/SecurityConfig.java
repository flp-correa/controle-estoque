package br.com.yahtosistemas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.yahtosistemas.security.AppUserDetailsService;

@EnableWebSecurity
@ComponentScan(basePackageClasses = AppUserDetailsService.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers("/layout/**")
			.antMatchers("/images/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
			.antMatchers("/**/usuarios/novo").hasRole("ADMINISTRADOR")
			.antMatchers("/**/usuarios*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/usuarios/*").hasRole("PESQUISAR_USUARIO")
			
			.antMatchers("/**/empresas*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/empresas/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/fornecedores*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/fornecedores/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/unidades-medida*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/unidades-medida/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/depositos*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/depositos/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/localizacoes*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/localizacoes/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/movimentacoes-estoque*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/movimentacoes-estoque/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/pedidos-vendas/painel").hasRole("PESQUISAR_PEDIDO_VENDA")
			.antMatchers("/**/pedidos-vendas/garcons").hasRole("GARCOM")
			.antMatchers("/**/pedidos-vendas/cozinha").hasRole("CHAPEIRO_COZINHA")
			.antMatchers("/**/pedidos-vendas/ingredientes").hasAnyRole("ADMINISTRADOR", "CADASTRAR_PEDIDO_VENDA")
			.antMatchers("/**/pedidos-vendas/adicionais").hasAnyRole("ADMINISTRADOR", "CADASTRAR_PEDIDO_VENDA")
			.antMatchers("/**/pedidos-vendas/novo").hasRole("CADASTRAR_PEDIDO_VENDA")
			.antMatchers("/**/pedidos-vendas*").hasRole("PESQUISAR_PEDIDO_VENDA")
			
			.antMatchers("/**/ingredientes*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/ingredientes/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/adicionais*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/adicionais/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/adicionais*").hasRole("ADMINISTRADOR")
			.antMatchers("/**/adicionais/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/clientes/novo").hasRole("CADASTRAR_CLIENTE")
			.antMatchers("/**/clientes*").hasRole("PESQUISAR_CLIENTE")
			
			.antMatchers("/**/mesas/trocar-de-mesa").hasRole("PESQUISAR_MESA")
			.antMatchers("/**/mesas/*").hasRole("CADASTRAR_MESA")
			.antMatchers("/**/mesas*").hasRole("CADASTRAR_MESA")
			
			.antMatchers("/**/produtos/novo").hasRole("CADASTRAR_PRODUTO")
			.antMatchers("/**/produtos*").hasRole("PESQUISAR_PRODUTO")
			
			.antMatchers("/**/relatorios/*").hasRole("ADMINISTRADOR")
			
			.antMatchers("/**/vendas/nova").hasRole("CADASTRAR_VENDA")
			.antMatchers("/**/vendas/*").hasRole("PESQUISAR_VENDA")
			.antMatchers("/**/vendas*").hasRole("PESQUISAR_VENDA")
			
			.antMatchers("/**/abre-fecha-caixas/novo").hasRole("OPERADOR_CAIXA")
			.antMatchers("/**/abre-fecha-caixas/*").hasAnyRole("OPERADOR_CAIXA", "ADMINISTRADOR")
			.antMatchers("/**/abre-fecha-caixas*").hasRole("ADMINISTRADOR")
			
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage("/login")
			.permitAll()
			.and()
		.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.and()
		.rememberMe()
			.key("yahtoEstoque")
			.tokenValiditySeconds(2419200) // 1 semana 604800, esta configurado para 4 semanas
			.userDetailsService(userDetailsService)
			.and()
		.sessionManagement()
			.invalidSessionUrl("/login");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
