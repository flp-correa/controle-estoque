package br.com.yahtosistemas.config;

import java.net.URISyntaxException;

import javax.cache.Caching;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.VersionResourceResolver;

import br.com.yahtosistemas.tenancy.TenancyInterceptor;

/**
 * 
 * @author Yahto Sistemas - Felipe Corrêa
 * 
 */
@Configuration
@EnableCaching
@EnableAsync
public class WebConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new TenancyInterceptor());
	}
	
	@Bean
	public CacheManager cacheManager() throws URISyntaxException {
		return new JCacheCacheManager(Caching.getCachingProvider()
				.getCacheManager(getClass().getResource("/env/ehcache.xml").toURI(), getClass().getClassLoader()));
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		VersionResourceResolver versionResolver = new VersionResourceResolver()
                .addContentVersionStrategy("/stylesheets/**", "/javascripts/**");
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:static/")
                .setCachePeriod(3600)
                //.setCachePeriod(0)
                .resourceChain(true)
                .addResolver(versionResolver);
	}
}
