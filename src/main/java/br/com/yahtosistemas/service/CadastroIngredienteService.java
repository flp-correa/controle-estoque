package br.com.yahtosistemas.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.repository.Ingredientes;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroIngredienteService {

	@Autowired
	private Ingredientes ingredientes;
	
	@Transactional
	public Ingrediente salvar(Ingrediente ingrediente) {
		Optional<Ingrediente> ingredienteOptional = ingredientes.findByNomeIgnoreCase(ingrediente.getNome());
		if (ingredienteOptional.isPresent()) {
			throw new EntidadeJaCadastradaException("Nome do ingrediente já cadastrado");
		}
		
		ingrediente.setTenantId(TenancyInterceptor.getTenantId());
		return ingredientes.saveAndFlush(ingrediente);
	}

	@Transactional
	public void excluir(Ingrediente ingrediente) {
		try {
			ingredientes.delete(ingrediente);
			ingredientes.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar ingrediente. Esta sendo usado por algum lanche ou prato.");
		}
		
	}
}
