package br.com.yahtosistemas.service;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.repository.Clientes;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroClienteService {

	@Transactional
	public void salvar(Cliente cliente, Clientes clientes) {
		
		if (cliente.getTipoPessoa().equals(TipoPessoa.JURIDICA)) {			
			cliente.getUnidadeEmpresa().validarUnidadeEstabelecimento(clientes, cliente);
		} else {
			cliente.setUnidadeEmpresa(null);
		}
		
		cliente.setTenantId(TenancyInterceptor.getTenantId());
		clientes.save(cliente);
	}

	@Transactional
	public void excluir(Cliente cliente, Clientes clientes) {
		try {
			clientes.delete(cliente);
			clientes.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar o cliente.");
		}		
	}
	
}
