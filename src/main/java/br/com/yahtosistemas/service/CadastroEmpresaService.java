package br.com.yahtosistemas.service;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.repository.Empresas;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroEmpresaService {

	@Transactional
	public void salvar(final Empresa empresa, final Empresas empresas) {
		empresa.getUnidadeEmpresa().validarUnidadeEstabelecimento(empresas, empresa);
		empresa.setTenantId(TenancyInterceptor.getTenantId());
		empresas.save(empresa);
	}
	
	@Transactional
	public void excluir(final Empresa empresa, final Empresas empresas) {
		try {
			empresas.delete(empresa);
			empresas.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar a empresa.");
		}		
	}
}
