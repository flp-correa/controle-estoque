package br.com.yahtosistemas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.Usuarios;
import br.com.yahtosistemas.service.exception.EmailUsuarioJaCadastradoException;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.SenhaObrigatoriaUsuarioException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroUsuarioService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public void salvar(Usuario usuario, final Usuarios usuarios, List<Usuario> usuarioExistente) {
		if (usuario.isNovo() && StringUtils.isEmpty(usuario.getSenha())) {
			throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória.");
		}
		
		validarUsuarioGrupo(usuario);
		
		if (usuario.isNovo() || !StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(this.passwordEncoder.encode(usuario.getSenha()));
		} else if (StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(usuarioExistente.get(0).getSenha());
		}
		usuario.setConfirmacaoSenha(usuario.getSenha());
		
		usuario.setTenantId(TenancyInterceptor.getTenantId());
		
		usuarios.save(usuario);
	}

	@Transactional
	public void excluir(Usuario usuario, final Usuarios usuarios) {
		try {
			usuarios.delete(usuario);
			usuarios.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar usuário.");
		}		
	}

	public List<Usuario> validarEmailParaTodoOSistema(Usuario usuario, final Usuarios usuarios) {
		List<Usuario> usuarioExistente = usuarios.findByEmailOrCodigo(usuario.getEmail(), usuario.getCodigo());

		if (!usuarioExistente.isEmpty() && (usuarioExistente.size() > 1 || !usuarioExistente.get(0).equals(usuario)) ) {
			throw new EmailUsuarioJaCadastradoException("E-mail já cadastrado");
		}
		return usuarioExistente;
	}
	
	private void validarUsuarioGrupo(Usuario usuario) {
		if (usuario.getGrupos().get(0).getCodigo() != 1 && usuario.getGrupos().get(0).getCodigo() != 4 && usuario.getDeposito().getCodigo() == null) {
			throw new EntidadeObrigatoriaException("Depósito deve ser informado");
		} else if (usuario.getGrupos().get(0).getCodigo() == 1 || usuario.getGrupos().get(0).getCodigo() == 4) {
			usuario.setDeposito(null);
		}
	}
}
