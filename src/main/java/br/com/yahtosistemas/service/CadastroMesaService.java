package br.com.yahtosistemas.service;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.repository.Mesas;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroMesaService {

	@Transactional
	public void salvar(@Valid Mesa mesa, Mesas mesas) {
		
		if (StringUtils.isEmpty(mesa.getDeposito().getCodigo())) {
			throw new EntidadeObrigatoriaException("Depósito deve ser informado");
		}
		if (mesas.findByNrMesaAndDeposito(mesa.getNrMesa(), mesa.getDeposito()).isPresent()) {
			throw new EntidadeJaCadastradaException("Mesa já cadastrada");
		}
		
		
		mesa.setTenantId(TenancyInterceptor.getTenantId());
		mesas.save(mesa);
		
	}

}
