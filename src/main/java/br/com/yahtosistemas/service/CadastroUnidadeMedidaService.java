package br.com.yahtosistemas.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.repository.UnidadesMedida;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroUnidadeMedidaService {

	@Transactional
	public void salvar(UnidadeMedida unidadeMedida, final UnidadesMedida unidadesMedida) {
		
		unidadeMedida.getTipoUnidadeMedida().obterUnidadeMedida().validarQuantidade(unidadeMedida);
		if (unidadesMedida.findByTipoUnidadeMedidaAndQuantidade(unidadeMedida.getTipoUnidadeMedida(), unidadeMedida.getQuantidade()).isPresent()) {
			throw new EntidadeJaCadastradaException("Unidade de medida já cadastrada");
		}
		
		unidadeMedida.setFamilia(unidadeMedida.getTipoUnidadeMedida().obterUnidadeMedida().getFamiliaUnidadeMedida());
		unidadeMedida.setTenantId(TenancyInterceptor.getTenantId());
		unidadesMedida.save(unidadeMedida);
	}
	
	@Transactional
	public void alterarSituacao(Long[] codigos, SituacaoUnidadeMedida situacaoUnidadeMedida, final UnidadesMedida unidadesMedida) {
		situacaoUnidadeMedida.executar(codigos, unidadesMedida);
	}
}
