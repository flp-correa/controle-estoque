package br.com.yahtosistemas.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroLocalizacaoService {
	
	@Autowired
	private SaldosEstoque saldosEstoque;

	@Transactional
	public void salvar(final Localizacao localizacao, final Localizacoes localizacoes) {
		validaQuantidadeEstoque(localizacao);
		if (localizacao.getProduto().isNovo()) {
			localizacao.setProduto(null);
		}
		
		definirValorKgParaGrama(localizacao);
		localizacao.setTenantId(TenancyInterceptor.getTenantId());
		localizacoes.save(localizacao);
	}

	@Transactional
	public void excluir(final Localizacao localizacao, final Localizacoes localizacoes) {
		try {
			localizacoes.delete(localizacao);
			localizacoes.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar a localização");
		}		
	}

	@Transactional(readOnly = true)
	public void validarExcluirLocalizacaoDoProduto(Produto produto) {
		BigDecimal saldoLocalizacao = saldosEstoque.saldoLocalizacaoEstoqueProtudo(produto, produto.getLocalizacao());
		
		if (!saldoLocalizacao.equals(BigDecimal.ZERO)) {
			throw new ImpossivelExcluirEntidadeException("Impossível excluir, localização possui saldo no estoque!");
		}
	}
	
	private void definirValorKgParaGrama(final Localizacao localizacao) {
		if (localizacao.getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {			
			localizacao.setEstoqueMinimo(localizacao.getEstoqueMinimo() * 1000);
			localizacao.setEstoqueMaximo(localizacao.getEstoqueMaximo() * 1000);
		}
	}
	
	private void validaQuantidadeEstoque(final Localizacao localizacao) {
		if (localizacao.getEstoqueMaximo().compareTo(localizacao.getEstoqueMinimo()) < 1) {
			throw new QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException("Estoque mínimo deve ser menor do que estoque máximo");
		}
	}
}
