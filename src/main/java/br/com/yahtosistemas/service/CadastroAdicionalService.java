package br.com.yahtosistemas.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Adicional;
import br.com.yahtosistemas.repository.Adicionais;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroAdicionalService {

	@Autowired
	private Adicionais adicionais;
	
	@Transactional
	public Adicional salvar(Adicional adicional) {
		if (adicional.isNovo()) {			
			Optional<Adicional> adicionalOptional = adicionais.findByNomeIgnoreCase(adicional.getNome());
			if (adicionalOptional.isPresent()) {
				throw new EntidadeJaCadastradaException("Nome do adicional já cadastrado");
			}
		}
		
		adicional.setTenantId(TenancyInterceptor.getTenantId());
		return adicionais.saveAndFlush(adicional);
	}

	@Transactional
	public void excluir(Adicional adicional) {
		try {
			adicionais.delete(adicional);
			adicionais.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar adicional.");
		}
		
	}
}
