package br.com.yahtosistemas.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.ItemVendaPedidoVenda;
import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.TipoPagamento;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.model.builder.ItemVendaBuilder;
import br.com.yahtosistemas.model.builder.VendaBuilder;
import br.com.yahtosistemas.repository.Vendas;
import br.com.yahtosistemas.service.event.movimentoEstoque.MovimentoEstoqueVendaEvent;
import br.com.yahtosistemas.service.exception.DataEntregaVendaObrigatorioException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorTotalItemVendaNaoPodeSerNegativoException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroVendaService {

	@Autowired
	private Vendas vendas;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Transactional
	public Venda salvar(Venda venda, Vendas vendas, StatusVenda status) {
		if (venda.isSalvarProibido()) {
			throw new RuntimeException("Usuário tentando salvar uma venda proibida");
		}
		
		validarSalvar(venda, status);
		
		if (venda.isNovo()) {
			venda.setDataHoraCriacao(LocalDateTime.now());
		} else {
			Venda vendaExistente = vendas.getOne(venda.getCodigo());
			venda.setDataHoraCriacao(vendaExistente.getDataHoraCriacao());
		}
		
		if (venda.getDataEntrega() != null) {
			venda.setDataHoraEntrega(LocalDateTime.of(venda.getDataEntrega()
					, venda.getHorarioEntrega() != null ? venda.getHorarioEntrega() : LocalTime.NOON));
		}
		
		if (StringUtils.isEmpty(venda.getCliente().getCodigo())) {
			venda.setCliente(null);
		} else {
			venda.setMesa(null);
		}
		
		
		venda.setStatus(status);
		venda.setTenantId(TenancyInterceptor.getTenantId());
		venda.getItens().forEach(i -> i.setTenantId(venda.getTenantId()));
		
		vendas.saveAndFlush(venda);
		if (status.equals(StatusVenda.FINALIZADA) || status.equals(StatusVenda.PENDENTE)) {	
			publisher.publishEvent(new MovimentoEstoqueVendaEvent(venda, venda.getUsuario()));
			//venda.getProdutosSemSaldoEstoque().forEach(p -> System.out.println(p.getDescricao()));
		}
		
		return venda;
	}
	
	@PreAuthorize("hasRole('ADMINISTRADOR')")
	@Transactional
	public void cancelar(Venda venda, Vendas vendas) {
		Venda vendaExistente = vendas.getOne(venda.getCodigo());
		StatusVenda statusVendaExistente = vendaExistente.getStatus();
		
		vendaExistente.setStatus(StatusVenda.CANCELADA);
		vendas.save(vendaExistente);
		if (statusVendaExistente.equals(StatusVenda.FINALIZADA)) {			
			publisher.publishEvent(new MovimentoEstoqueVendaEvent(vendaExistente, venda.getUsuario()));
		}
	}
	
	@Transactional
	public void finalizarPendentes(Long[] codigos, TipoPagamento tipoPagamento) {
		tipoPagamento.finalizarPendentes(codigos, vendas);
	}
	
	public Long criarESalvarVendaDoPedidoVenda(Usuario usuario, PedidoVenda pedidoVenda) {
		Venda venda = criarVendaBuilder(usuario, pedidoVenda);
		venda.adicionarItens(criarItensVendaBuilder(pedidoVenda.getItens()));
		
		return salvar(venda, vendas, StatusVenda.EM_ANDAMENTO).getCodigo();
	}
	
	private List<ItemVendaPedidoVenda> criarItensVendaBuilder(List<ItemPedidoVenda> itens) {
		return itens.stream().map(temp -> {
	            return new ItemVendaBuilder()
	            		.comProduto(temp.getProduto())
	            		.comQuantidade(temp.getQuantidade())
	            		.comValorUnitario(temp.getProduto().getEnviarCozinha() ? temp.getValorTotal() : temp.getValorUnitario())
	            		.comCodigo(null).construir();
        }).collect(Collectors.toList());
	}
	
	private Venda criarVendaBuilder(Usuario usuario, PedidoVenda pedidoVenda) {
		return new VendaBuilder()
				.comCliente(pedidoVenda.getCliente() != null ? pedidoVenda.getCliente() : new Cliente())
				.comMesa(pedidoVenda.getMesa() != null ? pedidoVenda.getMesa() : new Mesa())
				.comDataHoraEntrega(pedidoVenda.getDataHoraEntrega())
				.comObservacao(pedidoVenda.getObservacao())
				.comPedidoVenda(pedidoVenda)
				.comStatus(StatusVenda.EM_ANDAMENTO)
				.comUsuario(usuario)
				.comValorDinheiro(BigDecimal.ZERO)
				.comValorCartao(BigDecimal.ZERO)
				.comValorFrete(pedidoVenda.getValorFrete())
				.comValorTotal(pedidoVenda.getValorTotal())
				.comCodigo(null).construir();
	}

	private void validarSalvar(Venda venda, StatusVenda status) {
		validarValores(venda, status);
		validarSeInformouApenasHorarioEntrega(venda);
		validarSeInformouItens(venda);
		validarValorTotalNegativo(venda);
		if (status.equals(StatusVenda.FINALIZADA)) {
			venda.getTipoPagamento().validarVenda(venda);
		}
	}

	private void validarValores(Venda venda, StatusVenda status) {
		if (status.equals(StatusVenda.PENDENTE)) {
			venda.setValorCartao(BigDecimal.ZERO);
			venda.setValorDinheiro(BigDecimal.ZERO);
			venda.setValorAcrescimo(BigDecimal.ZERO);
			venda.setValorDesconto(BigDecimal.ZERO);
		} else {			
			if (venda.getValorCartao() == null) {
				venda.setValorCartao(BigDecimal.ZERO);
			}
			if (venda.getValorDinheiro() == null) {
				venda.setValorDinheiro(BigDecimal.ZERO);
			}
			if (venda.getValorAcrescimo() == null) {
				venda.setValorAcrescimo(BigDecimal.ZERO);
			}
			if (venda.getValorDesconto() == null) {
				venda.setValorDesconto(BigDecimal.ZERO);
			}
		}
	}
	
	private void validarValorTotalNegativo(Venda venda) {
		if (venda.getValorTotal().compareTo(BigDecimal.ZERO) < 0) {
			throw new ValorTotalItemVendaNaoPodeSerNegativoException("Valor total não pode ser negativo");
		}
	}
	
	private void validarSeInformouItens(Venda venda) {
		if (venda.getItens().isEmpty()) {
			throw new ProdutoObrigatorioException("Adicione pelo menos um produto na venda");
		}
	}

	private void validarSeInformouApenasHorarioEntrega(Venda venda) {
		if (venda.getHorarioEntrega() != null && venda.getDataEntrega() == null) {
			throw new DataEntregaVendaObrigatorioException("Informe uma data da entrega para um horário");
		}
	}
}
