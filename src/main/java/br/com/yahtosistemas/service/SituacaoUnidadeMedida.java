package br.com.yahtosistemas.service;

import br.com.yahtosistemas.repository.UnidadesMedida;

public enum SituacaoUnidadeMedida {

	ATIVAR {
		@Override
		public void executar(Long[] codigos, UnidadesMedida unidadesMedida) {
			unidadesMedida.findByCodigoIn(codigos).forEach(u -> u.setSituacao(true));
		}
	},
	
	DESATIVAR {
		@Override
		public void executar(Long[] codigos, UnidadesMedida unidadesMedida) {
			unidadesMedida.findByCodigoIn(codigos).forEach(u -> u.setSituacao(false));
		}
	};
	
	public abstract void executar(Long[] codigos, UnidadesMedida unidadesMedida);
}
