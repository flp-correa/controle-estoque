package br.com.yahtosistemas.service.event.movimentoEstoque;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.com.yahtosistemas.model.DestinoSaidaEstoque;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.builder.MovimentoEstoqueBuilder;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.repository.UnidadesMedida;
import br.com.yahtosistemas.service.CadastroMovimentoEstoqueService;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;

@Component
public class MovimentoEstoqueVendaListener {
	
	@Autowired
	private UnidadesMedida unidadesMedidas;
	
	@Autowired
	private MovimentacoesEstoque movimentacoesEstoque;
	
	@Autowired
	private SaldosEstoque saldosEstoque;
	
	@Autowired
	private Localizacoes localizacoes;
	
	@Autowired 
	private CadastroMovimentoEstoqueService cadastroMovimentoEstoqueService;

	@EventListener
	public void vendaSaidaEstoque(MovimentoEstoqueVendaEvent event) {
		List<MovimentoEstoque> movimentosEstoque = criarMovimentosEstoque(event);
		
		movimentosEstoque.forEach((me)-> {
			if (me.getProduto().getPossuiEstoque()) {
				BigDecimal saldoLocalizacao = saldosEstoque.saldoLocalizacaoEstoqueProtudo(me.getProduto(), me.getProduto().getLocalizacao());
				if (saldoLocalizacao.compareTo(BigDecimal.ZERO) == 0) {
					event.getVenda().getProdutosSemSaldoEstoque().add(me.getProduto());
				} else {
					if (me.getQuantidade().compareTo(saldoLocalizacao) > 0) {
						me.setQuantidade(saldoLocalizacao);
						event.getVenda().getProdutosSemSaldoEstoque().add(me.getProduto());
					}
					cadastroMovimentoEstoqueService.salvar(me, movimentacoesEstoque, event.getVenda().getUsuario());
				}
			}
		});
	}

	private List<MovimentoEstoque> criarMovimentosEstoque(MovimentoEstoqueVendaEvent event) {
		return event.getVenda().getItens().stream().map(temp -> {
			MovimentoEstoque movimentoEstoque = new MovimentoEstoque();
			if (temp.getProduto().getPossuiEstoque()) {						
				
				temp.getProduto().setLocalizacao(localizacoes.findByDepositoAndProduto(event.getVenda().getUsuario().getDeposito(), temp.getProduto()));
				
				movimentoEstoque = new MovimentoEstoqueBuilder()
						.comProduto(temp.getProduto())
						.comQuantidade(new BigDecimal(temp.getQuantidade()))
						.comValorUnitario(temp.getValorUnitario())
						.comTipoMovimento(event.getVenda().getStatus().equals(StatusVenda.FINALIZADA) || event.getVenda().getStatus().equals(StatusVenda.PENDENTE) 
								? TipoMovimentoEstoque.SAIDA : TipoMovimentoEstoque.ENTRADA)
						.comDestinoSaidaEstoque(event.getVenda().getStatus().equals(StatusVenda.FINALIZADA) || event.getVenda().getStatus().equals(StatusVenda.PENDENTE)
								? DestinoSaidaEstoque.VENDA : null)
						.comDevolucaoVenda(event.getVenda().getStatus().equals(StatusVenda.CANCELADA))
						.comDataHoraCadastro(LocalDateTime.now())
						.comUnidadeMedida(unidadesMedidas.findByTipoUnidadeMedida(temp.getProduto().getTipoUnidadeMedida()))
						.comUsuario(event.getVenda().getUsuario())
						.comVenda(event.getVenda())
						.comCodigo(null).construir();
			} else {
				movimentoEstoque.setProduto(temp.getProduto());
			}
			return movimentoEstoque;
        }).collect(Collectors.toList());

	}
}
