package br.com.yahtosistemas.service.event.movimentoEstoque;

import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.Venda;

public class MovimentoEstoqueVendaEvent {

	private Venda venda;
	private Usuario usuario;
	
	public MovimentoEstoqueVendaEvent(Venda venda, Usuario usuario) {
		this.venda = venda;
		this.usuario = usuario;
	}
	
	public Venda getVenda() {
		return this.venda;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}
}
