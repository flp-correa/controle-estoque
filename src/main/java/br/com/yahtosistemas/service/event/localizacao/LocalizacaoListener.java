package br.com.yahtosistemas.service.event.localizacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.service.event.localizacao.LocalizacaoEvent;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@Component
public class LocalizacaoListener {

	@Autowired
	private Localizacoes localizacoes;
	
	@EventListener
	public void produtoSalvo(LocalizacaoEvent event) {
		localizacoes.findByProduto(event.getProduto()).forEach(l -> l.setProduto(null));

		long[] localizacoesArray = event.getProduto().getLocalizacoes().stream().mapToLong(Localizacao::getCodigo).toArray();
		localizacoes.findByCodigoIn(localizacoesArray).forEach((l)-> {
			definirValorKgParaGrama(l);
			l.setProduto(event.getProduto());
		});
	}
	
	private void definirValorKgParaGrama(final Localizacao localizacao) {
		if (localizacao.getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {			
			localizacao.setEstoqueMinimo(localizacao.getEstoqueMinimo() * 1000);
			localizacao.setEstoqueMaximo(localizacao.getEstoqueMaximo() * 1000);
		}
	}
}
