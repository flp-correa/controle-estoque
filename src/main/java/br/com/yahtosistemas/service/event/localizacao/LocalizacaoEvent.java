package br.com.yahtosistemas.service.event.localizacao;

import br.com.yahtosistemas.model.Produto;

public class LocalizacaoEvent {

	private Produto produto;
	
	public LocalizacaoEvent(Produto produto) {
		this.produto = produto;
	}
	
	public Produto getProduto() {
		return produto;
	}
}
