package br.com.yahtosistemas.service.exception;

public class ObrigatorioInformarEmpresaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ObrigatorioInformarEmpresaException(String message) {
		super(message);
	}
}
