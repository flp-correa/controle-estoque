package br.com.yahtosistemas.service.exception;

public class ObrigatorioAdicionarIngredientesParaProdutoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ObrigatorioAdicionarIngredientesParaProdutoException(String message) {
		super(message);
	}
}
