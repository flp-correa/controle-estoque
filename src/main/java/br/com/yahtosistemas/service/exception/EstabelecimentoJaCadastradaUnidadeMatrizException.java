package br.com.yahtosistemas.service.exception;

public class EstabelecimentoJaCadastradaUnidadeMatrizException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public EstabelecimentoJaCadastradaUnidadeMatrizException(String message) {
		super(message);
	}
}
