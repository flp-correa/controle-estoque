package br.com.yahtosistemas.service.exception;

public class NaoPossuiSaldoNaLocalizacaoEstoqueException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public NaoPossuiSaldoNaLocalizacaoEstoqueException(String message) {
		super(message);
	}

}
