package br.com.yahtosistemas.service.exception;

public class DataEntregaVendaObrigatorioException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DataEntregaVendaObrigatorioException(String message) {
		super(message);
	}
}
