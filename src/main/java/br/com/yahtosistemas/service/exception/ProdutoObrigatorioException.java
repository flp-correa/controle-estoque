package br.com.yahtosistemas.service.exception;

public class ProdutoObrigatorioException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ProdutoObrigatorioException(String message) {
		super(message);
	}

}
