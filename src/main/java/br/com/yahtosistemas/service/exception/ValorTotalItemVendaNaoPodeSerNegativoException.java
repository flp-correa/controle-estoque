package br.com.yahtosistemas.service.exception;

public class ValorTotalItemVendaNaoPodeSerNegativoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ValorTotalItemVendaNaoPodeSerNegativoException(String message) {
		super(message);
	}

}
