package br.com.yahtosistemas.service.exception;

public class QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException(String message) {
		super(message);
	}

}
