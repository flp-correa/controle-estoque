package br.com.yahtosistemas.service.exception;

public class ItemLancheOuPratoEntregueObrigatorioException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ItemLancheOuPratoEntregueObrigatorioException(String message) {
		super(message);
	}
}
