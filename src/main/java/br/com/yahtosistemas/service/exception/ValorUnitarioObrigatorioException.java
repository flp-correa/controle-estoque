package br.com.yahtosistemas.service.exception;

public class ValorUnitarioObrigatorioException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public ValorUnitarioObrigatorioException(String message) {
		super(message);
	}

}
