package br.com.yahtosistemas.service.exception;

public class ErroSalvarPedidoVendaException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ErroSalvarPedidoVendaException(String message) {
		super(message);
	}
}
