package br.com.yahtosistemas.service.exception;

public class ValorDinheiroEValorCartaoIncorretosException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ValorDinheiroEValorCartaoIncorretosException(String message) {
		super(message);
	}
}
