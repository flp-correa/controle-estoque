package br.com.yahtosistemas.service.exception;

public class EntidadeObrigatoriaException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public EntidadeObrigatoriaException(String message) {
		super(message);
	}

}
