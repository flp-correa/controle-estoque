package br.com.yahtosistemas.service.exception;

public class ImpossivelCancelarPedidoVendaException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public ImpossivelCancelarPedidoVendaException(String message) {
		super(message);
	}

}
