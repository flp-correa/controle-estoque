package br.com.yahtosistemas.service.exception;

public class ObrigatorioAdicionarLocalizacoesParaProdutoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ObrigatorioAdicionarLocalizacoesParaProdutoException(String message) {
		super(message);
	}
}
