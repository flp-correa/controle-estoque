package br.com.yahtosistemas.service.exception;

public class QuantidadeDoTipoDeUnidadeInvalidaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public QuantidadeDoTipoDeUnidadeInvalidaException(String message) {
		super(message);
	}
}
