package br.com.yahtosistemas.service.exception;

public class LocalizacaoTransferenciaObrigatorioException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LocalizacaoTransferenciaObrigatorioException(String message) {
		super(message);
	}
}
