package br.com.yahtosistemas.service;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.yahtosistemas.dto.PeriodoRelatorio;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class RelatorioService {

	@Autowired
	private DataSource dataSource;
	
	public byte[] gerarRelatorioVendasFinalizadas(PeriodoRelatorio periodoRelatorio) throws Exception {
		
		validarDataEHora(periodoRelatorio);
		
		Timestamp dataHoraInicio = Timestamp.valueOf(LocalDateTime.of(periodoRelatorio.getDataInicio(), periodoRelatorio.getHoraInicio()));
		Timestamp dataHoraFim = Timestamp.valueOf(LocalDateTime.of(periodoRelatorio.getDataFim(), periodoRelatorio.getHoraFim()));
		
		Map<String, Object> parametros = montarParametrosPesquisa(periodoRelatorio, dataHoraInicio, dataHoraFim);
		
		InputStream inputStream = this.getClass()
				.getResourceAsStream("/relatorios/relatorio_vendas_finalizadas.jasper");
		
		Connection con = this.dataSource.getConnection();
		
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros, con);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} finally {
			con.close();
		}
	}
	
	public byte[] gerarRelatorioVendasFinalizadasPorProduto(PeriodoRelatorio periodoRelatorio) throws Exception {
		validarDataEHora(periodoRelatorio);
		
		Timestamp dataHoraInicio = Timestamp.valueOf(LocalDateTime.of(periodoRelatorio.getDataInicio(), periodoRelatorio.getHoraInicio()));
		Timestamp dataHoraFim = Timestamp.valueOf(LocalDateTime.of(periodoRelatorio.getDataFim(), periodoRelatorio.getHoraFim()));
		
		Map<String, Object> parametros = montarParametrosPesquisa(periodoRelatorio, dataHoraInicio, dataHoraFim);
		
		InputStream inputStream = this.getClass()
				.getResourceAsStream("/relatorios/relatorio_produtos_vendas_finalizadas.jasper");
		
		Connection con = this.dataSource.getConnection();
		
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros, con);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} finally {
			con.close();
		}
	}
	
	public byte[] gerarRelatorioComprasProdutos(String tenantId) throws Exception {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("format", "pdf");
		parametros.put("tenant_id", tenantId);
		
		InputStream inputStream = this.getClass()
				.getResourceAsStream("/relatorios/relatorio_compras_produtos.jasper");
		
		Connection con = this.dataSource.getConnection();
		
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros, con);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} finally {
			con.close();
		}
	}

	private Map<String, Object> montarParametrosPesquisa(PeriodoRelatorio periodoRelatorio, Timestamp dataHoraInicio,
			Timestamp dataHoraFim) {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("format", "pdf");
		parametros.put("data_hora_inicio", dataHoraInicio);
		parametros.put("data_hora_fim", dataHoraFim);
		parametros.put("tenant_id", periodoRelatorio.getTenantId());
		return parametros;
	}

	private void validarDataEHora(PeriodoRelatorio periodoRelatorio) {
		if (periodoRelatorio.getDataInicio() == null) {
			periodoRelatorio.setDataInicio(LocalDate.of(2018, 1, 1));
		}
		if (periodoRelatorio.getHoraInicio() == null) {
			periodoRelatorio.setHoraInicio(LocalTime.of(0, 0, 0));
		}
		
		if (periodoRelatorio.getDataFim() == null) {
			periodoRelatorio.setDataFim(LocalDate.now());
		}
		if (periodoRelatorio.getHoraFim() == null) {
			periodoRelatorio.setHoraFim(LocalTime.now());
		}
	}
}
