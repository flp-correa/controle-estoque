package br.com.yahtosistemas.service;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.repository.Fornecedores;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroFornecedorService {

	@Transactional
	public void salvar(Fornecedor fornecedor, Fornecedores fornecedores) {
		
		if (fornecedor.getTipoPessoa().equals(TipoPessoa.JURIDICA)) {			
			fornecedor.getUnidadeEmpresa().validarUnidadeEstabelecimento(fornecedores, fornecedor);
		} else {
			fornecedor.setUnidadeEmpresa(null);
		}
		fornecedor.setTenantId(TenancyInterceptor.getTenantId());
		fornecedores.save(fornecedor);
	}
	
	@Transactional
	public void excluir(Fornecedor fornecedor, Fornecedores fornecedores) {
		try {
			fornecedores.delete(fornecedor);
			fornecedores.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar o fornecedor.");
		}		
	}
}
