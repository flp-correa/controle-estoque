package br.com.yahtosistemas.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusItemCozinha;
import br.com.yahtosistemas.model.StatusPedidoVenda;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.AbreFechaCaixas;
import br.com.yahtosistemas.repository.ItensPedidosVendas;
import br.com.yahtosistemas.repository.PedidosVendas;
import br.com.yahtosistemas.repository.Vendas;
import br.com.yahtosistemas.service.exception.DataEntregaVendaObrigatorioException;
import br.com.yahtosistemas.service.exception.ErroSalvarPedidoVendaException;
import br.com.yahtosistemas.service.exception.ImpossivelCancelarPedidoVendaException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorTotalItemVendaNaoPodeSerNegativoException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroPedidoVendaService {
	
	@Autowired
	private CadastroVendaService cadastroVendaService;
	
	@Autowired
	private Vendas vendas;
	
	@Autowired
	private ItensPedidosVendas itensPedidosVendas;
	
	@Autowired
	private AbreFechaCaixas abreFechaCaixas;

	@Transactional
	public Long salvar(PedidoVenda pedidoVenda, StatusPedidoVenda status, PedidosVendas pedidosVendas, Usuario usuario) {
		if (pedidoVenda.isSalvarProibido()) {
			throw new RuntimeException("Usuário tentando salvar uma pedido de venda proibido");
		}
		
		pedidoVenda.setTenantId(TenancyInterceptor.getTenantId());
		validar(pedidoVenda, status, pedidosVendas);
		
		definirDataHoraCriacao(pedidoVenda, pedidosVendas);
		definirDataEntrega(pedidoVenda);
		definirSeEhClienteOuMesa(pedidoVenda);
		
		pedidoVenda.getItens().forEach((i)-> {
			removerIngredientesEAdicionaisNaoUtilizados(i);
			definirStatusCozinha(i);
			i.setPedidoVenda(pedidoVenda);
			i.setTenantId(pedidoVenda.getTenantId());
			if (i.getCodigo() != null && i.getCodigo() < 0) {
				i.setCodigo(null);
			}
		});
		
		pedidosVendas.save(pedidoVenda);
		
		if (status.equals(StatusPedidoVenda.FINALIZADO)) {
			return finalizarPedidoVendaECriarVenda(pedidoVenda.getCodigo(), pedidosVendas, usuario);
		} else {
			return null;
		}
	}
	
	@PreAuthorize("hasRole('ADMINISTRADOR')")
	@Transactional
	public void cancelar(Long codigo, PedidosVendas pedidosVendas) {
		Long codigoVenda = vendas.codigoVendaPorPedidoVenda(codigo);
		if (codigoVenda == null) {
			pedidosVendas.salvarStatusPedidoVenda(codigo, StatusPedidoVenda.CANCELADO); 
		} else {
			throw new ImpossivelCancelarPedidoVendaException(
					String.format("Impossível cancelar pedido de venda, já existe uma venda de código %s, para o pedido", codigoVenda));
		}
	}
	
	@PreAuthorize("hasRole('CADASTRAR_VENDA')")
	@Transactional
	public Long finalizarPedidoVendaECriarVenda(Long codigo, PedidosVendas pedidosVendas, Usuario usuario) {
		pedidosVendas.salvarStatusPedidoVenda(codigo, StatusPedidoVenda.FINALIZADO);
		PedidoVenda pedidoVenda = pedidosVendas.buscarComItens(codigo);
		pedidoVenda.getItens().forEach(i -> i.getAdicionais().forEach(a -> a.setSelecionado(Boolean.TRUE)));
		return cadastroVendaService.criarESalvarVendaDoPedidoVenda(usuario, pedidoVenda);
	}
	
	private void definirStatusCozinha(ItemPedidoVenda item) {
		if (item.getProduto().getEnviarCozinha()) {
			if (item.getCodigo() < 0) {
				item.setStatusCozinha(StatusItemCozinha.NAO_INICIADO);
			} else {
				StatusItemCozinha statusCozinha = itensPedidosVendas.buscarStatusCozinhaItemPedidoVenda(item.getCodigo()).getStatusCozinha();
				if (!item.getStatusCozinha().equals(statusCozinha) && !item.getStatusCozinha().equals(StatusItemCozinha.ENTREGUE)) {
					item.setStatusCozinha(statusCozinha);
				}
 			}
		}
	}
	
	private void removerIngredientesEAdicionaisNaoUtilizados(ItemPedidoVenda item) {
		if (item.getIngredientes() != null) {				
			item.getIngredientes().removeIf(ig -> !ig.getSelecionado());
			item.getAdicionais().removeIf(a -> !a.getSelecionado());
		}
	}

	private void definirSeEhClienteOuMesa(PedidoVenda pedidoVenda) {
		if (StringUtils.isEmpty(pedidoVenda.getMesa().getCodigo())) {
			pedidoVenda.setMesa(null);
		} else {
			pedidoVenda.setCliente(null);
		}
	}

	private void definirDataEntrega(PedidoVenda pedidoVenda) {
		if (pedidoVenda.getDataEntrega() != null) {
			pedidoVenda.setDataHoraEntrega(LocalDateTime.of(pedidoVenda.getDataEntrega()
					, pedidoVenda.getHorarioEntrega() != null ? pedidoVenda.getHorarioEntrega() : LocalTime.NOON));
		}
	}

	private void definirDataHoraCriacao(PedidoVenda pedidoVenda, PedidosVendas pedidosVendas) {
		if (pedidoVenda.isNovo()) {
			pedidoVenda.setDataHoraCriacao(LocalDateTime.now());
		} else {
			PedidoVenda vendaExistente = pedidosVendas.getOne(pedidoVenda.getCodigo());
			pedidoVenda.setDataHoraCriacao(vendaExistente.getDataHoraCriacao());
		}
	}
	
	private void validar(PedidoVenda pedidoVenda, StatusPedidoVenda status, PedidosVendas pedidosVendas) {
		if (abreFechaCaixas.findByTenantIdAndDataHoraFimIsNull(pedidoVenda.getTenantId()) == null) {
			throw new ErroSalvarPedidoVendaException("Impossível iniciar pedido, caixa esta fechado");
		}
		validarSeInformouApenasHorarioEntrega(pedidoVenda);
		validarSeInformouItens(pedidoVenda);
		validarValorTotalNegativo(pedidoVenda);
		status.validarPedidoVenda(pedidoVenda, pedidosVendas, itensPedidosVendas);
	}
	
	private void validarValorTotalNegativo(PedidoVenda pedidoVenda) {
		if (pedidoVenda.getValorTotal().compareTo(BigDecimal.ZERO) < 0) {
			throw new ValorTotalItemVendaNaoPodeSerNegativoException("Valor total não pode ser negativo");
		}
	}
	
	private void validarSeInformouItens(PedidoVenda pedidoVenda) {
		if (pedidoVenda.getItens().isEmpty()) {
			throw new ProdutoObrigatorioException("Adicione pelo menos um produto");
		}
	}

	private void validarSeInformouApenasHorarioEntrega(PedidoVenda pedidoVenda) {
		if (pedidoVenda.getHorarioEntrega() != null && pedidoVenda.getDataEntrega() == null) {
			throw new DataEntregaVendaObrigatorioException("Informe uma data da entrega para um horário");
		}
	}
}
