package br.com.yahtosistemas.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.repository.AbreFechaCaixas;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroAbreFechaCaixaService {

	@Transactional
	public void salvar(AbreFechaCaixa abreFechaCaixa, AbreFechaCaixas abreFechaCaixas) {
		abreFechaCaixa.setTenantId(TenancyInterceptor.getTenantId());
		abreFechaCaixas.save(abreFechaCaixa);
	}
}
