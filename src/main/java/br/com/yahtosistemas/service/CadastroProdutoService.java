package br.com.yahtosistemas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.Produtos;
import br.com.yahtosistemas.service.event.localizacao.LocalizacaoEvent;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.ObrigatorioAdicionarIngredientesParaProdutoException;
import br.com.yahtosistemas.service.exception.ObrigatorioAdicionarLocalizacoesParaProdutoException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroProdutoService {

	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Transactional
	public void salvar(Produto produto, Produtos produtos) {
		
		List<Localizacao> listaLocalizacoes = validarSeEhLocalFixo(produto);
		validarSeEhLancheOuPrato(produto);
		
		produto.setTenantId(TenancyInterceptor.getTenantId());
		produto = produtos.save(produto);
		
		produto.setLocalizacoes(listaLocalizacoes);
		publisher.publishEvent(new LocalizacaoEvent(produto));
	}

	@Transactional
	public void excluir(Produto produto, Produtos produtos) {
		try {
			produtos.delete(produto);
			produtos.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar o produto.");
		}		
	}
	
	private List<Localizacao> validarSeEhLocalFixo(Produto produto) {
		List<Localizacao> listaLocalizadoes = new ArrayList<>();
		if (produto.getLocalFixo()) {
			if (produto.getLocalizacoes().isEmpty()) {
				throw new ObrigatorioAdicionarLocalizacoesParaProdutoException("Adicione uma localização para o produto");
			}
			listaLocalizadoes.addAll(produto.getLocalizacoes());
		} else {
			produto.setLocalizacoes(new ArrayList<>());
		}
		return listaLocalizadoes;
	}
	
	private void validarSeEhLancheOuPrato(Produto produto) {
		if (produto.getEnviarCozinha() && produto.getIngredientes().isEmpty()) {
			throw new ObrigatorioAdicionarIngredientesParaProdutoException("Adicione pelo menos um ingrediente ao produto");
		}
	}
}
