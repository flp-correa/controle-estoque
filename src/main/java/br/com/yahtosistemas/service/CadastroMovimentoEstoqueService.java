package br.com.yahtosistemas.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.builder.SaldoEstoqueBuilder;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.movimentoEstoque.AcaoMovimentoEstoque;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroMovimentoEstoqueService {

	@Autowired
	private SaldosEstoque saldosEstoque;
	
	@Transactional
	public void salvar(MovimentoEstoque movimentoEstoque, MovimentacoesEstoque movimentacoesEstoque, Usuario usuario) {
		validarMovimentacaoEstoque(movimentoEstoque);
		
		SaldoEstoque saldoEstoque = getSaldoEstoque(movimentoEstoque);
		
		AcaoMovimentoEstoque acaoMovimentoEstoque = movimentoEstoque.getTipoMovimento().obterTipoMovimentoEstoque();
		acaoMovimentoEstoque.validar(movimentoEstoque, saldoEstoque);
		
		movimentoEstoque.setUsuario(usuario);
		movimentoEstoque.setTenantId(TenancyInterceptor.getTenantId());
		movimentoEstoque.adicionarSaldoEstoque(
				acaoMovimentoEstoque.criarSaldoEstoque(movimentoEstoque, saldoEstoque.getSaldoLocalizacao(), saldoEstoque.getSaldoEstoque()));
		movimentacoesEstoque.save(movimentoEstoque);
	}

	private SaldoEstoque getSaldoEstoque(MovimentoEstoque movimentoEstoque) {
		BigDecimal saldoTotalEstoque = saldosEstoque.saldoTotalEstoqueProduto(movimentoEstoque.getProduto());
		BigDecimal saldoLocalizacao = saldosEstoque.saldoLocalizacaoEstoqueProtudo(movimentoEstoque.getProduto(), movimentoEstoque.getProduto().getLocalizacao());
		
		return new SaldoEstoqueBuilder().comSaldoEstoque(saldoTotalEstoque).comSaldoLocalizacao(saldoLocalizacao).comCodigo(null).construir();
	}
	
	private void validarMovimentacaoEstoque(MovimentoEstoque movimentoEstoque) {
		if (StringUtils.isEmpty(movimentoEstoque.getProduto().getCodigo())) {
			throw new ProdutoObrigatorioException("Produto é obrigatório");
		}
		if (StringUtils.isEmpty(movimentoEstoque.getProduto().getLocalizacao().getCodigo())) {
			throw new ProdutoObrigatorioException("Localização é obrigatória");
		}
		if (StringUtils.isEmpty(movimentoEstoque.getUnidadeMedida().getCodigo())) {
			throw new ProdutoObrigatorioException("Unidade de medida é obrigatória");
		}
	}
}
