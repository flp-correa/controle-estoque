package br.com.yahtosistemas.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusItemCozinha;
import br.com.yahtosistemas.repository.ItensPedidosVendas;

@Service
public class CadastroItemPedidoVendaService {
	
	@Autowired
	private ItensPedidosVendas itensPedidosVendas;

	public void salvarStatusItemCozinha(Long[] codigos, StatusItemCozinha statusItemCozinha, ItensPedidosVendas itensPedidosVendas) {
		statusItemCozinha.salvarStatusItemCozinha(codigos, itensPedidosVendas);
	}

	public void salvarLoadCozinha(List<PedidoVenda> listaPedidosVendas) {
		if (!listaPedidosVendas.isEmpty()) {			
			listaPedidosVendas.forEach(p -> 
				itensPedidosVendas.salvarLoadCozinhaItem(
						p.getItens().stream().map(ItemPedidoVenda::getCodigo).collect(Collectors.toList())
						, Boolean.TRUE));
		}	
	}
}
