package br.com.yahtosistemas.service;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.repository.Depositos;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.tenancy.TenancyInterceptor;

@Service
public class CadastroDepositoService {

	@Transactional
	public void salvar(Deposito deposito, Depositos depositos) {
		deposito.setTenantId(TenancyInterceptor.getTenantId());
		depositos.save(deposito);
	}
	
	@Transactional
	public void excluir(final Deposito deposito, final Depositos depositos) {
		try {
			depositos.delete(deposito);
			depositos.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar o depósito");
		}		
	}
}
