package br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.dto.ProdutoDTO;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.repository.Produtos;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.service.exception.LocalizacaoTransferenciaObrigatorioException;
import br.com.yahtosistemas.service.exception.NaoPossuiSaldoNaLocalizacaoEstoqueException;
import br.com.yahtosistemas.service.movimentoEstoque.AcaoMovimentoEstoque;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

public class TransferenciaEstoque implements AcaoMovimentoEstoque {

	@Override
	public List<SaldoEstoque> criarSaldoEstoque(MovimentoEstoque movimentoEstoque, BigDecimal saldoLocalizacao, BigDecimal saldoEstoque) {
		List<SaldoEstoque> saldos = new ArrayList<>();
		
		saldos.add(new SaidaEstoque().criarSaldoEstoque(movimentoEstoque, saldoLocalizacao, saldoEstoque).get(0));
		
		saldos.add(new EntradaEstoque().criarSaldoEstoque(movimentoEstoque, getSaldoLocalizacaoTransferencia(movimentoEstoque),	saldos.get(0).getSaldoEstoque()).get(0));
		saldos.get(1).setLocalizacao(movimentoEstoque.getLocalizacaoTransferencia());
		
		return saldos;
	}

	@Override
	public void validar(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque) {
		movimentoEstoque.setVenda(null);
		movimentoEstoque.setEmpresaDestino(null);
		movimentoEstoque.setFornecedor(null);
		movimentoEstoque.setValorUnitario(null);
		
		if (StringUtils.isEmpty(movimentoEstoque.getLocalizacaoTransferencia().getCodigo())) {
			throw new LocalizacaoTransferenciaObrigatorioException("Localização de transferência é obrigatória");
		}
		
		validarSeTemSaldoNaLocalizacao(movimentoEstoque, saldoEstoque);
		validarSeCodigoLocalizacaoDiferenteEPossuiMesmoProduto(movimentoEstoque);
	}
	
	private BigDecimal getSaldoLocalizacaoTransferencia(MovimentoEstoque movimentoEstoque) {
		SaldosEstoque saldosEstoque = ControleEstoqueApplication.getBean(SaldosEstoque.class);
		return saldosEstoque.saldoLocalizacaoEstoqueProtudo(movimentoEstoque.getProduto(), movimentoEstoque.getLocalizacaoTransferencia());
	}

	private void validarSeTemSaldoNaLocalizacao(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque) {
		BigDecimal quantidadeTotalMovtoEstoque = movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade());
		
		if (quantidadeTotalMovtoEstoque.compareTo(saldoEstoque.getSaldoLocalizacao()) > 0) {
			String erro = movimentoEstoque.getUnidadeMedida().getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG) ? 
					"Produto não possui saldo. Saldo total: %.3f unidades. Saldo localização: %.3f unidades" :
						"Produto não possui saldo. Saldo total: %.0f unidades. Saldo localização: %.0f unidades";
			throw new NaoPossuiSaldoNaLocalizacaoEstoqueException(String.format(erro,
					saldoEstoque.getSaldoEstoque(), saldoEstoque.getSaldoLocalizacao()));
		}
	}

	private void validarSeCodigoLocalizacaoDiferenteEPossuiMesmoProduto(MovimentoEstoque movimentoEstoque) {
		if (movimentoEstoque.getProduto().getLocalizacao().equals(movimentoEstoque.getLocalizacaoTransferencia())) {
			throw new LocalizacaoTransferenciaObrigatorioException("Localização de transferência inválida");
		}
		
		Produtos produtos = ControleEstoqueApplication.getBean(Produtos.class);
		ProdutoDTO p1 = produtos.buscarProdutoPorCodigoLocalizacao(movimentoEstoque.getProduto().getLocalizacao().getCodigo());
		ProdutoDTO p2 = produtos.buscarProdutoPorCodigoLocalizacao(movimentoEstoque.getLocalizacaoTransferencia().getCodigo());
		
		if (!p1.getCodigo().equals(p2.getCodigo())) {
			throw new LocalizacaoTransferenciaObrigatorioException("Localização de transferência inválida");
		}
	}
}
