package br.com.yahtosistemas.service.movimentoEstoque;

import java.math.BigDecimal;
import java.util.List;

import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;

public interface AcaoMovimentoEstoque {

	public List<SaldoEstoque> criarSaldoEstoque(MovimentoEstoque movimentoEstoque, BigDecimal saldoLocalizacao, BigDecimal saldoEstoque);
	
	public void validar(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque);
}
