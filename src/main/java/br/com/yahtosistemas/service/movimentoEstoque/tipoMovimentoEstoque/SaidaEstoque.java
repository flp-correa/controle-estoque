package br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.DestinoSaidaEstoque;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.model.builder.SaldoEstoqueBuilder;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.service.exception.NaoPossuiSaldoNaLocalizacaoEstoqueException;
import br.com.yahtosistemas.service.exception.ValorUnitarioObrigatorioException;
import br.com.yahtosistemas.service.movimentoEstoque.AcaoMovimentoEstoque;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

public class SaidaEstoque implements AcaoMovimentoEstoque {

	@Override
	public List<SaldoEstoque> criarSaldoEstoque(MovimentoEstoque movimentoEstoque, BigDecimal saldoLocalizacao, BigDecimal saldoEstoque) {
		BigDecimal totalSaldoLocalizacao;
		BigDecimal totalSaldoEstoque;
		
		totalSaldoLocalizacao = saldoLocalizacao.subtract(movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade()));
		totalSaldoEstoque = saldoEstoque.subtract(movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade()));
		
		validarLocalizacao(movimentoEstoque.getProduto(), totalSaldoLocalizacao);
		
		return Arrays.asList(new SaldoEstoqueBuilder().comTenantId(movimentoEstoque.getTenantId())
										.comMovimentoEstoque(movimentoEstoque)
										.comLocalizacao(movimentoEstoque.getProduto().getLocalizacao())
										.comSaldoLocalizacao(totalSaldoLocalizacao)
										.comSaldoEstoque(totalSaldoEstoque)
										.comCodigo(null).construir());
	}

	@Override
	public void validar(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque) {
		setNullCamposDesnecessarios(movimentoEstoque);
		
		BigDecimal quantidadeTotalMovtoEstoque = movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade());
		
		if (quantidadeTotalMovtoEstoque.compareTo(saldoEstoque.getSaldoLocalizacao()) > 0) {
			String erro = formatarErroPorTipoUnidadeMedida(movimentoEstoque, saldoEstoque);
			throw new NaoPossuiSaldoNaLocalizacaoEstoqueException(erro);
		}
		
		if (StringUtils.isEmpty(movimentoEstoque.getValorUnitario()) || movimentoEstoque.getValorUnitario().compareTo(BigDecimal.ZERO) < 0) {
			throw new ValorUnitarioObrigatorioException("Valor unitário é obrigatório");
		}
	}

	private String formatarErroPorTipoUnidadeMedida(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque) {
		String erro = "";
		if (movimentoEstoque.getUnidadeMedida().getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {
			BigDecimal valorKgLocalizacao = saldoEstoque.getSaldoLocalizacao().divide(new BigDecimal(1000), 4, RoundingMode.HALF_EVEN);
			BigDecimal valorKgTotal = saldoEstoque.getSaldoEstoque().divide(new BigDecimal(1000), 4, RoundingMode.HALF_EVEN);
			if (valorKgTotal.compareTo(BigDecimal.ONE) < 0) {
				erro = String.format("Produto não possui saldo. Saldo total: %.0f GRAMAS. ", saldoEstoque.getSaldoEstoque());
			} else {
				erro = String.format("Produto não possui saldo. Saldo total: %s Kg. ", formatarValorBigDecimal(valorKgTotal));
			}
			
			if (valorKgLocalizacao.compareTo(BigDecimal.ONE) < 0) {
				erro += String.format("Saldo localização: %.0f GRAMAS", saldoEstoque.getSaldoLocalizacao());
			} else {
				erro += String.format("Saldo localização: %s Kg", formatarValorBigDecimal(valorKgLocalizacao));
			}
		} else if (movimentoEstoque.getUnidadeMedida().getTipoUnidadeMedida().equals(TipoUnidadeMedida.GRAMAS)) {
			erro = String.format("Produto não possui saldo. Saldo total: %.0f GRAMAS. Saldo localização: %.0f GRAMAS",
					saldoEstoque.getSaldoEstoque(), saldoEstoque.getSaldoLocalizacao());
		} else {
			erro = String.format("Produto não possui saldo. Saldo total: %.0f UNID. Saldo localização: %.0f UNID",
					saldoEstoque.getSaldoEstoque(), saldoEstoque.getSaldoLocalizacao());
		}
		return erro;
	}
	
	private String formatarValorBigDecimal(BigDecimal valor) {
		NumberFormat numberFormat = new DecimalFormat("#,##0.000", new DecimalFormatSymbols(new Locale("pt", "BR")));
		return numberFormat.format(valor);
	}

	private void setNullCamposDesnecessarios(MovimentoEstoque movimentoEstoque) {
		movimentoEstoque.setFornecedor(null);
		if (movimentoEstoque.getDestinoSaidaEstoque().equals(DestinoSaidaEstoque.UNIDADE_PROPRIA)) {
			movimentoEstoque.setVenda(null);
		} else {
			movimentoEstoque.setEmpresaDestino(null);
		}
	}
	
	private void validarLocalizacao(Produto produto, BigDecimal totalSaldoLocalizacao) {
		if (!produto.getLocalFixo() && totalSaldoLocalizacao.compareTo(BigDecimal.ZERO) == 0) {
			Localizacoes localizacoes = ControleEstoqueApplication.getBean(Localizacoes.class);
			localizacoes.getOne(produto.getLocalizacao().getCodigo()).setProduto(null);
		}
	}

}
