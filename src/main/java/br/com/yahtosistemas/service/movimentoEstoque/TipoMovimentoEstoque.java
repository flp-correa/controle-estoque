package br.com.yahtosistemas.service.movimentoEstoque;

import br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque.EntradaEstoque;
import br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque.SaidaEstoque;
import br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque.TransferenciaEstoque;

public enum TipoMovimentoEstoque {

	ENTRADA("Entrada") {
		@Override
		public AcaoMovimentoEstoque obterTipoMovimentoEstoque() {
			return new EntradaEstoque();
		}
		
	},
	TRANSFERENCIA("Transferência") {

		@Override
		public AcaoMovimentoEstoque obterTipoMovimentoEstoque() {
			return new TransferenciaEstoque();
		}
		
	},
	SAIDA("Saída") {
		@Override
		public AcaoMovimentoEstoque obterTipoMovimentoEstoque() {
			return new SaidaEstoque();
		}
	};
	
	public abstract AcaoMovimentoEstoque obterTipoMovimentoEstoque();
	
	private String descricao;
	
	private TipoMovimentoEstoque(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
