package br.com.yahtosistemas.service.movimentoEstoque.tipoMovimentoEstoque;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.model.builder.SaldoEstoqueBuilder;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.service.exception.ValorUnitarioObrigatorioException;
import br.com.yahtosistemas.service.movimentoEstoque.AcaoMovimentoEstoque;

public class EntradaEstoque implements AcaoMovimentoEstoque {

	@Override
	public List<SaldoEstoque> criarSaldoEstoque(MovimentoEstoque movimentoEstoque, BigDecimal saldoLocalizacao, BigDecimal saldoEstoque) {
		BigDecimal totalSaldoLocalizacao;
		BigDecimal totalSaldoEstoque;
		
		totalSaldoLocalizacao = movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade()).add(saldoLocalizacao);
		totalSaldoEstoque = movimentoEstoque.getQuantidade().multiply(movimentoEstoque.getUnidadeMedida().getQuantidade()).add(saldoEstoque);			
		
		validarLocalizacao(movimentoEstoque.getProduto());
		
		return Arrays.asList(new SaldoEstoqueBuilder().comTenantId(movimentoEstoque.getTenantId())
										.comMovimentoEstoque(movimentoEstoque)
										.comLocalizacao(movimentoEstoque.getProduto().getLocalizacao())
										.comSaldoLocalizacao(totalSaldoLocalizacao)
										.comSaldoEstoque(totalSaldoEstoque)
										.comCodigo(null).construir());
	}

	@Override
	public void validar(MovimentoEstoque movimentoEstoque, SaldoEstoque saldoEstoque) {
		if (movimentoEstoque.isDevolucaoVenda()) {
			MovimentacoesEstoque movimentacoesEstoque = ControleEstoqueApplication.getBean(MovimentacoesEstoque.class);
			movimentoEstoque.setValorUnitario(movimentacoesEstoque.valorUnitarioUltimoMovtoEntradaEstoqueProduto(movimentoEstoque.getProduto()));
		} else {
			movimentoEstoque.setVenda(null);
			movimentoEstoque.setEmpresaDestino(null);
			
			if (StringUtils.isEmpty(movimentoEstoque.getFornecedor().getCodigo())) {
				throw new EntidadeObrigatoriaException("Fornecedor é obrigatório");
			}
			if (StringUtils.isEmpty(movimentoEstoque.getValorUnitario()) || movimentoEstoque.getValorUnitario().compareTo(BigDecimal.ZERO) < 1) {
				throw new ValorUnitarioObrigatorioException("Valor unitário é obrigatório");
			}			
		}
	}
	
	private void validarLocalizacao(Produto produto) {
		Localizacoes localizacoes = ControleEstoqueApplication.getBean(Localizacoes.class);
		Localizacao localizacao = localizacoes.getOne(produto.getLocalizacao().getCodigo());
		if (localizacao.getProduto() == null) {
			localizacao.setProduto(produto);
		}
	}
}
