package br.com.yahtosistemas.service.unidadeMedida.tipoUnidade;

import java.math.BigDecimal;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.service.exception.QuantidadeDoTipoDeUnidadeInvalidaException;
import br.com.yahtosistemas.service.unidadeMedida.AcaoTipoUnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.FamiliaUnidadeMedida;

public class Pacote implements AcaoTipoUnidadeMedida {

	@Override
	public String getDescricaoComQuantidade(BigDecimal quantidade) {
		return String.format("Pacote com %s unidades", quantidade.intValue());
	}

	@Override
	public void validarQuantidade(UnidadeMedida unidade) {
		if (StringUtils.isEmpty(unidade.getQuantidade()) || unidade.getQuantidade().equals(BigDecimal.ZERO)) {
			throw new QuantidadeDoTipoDeUnidadeInvalidaException("Quantidade inválida");
		}
	}

	@Override
	public FamiliaUnidadeMedida getFamiliaUnidadeMedida() {
		return FamiliaUnidadeMedida.UNIDADE;
	}
}
