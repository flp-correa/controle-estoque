package br.com.yahtosistemas.service.unidadeMedida;

import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Caixa;
import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Duzia;
import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Grama;
import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Kilograma;
import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Pacote;
import br.com.yahtosistemas.service.unidadeMedida.tipoUnidade.Unidade;

public enum TipoUnidadeMedida {

	UNID("Unidade") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Unidade();
		}
	},
	CX("Caixa") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Caixa();
		}
	},
	DUZIA("Duzia") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Duzia();
		}
	},
	KG("Kilograma") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Kilograma();
		}
		
	},
	GRAMAS("Gramas") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Grama();
		}
		
	},
	PACOTE("Pacote") {
		@Override
		public AcaoTipoUnidadeMedida obterUnidadeMedida() {
			return new Pacote();
		}
	};
	
	public abstract AcaoTipoUnidadeMedida obterUnidadeMedida();
	
	private String descricao;
	
	TipoUnidadeMedida(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
