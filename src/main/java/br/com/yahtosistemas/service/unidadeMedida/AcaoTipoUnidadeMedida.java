package br.com.yahtosistemas.service.unidadeMedida;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.UnidadeMedida;

public interface AcaoTipoUnidadeMedida {

	public FamiliaUnidadeMedida getFamiliaUnidadeMedida();
	public String getDescricaoComQuantidade(BigDecimal quantidade);
	public void validarQuantidade(UnidadeMedida unidade);
}
