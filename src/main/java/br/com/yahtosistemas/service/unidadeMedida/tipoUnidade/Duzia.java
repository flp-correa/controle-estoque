package br.com.yahtosistemas.service.unidadeMedida.tipoUnidade;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.AcaoTipoUnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.FamiliaUnidadeMedida;

public class Duzia implements AcaoTipoUnidadeMedida {

	@Override
	public void validarQuantidade(UnidadeMedida unidade) {
		unidade.setQuantidade(new BigDecimal(12));
	}
	
	@Override
	public String getDescricaoComQuantidade(BigDecimal quantidade) {
		return "Duzia";
	}

	@Override
	public FamiliaUnidadeMedida getFamiliaUnidadeMedida() {
		return FamiliaUnidadeMedida.UNIDADE;
	}
}
