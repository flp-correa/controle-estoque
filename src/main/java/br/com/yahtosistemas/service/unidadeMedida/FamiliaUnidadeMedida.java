package br.com.yahtosistemas.service.unidadeMedida;

public enum FamiliaUnidadeMedida {

	MASSA("Kilograma / Gramas"),
	VOLUME("Litro / Mililitro"),
	UNIDADE("Unidade");
	
	private String descricao;
	
	FamiliaUnidadeMedida(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
