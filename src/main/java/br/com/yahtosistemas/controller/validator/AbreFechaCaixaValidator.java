package br.com.yahtosistemas.controller.validator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.yahtosistemas.dto.VendaDTO;
import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.model.OpcaoAbreFechaCaixa;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.PedidosVendas;
import br.com.yahtosistemas.repository.Usuarios;
import br.com.yahtosistemas.repository.Vendas;

@Component
public class AbreFechaCaixaValidator implements Validator {

	@Autowired
	private Usuarios usuarios;
	
	@Autowired
	private Vendas vendas;
	
	@Autowired
	private PedidosVendas pedidosVendas;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return AbreFechaCaixa.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AbreFechaCaixa abreFechaCaixa = (AbreFechaCaixa) target;
		
		validarUsuario(errors, abreFechaCaixa);
		validarValoresNulos(errors, abreFechaCaixa);
		validarValorSangria(errors, abreFechaCaixa);
		validarValorEntrada(errors, abreFechaCaixa);
		validarObservacao(errors, abreFechaCaixa);
		validarSePodeAbrirFecharCaixa(errors, abreFechaCaixa);
	}

	private void validarObservacao(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		if (abreFechaCaixa.getObservacao().length() > 600) {
			errors.rejectValue("observacao", "", "Campo observação suporta no máximo 600 caracteres");
		}
	}

	private void validarSePodeAbrirFecharCaixa(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		if (abreFechaCaixa.getOpcaoAbreFechaCaixa() != null && abreFechaCaixa.getOpcaoAbreFechaCaixa().equals(OpcaoAbreFechaCaixa.FECHAR_CAIXA)) {
			
			if (vendas.seExisteVendaEmAndamento().isPresent()) {
				errors.rejectValue("", "", "Todos as vendas devem estar encerradas");
			}
			
			if (pedidosVendas.seExistePedidoVendaEmAndamento().isPresent()) {
				errors.rejectValue("", "", "Todos os pedidos de vendas devem estar encerrados");
			}
			if (!errors.hasErrors()) {
				abreFechaCaixa.setDataHoraFim(LocalDateTime.now());
			}
		}
	}

	private void validarValorSangria(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		if (abreFechaCaixa.getOpcaoAbreFechaCaixa() != null && abreFechaCaixa.getOpcaoAbreFechaCaixa().equals(OpcaoAbreFechaCaixa.RETIRAR_DINHEIRO)) {
			if (abreFechaCaixa.getValorSangria().compareTo(abreFechaCaixa.getValorSangriaOriginal()) < 1) {
				abreFechaCaixa.setValorSangria(abreFechaCaixa.getValorSangriaOriginal());
				errors.rejectValue("valorSangria", "", "Valor da sangria não pode ser menor nem igual ao valor já registrado");
			}
			VendaDTO vendaDTO = vendas.buscarValoresFecharCaixa(abreFechaCaixa.getDataHoraInicio());
			BigDecimal totalDinheiro = vendaDTO.getTotalDinheiro() != null ? vendaDTO.getTotalDinheiro() : BigDecimal.ZERO;
			
 			if (totalDinheiro.add(abreFechaCaixa.getValorInicioDinheiro()).compareTo(abreFechaCaixa.getValorSangria()) < 0) {
				abreFechaCaixa.setValorSangria(abreFechaCaixa.getValorSangriaOriginal());
				errors.rejectValue("valorSangria", "", "Valor da sangria não pode ser maior do que o valor em caixa");
			}
		}
	}
	
	private void validarValorEntrada(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		if (abreFechaCaixa.getOpcaoAbreFechaCaixa() != null && abreFechaCaixa.getOpcaoAbreFechaCaixa().equals(OpcaoAbreFechaCaixa.ENTRAR_DINHEIRO)) {
			if (abreFechaCaixa.getValorEntrada().compareTo(abreFechaCaixa.getValorEntradaOriginal()) < 1) {
				abreFechaCaixa.setValorEntrada(abreFechaCaixa.getValorEntradaOriginal());
				errors.rejectValue("valorEntrada", "", "Valor de entrada não pode ser menor nem igual ao valor já registrado");
			}
		}
	}

	private void validarValoresNulos(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		if (abreFechaCaixa.isNovo()) {
			setNullCamposQdoNaoFechaCaixa(abreFechaCaixa);
			abreFechaCaixa.setValorSangria(null);
		} else {
			if (abreFechaCaixa.getOpcaoAbreFechaCaixa() == null) {
				errors.rejectValue("opcaoAbreFechaCaixa", "", "Escolha retirar dinheiro ou fechar caixa");
			} else if (!abreFechaCaixa.getOpcaoAbreFechaCaixa().equals(OpcaoAbreFechaCaixa.FECHAR_CAIXA)) {
				setNullCamposQdoNaoFechaCaixa(abreFechaCaixa);
			}
		}
	}

	private void setNullCamposQdoNaoFechaCaixa(AbreFechaCaixa abreFechaCaixa) {
		abreFechaCaixa.setDataHoraFim(null);
		abreFechaCaixa.setTotalCartao(null);
		abreFechaCaixa.setTotalDinheiro(null);
		abreFechaCaixa.setValorTotal(null);
	}

	private void validarUsuario(Errors errors, AbreFechaCaixa abreFechaCaixa) {
		Optional<Usuario> usuarioOptional = usuarios.porEmailEAtivo(abreFechaCaixa.getUsuario().getEmail());
		if (usuarioOptional.isPresent()) {
			List<String> permissoes = usuarios.permissoes(usuarioOptional.get());
			if (permissoes.contains("ROLE_ADMINISTRADOR")) {
				if (!new BCryptPasswordEncoder().matches(abreFechaCaixa.getUsuario().getSenha(), usuarioOptional.get().getSenha())) {
					errors.rejectValue("usuario", "", "Senha inválida");
				}
			} else {
				errors.rejectValue("usuario", "", "Usuário não autorizado");
			}
		} else {
			errors.rejectValue("usuario", "", "E-mail não encontrado");
		}
	}

}
