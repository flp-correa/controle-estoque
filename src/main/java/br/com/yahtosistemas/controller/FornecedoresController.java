package br.com.yahtosistemas.controller;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.Estados;
import br.com.yahtosistemas.repository.Fornecedores;
import br.com.yahtosistemas.repository.Produtos;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroFornecedorService;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.session.TabelaProdutosFornecedorSession;

@Controller
@RequestMapping("/{tenantid}/fornecedores")
public class FornecedoresController {
	
	@Autowired
	private TabelaProdutosFornecedorSession tabelaProdutosFornecedor;
	
	@Autowired
	private Fornecedores fornecedores;
	
	@Autowired
	private Produtos produtos;
	
	@Autowired
	private CadastroFornecedorService cadastroFornecedorService;
	
	@Autowired
	private Estados estados;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, EstabelecimentoFilter estabelecimentoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("fornecedor/PesquisaFornecedores");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Fornecedor> paginaWrapper = new PageWrapper<>(fornecedores.filtrar(estabelecimentoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}

	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Fornecedor fornecedor, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("fornecedor/CadastroFornecedor");
			
			setUuid(fornecedor);
			
			mv.addObject("produtos", fornecedor.getProdutos());
			mv.addObject("estados", estados.findAll());
			mv.addObject("unidadesEmpresas", UnidadeEstabelecimento.values());
			mv.addObject("tiposPessoa", TipoPessoa.values());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Fornecedor fornecedor = fornecedores.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), fornecedor, usuarioSistema);
				
				setUuid(fornecedor);
				fornecedor.getProdutos().forEach((p )-> {
					p.setDetalhes(fornecedor.isDetalhes());
					tabelaProdutosFornecedor.adicionarProduto(fornecedor.getUuid(), p);
				});
				mv.addObject(fornecedor);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Fornecedor fornecedor, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		fornecedor.setProdutos(tabelaProdutosFornecedor.getProdutos(fornecedor.getUuid()));
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), fornecedor, usuarioSistema);
		}
		
		try {			
			cadastroFornecedorService.salvar(fornecedor, fornecedores);
		} catch (EstabelecimentoJaCadastradaUnidadeMatrizException e) {
			result.rejectValue("unidadeEmpresa", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), fornecedor, usuarioSistema);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("cnpj", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), fornecedor, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Fornecedor salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/fornecedores/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Fornecedor fornecedor) {
		try {
			cadastroFornecedorService.excluir(fornecedor, fornecedores);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<EstabelecimentoDTO> pesquisar(String nomeEmpresarial) {
		validarTamanhoNome(nomeEmpresarial);
		return fornecedores.porCnpjOuNomeEmpresarial(nomeEmpresarial);
	}
	
	@PostMapping("/produto")
	public ModelAndView adicionarProduto(Long codigoObject, String uuid) {
		Produto produto = produtos.findByCodigo(codigoObject);
		tabelaProdutosFornecedor.adicionarProduto(uuid, produto);
		return mvTabelaProdutosFornecedor(uuid);
	}
	

	@DeleteMapping("/produto/{uuid}/{codigoObject}")
	public ModelAndView excluirProduto(@PathVariable("codigoObject") Produto produto, @PathVariable("uuid") String uuid){
		tabelaProdutosFornecedor.excluirProduto(uuid, produto);
		return mvTabelaProdutosFornecedor(uuid);
	}

	private ModelAndView mvTabelaProdutosFornecedor(String uuid) {
		ModelAndView mv = new ModelAndView("fornecedor/TabelaProdutosFornecedor");
		mv.addObject("produtos", tabelaProdutosFornecedor.getProdutos(uuid));
		return mv;
	}
	
	private void setUuid(Fornecedor fornecedor) {
		if (StringUtils.isEmpty(fornecedor.getUuid())) {
			fornecedor.setUuid(UUID.randomUUID().toString());
		}
	}
	
	private void validarTamanhoNome(String nome) {
		if (StringUtils.isEmpty(nome) || nome.length() < 3) {
			throw new IllegalArgumentException();
		}
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Void> tratarIllegalArgumentException(IllegalArgumentException e) {
		return ResponseEntity.badRequest().build();
	}
}
