package br.com.yahtosistemas.controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.FaturamentoPorMes;
import br.com.yahtosistemas.dto.VendaDTO;
import br.com.yahtosistemas.dto.VendaMes;
import br.com.yahtosistemas.model.ItemVenda;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.TipoPagamento;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.repository.Clientes;
import br.com.yahtosistemas.repository.Vendas;
import br.com.yahtosistemas.repository.filter.VendaFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroVendaService;
import br.com.yahtosistemas.service.exception.DataEntregaVendaObrigatorioException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorDinheiroEValorCartaoIncorretosException;
import br.com.yahtosistemas.service.exception.ValorTotalItemVendaNaoPodeSerNegativoException;
import br.com.yahtosistemas.session.TabelasItensSession;

@Controller
@RequestMapping("/{tenantid}/vendas")
public class VendasController {

	@Autowired
	private Vendas vendas;
	
	@Autowired
	private CadastroVendaService cadastroVendaService;
	
	@Autowired
	private Clientes clientes;
	
	@Autowired
	private TabelasItensSession tabelaItens;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, VendaFilter vendaFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema,
			@PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("venda/PesquisaVendas");
			mv.addObject("todosStatus", StatusVenda.values());
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Venda> paginaWrapper = new PageWrapper<>(vendas.filtrar(vendaFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/pendentes")
	public ModelAndView buscarVendasPendentes(@PathVariable String tenantid, VendaFilter vendaFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("venda/PesquisaVendasPendentes");
			mv.addObject("tenantid", tenantid);
			mv.addObject("vendas", vendas.findByTenantIdAndStatusAndCliente_NomeEmpresarialContaining(
					tenantid, StatusVenda.PENDENTE, vendaFilter.getNomeCliente() == null ? "" : vendaFilter.getNomeCliente()));
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PutMapping("/pendentes")
	@ResponseStatus(HttpStatus.OK)
	public void atualizarStatus(@RequestParam("codigos[]") Long[] codigos, @RequestParam("situacao") TipoPagamento tipoPagamento) {
		cadastroVendaService.finalizarPendentes(codigos, tipoPagamento);
	}
	
	@GetMapping("/nova")
	public ModelAndView nova(@PathVariable String tenantid, Venda venda, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("venda/CadastroVenda");
			
			setUuid(venda);
			setClienteBalcao(venda, tenantid);
			setTipoPagamento(venda);
			
			mv.addObject("itens", venda.getItens());
			mv.addObject("valorFrete", venda.getValorFrete());
			mv.addObject("valorDesconto", venda.getValorDesconto());
			mv.addObject("valorAcrescimo", venda.getValorAcrescimo());
			mv.addObject("valorTotalItens", tabelaItens.getValorTotal(venda.getUuid()));
			mv.addObject("tiposPagamentos", TipoPagamento.values());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, @AuthenticationPrincipal UsuarioSistema usuarioSistema,
			HttpServletRequest request) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {				
			Venda venda = vendas.buscarComItens(codigo);
			if (venda != null) {				
				setUuid(venda);
				venda.setDetalhes(!request.isUserInRole("CADASTRAR_VENDA") ||
						!venda.getStatus().equals(StatusVenda.EM_ANDAMENTO));
				
				venda.getItens().forEach(i -> tabelaItens.adicionarItem(venda.getUuid(), i, i.getQuantidade()));
				
				ModelAndView mv = nova(tenantid, venda, usuarioSistema);
				mv.addObject(venda);
				return mv;
			}
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping(value ={ "/nova", "{\\+d}" }, params = "salvarEmAndamento")
	public ModelAndView salvarEmAndamento(@Valid Venda venda, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		return rotinaSalvar(venda, result, usuarioSistema, attributes, StatusVenda.EM_ANDAMENTO);
	}

	
	@PostMapping(value ={ "/nova", "{\\+d}" }, params = "salvarPendente")
	public ModelAndView salvarPendente(@Valid Venda venda, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		return rotinaSalvar(venda, result, usuarioSistema, attributes, StatusVenda.PENDENTE);
	}
	
	@PostMapping(value ={ "/nova", "{\\+d}" }, params = "salvarFinalizar")
	public ModelAndView salvarFinalizar(@Valid Venda venda, BindingResult result, RedirectAttributes attributes, 
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		return rotinaSalvar(venda, result, usuarioSistema, attributes, StatusVenda.FINALIZADA);
	}
	
	private ModelAndView rotinaSalvar(Venda venda, BindingResult result, UsuarioSistema usuarioSistema, RedirectAttributes attributes, StatusVenda status) {
		
		venda.adicionarItens(tabelaItens.getItens(venda.getUuid()));
		venda.calcularValorTotal();
		
		if (result.hasErrors()) {
			return nova(usuarioSistema.getUsuario().getTenantId(), venda, usuarioSistema);
		}
		
		venda.setUsuario(usuarioSistema.getUsuario());
		
		try {			
			cadastroVendaService.salvar(venda, vendas, status);
		} catch (DataEntregaVendaObrigatorioException e) {
			result.rejectValue("dataEntrega", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), venda, usuarioSistema);
		} catch (ProdutoObrigatorioException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), venda, usuarioSistema);
		} catch (ValorTotalItemVendaNaoPodeSerNegativoException e) {
			result.rejectValue("valorTotal", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), venda, usuarioSistema);
		}  catch (ValorDinheiroEValorCartaoIncorretosException e) {
			result.rejectValue("valorTotal", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), venda, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Venda salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/vendas/%s", usuarioSistema.getUsuario().getTenantId(), venda.getCodigo()));
	}
	
	@PostMapping(value = "/{codigo}", params = "cancelar")
	public ModelAndView cancelar(Venda venda, BindingResult result
				, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		try {
			cadastroVendaService.cancelar(venda, vendas);
		} catch (AccessDeniedException e) {
			return new ModelAndView("error").addObject("status", 403);
		}
		
		attributes.addFlashAttribute("mensagem", "Venda cancelada com sucesso");
		return new ModelAndView(String.format("redirect:/%s/vendas/%s", usuarioSistema.getUsuario().getTenantId(), venda.getCodigo()));
	}
	
	@GetMapping("/fechar-caixa")
	public @ResponseBody VendaDTO buscarVendasFecharCaixa(String dataHoraInicio) {
		return vendas.buscarValoresFecharCaixa(LocalDateTime.parse(dataHoraInicio));
	}
	
	@PostMapping("/item")
	public ModelAndView adicionarItem(Long codigoProduto, String uuid) {
		tabelaItens.adicionarItem(uuid, new ItemVenda(), 1);
		return mvTabelaItensVenda(uuid);
	}
	
	@PutMapping("/item/{codigoProduto}")
	public ModelAndView alterarQuantidadeItem(@PathVariable("codigoProduto") Produto produto
			, Integer quantidade, String uuid) {
		tabelaItens.alterarQuantidadeItens(uuid, produto, quantidade);
		return mvTabelaItensVenda(uuid);
	}
	
	@PutMapping("/item/valor-unitario/{codigoItem}")
	public ModelAndView alterarValorUnitarioItem(@PathVariable("codigoItem") Long codigoItem, BigDecimal valorUnitario, String uuid) {
		tabelaItens.alterarValorUnitarioItens(uuid, codigoItem, valorUnitario);
		return mvTabelaItensVenda(uuid);
	}
	
	@DeleteMapping("/item/{uuid}/{codigoItem}")
	public ModelAndView excluirItem(@PathVariable("codigoItem") Long codigoItem
			, @PathVariable String uuid) {
		tabelaItens.excluirItem(uuid, codigoItem);
		return mvTabelaItensVenda(uuid);
	}
	
	@GetMapping("/totalPorMes")
	public @ResponseBody List<VendaMes> listarTotalVendaPorMes(@PathVariable String tenantid) {
		return vendas.totalPorMes(tenantid);
	}
	
	@GetMapping("/totalFaturamentoPorMes")
	public @ResponseBody List<FaturamentoPorMes> listarTotalFaturamentoPorMes(@PathVariable String tenantid) {
		return vendas.totalFaturamentoPorMes(tenantid);
	}
	
	private void setTipoPagamento(Venda venda) {
		if (venda.getValorCartao().doubleValue() != 0 && venda.getValorDinheiro().doubleValue() != 0) {
			venda.setTipoPagamento(TipoPagamento.DINHEIRO_E_CARTAO);
		} else if (venda.getValorCartao().doubleValue() != 0) {
			venda.setTipoPagamento(TipoPagamento.CARTAO);
		} else {
			venda.setTipoPagamento(TipoPagamento.DINHEIRO);
		}
	}
	
	private void setClienteBalcao(Venda venda, String tenantid) {
		if (venda.isNovo() && (venda.getCliente() == null || StringUtils.isEmpty(venda.getCliente().getCodigo()))) {
			venda.setCliente(clientes.findByTenantIdAndNomeEmpresarial(tenantid, "Atendimento no balcão"));
		}
	}
	
	private ModelAndView mvTabelaItensVenda(String uuid) {
		ModelAndView mv = new ModelAndView("venda/TabelaItensVenda");
		mv.addObject("itens", tabelaItens.getItens(uuid));
		mv.addObject("valorTotal", tabelaItens.getValorTotal(uuid));
		return mv;
	}
	
	private void setUuid(Venda venda) {
		if (StringUtils.isEmpty(venda.getUuid())) {
			venda.setUuid(UUID.randomUUID().toString());
		}
	}
}
