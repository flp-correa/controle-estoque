package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.repository.UnidadesMedida;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroUnidadeMedidaService;
import br.com.yahtosistemas.service.SituacaoUnidadeMedida;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.QuantidadeDoTipoDeUnidadeInvalidaException;
import br.com.yahtosistemas.service.unidadeMedida.FamiliaUnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@Controller
@RequestMapping("/{tenantid}/unidades-medida")
public class UnidadesMedidaController {

	@Autowired
	private UnidadesMedida unidadesMedida;
	
	@Autowired
	private CadastroUnidadeMedidaService cadastroUnidadeMedidaService;

	@Transactional(readOnly = true)
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("unidadeMedida/PesquisaUnidadesMedida");
			mv.addObject("tenantid", tenantid);
			mv.addObject("unidadesMedida", unidadesMedida.findAll());
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, UnidadeMedida unidadeMedida, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("unidadeMedida/CadastroUnidadeMedida");
			
			mv.addObject("unidadesDeMedida", TipoUnidadeMedida.values());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView detalhes(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				UnidadeMedida unidadeMedida = unidadesMedida.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), unidadeMedida, usuarioSistema);
				unidadeMedida.setDetalhes(true);	
				mv.addObject(unidadeMedida);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid UnidadeMedida unidadeMedida, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), unidadeMedida, usuarioSistema);
		}
		
		try {			
			cadastroUnidadeMedidaService.salvar(unidadeMedida, unidadesMedida);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("tipoUnidadeMedida", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), unidadeMedida, usuarioSistema);
		} catch (QuantidadeDoTipoDeUnidadeInvalidaException e) {
			result.rejectValue("quantidade", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), unidadeMedida, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Unidade de medida salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/unidades-medida/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@PutMapping("/situacao")
	@ResponseStatus(HttpStatus.OK)
	public void atualizarStatus(@RequestParam("codigos[]") Long[] codigos, @RequestParam("situacao") SituacaoUnidadeMedida situacaoUnidadeMedida) {
		cadastroUnidadeMedidaService.alterarSituacao(codigos, situacaoUnidadeMedida, unidadesMedida);
	}
	
	@RequestMapping(path = "/buscar-por-familia", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<UnidadeMedida> pesquisarCadastroMovtoEstoque(String familiaUnidadeMedida, @PathVariable String tenantid) {
		return unidadesMedida.findByTenantIdAndFamilia(tenantid, FamiliaUnidadeMedida.valueOf(familiaUnidadeMedida));
	}
}
