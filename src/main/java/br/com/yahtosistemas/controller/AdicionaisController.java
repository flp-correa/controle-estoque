package br.com.yahtosistemas.controller;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.Adicional;
import br.com.yahtosistemas.repository.Adicionais;
import br.com.yahtosistemas.repository.filter.AdicionalFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroAdicionalService;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;

@Controller
@RequestMapping("/{tenantid}/adicionais")
public class AdicionaisController {
	
	@Autowired
	private CadastroAdicionalService cadastroAdicionalService;
	
	@Autowired
	private Adicionais adicionais;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, AdicionalFilter adicionalFilter, HttpServletRequest httpServletRequest
			, @PageableDefault(size = 10) Pageable pageable, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("adicional/PesquisaAdicionais");
			
			PageWrapper<Adicional> paginaWrapper = new PageWrapper<>(adicionais.filtrar(adicionalFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}

	@RequestMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Adicional adicional, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("adicional/CadastroAdicional");
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {				
				Adicional adicional = adicionais.getOne(codigo);
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), adicional, usuarioSistema);
				mv.addObject(adicional);
				return mv;
			}
		} catch (EntityNotFoundException e) {}	
		return new ModelAndView("error").addObject("status", 404);	
	}

	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Adicional adicional, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), adicional, usuarioSistema);
		}
		
		try {
			cadastroAdicionalService.salvar(adicional);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), adicional, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Adicional salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/adicionais/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Adicional adicional) {
		try {
			cadastroAdicionalService.excluir(adicional);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
}
