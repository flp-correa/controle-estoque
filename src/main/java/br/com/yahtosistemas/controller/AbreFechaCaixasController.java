package br.com.yahtosistemas.controller;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.controller.validator.AbreFechaCaixaValidator;
import br.com.yahtosistemas.model.AbreFechaCaixa;
import br.com.yahtosistemas.model.OpcaoAbreFechaCaixa;
import br.com.yahtosistemas.repository.AbreFechaCaixas;
import br.com.yahtosistemas.repository.filter.AbreFechaCaixaFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroAbreFechaCaixaService;

@Controller
@RequestMapping("/{tenantid}/abre-fecha-caixas")
public class AbreFechaCaixasController {

	@Autowired
	private CadastroAbreFechaCaixaService cadastroAbreFechaCaixaService;
	
	@Autowired
	private AbreFechaCaixas abreFechaCaixas;
	
	@Autowired
	private AbreFechaCaixaValidator abreFechaCaixaValidator;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, AbreFechaCaixaFilter AbreFechaCaixaFilter, HttpServletRequest httpServletRequest
			, @PageableDefault(size = 10) Pageable pageable, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("abreFechaCaixa/PesquisaAbreFechaCaixas");
			
			PageWrapper<AbreFechaCaixa> paginaWrapper = new PageWrapper<>(abreFechaCaixas.filtrar(AbreFechaCaixaFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, AbreFechaCaixa abreFechaCaixa, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			
			if (abreFechaCaixa.isNovo()) {				
				AbreFechaCaixa caixa = abreFechaCaixas.findByTenantIdAndDataHoraFimIsNull(tenantid);
				if (caixa != null) {
					return new ModelAndView(String.format("redirect:/%s/abre-fecha-caixas/%s", usuarioSistema.getUsuario().getTenantId(), caixa.getCodigo()));
				}
			}
			
			ModelAndView mv = new ModelAndView("abreFechaCaixa/CadastroAbreFechaCaixa");
			mv.addObject("opcoesAbreFechaCaixa", OpcaoAbreFechaCaixa.values());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {				
				AbreFechaCaixa abreFechaCaixa = abreFechaCaixas.getOne(codigo);
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), abreFechaCaixa, usuarioSistema);
				abreFechaCaixa.setDetalhes(abreFechaCaixa.getDataHoraFim() != null);
				mv.addObject(abreFechaCaixa);
				return mv;
			}
		} catch (EntityNotFoundException e) {}	
		return new ModelAndView("error").addObject("status", 404);	
	}

	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(AbreFechaCaixa abreFechaCaixa, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		abreFechaCaixaValidator.validate(abreFechaCaixa, result);
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), abreFechaCaixa, usuarioSistema);
		}
		
		cadastroAbreFechaCaixaService.salvar(abreFechaCaixa, abreFechaCaixas);
		
		attributes.addFlashAttribute("mensagem", "Abrir/fechar caixa salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/abre-fecha-caixas/%s", usuarioSistema.getUsuario().getTenantId(), abreFechaCaixa.getCodigo()));
	}
}
