package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.Clientes;
import br.com.yahtosistemas.repository.Estados;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroClienteService;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;

@Controller
@RequestMapping("/{tenantid}/clientes")
public class ClientesController {
	
	@Autowired
	private Clientes clientes;
	
	@Autowired
	private CadastroClienteService cadastroClienteService;
	
	@Autowired
	private Estados estados;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, EstabelecimentoFilter estabelecimentoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("cliente/PesquisaClientes");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Cliente> paginaWrapper = new PageWrapper<>(clientes.filtrar(estabelecimentoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}

	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Cliente cliente, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("cliente/CadastroCliente");
			
			mv.addObject("estados", estados.findAll());
			mv.addObject("unidadesEmpresas", UnidadeEstabelecimento.values());
			mv.addObject("tiposPessoa", TipoPessoa.values());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Cliente cliente = clientes.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), cliente, usuarioSistema);
				cliente.setDetalhes(cliente.getNomeEmpresarial().equals("Atendimento no balcão"));
				
				mv.addObject(cliente);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Cliente cliente, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), cliente, usuarioSistema);
		}
		
		try {			
			cadastroClienteService.salvar(cliente, clientes);
		} catch (EstabelecimentoJaCadastradaUnidadeMatrizException e) {
			result.rejectValue("unidadeEmpresa", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), cliente, usuarioSistema);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("cnpj", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), cliente, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Cliente salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/clientes/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Cliente cliente) {
		try {
			cadastroClienteService.excluir(cliente, clientes);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<EstabelecimentoDTO> pesquisar(String nomeEmpresarial) {
		validarTamanhoNome(nomeEmpresarial);
		return clientes.porCnpjOuNomeEmpresarial(nomeEmpresarial);
	}
	
	private void validarTamanhoNome(String nome) {
		if (StringUtils.isEmpty(nome) || nome.length() < 3) {
			throw new IllegalArgumentException();
		}
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Void> tratarIllegalArgumentException(IllegalArgumentException e) {
		return ResponseEntity.badRequest().build();
	}
}
