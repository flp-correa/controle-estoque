package br.com.yahtosistemas.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.Adicional;
import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.ItemVendaPedidoVenda;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.StatusItemCozinha;
import br.com.yahtosistemas.model.StatusPedidoVenda;
import br.com.yahtosistemas.model.TipoPedidoVenda;
import br.com.yahtosistemas.repository.Adicionais;
import br.com.yahtosistemas.repository.Clientes;
import br.com.yahtosistemas.repository.Ingredientes;
import br.com.yahtosistemas.repository.ItensPedidosVendas;
import br.com.yahtosistemas.repository.Mesas;
import br.com.yahtosistemas.repository.PedidosVendas;
import br.com.yahtosistemas.repository.filter.PedidoVendaFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroItemPedidoVendaService;
import br.com.yahtosistemas.service.CadastroPedidoVendaService;
import br.com.yahtosistemas.service.exception.DataEntregaVendaObrigatorioException;
import br.com.yahtosistemas.service.exception.ErroSalvarPedidoVendaException;
import br.com.yahtosistemas.service.exception.ImpossivelCancelarPedidoVendaException;
import br.com.yahtosistemas.service.exception.ItemLancheOuPratoEntregueObrigatorioException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorTotalItemVendaNaoPodeSerNegativoException;
import br.com.yahtosistemas.session.TabelasItensSession;

@Controller
@RequestMapping("/{tenantid}/pedidos-vendas")
public class PedidosVendasController {

	@Autowired
	private PedidosVendas pedidosVendas;
	
	@Autowired
	private ItensPedidosVendas itensPedidosVendas;
	
	@Autowired
	private Mesas mesas;
	
	@Autowired
	private Clientes clientes;
	
	@Autowired
	private Ingredientes ingredientes;
	
	@Autowired
	private Adicionais adicionais;
	
	@Autowired
	private CadastroPedidoVendaService cadastroPedidoVendaService;
	
	@Autowired
	private CadastroItemPedidoVendaService cadastroItemPedidoVendaService;
	
	@Autowired
	private TabelasItensSession tabelaItens;
	
	@Autowired
    private SimpMessagingTemplate socketTemplate;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, PedidoVendaFilter pedidoVendaFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema,
			@PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("pedidoVenda/PesquisaPedidosVendas");
			mv.addObject("todosStatus", StatusPedidoVenda.values());
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<PedidoVenda> paginaWrapper = new PageWrapper<>(pedidosVendas.filtrar(pedidoVendaFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/painel")
	public ModelAndView pesquisarPedidosVendasPainel(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("pedidoVenda/PainelPesquisaPedidosVendas");
			mv.addObject("pedidosVendas", pedidosVendas.buscarPainelPedidos());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/garcons")
	public ModelAndView pesquisarPedidosVendasGarcons(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("pedidoVenda/PesquisaPedidosVendasGarcons");
			mv.addObject("pedidosVendas", pedidosVendas.buscarTodosPedidosParaGarcom());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/cozinha")
	public ModelAndView pesquisarPedidosVendasCozinha(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("pedidoVenda/PesquisaPedidosVendasCozinha");
			List<PedidoVenda> listaPedidosVendas = pedidosVendas.buscarPedidosParaCozinha(null);
			
			cadastroItemPedidoVendaService.salvarLoadCozinha(listaPedidosVendas);
			
			listaPedidosVendas.forEach((p) -> {
				setUuid(p);
				p.getItens().forEach(i -> i.restaurarIngredientesOriginal());
				Collections.sort(p.getItens());
			});
			
			mv.addObject("pedidosVendas", listaPedidosVendas);
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/buscar-cozinha/{codigoPedidoVenda}")
	public ModelAndView pesquisarPedidosVendasCozinhaLoop(@PathVariable String tenantid, @PathVariable Long codigoPedidoVenda,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {		
			
			ModelAndView mv = new ModelAndView("pedidoVenda/ContainerItensPedidoVendaCozinha");
			List<PedidoVenda> listaPedidosVendas = pedidosVendas.buscarPedidosParaCozinha(codigoPedidoVenda);
			cadastroItemPedidoVendaService.salvarLoadCozinha(listaPedidosVendas);
			
			listaPedidosVendas.forEach((p) -> {
				setUuid(p);
				p.getItens().forEach((i) -> {
					i.restaurarIngredientesOriginal();
					i.setUuid(p.getUuid());
				});
				Collections.sort(p.getItens());
			});
			
			mv.addObject("pedidosVendas", listaPedidosVendas);
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, PedidoVenda pedidoVenda, 
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("pedidoVenda/CadastroPedidoVenda");
			
			setUuid(pedidoVenda);
			setClienteBalcao(pedidoVenda, tenantid);
			
			mv.addObject("itens", pedidoVenda.getItens());
			mv.addObject("valorFrete", pedidoVenda.getValorFrete());
			mv.addObject("valorTotalItens", tabelaItens.getValorTotal(pedidoVenda.getUuid()));
			mv.addObject("tipoPedidoVenda", TipoPedidoVenda.values());
			mv.addObject("tenantid", tenantid);
			
			if (pedidoVenda.isNovo()) {
				mv.addObject("mesas", mesas.findByDepositoOrderByNrMesaAsc(usuarioSistema.getUsuario().getDeposito()));
			}
			
			return mv;
		}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
				@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest request) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {				
			PedidoVenda pedidoVenda = pedidosVendas.buscarComItens(codigo);
			if (pedidoVenda != null) {					
				if (pedidoVenda.getCliente() == null) {
					pedidoVenda.setTipoPedidoVenda(TipoPedidoVenda.PEDIDO_MESA);
				} else {
					pedidoVenda.setTipoPedidoVenda(TipoPedidoVenda.PEDIDO_CLIENTE);
				}
				pedidoVenda.setDetalhes(!request.isUserInRole("CADASTRAR_PEDIDO_VENDA") ||
						!pedidoVenda.getStatus().equals(StatusPedidoVenda.EM_ANDAMENTO));
				
				setUuid(pedidoVenda);
				List<Adicional> listaAdicionais = adicionais.findByTenantId(tenantid);
				
				pedidoVenda.getItens().forEach(i -> {
					i.setUuid(pedidoVenda.getUuid());
					i.restaurarIngredientesOriginal();
					i.restaurarAdicionaisOriginal(listaAdicionais);
					i.ordenarIngredientesItemPorNome();
					i.ordenarAdicionaisItemPorNome();
					tabelaItens.adicionarItem(pedidoVenda.getUuid(), i, i.getQuantidade());
				});
				Collections.sort(pedidoVenda.getItens());
				ModelAndView mv = novo(tenantid, pedidoVenda, usuarioSistema, request);
				mv.addObject(pedidoVenda);
				return mv;
			}
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping(value ={ "/novo", "{\\+d}" }, params = "salvarFinalizar")
	public ModelAndView salvarEFinalizar(@Valid PedidoVenda pedidoVenda, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		pedidoVenda.adicionarItens(tabelaItens.getItens(pedidoVenda.getUuid()));
		pedidoVenda.calcularValorTotal();
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		}
		
		pedidoVenda.setUsuario(usuarioSistema.getUsuario());
		
		Long codigoVenda = null;
		try {			
			codigoVenda = cadastroPedidoVendaService.salvar(pedidoVenda, StatusPedidoVenda.FINALIZADO, pedidosVendas, usuarioSistema.getUsuario());
		} catch (DataEntregaVendaObrigatorioException e) {
			result.rejectValue("dataEntrega", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ProdutoObrigatorioException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ValorTotalItemVendaNaoPodeSerNegativoException e) {
			result.rejectValue("valorTotal", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ItemLancheOuPratoEntregueObrigatorioException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ErroSalvarPedidoVendaException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		}
		
		//attributes.addFlashAttribute("mensagem", "Pedido de venda salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/vendas/%s", usuarioSistema.getUsuario().getTenantId(), codigoVenda));
	}
	
	@PostMapping(value ={ "/novo", "{\\+d}" }, params = "salvarEmAndamento")
	public ModelAndView salvarEManterEmAndamento(@Valid PedidoVenda pedidoVenda, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		
		pedidoVenda.adicionarItens(tabelaItens.getItens(pedidoVenda.getUuid()));
		pedidoVenda.calcularValorTotal();
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		}
		
		pedidoVenda.setUsuario(usuarioSistema.getUsuario());
		
		try {			
			cadastroPedidoVendaService.salvar(pedidoVenda, StatusPedidoVenda.EM_ANDAMENTO, pedidosVendas, usuarioSistema.getUsuario());
		} catch (DataEntregaVendaObrigatorioException e) {
			result.rejectValue("dataEntrega", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ProdutoObrigatorioException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ValorTotalItemVendaNaoPodeSerNegativoException e) {
			result.rejectValue("valorTotal", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (ErroSalvarPedidoVendaException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		}
		
		if (pedidoVenda.getItens().stream().filter(i -> i.getProduto().getEnviarCozinha() && !i.getLoadCozinha()).findFirst().isPresent()) {
			socketTemplate.convertAndSend(String.format("/enviar-cozinha/%s", usuarioSistema.getUsuario().getTenantId()),
					pedidoVenda.getCodigo());
		}
		attributes.addFlashAttribute("mensagem", "Pedido de venda salvo com sucesso");
		if (httpServletRequest.isUserInRole("ADMINISTRADOR")) {
			return new ModelAndView(String.format("redirect:/%s/pedidos-vendas/%s", usuarioSistema.getUsuario().getTenantId(), pedidoVenda.getCodigo()));
		} else {
			return new ModelAndView(String.format("redirect:/%s/pedidos-vendas/novo", usuarioSistema.getUsuario().getTenantId()));
		}
	}

	@PostMapping(value = "/{codigo}", params = "cancelar")
	public ModelAndView cancelar(PedidoVenda pedidoVenda, BindingResult result, RedirectAttributes attributes, 
			HttpServletRequest httpServletRequest, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		pedidoVenda.adicionarItens(tabelaItens.getItens(pedidoVenda.getUuid()));
		try {
			cadastroPedidoVendaService.cancelar(pedidoVenda.getCodigo(), pedidosVendas);
		} catch (ImpossivelCancelarPedidoVendaException e) {
			result.rejectValue("", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), pedidoVenda, usuarioSistema, httpServletRequest);
		} catch (AccessDeniedException e) {
			return new ModelAndView("error").addObject("status", 403);
		}
		
		attributes.addFlashAttribute("mensagem", "Pedido de venda cancelado com sucesso");
		return new ModelAndView(String.format("redirect:/%s/pedidos-vendas/%s", usuarioSistema.getUsuario().getTenantId(), pedidoVenda.getCodigo()));
	}
	
	@PostMapping("/finalizar/{codigo}")
	public @ResponseBody Long finalizarPedidoVendaECriarVenda(@PathVariable("codigo") Long codigo, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		return cadastroPedidoVendaService.finalizarPedidoVendaECriarVenda(codigo, pedidosVendas, usuarioSistema.getUsuario());
	}
	
	@PostMapping("/cancelar/{codigo}")
	public @ResponseBody ResponseEntity<?> cancelarPedidoVenda(@PathVariable("codigo") Long codigo, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		cadastroPedidoVendaService.cancelar(codigo, pedidosVendas);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping("/status-cozinha")
	@ResponseStatus(HttpStatus.OK)
	public void alterarStatusCozinhaPedidoVenda(String uuid, @RequestParam("codigos[]") Long[] codigos, 
			@RequestParam("statusCozinha") StatusItemCozinha statusCozinha) {
		
		if (uuid == null) {
			cadastroItemPedidoVendaService.salvarStatusItemCozinha(codigos, statusCozinha, itensPedidosVendas);
		} else {			
			tabelaItens.getItens(uuid).forEach((i) -> {
				if (Arrays.asList(codigos).contains(i.getCodigo())) {
					((ItemPedidoVenda)i).setStatusCozinha(statusCozinha);
				}
			});
		}
		
	}
	
	@GetMapping("/item/{uuid}/{codigoItem}")
	public @ResponseBody ItemVendaPedidoVenda editarItem(@PathVariable("codigoItem") Long codigo, @PathVariable String uuid) {
		return tabelaItens.getItens(uuid).stream().filter(i -> i.getCodigo().equals(codigo)).findFirst().get();
	}

	@PostMapping("/item")
	public ModelAndView adicionarItem(@RequestBody ItemPedidoVenda itemPedidoVenda) {
		tabelaItens.adicionarItem(itemPedidoVenda.getUuid(), itemPedidoVenda, 1);
		return mvTabelaItensVenda(itemPedidoVenda.getUuid());
	}
	
	@PutMapping("/item/quantidade/{codigoProduto}")
	public ModelAndView alterarQuantidadeItem(@PathVariable("codigoProduto") Produto produto
			, Integer quantidade, String uuid) {

		quantidade = validarQuantidadeItem(produto, uuid, quantidade);
		tabelaItens.alterarQuantidadeItens(uuid, produto, quantidade);
		return mvTabelaItensVenda(uuid);
	}
	
	private Integer validarQuantidadeItem(Produto produto, String uuid, Integer quantidade) {
		ItemVendaPedidoVenda item = tabelaItens.getItens(uuid).stream().filter(i -> i.getProduto().equals(produto)).findFirst().get();
		
		if (item.getCodigo() > 0) {
			Integer qtdSalvo = itensPedidosVendas.getOne(item.getCodigo()).getQuantidade();  
			if (qtdSalvo > quantidade) {
				quantidade = qtdSalvo;
			}
		}
		return quantidade;
	}
	
	@PutMapping("/item/valor-unitario")
	public ModelAndView alterarValorUnitarioItem(Long codigoItem, BigDecimal valorUnitario, String uuid) {
		tabelaItens.alterarValorUnitarioItens(uuid, codigoItem, valorUnitario);
		return mvTabelaItensVenda(uuid);
	}
	
	/*@PutMapping("/item/atualizar-ingredientes")
	@ResponseStatus(HttpStatus.OK)
	public void atualizarIngredientes(@RequestParam("codigos[]") Long[] codigos, @RequestParam("situacao") SituacaoUnidadeMedida situacaoUnidadeMedida) {
		cadastroUnidadeMedidaService.alterarSituacao(codigos, situacaoUnidadeMedida, unidadesMedida);
	}*/
	
	@DeleteMapping("/item/{uuid}/{codigoItem}")
	public ModelAndView excluirItem(@PathVariable("codigoItem") Long codigoItem
			, @PathVariable String uuid) {
		if (codigoItem > 0) {
			StatusItemCozinha statusItemCozinha = itensPedidosVendas.buscarStatusCozinhaItemPedidoVenda(codigoItem).getStatusCozinha();
			if (statusItemCozinha != null && (statusItemCozinha.equals(StatusItemCozinha.INICIADO) || statusItemCozinha.equals(StatusItemCozinha.PRONTO))) {
				((ItemPedidoVenda)tabelaItens.getItens(uuid)
						.stream()
						.filter(i -> i.getCodigo().equals(codigoItem))
						.findFirst()
						.get()).setStatusCozinha(statusItemCozinha);
				
				return mvTabelaItensVenda(uuid);
			}
		}
		tabelaItens.excluirItem(uuid, codigoItem);
		return mvTabelaItensVenda(uuid);
	}
	
	@GetMapping("/ingredientes")
	public ModelAndView listarIngredientesPeloProduto(String uuid, Long codigoItemPedidoVenda, Long codigoProduto) {
		ModelAndView mv = new ModelAndView("pedidoVenda/TabelaIngredientesItemPedidoVenda");
		Optional<ItemVendaPedidoVenda> itemPedidoVenda = tabelaItens.getItens(uuid).stream().filter(i -> i.getCodigo().equals(codigoItemPedidoVenda)).findFirst();
		if (itemPedidoVenda.isPresent()) {
			mv.addObject("ingredientes", ((ItemPedidoVenda)itemPedidoVenda.get()).getIngredientes());
			mv.addObject("prontoOuEntregue", 
					((ItemPedidoVenda)itemPedidoVenda.get()).getStatusCozinha().equals(StatusItemCozinha.PRONTO) ||
					((ItemPedidoVenda)itemPedidoVenda.get()).getStatusCozinha().equals(StatusItemCozinha.ENTREGUE));
		} else {			
			mv.addObject("ingredientes", ingredientes.listaIngredientesPorProduto(codigoProduto)
					.stream().sorted(Comparator.comparing(Ingrediente::getNome)).collect(Collectors.toList()));
			mv.addObject("prontoOuEntregue", Boolean.FALSE);
		}
		return mv;
	}
	
	@GetMapping("/adicionais")
	public ModelAndView listarAdicionaisPeloProduto(@PathVariable String tenantid, String uuid, Long codigoItemPedidoVenda) {
		ModelAndView mv = new ModelAndView("pedidoVenda/TabelaAdicionaisItemPedidoVenda");
		Optional<ItemVendaPedidoVenda> itemPedidoVenda = tabelaItens.getItens(uuid).stream().filter(i -> i.getCodigo().equals(codigoItemPedidoVenda)).findFirst();
		if (itemPedidoVenda.isPresent()) {
			mv.addObject("adicionais", ((ItemPedidoVenda)itemPedidoVenda.get()).getAdicionais());
			mv.addObject("prontoOuEntregue", 
					((ItemPedidoVenda)itemPedidoVenda.get()).getStatusCozinha().equals(StatusItemCozinha.PRONTO) ||
					((ItemPedidoVenda)itemPedidoVenda.get()).getStatusCozinha().equals(StatusItemCozinha.ENTREGUE));
		} else {			
			mv.addObject("adicionais", adicionais.findByTenantId(tenantid)
					.stream().sorted(Comparator.comparing(Adicional::getNome)).collect(Collectors.toList()));
			mv.addObject("prontoOuEntregue", Boolean.FALSE);
		}
		return mv;
	}
	
	private void setClienteBalcao(PedidoVenda pedidoVenda, String tenantId) {
		if (pedidoVenda.isNovo() && (pedidoVenda.getCliente() == null || StringUtils.isEmpty(pedidoVenda.getCliente().getCodigo()))) {
			pedidoVenda.setCliente(clientes.findByTenantIdAndNomeEmpresarial(tenantId, "Atendimento no balcão"));
		}
	}
	
	private ModelAndView mvTabelaItensVenda(String uuid) {
		ModelAndView mv = new ModelAndView("pedidoVenda/TabelaItensPedidoVenda");
		mv.addObject("itens", tabelaItens.getItens(uuid));
		mv.addObject("valorTotal", tabelaItens.getValorTotal(uuid));
		return mv;
	}
	
	private void setUuid(PedidoVenda pedidoVenda) {
		if (StringUtils.isEmpty(pedidoVenda.getUuid())) {
			pedidoVenda.setUuid(UUID.randomUUID().toString());
		}
	}
}
