package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.DepositoDTO;
import br.com.yahtosistemas.model.Deposito;
import br.com.yahtosistemas.repository.Depositos;
import br.com.yahtosistemas.repository.filter.DepositoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroDepositoService;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;

@Controller
@RequestMapping("/{tenantid}/depositos")
public class DepositosController {
	
	@Autowired
	private Depositos depositos;
	
	@Autowired
	private CadastroDepositoService cadastroDepositoService;

	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, DepositoFilter depositoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("deposito/PesquisaDepositos");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Deposito> paginaWrapper = new PageWrapper<>(depositos.filtrar(depositoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Deposito deposito, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("deposito/CadastroDeposito");
			
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Deposito deposito = depositos.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), deposito, usuarioSistema);
				
				mv.addObject(deposito);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Deposito deposito, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), deposito, usuarioSistema);
		}
		
		cadastroDepositoService.salvar(deposito, depositos);
		attributes.addFlashAttribute("mensagem", "Depósito salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/depositos/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Deposito deposito) {
		try {
			cadastroDepositoService.excluir(deposito, depositos);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<DepositoDTO> pesquisar(String descricao) {
		return depositos.porDescricaoPesquisaRapida(descricao);
	}
}
