package br.com.yahtosistemas.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.yahtosistemas.repository.Vendas;
import br.com.yahtosistemas.security.UsuarioSistema;

/**
 * 
 * @author Felipe Corrêa
 * 
 * Empresa: Yahto Sistemas
 * 
 */
@Controller
@RequestMapping("/")
public class DashboardController {
	
	@Autowired
	private Vendas vendas;
	
	@GetMapping()
	public ModelAndView dashboardPosLogin(@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		return new ModelAndView(String.format("redirect:/%s", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@GetMapping("/{tenantid}")
	public ModelAndView dashboard(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema, HttpServletRequest httpServletRequest) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv;
			if (httpServletRequest.isUserInRole("ADMINISTRADOR")) {
				mv = new ModelAndView("DashboardAdministrador");
				mv.addObject("vendasNoAno", vendas.valorTotalNoAno());
				mv.addObject("vendasNoMes", vendas.valorTotalNoMes());
				mv.addObject("ticketMedio", vendas.valorTicketMedioNoAno());
			} else {
				mv = new ModelAndView("Dashboard");
			}
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
}