package br.com.yahtosistemas.controller;

import javax.validation.Valid;

import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.yahtosistemas.dto.PeriodoRelatorio;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.RelatorioService;

@Controller
@RequestMapping("/{tenantid}/relatorios")
public class RelatoriosController {

	@Autowired
	private RelatorioService relatorioService;
	
	@GetMapping("/vendas-finalizadas")
	public ModelAndView relatorioVendasFinalizadas(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("relatorio/RelatorioVendasFinalizadas");
			mv.addObject(new PeriodoRelatorio());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@PostMapping("/vendas-finalizadas")
	public ResponseEntity<byte[]> gerarRelatorioVendasFinalizadas(@Valid PeriodoRelatorio periodoRelatorio, 
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) throws Exception {
		
		periodoRelatorio.setTenantId(usuarioSistema.getUsuario().getTenantId());
		byte[] relatorio = relatorioService.gerarRelatorioVendasFinalizadas(periodoRelatorio); 
		
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@GetMapping("/vendas-finalizadas-por-produto")
	public ModelAndView relatorioVendasFinalizadasPorProduto(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("relatorio/RelatorioVendasFinalizadasPorProduto");
			mv.addObject(new PeriodoRelatorio());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@PostMapping("/vendas-finalizadas-por-produto")
	public ResponseEntity<byte[]> gerarRelatorioVendasFinalizadasPorProduto(@Valid PeriodoRelatorio periodoRelatorio, 
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) throws Exception {
		
		periodoRelatorio.setTenantId(usuarioSistema.getUsuario().getTenantId());
		byte[] relatorio = relatorioService.gerarRelatorioVendasFinalizadasPorProduto(periodoRelatorio);
		
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@GetMapping("/compras-produtos")
	public ModelAndView relatorioProdutosAbaixoEstoqueMinimo(@PathVariable String tenantid, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("relatorio/RelatorioComprasProdutos");
			mv.addObject(new PeriodoRelatorio());
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@PostMapping("/compras-produtos")
	public ResponseEntity<byte[]> gerarRelatorioProdutosAbaixoEstoqueMinimo(@AuthenticationPrincipal UsuarioSistema usuarioSistema) throws Exception {
		
		byte[] relatorio = relatorioService.gerarRelatorioComprasProdutos(usuarioSistema.getUsuario().getTenantId());
		
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
}
