package br.com.yahtosistemas.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.yahtosistemas.security.UsuarioSistema;

/**
 * 
 * @author Felipe Corrêa
 * 
 * Empresa: Yahto Sistemas
 * 
 */
@Controller
public class SegurancaController {

	@GetMapping("/login")
	public String login(@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema != null) {
			return "redirect:/" + usuarioSistema.getUsuario().getTenantId();
		}
		
		return "Login";
	}
	
	@GetMapping("/403")
	public String acessoNegado() {
		return "403";
	}
}
