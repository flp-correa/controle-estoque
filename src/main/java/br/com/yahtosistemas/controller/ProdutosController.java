package br.com.yahtosistemas.controller;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.ProdutoDTO;
import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.repository.Ingredientes;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.repository.Produtos;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.repository.filter.ProdutoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroLocalizacaoService;
import br.com.yahtosistemas.service.CadastroProdutoService;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.ObrigatorioAdicionarIngredientesParaProdutoException;
import br.com.yahtosistemas.service.exception.ObrigatorioAdicionarLocalizacoesParaProdutoException;
import br.com.yahtosistemas.service.exception.QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;
import br.com.yahtosistemas.session.TabelaIngredientesProdutoSession;
import br.com.yahtosistemas.session.TabelaLocalizacoesProdutoSession;

@Controller
@RequestMapping("/{tenantid}/produtos")
public class ProdutosController {
	
	@Autowired
	private TabelaLocalizacoesProdutoSession tabelaLocalizacoesProduto;

	@Autowired
	private TabelaIngredientesProdutoSession tabelaIngredientesProduto;
	
	@Autowired
	private Produtos produtos;
	
	@Autowired
	private Localizacoes localizacoes;
	
	@Autowired
	private Ingredientes ingredientes;
	
	@Autowired
	private SaldosEstoque saldosEstoque;
	
	@Autowired
	private CadastroProdutoService cadastroProdutoService;
	
	@Autowired
	private CadastroLocalizacaoService cadastroLocalizacaoService;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, ProdutoFilter produtoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("produto/PesquisaProdutos");
			mv.addObject("tenantid", tenantid);
			
			produtoFilter.setVendedor(!httpServletRequest.isUserInRole("CADASTRAR_PRODUTO"));
			PageWrapper<Produto> paginaWrapper = new PageWrapper<>(produtos.filtrar(produtoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Produto produto, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("produto/CadastroProduto");
			
			if (produto.getLocalFixo()) {
				setUuid(produto);
				mv.addObject("localizacoes", produto.getLocalizacoes());
			}
			
			mv.addObject("tiposUnidadesDeMedida", Arrays.asList(TipoUnidadeMedida.UNID, TipoUnidadeMedida.KG, TipoUnidadeMedida.GRAMAS));
			mv.addObject("localizacoes", produto.getLocalizacoes());
			mv.addObject("ingredientes", produto.getIngredientes());
			mv.addObject("novoProduto", produto.isNovo());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Produto produto = produtos.getOne(codigo);
				produto.setQuantidadeEstoque(saldosEstoque.saldoTotalEstoqueProduto(produto));
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), produto, usuarioSistema);
				produto.setDetalhes(!request.isUserInRole("CADASTRAR_PRODUTO"));
					
				setUuid(produto);
				produto.getLocalizacoes().forEach((l )-> {
					l.setDetalhes(produto.isDetalhes() || !produto.getLocalFixo());
					l.setSaldoEstoque(saldosEstoque.saldoLocalizacaoEstoqueProtudo(produto, l));
					tabelaLocalizacoesProduto.adicionarLocalizacao(produto.getUuid(), l);
				});
				if (produto.getLancheOuPrato()) {
					produto.getIngredientes().forEach(p -> tabelaIngredientesProduto.adicionarIngrediente(produto.getUuid(), p));
				}
				mv.addObject("detalhes", !produto.getLocalFixo());
				mv.addObject(produto);
				return mv;
			}
		} catch (EntityNotFoundException e) {}	
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Produto produto, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		produto.setLocalizacoes(tabelaLocalizacoesProduto.getLocalizacoes(produto.getUuid()));
		produto.setIngredientes(tabelaIngredientesProduto.getIngredientes(produto.getUuid()));
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), produto, usuarioSistema);
		}
		
		try {			
			cadastroProdutoService.salvar(produto, produtos);
		} catch (ObrigatorioAdicionarLocalizacoesParaProdutoException e) {
			result.rejectValue("localizacoes", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), produto, usuarioSistema);
		}  catch (QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException e) {
			result.rejectValue("estoqueMinimo", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), produto, usuarioSistema);
		} catch (ObrigatorioAdicionarIngredientesParaProdutoException e) {
			result.rejectValue("ingredientes", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), produto, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Produto salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/produtos/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Produto produto) {
		try {
			cadastroProdutoService.excluir(produto, produtos);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/filtro-cadastro-fornecedor", method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> listarProdutos(String campoPesquisa) {
		return ResponseEntity.status(HttpStatus.OK).body(produtos.buscarPorDescricaoEPossuiEstoque(campoPesquisa));
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<ProdutoDTO> pesquisarCadastroMovtoEstoque(String descricao, boolean buscarPorFornecedor, Long codigoFornecedor, String tipoMovimento) {
		if (!buscarPorFornecedor) {
			validarTamanhoNome(descricao);
			codigoFornecedor = null;
		}
		return produtos.porDescricaoPesquisaRapidaCadastroMovtoEstoque(descricao, codigoFornecedor, TipoMovimentoEstoque.valueOf(tipoMovimento));
	}
	
	@RequestMapping(value = "/filtro-cadastro-pedido-ou-venda", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<ProdutoDTO> pesquisarCadastroPedidoVenda(String descricao, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		validarTamanhoNome(descricao);
		return produtos.buscarPorDescricaoEDepositoOuNaoPossuiEstoque(descricao, usuarioSistema.getUsuario().getDeposito());
	}
	
	@PostMapping("/localizacao")
	public ModelAndView adicionarLocalizacao(Long codigoLocalizacao, String uuid) {
		Localizacao localizacao = localizacoes.findByCodigo(codigoLocalizacao);
		tabelaLocalizacoesProduto.adicionarLocalizacao(uuid, localizacao);
		return mvTabelaLocalizacoesProduto(uuid, localizacao.getProduto() == null);
	}
	
	@PostMapping("/ingrediente")
	public ModelAndView adicionarIngrediente(Long codigoIngrediente, String uuid) {
		Ingrediente ingrediente = ingredientes.findByCodigo(codigoIngrediente);
		tabelaIngredientesProduto.adicionarIngrediente(uuid, ingrediente);
		return mvTabelaIngredientesProduto(uuid);
	}
	
	@RequestMapping(value = "/localizacao/validar-exclusao", method = RequestMethod.DELETE)
	public ResponseEntity<?> validarExclusaoLocalizacao(@RequestBody Produto produto) {
		if (!produto.isNovo()) {			
			try {
				cadastroLocalizacaoService.validarExcluirLocalizacaoDoProduto(produto);
			} catch (ImpossivelExcluirEntidadeException e) {
				return ResponseEntity.badRequest().body(e.getMessage());
			}
		}
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/localizacao/{uuid}/{codigoLocalizacao}")
	public ModelAndView excluirLocalizacao(@PathVariable("codigoLocalizacao") Localizacao localizacao, @PathVariable("uuid") String uuid){
		tabelaLocalizacoesProduto.excluirLocalizacao(uuid, localizacao);
		return mvTabelaLocalizacoesProduto(uuid, localizacao.getProduto() == null);
	}
	
	@DeleteMapping("/ingrediente/{uuid}/{codigoIngrediente}")
	public ModelAndView excluirIngrediente(@PathVariable("codigoIngrediente") Ingrediente ingrediente, @PathVariable("uuid") String uuid){
		tabelaIngredientesProduto.excluirIngrediente(uuid, ingrediente);
		return mvTabelaIngredientesProduto(uuid);
	}

	private ModelAndView mvTabelaLocalizacoesProduto(String uuid, boolean novoProduto) {
		ModelAndView mv = new ModelAndView("produto/TabelaLocalizacoesProduto");
		mv.addObject("localizacoes", tabelaLocalizacoesProduto.getLocalizacoes(uuid));
		mv.addObject("novoProduto", novoProduto);
		return mv;
	}
	
	private ModelAndView mvTabelaIngredientesProduto(String uuid) {
		ModelAndView mv = new ModelAndView("produto/TabelaIngredientesProduto");
		mv.addObject("ingredientes", tabelaIngredientesProduto.getIngredientes(uuid));
		return mv;
	}
	
	private void setUuid(Produto produto) {
		if (StringUtils.isEmpty(produto.getUuid())) {
			produto.setUuid(UUID.randomUUID().toString());
		}
	}
	
	private void validarTamanhoNome(String nome) {
		if (StringUtils.isEmpty(nome) || nome.length() < 3) {
			throw new IllegalArgumentException();
		}
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Void> tratarIllegalArgumentException(IllegalArgumentException e) {
		return ResponseEntity.badRequest().build();
	}
}
