package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.Ingrediente;
import br.com.yahtosistemas.repository.Ingredientes;
import br.com.yahtosistemas.repository.filter.IngredienteFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroIngredienteService;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;

@Controller
@RequestMapping("/{tenantid}/ingredientes")
public class IngredientesController {
	
	@Autowired
	private CadastroIngredienteService cadastroIngredienteService;
	
	@Autowired
	private Ingredientes ingredientes;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, IngredienteFilter ingredienteFilter, HttpServletRequest httpServletRequest
			, @PageableDefault(size = 10) Pageable pageable, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {			
			ModelAndView mv = new ModelAndView("ingrediente/PesquisaIngredientes");
			
			PageWrapper<Ingrediente> paginaWrapper = new PageWrapper<>(ingredientes.filtrar(ingredienteFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}

	@RequestMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Ingrediente ingrediente, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("ingrediente/CadastroIngrediente");
			mv.addObject("tenantid", tenantid);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {				
				Ingrediente ingrediente = ingredientes.getOne(codigo);
				ingrediente.setDetalhes(true);
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), ingrediente, usuarioSistema);
				mv.addObject(ingrediente);
				return mv;
			}
		} catch (EntityNotFoundException e) {}	
		return new ModelAndView("error").addObject("status", 404);	
	}

	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Ingrediente ingrediente, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), ingrediente, usuarioSistema);
		}
		
		try {
			cadastroIngredienteService.salvar(ingrediente);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), ingrediente, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Ingrediente salvo com sucesso");
		return new ModelAndView(String.format("redirect:/%s/ingredientes/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Ingrediente ingrediente) {
		try {
			cadastroIngredienteService.excluir(ingrediente);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/filtro-cadastro-produto", method = RequestMethod.GET)
	public ResponseEntity<List<Ingrediente>> listarIngredientes(String campoPesquisa, @PathVariable String tenantid) {
		return ResponseEntity.status(HttpStatus.OK).body(ingredientes.findByTenantIdAndNomeContaining(tenantid, campoPesquisa));
	}
}
