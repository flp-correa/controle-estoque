package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.repository.Mesas;
import br.com.yahtosistemas.repository.filter.MesaFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroMesaService;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;

@Controller
@RequestMapping("/{tenantid}/mesas")
public class MesasController {
	
	@Autowired
	private Mesas mesas;
	
	@Autowired
	private CadastroMesaService cadastroMesaService;

	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, MesaFilter mesaFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("mesa/PesquisaMesas");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Mesa> paginaWrapper = new PageWrapper<>(mesas.filtrar(mesaFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/nova")
	public ModelAndView nova(@PathVariable String tenantid, Mesa mesa, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("mesa/CadastroMesa");
			
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Mesa mesa = mesas.getOne(codigo);
				ModelAndView mv = nova(usuarioSistema.getUsuario().getTenantId(), mesa, usuarioSistema);
				mesa.setDetalhes(true);
					
				mv.addObject(mesa);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/nova", "{\\+d}" })
	public ModelAndView salvar(@Valid Mesa mesa, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return nova(usuarioSistema.getUsuario().getTenantId(), mesa, usuarioSistema);
		}
		
		try {			
			cadastroMesaService.salvar(mesa, mesas);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("nrMesa", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), mesa, usuarioSistema);
		} catch (EntidadeObrigatoriaException e) {
			result.rejectValue("deposito", e.getMessage(), e.getMessage());
			return nova(usuarioSistema.getUsuario().getTenantId(), mesa, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Mesa salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/mesas/nova", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@RequestMapping(path = "/trocar-de-mesa", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<Mesa> pesquisarParaTrocarDeMesa(String nrMesa, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		return mesas.findByNrMesaContainingAndDeposito(nrMesa, usuarioSistema.getUsuario().getDeposito());
	}
}
