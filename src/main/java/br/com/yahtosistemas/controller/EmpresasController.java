package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.EstabelecimentoDTO;
import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.Empresas;
import br.com.yahtosistemas.repository.Estados;
import br.com.yahtosistemas.repository.filter.EstabelecimentoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroEmpresaService;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;

@Controller
@RequestMapping("/{tenantid}/empresas")
public class EmpresasController {

	@Autowired
	private Empresas empresas;

	@Autowired
	private CadastroEmpresaService cadastroEmpresaService; 
	
	@Autowired
	private Estados estados;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, EstabelecimentoFilter estabelecimentoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("empresa/PesquisaEmpresas");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Empresa> paginaWrapper = new PageWrapper<>(empresas.filtrar(estabelecimentoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Empresa empresa, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("empresa/CadastroEmpresa");
			
			mv.addObject("estados", estados.findAll());
			mv.addObject("unidadesEmpresas", UnidadeEstabelecimento.values());
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Empresa empresa = empresas.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), empresa, usuarioSistema);
				
				mv.addObject(empresa);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Empresa empresa, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), empresa, usuarioSistema);
		}
		
		try {			
			cadastroEmpresaService.salvar(empresa, empresas);
		} catch (EstabelecimentoJaCadastradaUnidadeMatrizException e) {
			result.rejectValue("unidadeEmpresa", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), empresa, usuarioSistema);
		} catch (EntidadeJaCadastradaException e) {
			result.rejectValue("cpfOuCnpj", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), empresa, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Empresa salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/empresas/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Empresa empresa) {
		try {
			cadastroEmpresaService.excluir(empresa, empresas);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<EstabelecimentoDTO> pesquisarParaDeposito(String nomeEmpresarial) {
		return empresas.porCnpjOuNomeEmpresarialEPossuiEstoque(nomeEmpresarial);
	}
	
	@RequestMapping(path = "/movto-estoque", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<EstabelecimentoDTO> pesquisarParaMovtoEstoque(String cnpjOuNomeEmpresarial, Long codigoEmpresaSaida) {
		return empresas.porCnpjOuNomeEmpresarial(cnpjOuNomeEmpresarial);
	}
}
