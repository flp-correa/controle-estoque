package br.com.yahtosistemas.controller;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.dto.LocalizacaoDTO;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.repository.filter.LocalizacaoFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroLocalizacaoService;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@Controller
@RequestMapping("/{tenantid}/localizacoes")
public class LocalizacoesController {

	@Autowired
	private Localizacoes localizacoes;
	
	@Autowired
	private CadastroLocalizacaoService cadastroLocalizacaoService;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, LocalizacaoFilter localizacaoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("localizacao/PesquisaLocalizacoes");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Localizacao> paginaWrapper = new PageWrapper<>(localizacoes.filtrar(localizacaoFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}
	
	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Localizacao localizacao, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("localizacao/CadastroLocalizacao");
			
			mv.addObject("tiposUnidadesDeMedida", Arrays.asList(TipoUnidadeMedida.UNID, TipoUnidadeMedida.KG, TipoUnidadeMedida.GRAMAS));
			mv.addObject("tenantid", tenantid);
			
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				Localizacao localizacao = localizacoes.getOne(codigo);				
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), localizacao, usuarioSistema);
				
				mv.addObject(localizacao);
				return mv;
			}
		} catch (EntityNotFoundException e) {}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Localizacao localizacao, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), localizacao, usuarioSistema);
		}
		
		try {			
			cadastroLocalizacaoService.salvar(localizacao, localizacoes);
		}  catch (QuantidadeEstoqueMinimoDeveSerMenorQueMaximoException e) {
			result.rejectValue("estoqueMinimo", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), localizacao, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Localização salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/localizacoes/novo", usuarioSistema.getUsuario().getTenantId()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Localizacao localizacao) {
		try {
			cadastroLocalizacaoService.excluir(localizacao, localizacoes);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value = "/filtro-cadastro-produto", method = RequestMethod.GET)
	public ResponseEntity<List<LocalizacaoDTO>> listarLocalizacoes(String campoPesquisa) {
		return ResponseEntity.status(HttpStatus.OK).body(localizacoes.buscarPorEnderecoOuDescricao(campoPesquisa, null, null));
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<LocalizacaoDTO> pesquisar(String descricao, Long codigoProduto, Long codigoLocalizacao) {
		return localizacoes.buscarPorEnderecoOuDescricao(descricao, codigoProduto, codigoLocalizacao);
	}
}
