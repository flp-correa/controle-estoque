package br.com.yahtosistemas.controller;

import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.DestinoSaidaEstoque;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;
import br.com.yahtosistemas.model.builder.MovimentoEstoqueBuilder;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.repository.Produtos;
import br.com.yahtosistemas.repository.SaldosEstoque;
import br.com.yahtosistemas.repository.UnidadesMedida;
import br.com.yahtosistemas.repository.filter.MovimentoEstoqueFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroMovimentoEstoqueService;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.service.exception.LocalizacaoTransferenciaObrigatorioException;
import br.com.yahtosistemas.service.exception.NaoPossuiSaldoNaLocalizacaoEstoqueException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorUnitarioObrigatorioException;

@Controller
@RequestMapping("/{tenantid}/movimentacoes-estoque")
public class MovimentacoesEstoqueController {
	
	@Autowired
	private MovimentacoesEstoque movimentacoesEstoque; 
	
	@Autowired
	private SaldosEstoque saldosEstoque;
	
	@Autowired
	private UnidadesMedida unidadesMedida;
	
	@Autowired
	private Produtos produtos;
	
	@Autowired
	private CadastroMovimentoEstoqueService cadastroMovimentoEstoqueService;
	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, MovimentoEstoqueFilter movimentoEstoqueFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("movimentoEstoque/PesquisaMovimentacoesEstoque");
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<MovimentoEstoque> paginaWrapper = new PageWrapper<>(movimentacoesEstoque.filtrar(movimentoEstoqueFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}

	@GetMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, MovimentoEstoque movimentoEstoque, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("movimentoEstoque/CadastroMovimentoEstoque");
			
			mv.addObject("tenantid", tenantid);
			mv.addObject("destinosSaidaEstoque", DestinoSaidaEstoque.values());
			if (movimentoEstoque.produtoInformado()) {
				mv.addObject("unidadesDeMedida", 
						unidadesMedida.findByTenantIdAndFamilia(tenantid,
								produtos.buscarApenasTipoUnidadeMedidaPorCodigoProduto(movimentoEstoque.getProduto().getCodigo())
								.getTipoUnidadeMedida().obterUnidadeMedida().getFamiliaUnidadeMedida()));
			}
			if (movimentoEstoque.getUsuario() == null) {
				movimentoEstoque.setUsuario(usuarioSistema.getUsuario());
			}
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		try {
			if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
				List<SaldoEstoque> saldos = saldosEstoque.findByMovimentoEstoque(new MovimentoEstoqueBuilder().comCodigo(codigo).construir());
				MovimentoEstoque movimentoEstoque = saldos.get(0).getMovimentoEstoque();
				movimentoEstoque.getProduto().setLocalizacao(saldos.get(0).getLocalizacao());
				if (saldos.size() == 2) {
					movimentoEstoque.setLocalizacaoTransferencia(saldos.get(1).getLocalizacao());
				} else if (movimentoEstoque.getEmpresaDestino() != null) {
					movimentoEstoque.setDestinoSaidaEstoque(DestinoSaidaEstoque.UNIDADE_PROPRIA);
				} else if (movimentoEstoque.getVenda() != null) {
					movimentoEstoque.setDestinoSaidaEstoque(DestinoSaidaEstoque.VENDA);
				}
				ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
				movimentoEstoque.setDetalhes(true);
					
				mv.addObject(movimentoEstoque);
				return mv;
			}
		} catch (EntityNotFoundException e) {}	
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping("/novo")
	public ModelAndView salvar(@Valid MovimentoEstoque movimentoEstoque, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		}
		
		try {
			cadastroMovimentoEstoqueService.salvar(movimentoEstoque, movimentacoesEstoque, usuarioSistema.getUsuario());
		} catch (ProdutoObrigatorioException e) {
			result.rejectValue("produto.codigo", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		} catch (EntidadeObrigatoriaException e) {
			result.rejectValue("fornecedor.codigo", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		} catch (ValorUnitarioObrigatorioException e) {
			result.rejectValue("valorUnitario", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		} catch (NaoPossuiSaldoNaLocalizacaoEstoqueException e) {
			result.rejectValue("quantidade", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		} catch (LocalizacaoTransferenciaObrigatorioException e) {
			result.rejectValue("localizacaoTransferencia.codigo", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), movimentoEstoque, usuarioSistema);
		}
		
		attributes.addFlashAttribute("mensagem", "Movimentação salva com sucesso");
		return new ModelAndView(String.format("redirect:/%s/movimentacoes-estoque/novo", usuarioSistema.getUsuario().getTenantId()));
	}
}
