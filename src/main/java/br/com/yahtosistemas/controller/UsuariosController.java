package br.com.yahtosistemas.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.controller.page.PageWrapper;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.repository.Grupos;
import br.com.yahtosistemas.repository.Usuarios;
import br.com.yahtosistemas.repository.filter.UsuarioFilter;
import br.com.yahtosistemas.security.UsuarioSistema;
import br.com.yahtosistemas.service.CadastroUsuarioService;
import br.com.yahtosistemas.service.exception.EmailUsuarioJaCadastradoException;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.service.exception.SenhaObrigatoriaUsuarioException;

@Controller
@RequestMapping("/{tenantid}/usuarios")
public class UsuariosController {
	
	@Autowired
	private CadastroUsuarioService cadastroUsuarioService;
	
	@Autowired
	private Usuarios usuarios;
	
	@Autowired
	private Grupos grupos;

	
	@GetMapping
	public ModelAndView pesquisar(@PathVariable String tenantid, UsuarioFilter usuarioFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("usuario/PesquisaUsuarios");
			mv.addObject("grupos", grupos.findAll());
			mv.addObject("tenantid", tenantid);
			
			PageWrapper<Usuario> paginaWrapper = new PageWrapper<>(usuarios.filtrar(usuarioFilter, pageable)
					, httpServletRequest);
			mv.addObject("pagina", paginaWrapper);
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404); 
	}

	@RequestMapping("/novo")
	public ModelAndView novo(@PathVariable String tenantid, Usuario usuario, @AuthenticationPrincipal UsuarioSistema usuarioSistema,
			HttpServletRequest request) {
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			ModelAndView mv = new ModelAndView("usuario/CadastroUsuario");
			
			mv.addObject("grupos", grupos.findAll());
			mv.addObject("tenantid", tenantid);
			usuario.setDetalhes(!request.isUserInRole("ADMINISTRADOR"));
			return mv;
		}
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable String tenantid, @PathVariable Long codigo, 
			HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (usuarioSistema.getUsuario().getTenantId().equals(tenantid)) {
			if (usuarioSistema.getUsuario().getCodigo().equals(codigo) || request.isUserInRole("ADMINISTRADOR")) {				
				Usuario usuario = usuarios.buscarComGrupos(codigo);
				if (usuario != null) {					
					ModelAndView mv = novo(usuarioSistema.getUsuario().getTenantId(), usuario, usuarioSistema, request);
					mv.addObject(usuario);
					return mv;
				}
			} else {
				return new ModelAndView("error").addObject("status", 403);
			}
		}
		
		return new ModelAndView("error").addObject("status", 404);
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Usuario usuario, BindingResult result, RedirectAttributes attributes, 
			 HttpServletRequest request, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		if (result.hasErrors()) {
			return novo(usuarioSistema.getUsuario().getTenantId(), usuario, usuarioSistema, request);
		}
		
		
		
		try {
			List<Usuario> usuarioExiste = cadastroUsuarioService.validarEmailParaTodoOSistema(usuario, usuarios);
			cadastroUsuarioService.salvar(usuario, usuarios, usuarioExiste);
		} catch (EmailUsuarioJaCadastradoException e) {
			result.rejectValue("email", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), usuario, usuarioSistema, request);
		} catch (SenhaObrigatoriaUsuarioException e) {
			result.rejectValue("senha", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), usuario, usuarioSistema, request);
		} catch (EntidadeObrigatoriaException e) {
			result.rejectValue("deposito", e.getMessage(), e.getMessage());
			return novo(usuarioSistema.getUsuario().getTenantId(), usuario, usuarioSistema, request);
		}
		
		attributes.addFlashAttribute("mensagem", "Usuário salvo com sucesso");
		return request.isUserInRole("ADMINISTRADOR") ? new ModelAndView(String.format("redirect:/%s/usuarios/novo", usuarioSistema.getUsuario().getTenantId())) : 
						new ModelAndView(String.format("redirect:/%s/usuarios/%d", usuarioSistema.getUsuario().getTenantId(), usuario.getCodigo()));
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Usuario usuario) {
		try {
			cadastroUsuarioService.excluir(usuario, usuarios);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
}
