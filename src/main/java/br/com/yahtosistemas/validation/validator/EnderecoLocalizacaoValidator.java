package br.com.yahtosistemas.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.dto.LocalizacaoDTO;
import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.repository.Localizacoes;
import br.com.yahtosistemas.validation.ValidarEnderecoLocalizacao;

public class EnderecoLocalizacaoValidator implements ConstraintValidator<ValidarEnderecoLocalizacao, Object> {

	@Autowired
	private Localizacoes localizacoes;
	
	private String atributoEndereco;
	
	@Override
	public void initialize(ValidarEnderecoLocalizacao constraintAnnotation) {
		this.atributoEndereco = constraintAnnotation.atributoEndereco();
	}
	
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		if (localizacoes == null) {
			return true;
		}
		Localizacao localizacao = (Localizacao) object;
		
		String mensagem = validarEnderecoDaLocalizacao(localizacao);
		if (!StringUtils.isEmpty(mensagem)) {
			context.disableDefaultConstraintViolation();
			ConstraintViolationBuilder violationBuilder = context.buildConstraintViolationWithTemplate(mensagem);
			violationBuilder.addPropertyNode(this.atributoEndereco).addConstraintViolation();
		}
		return StringUtils.isEmpty(mensagem);
	}

	private String validarEnderecoDaLocalizacao(final Localizacao localizacao) {
		String mensagem = "";
		if ( (localizacao.getBlocado() && localizacao.getEnderecoSemFormatacao().length() != 7) || 
				(!localizacao.getBlocado() && localizacao.getEnderecoSemFormatacao().length() != 10)) {
			mensagem = "Endereço da localização inválido";
		} else  {
			LocalizacaoDTO locaDTO = localizacoes.getLocalizacaoDTO(localizacao.getEnderecoSemFormatacao());
			
			if ( ( locaDTO != null && !locaDTO.getCodigo().equals(localizacao.getCodigo()) ) || 
					( locaDTO != null && localizacao.isNovo() ) ) {
				
				mensagem = "Localização já cadastrada";
			}
		}
		return mensagem;
	}
}
