package br.com.yahtosistemas.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.yahtosistemas.validation.ValidarRelacionamentoManyToOne;

public class RelacionamentoManyToOneValidator implements ConstraintValidator<ValidarRelacionamentoManyToOne, Object> {

	private boolean buscarNoPai;
	
	@Override
	public void initialize(ValidarRelacionamentoManyToOne constraintAnnotation) {
		this.buscarNoPai = constraintAnnotation.buscarNoPai();
	}
	
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		try {
			if (this.buscarNoPai) {
				return object.getClass().getSuperclass().getDeclaredMethod("getCodigo").invoke(object) != null;			
			} else {			
				return object.getClass().getDeclaredMethod("getCodigo").invoke(object) != null;
			}
		} catch (Exception e) {
			throw new RuntimeException("Erro recuperando valores do código da entidade", e);
		}
	}

}
