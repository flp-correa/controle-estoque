package br.com.yahtosistemas.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.DestinoSaidaEstoque;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;
import br.com.yahtosistemas.validation.ValidarDestinoSaidaEstoque;

public class DestinoSaidaEstoqueValidator implements ConstraintValidator<ValidarDestinoSaidaEstoque, Object> {

	private String atributoDestinoSaidaEstoque;
	private String atributoEmpresaDestino;
	private String atributoVendaDestino;
	
	@Override
	public void initialize(ValidarDestinoSaidaEstoque constraintAnnotation) {
		this.atributoDestinoSaidaEstoque = constraintAnnotation.atributoDestinoSaidaEstoque();
		this.atributoEmpresaDestino = constraintAnnotation.atributoEmpresaDestino();
		this.atributoVendaDestino = constraintAnnotation.atributoVenda();
	}
	
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		try {
			MovimentoEstoque movimentoEstoque = (MovimentoEstoque) object;
			
			String[] mensagem = validarDestino(movimentoEstoque);
			if (!StringUtils.isEmpty(mensagem[0])) {
				context.disableDefaultConstraintViolation();
				ConstraintViolationBuilder violationBuilder = context.buildConstraintViolationWithTemplate(mensagem[1]);
				
				switch (mensagem[0]) {
				case "DestinoSaidaEstoque":					
					violationBuilder.addPropertyNode(this.atributoDestinoSaidaEstoque).addConstraintViolation();
					break;
				case "EmpresaDestino":
					violationBuilder.addPropertyNode(this.atributoEmpresaDestino).addConstraintViolation();
					break;
				default:
					violationBuilder.addPropertyNode(this.atributoVendaDestino).addConstraintViolation();
					break;
				}
			}
			return StringUtils.isEmpty(mensagem[0]);
			
		} catch (Exception e) {
			throw new RuntimeException("Erro recuperando valores dos atributos", e);
		}
	}

	private String[] validarDestino(MovimentoEstoque movimentoEstoque) {
		if (movimentoEstoque.getTipoMovimento().equals(TipoMovimentoEstoque.SAIDA)) {
			if (movimentoEstoque.getDestinoSaidaEstoque() == null) {
				return new String[] {"DestinoSaidaEstoque", "Destino da saída é obrigatório"};
			}
			if (movimentoEstoque.getDestinoSaidaEstoque().equals(DestinoSaidaEstoque.UNIDADE_PROPRIA) && movimentoEstoque.getEmpresaDestino().getCodigo() == null) {
				return new String[] {"EmpresaDestino", "Empresa de destino da saída é obrigatório"};
			} 
			if (movimentoEstoque.getDestinoSaidaEstoque().equals(DestinoSaidaEstoque.VENDA) && movimentoEstoque.getVenda().getCodigo() == null) {
				return new String[] {"Venda", "Venda é obrigatória"};
			}
		}
		return new String[] {""};
	}

}
