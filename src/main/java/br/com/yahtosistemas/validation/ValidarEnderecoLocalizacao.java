package br.com.yahtosistemas.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

import br.com.yahtosistemas.validation.validator.EnderecoLocalizacaoValidator;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { EnderecoLocalizacaoValidator.class })
public @interface ValidarEnderecoLocalizacao {

	@OverridesAttribute(constraint = Pattern.class, name = "message")
	String message() default "Endereço da localização é obrigatório";
	
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};

	String atributoBlocado();
	String atributoEndereco();
}
