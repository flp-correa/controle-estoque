package br.com.yahtosistemas.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Pattern(regexp = "(\\d{3})?")
public @interface MESA {

	@OverridesAttribute(constraint = Pattern.class, name = "message")
	String message() default "Nº da mesa inválido, padrão é 000";
	
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
