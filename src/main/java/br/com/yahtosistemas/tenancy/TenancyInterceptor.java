package br.com.yahtosistemas.tenancy;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 * @author Yahto Sistemas - Felipe Corrêa
 * 
 */
public class TenancyInterceptor extends HandlerInterceptorAdapter {
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		Map<String, Object> pathVars = (Map<String, Object>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		
		if (pathVars != null) {	
			HandlerMethod handlerMethod = (HandlerMethod) handler;
		
				
			if (pathVars.containsKey("tenantid")) {
				request.setAttribute("TENANT_ID", pathVars.get("tenantid"));
			}
			
			request.setAttribute("SALVANDO_USUARIO", Boolean.FALSE);
			if (handlerMethod.getMethod().getName().equals("salvarUsuario")) {
				request.setAttribute("SALVANDO_USUARIO", Boolean.TRUE);
			}
		}
		
		return true;
	}
	
	public static String getTenantId() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		
		if (requestAttributes != null) {
			return (String) requestAttributes.getAttribute("TENANT_ID", RequestAttributes.SCOPE_REQUEST);
		}
		
		return null;
	}
	
	public static Boolean getSalvandoUsuario() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		
		if (requestAttributes != null) {
			return (Boolean) requestAttributes.getAttribute("SALVANDO_USUARIO", RequestAttributes.SCOPE_REQUEST);
		}
		
		return null;
	}

}
