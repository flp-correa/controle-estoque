package br.com.yahtosistemas.tenancy;

import javax.persistence.EntityManager;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Yahto Sistemas - Felipe Corrêa
 * 
 */
@Component
@Aspect
@Transactional(propagation = Propagation.REQUIRED)
public class TenancyAspect {

	@Autowired
	private EntityManager manager;

	@Before("@annotation(org.springframework.transaction.annotation.Transactional)")
	public void definirTenant() {
		String tenantId = TenancyInterceptor.getTenantId();
		Boolean salvandoUsuario = TenancyInterceptor.getSalvandoUsuario();
		
		if (tenantId != null && salvandoUsuario != null && !salvandoUsuario) {
			manager.unwrap(Session.class).enableFilter("tenant").setParameter("id", tenantId);
		}
	}
}
