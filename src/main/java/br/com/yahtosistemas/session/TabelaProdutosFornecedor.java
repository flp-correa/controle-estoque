package br.com.yahtosistemas.session;

import java.util.ArrayList;
import java.util.List;

import br.com.yahtosistemas.model.Produto;

class TabelaProdutosFornecedor {

	private String uuid;
	private List<Produto> produtos = new ArrayList<>();
	
	public TabelaProdutosFornecedor(String uuid) {
		this.uuid = uuid;
	}
	
	public void adicionarProduto(Produto produto) {
		if (!produtos.contains(produto)) {
			produtos.add(0, produto);
		}
	}
	
	public void excluirProduto(Produto produto) {
		produtos.remove(produto);
	}

	public int total(){
		return produtos.size();
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaProdutosFornecedor other = (TabelaProdutosFornecedor) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
