package br.com.yahtosistemas.session;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import br.com.yahtosistemas.model.ItemVendaPedidoVenda;
import br.com.yahtosistemas.model.Produto;

@SessionScope
@Component
public class TabelasItensSession {

	private Set<TabelaItensVendaPedidoVenda> tabelas = new HashSet<>();

	public void adicionarItem(String uuid, ItemVendaPedidoVenda itemVendaPedidoVenda, int quantidade) {
		TabelaItensVendaPedidoVenda tabela = buscarTabelaPorUuid(uuid);
		tabela.adicionarItem(itemVendaPedidoVenda, quantidade);
		tabelas.add(tabela);
	}

	public void alterarQuantidadeItens(String uuid, Produto produto, Integer quantidade) {
		TabelaItensVendaPedidoVenda tabela = buscarTabelaPorUuid(uuid);
		tabela.alterarQuantidadeItens(produto, quantidade);
	}

	public void alterarValorUnitarioItens(String uuid, Long codigoItem, BigDecimal valorUnitario) {
		TabelaItensVendaPedidoVenda tabela = buscarTabelaPorUuid(uuid);
		tabela.alterarValorUnitarioItens(codigoItem, valorUnitario);
	}
	
	public void excluirItem(String uuid, Long codigoItem) {
		TabelaItensVendaPedidoVenda tabela = buscarTabelaPorUuid(uuid);
		tabela.excluirItem(codigoItem);
	}

	public List<ItemVendaPedidoVenda> getItens(String uuid) {
		return buscarTabelaPorUuid(uuid).getItens();
	}
	
	public Object getValorTotal(String uuid) {
		return buscarTabelaPorUuid(uuid).getValorTotal();
	}

	private TabelaItensVendaPedidoVenda buscarTabelaPorUuid(String uuid) {
		TabelaItensVendaPedidoVenda tabela = tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.orElse(new TabelaItensVendaPedidoVenda(uuid));
		return tabela;
	}
}
