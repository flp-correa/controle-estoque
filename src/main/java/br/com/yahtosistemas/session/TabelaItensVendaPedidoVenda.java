package br.com.yahtosistemas.session;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import br.com.yahtosistemas.model.ItemPedidoVenda;
import br.com.yahtosistemas.model.ItemVendaPedidoVenda;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.StatusItemCozinha;

class TabelaItensVendaPedidoVenda {

	private String uuid;
	private List<ItemVendaPedidoVenda> itens = new ArrayList<>();
	
	public TabelaItensVendaPedidoVenda(String uuid) {
		this.uuid = uuid;
	}

	public BigDecimal getValorTotal() {
		return itens.stream()
				.map(ItemVendaPedidoVenda::getValorTotal)
				.reduce(BigDecimal::add)
				.orElse(BigDecimal.ZERO);
	}
	
	public void adicionarItem(ItemVendaPedidoVenda itemVendaPedidoVenda, Integer quantidade) {
		Optional<ItemVendaPedidoVenda> itemVendaOptional;
		if (itemVendaPedidoVenda.getProduto().getLancheOuPrato() &&
				(itemVendaPedidoVenda.getProduto().getPorQuilo() || itemVendaPedidoVenda.getProduto().getEnviarCozinha())) {			
			itemVendaOptional = buscarItemPorCodigoItemVendaPedidoVenda(itemVendaPedidoVenda.getCodigo());
		} else {
			itemVendaOptional = buscarItemPorProduto(itemVendaPedidoVenda.getProduto());
		}
		
		ItemVendaPedidoVenda itemVenda = null;
		if (itemVendaOptional.isPresent()) {
			itemVenda = itemVendaOptional.get();
			
			if (!(itemVendaPedidoVenda.getProduto().getLancheOuPrato() &&
				(itemVendaPedidoVenda.getProduto().getPorQuilo() || itemVendaPedidoVenda.getProduto().getEnviarCozinha()))) {
				itemVenda.setQuantidade(itemVenda.getQuantidade() + quantidade);
			} else if (itemVendaPedidoVenda instanceof ItemPedidoVenda) {
				((ItemPedidoVenda)itemVenda).setAdicionais(((ItemPedidoVenda) itemVendaPedidoVenda).getAdicionais());
				((ItemPedidoVenda)itemVenda).setIngredientes(((ItemPedidoVenda) itemVendaPedidoVenda).getIngredientes());
			}
		} else {
			itemVenda = itemVendaPedidoVenda;
			itemVenda.setQuantidade(quantidade);
			itemVenda.setValorUnitario(itemVendaPedidoVenda.getValorUnitario() != null ? itemVendaPedidoVenda.getValorUnitario() :
																						itemVendaPedidoVenda.getProduto().getValorUnitario());
			itens.add(itemVenda);
			if (itemVenda.getProduto().getEnviarCozinha() && itemVenda.getCodigo() < 1) {
				((ItemPedidoVenda)itemVenda).setStatusCozinha(StatusItemCozinha.NAO_INICIADO);
				((ItemPedidoVenda)itemVenda).setLoadCozinha(Boolean.FALSE);;
			}
		}
		Collections.sort(itens);
	}
	
	public void alterarQuantidadeItens(Produto produto, Integer quantidade) {
		ItemVendaPedidoVenda itemVenda = buscarItemPorProduto(produto).get();
		itemVenda.setQuantidade(quantidade);
	}

	public void alterarValorUnitarioItens(Long codigoItem, BigDecimal valorUnitario) {
		ItemVendaPedidoVenda itemVenda = buscarItemPorCodigoItemVendaPedidoVenda(codigoItem).get();
		itemVenda.setValorUnitario(valorUnitario);
	}
	
	public void excluirItem(Long codigoItem) {
		int indice = IntStream.range(0, itens.size())
				.filter(i -> itens.get(i).getCodigo().equals(codigoItem))
				.findAny().getAsInt();
		itens.remove(indice);
	}
	
	public int total() {
		return itens.size();
	}

	public List<ItemVendaPedidoVenda> getItens() {
		return itens;
	}
	
	private Optional<ItemVendaPedidoVenda> buscarItemPorProduto(Produto produto) {
		return itens.stream()
				.filter(i -> i.getProduto().equals(produto))
				.findAny();
	}
	
	private Optional<ItemVendaPedidoVenda> buscarItemPorCodigoItemVendaPedidoVenda(Long codigo) {
		return itens.stream()
				.filter(i -> i.getCodigo().equals(codigo))
				.findAny();
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaItensVendaPedidoVenda other = (TabelaItensVendaPedidoVenda) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}