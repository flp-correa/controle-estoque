package br.com.yahtosistemas.session;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import br.com.yahtosistemas.model.Localizacao;

@SessionScope
@Component
public class TabelaLocalizacoesProdutoSession {

	private Set<TabelaLocalizacoesProduto> tabelas = new HashSet<>();

	public void adicionarLocalizacao(String uuid, Localizacao localizacao) {
		TabelaLocalizacoesProduto tabela = buscarTabelaPorUuid(uuid);
		
		tabela.adicionarLocalizacao(localizacao);
		tabelas.add(tabela);
	}


	public void excluirLocalizacao(String uuid, Localizacao localizacao) {
		TabelaLocalizacoesProduto tabela = buscarTabelaPorUuid(uuid);
		tabela.excluirLocalizacao(localizacao);
	}

	public List<Localizacao> getLocalizacoes(String uuid) {
		return buscarTabelaPorUuid(uuid).getLocalizacoes();
	}
	
	private TabelaLocalizacoesProduto buscarTabelaPorUuid(String uuid) {
		TabelaLocalizacoesProduto tabela = tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.orElse(new TabelaLocalizacoesProduto(uuid));
		return tabela;
	}


	public void setLocalizacao(String uuid, List<Localizacao> localizacoes) {
		tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.get().setLocalizacoes(localizacoes);
		
	}

}
