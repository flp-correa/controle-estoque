package br.com.yahtosistemas.session;

import java.util.ArrayList;
import java.util.List;

import br.com.yahtosistemas.model.Localizacao;

class TabelaLocalizacoesProduto {

	private String uuid;
	private List<Localizacao> localizacoes = new ArrayList<>();
	
	public TabelaLocalizacoesProduto(String uuid) {
		this.uuid = uuid;
	}
	
	public void adicionarLocalizacao(Localizacao localizacao) {
		if (!localizacaoExixte(localizacao)) {
			localizacoes.add(0, localizacao);
		}
	}
	
	public void excluirLocalizacao(Localizacao localizacao) {
		localizacoes.remove(localizacao);
	}

	public int total(){
		return localizacoes.size();
	}

	public void setLocalizacoes(List<Localizacao> localizacoes) {
		this.localizacoes = localizacoes;
	}
	
	public List<Localizacao> getLocalizacoes() {
		return localizacoes;
	}

	public String getUuid() {
		return uuid;
	}
	
	private boolean localizacaoExixte(Localizacao localizacao) {
		if (localizacoes.contains(localizacao) || 
				localizacoes.stream().filter(l -> l.getDeposito().equals(localizacao.getDeposito())).findFirst().isPresent()) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaLocalizacoesProduto other = (TabelaLocalizacoesProduto) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
