package br.com.yahtosistemas.session;

import java.util.ArrayList;
import java.util.List;

import br.com.yahtosistemas.model.Ingrediente;

class TabelaIngredientesProduto {

	private String uuid;
	private List<Ingrediente> ingredientes = new ArrayList<>();
	
	public TabelaIngredientesProduto(String uuid) {
		this.uuid = uuid;
	}
	
	public void adicionarIngrediente(Ingrediente ingrediente) {
		if (!ingredientes.contains(ingrediente)) {
			ingredientes.add(0, ingrediente);
		}
	}
	
	public void excluirIngrediente(Ingrediente ingrediente) {
		ingredientes.remove(ingrediente);
	}

	public int total(){
		return ingredientes.size();
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaIngredientesProduto other = (TabelaIngredientesProduto) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
