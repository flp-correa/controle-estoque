package br.com.yahtosistemas.session;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import br.com.yahtosistemas.model.Produto;

@SessionScope
@Component
public class TabelaProdutosFornecedorSession {

	private Set<TabelaProdutosFornecedor> tabelas = new HashSet<>();

	public void adicionarProduto(String uuid, Produto produto) {
		TabelaProdutosFornecedor tabela = buscarTabelaPorUuid(uuid);
		
		tabela.adicionarProduto(produto);
		tabelas.add(tabela);
	}


	public void excluirProduto(String uuid, Produto produto) {
		TabelaProdutosFornecedor tabela = buscarTabelaPorUuid(uuid);
		tabela.excluirProduto(produto);
	}

	public List<Produto> getProdutos(String uuid) {
		return buscarTabelaPorUuid(uuid).getProdutos();
	}
	
	private TabelaProdutosFornecedor buscarTabelaPorUuid(String uuid) {
		TabelaProdutosFornecedor tabela = tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.orElse(new TabelaProdutosFornecedor(uuid));
		return tabela;
	}


	public void setProduto(String uuid, List<Produto> produtos) {
		tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.get().setProdutos(produtos);
		
	}

}
