package br.com.yahtosistemas.session;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import br.com.yahtosistemas.model.Ingrediente;

@SessionScope
@Component
public class TabelaIngredientesProdutoSession {

	private Set<TabelaIngredientesProduto> tabelas = new HashSet<>();

	public void adicionarIngrediente(String uuid, Ingrediente ingrediente) {
		TabelaIngredientesProduto tabela = buscarTabelaPorUuid(uuid);
		
		tabela.adicionarIngrediente(ingrediente);
		tabelas.add(tabela);
	}


	public void excluirIngrediente(String uuid, Ingrediente ingrediente) {
		TabelaIngredientesProduto tabela = buscarTabelaPorUuid(uuid);
		tabela.excluirIngrediente(ingrediente);
	}

	public List<Ingrediente> getIngredientes(String uuid) {
		return buscarTabelaPorUuid(uuid).getIngredientes();
	}
	
	private TabelaIngredientesProduto buscarTabelaPorUuid(String uuid) {
		TabelaIngredientesProduto tabela = tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.orElse(new TabelaIngredientesProduto(uuid));
		return tabela;
	}


	public void setIngrediente(String uuid, List<Ingrediente> ingredientes) {
		tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.get().setIngredientes(ingredientes);
		
	}

}
