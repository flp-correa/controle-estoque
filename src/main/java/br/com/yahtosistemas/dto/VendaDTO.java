package br.com.yahtosistemas.dto;

import java.math.BigDecimal;

public class VendaDTO {

	private BigDecimal totalDinheiro;
	private BigDecimal totalCartao;
	private BigDecimal valorTotal;
	
	public VendaDTO(BigDecimal totalDinheiro, BigDecimal totalCartao, BigDecimal valorTotal) {
		this.totalDinheiro = totalDinheiro;
		this.totalCartao = totalCartao;
		this.valorTotal = valorTotal;
	}
	public BigDecimal getTotalDinheiro() {
		return totalDinheiro;
	}
	public void setTotalDinheiro(BigDecimal totalDinheiro) {
		this.totalDinheiro = totalDinheiro;
	}
	public BigDecimal getTotalCartao() {
		return totalCartao;
	}
	public void setTotalCartao(BigDecimal totalCartao) {
		this.totalCartao = totalCartao;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
}
