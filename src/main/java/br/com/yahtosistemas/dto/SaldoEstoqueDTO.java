package br.com.yahtosistemas.dto;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;

public class SaldoEstoqueDTO {

	private Long codigo;
	private MovimentoEstoque movimentoEstoque;
	private BigDecimal saldoLocalizacao;
	private BigDecimal saldoEstoque;
	private SaldoEstoque entidadeSaldoEstoque;

	public SaldoEstoqueDTO(Long codigo, BigDecimal saldoLocalizacao) {
		this.codigo = codigo;
		this.saldoLocalizacao = saldoLocalizacao;
	}
	
	public SaldoEstoqueDTO(SaldoEstoque criarSaldoEstoque) {
		this.entidadeSaldoEstoque = criarSaldoEstoque;
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public MovimentoEstoque getMovimentoEstoque() {
		return movimentoEstoque;
	}
	public void setMovimentoEstoque(MovimentoEstoque movimentoEstoque) {
		this.movimentoEstoque = movimentoEstoque;
	}
	public BigDecimal getSaldoLocalizacao() {
		return saldoLocalizacao;
	}
	public void setSaldoLocalizacao(BigDecimal saldoLocalizacao) {
		this.saldoLocalizacao = saldoLocalizacao;
	}
	public BigDecimal getSaldoEstoque() {
		return saldoEstoque;
	}
	public void setSaldoEstoque(BigDecimal saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}
	public SaldoEstoque getEntidadeSaldoEstoque() {
		return entidadeSaldoEstoque;
	}
	public void setEntidadeSaldoEstoque(SaldoEstoque entidadeSaldoEstoque) {
		this.entidadeSaldoEstoque = entidadeSaldoEstoque;
	}
}
