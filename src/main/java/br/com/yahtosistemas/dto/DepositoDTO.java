package br.com.yahtosistemas.dto;

import br.com.yahtosistemas.model.Empresa;

public class DepositoDTO {

	private Long codigo;
	private String descricao;
	private Empresa empresa;
	
	public DepositoDTO(Long codigo, String descricao, Empresa empresa) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.empresa = empresa;
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
