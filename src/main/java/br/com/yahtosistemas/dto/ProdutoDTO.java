package br.com.yahtosistemas.dto;

import java.math.BigDecimal;

import org.springframework.util.StringUtils;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

public class ProdutoDTO {

	private Long codigo;
	private String descricao;
	private TipoUnidadeMedida tipoUnidadeMedida;
	private BigDecimal valorUnitario;
	private Localizacao localizacao;
	private Boolean lancheOuPrato;
	private Boolean porQuilo;
	private Boolean enviarCozinha;
	private String descricaoTipoUnidadeMedida;
	private String familiaUnidadeMedida;
	private Boolean localFixo;
	private String foto;
	private String urlThumbnailFoto;
	
	public ProdutoDTO(Long codigo) {
		this.codigo = codigo;
	}
	
	public ProdutoDTO(Long codigo, String descricao, BigDecimal valorUnitario, String foto, Boolean lancheOuPrato, Boolean porQuilo, Boolean enviarCozinha) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.valorUnitario = valorUnitario;
		this.foto = StringUtils.isEmpty(foto) ? "cerveja-mock.png" : foto;
		this.lancheOuPrato = lancheOuPrato;
		this.porQuilo = porQuilo;
		this.enviarCozinha = enviarCozinha;
	}
	
	public ProdutoDTO(Long codigo, TipoUnidadeMedida tipoUnidadeMedida) {
		this.codigo = codigo;
		this.tipoUnidadeMedida = tipoUnidadeMedida;
	}

	public ProdutoDTO(Long codigo, String descricao, TipoUnidadeMedida tipoUnidadeMedida, Boolean localFixo, Localizacao localizacao) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.tipoUnidadeMedida = tipoUnidadeMedida;
		this.localizacao = localizacao;
		this.familiaUnidadeMedida = tipoUnidadeMedida.obterUnidadeMedida().getFamiliaUnidadeMedida().name();
		this.descricaoTipoUnidadeMedida = tipoUnidadeMedida.getDescricao();
		this.localFixo = localFixo;
	}

	public ProdutoDTO(Long codigo, String descricao, BigDecimal valorUnitario, TipoUnidadeMedida tipoUnidadeMedida) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.tipoUnidadeMedida = tipoUnidadeMedida;
		this.valorUnitario = valorUnitario;
		this.familiaUnidadeMedida = tipoUnidadeMedida.obterUnidadeMedida().getFamiliaUnidadeMedida().name();
		this.descricaoTipoUnidadeMedida = tipoUnidadeMedida.getDescricao();
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoUnidadeMedida getTipoUnidadeMedida() {
		return tipoUnidadeMedida;
	}

	public void setTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida) {
		this.tipoUnidadeMedida = tipoUnidadeMedida;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	public Boolean getLancheOuPrato() {
		return lancheOuPrato;
	}

	public void setLancheOuPrato(Boolean lancheOuPrato) {
		this.lancheOuPrato = lancheOuPrato;
	}

	public String getDescricaoTipoUnidadeMedida() {
		return descricaoTipoUnidadeMedida;
	}

	public void setDescricaoTipoUnidadeMedida(String descricaoTipoUnidadeMedida) {
		this.descricaoTipoUnidadeMedida = descricaoTipoUnidadeMedida;
	}

	public String getFamiliaUnidadeMedida() {
		return familiaUnidadeMedida;
	}

	public void setFamiliaUnidadeMedida(String familiaUnidadeMedida) {
		this.familiaUnidadeMedida = familiaUnidadeMedida;
	}

	public Boolean getLocalFixo() {
		return localFixo;
	}

	public void setLocalFixo(Boolean localFixo) {
		this.localFixo = localFixo;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getUrlThumbnailFoto() {
		return urlThumbnailFoto;
	}

	public void setUrlThumbnailFoto(String urlThumbnailFoto) {
		this.urlThumbnailFoto = urlThumbnailFoto;
	}

	public Boolean getPorQuilo() {
		return porQuilo;
	}

	public void setPorQuilo(Boolean porQuilo) {
		this.porQuilo = porQuilo;
	}

	public Boolean getEnviarCozinha() {
		return enviarCozinha;
	}

	public void setEnviarCozinha(Boolean enviarCozinha) {
		this.enviarCozinha = enviarCozinha;
	}
}
