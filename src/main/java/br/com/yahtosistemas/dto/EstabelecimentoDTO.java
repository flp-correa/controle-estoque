package br.com.yahtosistemas.dto;

import br.com.yahtosistemas.model.TipoPessoa;

public class EstabelecimentoDTO {

	private Long codigo;
	private String cpfOuCnpj;
	private String nomeEmpresarial;
	private TipoPessoa tipoPessoa;
	
	public EstabelecimentoDTO(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}
	
	public EstabelecimentoDTO(Long codigo, String cpfOuCnpj, String nomeEmpresarial, TipoPessoa tipoPessoa) {
		this.codigo = codigo;
		this.cpfOuCnpj = cpfOuCnpj;
		this.nomeEmpresarial = nomeEmpresarial;
		this.tipoPessoa = tipoPessoa;
	}
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getCpfOuCnpj() {
		return this.tipoPessoa.equals(TipoPessoa.JURIDICA) ? cpfOuCnpj.replaceAll("(\\d{2})(\\d{3})(\\d{3})(\\d{4})", "$1.$2.$3/$4-") : 
				this.cpfOuCnpj.replaceAll("(\\d{3})(\\d{3})(\\d{3})", "$1.$2.$3-");
	}
	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}
	public String getNomeEmpresarial() {
		return nomeEmpresarial;
	}
	public void setNomeEmpresarial(String nomeEmpresarial) {
		this.nomeEmpresarial = nomeEmpresarial;
	}
	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
}
