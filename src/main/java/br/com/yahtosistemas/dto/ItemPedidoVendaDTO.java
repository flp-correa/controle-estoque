package br.com.yahtosistemas.dto;

import br.com.yahtosistemas.model.StatusItemCozinha;

public class ItemPedidoVendaDTO {

	private Long codigo;
	private StatusItemCozinha statusCozinha;
	
	public ItemPedidoVendaDTO(Long codigo, StatusItemCozinha statusCozinha) {
		this.codigo = codigo;
		this.statusCozinha = statusCozinha;
	}
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public StatusItemCozinha getStatusCozinha() {
		return statusCozinha;
	}
	public void setStatusCozinha(StatusItemCozinha statusCozinha) {
		this.statusCozinha = statusCozinha;
	}
}
