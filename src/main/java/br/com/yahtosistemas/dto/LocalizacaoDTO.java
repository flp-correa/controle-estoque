package br.com.yahtosistemas.dto;

import br.com.yahtosistemas.model.Deposito;

public class LocalizacaoDTO {

	private Long codigo;
	private String endereco;
	private String descricao;
	private Deposito deposito;
	private Boolean blocado;
	
	public LocalizacaoDTO(Long codigo, Boolean blocado, String endereco) {
		this.codigo = codigo;
		this.endereco = endereco;
		this.blocado = blocado;
	}
	
	public LocalizacaoDTO(String endereco) {
		this.endereco = endereco;
	}

	public LocalizacaoDTO(Long codigo, Boolean blocado, String endereco, String descricao, Deposito deposito) {
		this.codigo = codigo;
		this.blocado = blocado;
		this.endereco = endereco;
		this.descricao = descricao;
		this.deposito = deposito;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Deposito getDeposito() {
		return deposito;
	}

	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}

	public Boolean getBlocado() {
		return blocado;
	}

	public void setBlocado(Boolean blocado) {
		this.blocado = blocado;
	}

	public String getEnderecoComFormatacao() {
		return this.blocado ? this.endereco.replaceAll("(\\d{2})(\\d{2})", "$1-$2-") : 
			this.endereco.replaceAll("(\\d{2})(\\d{2})(\\d{3})(\\d{2})(\\d{1})", "$1-$2-$3-$4-$5");
	}
}
