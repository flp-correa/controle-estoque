package br.com.yahtosistemas.dto;

public class ClienteDTO {

	private Long codigo;
	private String nrMesa;
	
	public ClienteDTO(Long codigo, String nrMesa) {
		this.codigo = codigo;
		this.nrMesa = nrMesa;
	}
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNrMesa() {
		return nrMesa;
	}
	public void setNrMesa(String nrMesa) {
		this.nrMesa = nrMesa;
	}
}
