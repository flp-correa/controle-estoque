package br.com.yahtosistemas.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.model.StatusPedidoVenda;

public class PedidoVendaDTO {

	private Long codigo;
	private Mesa mesa = new Mesa();
	private Cliente cliente = new Cliente();
	private BigDecimal valorTotal;
	private boolean lancheOuPratoPronto;
	private StatusPedidoVenda status;
	private LocalDateTime dataHoraCriacao;
	private String observacao;
	
	public PedidoVendaDTO(Long codigo, String nrMesa, String nomeCliente, BigDecimal valorTotal, boolean lancheOuPratoPronto) {
		this.codigo = codigo;
		this.mesa.setNrMesa(nrMesa);
		this.cliente.setNomeEmpresarial(nomeCliente);
		this.valorTotal = valorTotal;
		this.setLancheOuPratoPronto(lancheOuPratoPronto);
	}
	
	public PedidoVendaDTO(Long codigo, String nrMesa, String nomeCliente, BigDecimal valorTotal, LocalDateTime dataHoraCriacao, String observacao) {
		this.codigo = codigo;
		this.mesa.setNrMesa(nrMesa);
		this.cliente.setNomeEmpresarial(nomeCliente);
		this.valorTotal = valorTotal;
		this.dataHoraCriacao = dataHoraCriacao;
		this.observacao = observacao;
	}
	
	public PedidoVendaDTO(Long codigo, StatusPedidoVenda status) {
		this.codigo = codigo;
		this.status = status;
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public Mesa getMesa() {
		return mesa;
	}
	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public boolean isLancheOuPratoPronto() {
		return lancheOuPratoPronto;
	}
	public void setLancheOuPratoPronto(boolean lancheOuPratoPronto) {
		this.lancheOuPratoPronto = lancheOuPratoPronto;
	}
	public StatusPedidoVenda getStatus() {
		return status;
	}
	public void setStatus(StatusPedidoVenda status) {
		this.status = status;
	}

	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(LocalDateTime dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
