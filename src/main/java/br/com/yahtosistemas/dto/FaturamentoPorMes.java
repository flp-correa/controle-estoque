package br.com.yahtosistemas.dto;

import java.math.BigDecimal;

/**
 * 
 * @author Felipe Corrêa
 * 
 * Empresa: Yahto Sistemas
 * 
 */
public class FaturamentoPorMes {
	
	private String mes;
	private BigDecimal total;
	
	public FaturamentoPorMes() {}
	
	public FaturamentoPorMes(String mes, BigDecimal total) {
		this.mes = mes;
		this.total = total;
	}

	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}

	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}
