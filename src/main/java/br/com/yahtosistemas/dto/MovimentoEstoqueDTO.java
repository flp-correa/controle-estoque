package br.com.yahtosistemas.dto;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.UnidadeMedida;

public class MovimentoEstoqueDTO {

	private Long codigo;
	private BigDecimal valorUnitario;
	private Produto produto;
	private UnidadeMedida unidadeMedida;
	
	public MovimentoEstoqueDTO(Long codigo, Produto produto) {
		this.codigo = codigo;
		this.produto = produto;
	}

	public MovimentoEstoqueDTO(BigDecimal valorUnitario, UnidadeMedida unidadeMedida) {
		this.valorUnitario = valorUnitario;
		this.unidadeMedida = unidadeMedida;
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
}
