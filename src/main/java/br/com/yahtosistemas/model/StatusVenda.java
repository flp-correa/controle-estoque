package br.com.yahtosistemas.model;

public enum StatusVenda {

	EM_ANDAMENTO("Em andamento"),
	PENDENTE("Pendente"),
	FINALIZADA("Finalizada"), 
	CANCELADA("Cancelada");

	private String descricao;

	StatusVenda(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
