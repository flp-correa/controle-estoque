package br.com.yahtosistemas.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "fornecedor")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Fornecedor extends Estabelecimento {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "carregar_produtos")
	private Boolean carregarProdutos;
	
	@Length(max = 150, message = "Condição de pagamento deve ter no máximo 150 caracteres")
	@Column(name = "condicao_pagamento")
	private String condicaoPagamento;
	
	@ManyToMany
	@JoinTable(name = "fornecedor_produto", joinColumns = @JoinColumn(name = "codigo_fornecedor")
				, inverseJoinColumns = @JoinColumn(name = "codigo_produto"))	
	private List<Produto> produtos;
	
	public Boolean getCarregarProdutos() {
		return carregarProdutos;
	}

	public void setCarregarProdutos(Boolean carregarProdutos) {
		this.carregarProdutos = carregarProdutos;
	}

	public String getCondicaoPagamento() {
		return condicaoPagamento;
	}

	public void setCondicaoPagamento(String condicaoPagamento) {
		this.condicaoPagamento = condicaoPagamento;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String getTituloCadastro() {
		if (isDetalhes()) {
			return String.format("Detalhes do fornecedor - %s", this.getNomeEmpresarial());
		} else {
			return isNovo() ? "Cadastro de fornecedor" : String.format("Edição do fornecedor - %s", this.getNomeEmpresarial());
		}
	}
}
