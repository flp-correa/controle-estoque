package br.com.yahtosistemas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "empresa")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Empresa extends Estabelecimento {

	private static final long serialVersionUID = 1L;

	@Column(name = "possui_estoque")
	private Boolean possuiEstoque = Boolean.TRUE;

	public Empresa() {
		setTipoPessoa(TipoPessoa.JURIDICA);
	}
	
	public Boolean getPossuiEstoque() {
		return possuiEstoque;
	}

	public void setPossuiEstoque(Boolean possuiEstoque) {
		this.possuiEstoque = possuiEstoque;
	}

	@Override
	public String getTituloCadastro() {
		if (isDetalhes()) {
			return String.format("Detalhes da empresa - %s", this.getNomeEmpresarial());
		} else {
			return isNovo() ? "Cadastro de empresa" : String.format("Edição da empresa - %s", this.getNomeEmpresarial());
		}
	}
	
	public String getCodigoENomeEmpresarialEmpresa() {
		return String.format("%s - %s", this.getCodigo(), this.getNomeEmpresarial());
	}

}
