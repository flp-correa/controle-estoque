package br.com.yahtosistemas.model;

public enum TipoPedidoVenda {

	PEDIDO_CLIENTE("Para cliente"),
	PEDIDO_MESA("Para mesa");
	
	private String descricao;
	
	private TipoPedidoVenda(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
