package br.com.yahtosistemas.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "venda")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
@DynamicUpdate
public class Venda extends VendaPedidoVenda {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private StatusVenda status = StatusVenda.EM_ANDAMENTO;
	
	@ManyToOne
	@JoinColumn(name = "codigo_pedido_venda")
	private PedidoVenda pedidoVenda;
	
	@Column(name = "valor_dinheiro")
	private BigDecimal valorDinheiro;
	
	@Column(name = "valor_cartao")
	private BigDecimal valorCartao;
	
	@Column(name = "valor_acrescimo")
	private BigDecimal valorAcrescimo;

	@Column(name = "valor_desconto")
	private BigDecimal valorDesconto;

	@OneToMany(mappedBy = "venda", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ItemVenda> itens = new ArrayList<>();
	
	@Transient
	private TipoPagamento tipoPagamento;
	
	@Transient
	private BigDecimal valorTroco;

	@Transient
	private List<Produto> produtosSemSaldoEstoque = new ArrayList<>();;
	
	public StatusVenda getStatus() {
		return status;
	}

	public void setStatus(StatusVenda status) {
		this.status = status;
	}
	
	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}

	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}

	public BigDecimal getValorAcrescimo() {
		return valorAcrescimo;
	}

	public void setValorAcrescimo(BigDecimal valorAcrescimo) {
		this.valorAcrescimo = valorAcrescimo;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public List<ItemVenda> getItens() {
		return itens;
	}

	public void setItens(List<ItemVenda> itens) {
		this.itens = itens;
	}

	public TipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(TipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public BigDecimal getValorDinheiro() {
		return valorDinheiro;
	}

	public void setValorDinheiro(BigDecimal valorDinheiro) {
		this.valorDinheiro = valorDinheiro;
	}

	public BigDecimal getValorCartao() {
		return valorCartao;
	}

	public void setValorCartao(BigDecimal valorCartao) {
		this.valorCartao = valorCartao;
	}

	public BigDecimal getValorTroco() {
		return valorTroco;
	}

	public void setValorTroco(BigDecimal valorTroco) {
		this.valorTroco = valorTroco;
	}

	public List<Produto> getProdutosSemSaldoEstoque() {
		return produtosSemSaldoEstoque;
	}

	public void setProdutosSemSaldoEstoque(List<Produto> produtosSemSaldoEstoque) {
		this.produtosSemSaldoEstoque = produtosSemSaldoEstoque;
	}

	public void adicionarItens(List<ItemVendaPedidoVenda> itens) {
		this.itens = itens.stream()
			    .filter(e-> e instanceof ItemVenda)
			    .map(e->(ItemVenda) e)
			    .collect(Collectors.toList());
		
		this.itens.forEach(i -> i.setVenda(this));
	}
	
	public BigDecimal getValorTotalItens() {
		return getItens().stream()
				.map(ItemVenda::getValorTotal)
				.reduce(BigDecimal::add)
				.orElse(BigDecimal.ZERO);
	}
	
	public void calcularValorTotal() {
		setValorTotal(calcularValorTotal(getValorTotalItens(), getValorAcrescimo(), getValorFrete(), getValorDesconto()));
	}
	
	public boolean isCancelarPermitido() {
		return !getStatus().equals(StatusVenda.CANCELADA) && !isNovo();
	}
	
	public boolean isSalvarProibido() {
		return !getStatus().equals(StatusVenda.EM_ANDAMENTO);
	}
	
	public String getTituloCadastro() {
		if (isDetalhes()) {
			return String.format("Detalhes da venda %s", this.getCodigo());
		} else {
			return isNovo() ? "Cadastro de venda" : String.format("Venda de código %s", this.getCodigo());
		}
	}
	
	private BigDecimal calcularValorTotal(BigDecimal valorTotalItens, BigDecimal valorAcrescimo, BigDecimal valorFrete, BigDecimal valorDesconto) {
		BigDecimal valorTotal = valorTotalItens
				.add(Optional.ofNullable(valorAcrescimo).orElse(BigDecimal.ZERO))
				.add(Optional.ofNullable(valorFrete).orElse(BigDecimal.ZERO))
				.subtract(Optional.ofNullable(valorDesconto).orElse(BigDecimal.ZERO));
		return valorTotal;
	}
}