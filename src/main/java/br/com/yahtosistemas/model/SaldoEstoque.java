package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "saldo_estoque")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class SaldoEstoque implements Serializable {

	private static final long serialVersionUID = -9108316806737963227L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;
	
	@ManyToOne
	@JoinColumn(name = "codigo_movto_estoque")
	private MovimentoEstoque movimentoEstoque;
	
	@ManyToOne
	@JoinColumn(name = "codigo_localizacao")
	private Localizacao localizacao;
	
	@Column(name = "saldo_localizacao")
	private BigDecimal saldoLocalizacao;
	
	@NotNull(message = "saldo no estoque é obrigatório")
	@Column(name = "saldo_estoque")
	private BigDecimal saldoEstoque;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public MovimentoEstoque getMovimentoEstoque() {
		return movimentoEstoque;
	}

	public void setMovimentoEstoque(MovimentoEstoque movimentoEstoque) {
		this.movimentoEstoque = movimentoEstoque;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	public BigDecimal getSaldoLocalizacao() {
		return saldoLocalizacao;
	}

	public void setSaldoLocalizacao(BigDecimal saldoLocalizacao) {
		this.saldoLocalizacao = saldoLocalizacao;
	}

	public BigDecimal getSaldoEstoque() {
		return saldoEstoque;
	}

	public void setSaldoEstoque(BigDecimal saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaldoEstoque other = (SaldoEstoque) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
