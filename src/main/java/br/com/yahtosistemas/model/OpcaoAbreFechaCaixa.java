package br.com.yahtosistemas.model;

public enum OpcaoAbreFechaCaixa {

	RETIRAR_DINHEIRO("Retirar dinheiro"),
	ENTRAR_DINHEIRO("Entrar dinheiro"),
	FECHAR_CAIXA("Fechar caixa");
	
	private String descricao;
	
	private OpcaoAbreFechaCaixa(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
