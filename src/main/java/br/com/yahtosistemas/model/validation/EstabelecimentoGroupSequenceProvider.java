package br.com.yahtosistemas.model.validation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import br.com.yahtosistemas.model.Estabelecimento;

public class EstabelecimentoGroupSequenceProvider implements DefaultGroupSequenceProvider<Estabelecimento> {

	@Override
	public List<Class<?>> getValidationGroups(Estabelecimento estabelecimento) {
		List<Class<?>> grupos = new ArrayList<>();
		grupos.add(Estabelecimento.class);
		
		if (isPessoaSelecionada(estabelecimento)) {
			grupos.add(estabelecimento.getTipoPessoa().getGrupo());
		}
		
		return grupos;
	}

	private boolean isPessoaSelecionada(Estabelecimento estabelecimento) {
		return estabelecimento != null && estabelecimento.getTipoPessoa() != null;
	}

}
