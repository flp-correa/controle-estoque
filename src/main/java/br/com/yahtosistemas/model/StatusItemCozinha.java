package br.com.yahtosistemas.model;

import br.com.yahtosistemas.repository.ItensPedidosVendas;

public enum StatusItemCozinha {

	NAO_INICIADO("Não iniciado") {
		@Override
		public void salvarStatusItemCozinha(Long[] codigos, ItensPedidosVendas itensPedidosVendas) {
			itensPedidosVendas.salvarStatusItemCozinha(codigos, NAO_INICIADO);
		}
	},
	INICIADO("Iniciado") {
		@Override
		public void salvarStatusItemCozinha(Long[] codigos, ItensPedidosVendas itensPedidosVendas) {
			itensPedidosVendas.salvarStatusItemCozinha(codigos, INICIADO);
		}
	},
	PRONTO("Pronto") {
		@Override
		public void salvarStatusItemCozinha(Long[] codigos, ItensPedidosVendas itensPedidosVendas) {
			itensPedidosVendas.salvarStatusItemCozinha(codigos, PRONTO);
		}
	},
	ENTREGUE("Entregue") {
		@Override
		public void salvarStatusItemCozinha(Long[] codigos, ItensPedidosVendas itensPedidosVendas) {
			itensPedidosVendas.salvarStatusItemCozinha(codigos, ENTREGUE);
		}
	};

	private String descricao;

	StatusItemCozinha(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public abstract void salvarStatusItemCozinha(Long[] codigos, ItensPedidosVendas itensPedidosVendas);
}
