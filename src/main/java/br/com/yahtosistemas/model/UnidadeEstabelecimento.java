package br.com.yahtosistemas.model;

import java.util.Optional;

import br.com.yahtosistemas.repository.helper.EstabelecimentosQueries;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;

public enum UnidadeEstabelecimento {
	
	MATRIZ("Matriz") {
		@Override
		public void validarUnidadeEstabelecimento(EstabelecimentosQueries repository, Estabelecimento estabelecimento) {
			if (estabelecimento.isNovo()) {
				validarSeCnpjEstabelecimentoJaExiste(repository, estabelecimento);
			}
			
			Optional<Estabelecimento> matrizCadastrada = repository.seJaExisteMatrizCadastrada(estabelecimento.getCpfOuCnpjSemFormatacao().substring(0, 8)); 
			if (matrizCadastrada.isPresent() && !estabelecimento.isCpfOuCnpjIguais(matrizCadastrada.get().getCpfOuCnpjSemFormatacao())) {
				
				throw new EstabelecimentoJaCadastradaUnidadeMatrizException("Estabelecimento já possui uma unidade cadastrada como matriz: CNPJ " +
						matrizCadastrada.get().getCpfOuCnpj());
			}
		}
	}, 
	FILIAL("Filial") {
		@Override
		public void validarUnidadeEstabelecimento(EstabelecimentosQueries repository, Estabelecimento estabelecimento) {
			if (estabelecimento.isNovo()) {
				validarSeCnpjEstabelecimentoJaExiste(repository, estabelecimento);
			}
		}
	};
	
	private String descricao;
	
	UnidadeEstabelecimento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public abstract void validarUnidadeEstabelecimento(EstabelecimentosQueries repository, Estabelecimento estabelecimento);
	
	protected void validarSeCnpjEstabelecimentoJaExiste(EstabelecimentosQueries repository, Estabelecimento estabelecimento) {
		if (repository.seJaExisteCnpjCadastrado(estabelecimento.getCpfOuCnpjSemFormatacao()) != null) {
			throw new EntidadeJaCadastradaException("CNPJ já cadastrado");
		}
	}
}