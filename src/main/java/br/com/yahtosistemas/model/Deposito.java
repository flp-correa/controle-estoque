package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;

import br.com.yahtosistemas.validation.ValidarRelacionamentoManyToOne;

@Entity
@Table(name = "deposito")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Deposito implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;

	@ValidarRelacionamentoManyToOne(message = "Empresa deve ser informada", buscarNoPai = true)
	@ManyToOne
	@JoinColumn(name = "codigo_empresa")
	private Empresa empresa;

	@NotBlank(message = "Descrição deve ser informada")
	@Length(max = 100, message = "Descrição deve ter no máximo 100 caracteres")
	private String descricao;
	
	@Length(max = 200, message = "Observação deve ter no máximo 200 caracteres")
	private String observacao;
	
	private Boolean situacao = Boolean.TRUE;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();

	@Transient
	private boolean detalhes;
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getSituacao() {
		return situacao;
	}

	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes do depósito - %s", this.getDescricao());
		} else {
			return isNovo() ? "Cadastro de depósito" : String.format("Edição do depósito - %s", this.getDescricao());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Deposito other = (Deposito) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
