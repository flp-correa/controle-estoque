package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.hibernate.validator.group.GroupSequenceProvider;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.yahtosistemas.model.validation.EstabelecimentoGroupSequenceProvider;
import br.com.yahtosistemas.model.validation.group.CnpjGroup;
import br.com.yahtosistemas.model.validation.group.CpfGroup;

@MappedSuperclass
@GroupSequenceProvider(EstabelecimentoGroupSequenceProvider.class)
public abstract class Estabelecimento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;
	
	@Column(name = "unidade")
	@NotNull(groups = CnpjGroup.class, message = "Unidade é obrigatório")
	@Enumerated(EnumType.STRING)
	private UnidadeEstabelecimento unidadeEmpresa;
	
	@NotNull(message = "Tipo pessoa é obrigatório")
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pessoa")
	private TipoPessoa tipoPessoa;

	@CPF(groups = CpfGroup.class)
	@CNPJ(groups = CnpjGroup.class)
	@Column(name = "cpf_cnpj")
	private String cpfOuCnpj;
	
	@Length(max = 150, message = "Nome empresarial deve ter no máximo 150 caracteres")
	@NotBlank(groups = CpfGroup.class, message = "Nome é obrigatório")
	@NotBlank(groups = CnpjGroup.class, message = "Nome empresarial é obrigatório")
	@Column(name = "nome_empresarial")
	private String nomeEmpresarial;
	
	@Length(max = 150, message = "Nome fantasia deve ter no máximo 150 caracteres")
	@Column(name = "nome_fantasia")
	private String nomeFantasia;
	
	@NotBlank(groups = {CpfGroup.class, CnpjGroup.class}, message = "E-mail é obrigatório")
	@Length(groups = {CpfGroup.class, CnpjGroup.class}, max = 50, message = "E-mail deve ter no máximo 50 caracteres")
	@Email(groups = {CpfGroup.class, CnpjGroup.class}, message = "E-mail inválido")
	private String email;
	
	private String telefone1;
	
	private String telefone2;
	
	private String celular;
	
	private String fax;
	
	@JsonIgnore
	@Valid
	@Embedded
	private Endereco endereco;

	@Length(max = 300, message = "Observação deve ter no máximo 300 caracteres")
	private String observacao;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();
	
	@Transient
	private String uuid;
	
	@Transient
	private boolean detalhes;
	
	public abstract String getTituloCadastro();
	
	@PrePersist @PreUpdate
	private void prePersistPreUpdate() {
		if (this.cpfOuCnpj != null) {
			this.cpfOuCnpj = TipoPessoa.removerFormatacao(this.cpfOuCnpj);
		}
	}
	
	@PostLoad
	private void postLoad() {
		if (this.cpfOuCnpj != null) {
			this.cpfOuCnpj = this.tipoPessoa.formatar(this.cpfOuCnpj);
		}
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public UnidadeEstabelecimento getUnidadeEmpresa() {
		return unidadeEmpresa;
	}

	public void setUnidadeEmpresa(UnidadeEstabelecimento unidadeEmpresa) {
		this.unidadeEmpresa = unidadeEmpresa;
	}
	
	public TipoPessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(TipoPessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public String getNomeEmpresarial() {
		return nomeEmpresarial;
	}

	public void setNomeEmpresarial(String nomeEmpresarial) {
		this.nomeEmpresarial = nomeEmpresarial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isDetalhes() {
		return detalhes;
	}

	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public boolean isCpfOuCnpjIguais(String cpfOuCnpj) {
		return getCpfOuCnpjSemFormatacao().equals(cpfOuCnpj);
	}
	
	public String getCpfOuCnpjSemFormatacao() {
		return TipoPessoa.removerFormatacao(this.cpfOuCnpj);
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estabelecimento other = (Estabelecimento) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
