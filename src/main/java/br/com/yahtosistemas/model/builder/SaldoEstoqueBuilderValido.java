package br.com.yahtosistemas.model.builder;

import br.com.yahtosistemas.model.SaldoEstoque;

public class SaldoEstoqueBuilderValido {

	private SaldoEstoque instancia;

	public SaldoEstoqueBuilderValido(SaldoEstoque instancia) {
		this.instancia = instancia;
	}
	
	public SaldoEstoque construir() {
		return instancia;
	}
}
