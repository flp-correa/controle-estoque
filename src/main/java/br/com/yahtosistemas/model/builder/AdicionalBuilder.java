package br.com.yahtosistemas.model.builder;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.Adicional;

public class AdicionalBuilder {

	private Adicional instancia;
	
	public AdicionalBuilder() {
		instancia = new Adicional();
	}
	
	public AdicionalBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}
	
	public AdicionalBuilder comNome(String nome) {
		instancia.setNome(nome);
		return this;
	}
	
	public AdicionalBuilder comValorUnitario(BigDecimal valorUnitario) {
		instancia.setValorUnitario(valorUnitario);
		return this;
	}
	
	public Adicional construir() {
		return instancia;
	}
}
