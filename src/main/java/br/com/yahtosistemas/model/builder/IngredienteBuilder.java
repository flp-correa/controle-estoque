package br.com.yahtosistemas.model.builder;

import br.com.yahtosistemas.model.Ingrediente;

public class IngredienteBuilder {

	private Ingrediente instancia;
	
	public IngredienteBuilder() {
		instancia = new Ingrediente();
	}
	
	public IngredienteBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}
	
	public IngredienteBuilder comNome(String nome) {
		instancia.setNome(nome);
		return this;
	}
	
	public Ingrediente construir() {
		return instancia;
	}
}
