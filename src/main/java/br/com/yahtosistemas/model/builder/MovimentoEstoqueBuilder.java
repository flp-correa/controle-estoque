package br.com.yahtosistemas.model.builder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import br.com.yahtosistemas.model.DestinoSaidaEstoque;
import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.UnidadeMedida;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.Venda;
import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;

public class MovimentoEstoqueBuilder {

	private MovimentoEstoque instancia;
	
	public MovimentoEstoqueBuilder() {
		this.instancia = new MovimentoEstoque();
	}
	
	public MovimentoEstoqueBuilder comTipoMovimento(TipoMovimentoEstoque tipoMovimento) {
		instancia.setTipoMovimento(tipoMovimento);
		return this;
	}
	
	public MovimentoEstoqueBuilder comFornecedor(Long codigo) {
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setCodigo(codigo);
		instancia.setFornecedor(fornecedor);
		return this;
	}
	
	public MovimentoEstoqueBuilder comProduto(Produto produto) {
		instancia.setProduto(produto);
		return this;
	}
	
	public MovimentoEstoqueBuilder comQuantidade(BigDecimal quantidade) {
		instancia.setQuantidade(quantidade);
		return this;
	}
	
	public MovimentoEstoqueBuilder comValorUnitario(BigDecimal valorUnitario) {
		instancia.setValorUnitario(valorUnitario);
		return this;
	}
	
	public MovimentoEstoqueBuilder comVenda(Venda venda) {
		instancia.setVenda(venda);
		return this;
	}
	
	public MovimentoEstoqueBuilder comEmpresaDestino(Empresa empresa) {
		instancia.setEmpresaDestino(empresa);
		return this;
	}
	
	public MovimentoEstoqueBuilder comUsuario(Usuario usuario) {
		instancia.setUsuario(usuario);
		return this;
	}
	
	public MovimentoEstoqueBuilder comUnidadeMedida(UnidadeMedida unidadeMedida) {
		instancia.setUnidadeMedida(unidadeMedida);
		return this;
	}
	
	public MovimentoEstoqueBuilder comDataHoraCadastro(LocalDateTime dataHoraCadastro) {
		instancia.setDataHoraCadastro(dataHoraCadastro);
		return this;
	}
		
	public MovimentoEstoqueBuilder comDestinoSaidaEstoque(DestinoSaidaEstoque destinoSaidaEstoque) {
		instancia.setDestinoSaidaEstoque(destinoSaidaEstoque);
		return this;
	}
	
	public MovimentoEstoqueBuilder comDevolucaoVenda(boolean devolucaoVenda) {
		instancia.setDevolucaoVenda(devolucaoVenda);
		return this;
	}
	
	public MovimentoEstoqueBuilderValido comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return new MovimentoEstoqueBuilderValido(instancia);
	}
	
}
