package br.com.yahtosistemas.model.builder;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.ItemVenda;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.Venda;

public class ItemVendaBuilder {

	private ItemVenda instancia;
	
	public ItemVendaBuilder() {
		instancia = new ItemVenda();
	}
	
	public ItemVendaBuilder comTenantId(String tenantId) {
		instancia.setTenantId(tenantId);
		return this;
	}
	
	public ItemVendaBuilder comQuantidade(Integer quantidade) {
		instancia.setQuantidade(quantidade);
		return this;
	}
	
	public ItemVendaBuilder comValorUnitario(BigDecimal valorUnitario) {
		instancia.setValorUnitario(valorUnitario);
		return this;
	}
	
	public ItemVendaBuilder comProduto(Produto produto) {
		instancia.setProduto(produto);
		return this;
	}
	
	public ItemVendaBuilder comVenda(Venda venda) {
		instancia.setVenda(venda);
		return this;
	}
	
	public ItemVendaBuilderValido comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return new ItemVendaBuilderValido(instancia);
	}
	
}
