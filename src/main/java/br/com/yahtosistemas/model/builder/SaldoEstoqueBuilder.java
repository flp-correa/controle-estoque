package br.com.yahtosistemas.model.builder;

import java.math.BigDecimal;

import br.com.yahtosistemas.model.Localizacao;
import br.com.yahtosistemas.model.MovimentoEstoque;
import br.com.yahtosistemas.model.SaldoEstoque;

public class SaldoEstoqueBuilder {

	private SaldoEstoque instancia;
	
	public SaldoEstoqueBuilder() {
		instancia = new SaldoEstoque();
	}
	
	public SaldoEstoqueBuilder comTenantId(String tenantId) {
		instancia.setTenantId(tenantId);
		return this;
	}
	
	public SaldoEstoqueBuilder comMovimentoEstoque(MovimentoEstoque movimentoEstoque) {
		instancia.setMovimentoEstoque(movimentoEstoque);
		return this;
	}
	
	public SaldoEstoqueBuilder comLocalizacao(Localizacao localizacao) {
		instancia.setLocalizacao(localizacao);
		return this;
	}
	
	public SaldoEstoqueBuilder comSaldoLocalizacao(BigDecimal saldoLocalizacao) {
		instancia.setSaldoLocalizacao(saldoLocalizacao);
		return this;
	}
	
	public SaldoEstoqueBuilder comSaldoEstoque(BigDecimal saldoEstoque) {
		instancia.setSaldoEstoque(saldoEstoque);
		return this;
	}
	
	public SaldoEstoqueBuilderValido comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		
		return new SaldoEstoqueBuilderValido(instancia);
	}
}
