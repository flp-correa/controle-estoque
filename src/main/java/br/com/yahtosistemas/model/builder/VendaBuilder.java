package br.com.yahtosistemas.model.builder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import br.com.yahtosistemas.model.Cliente;
import br.com.yahtosistemas.model.ItemVenda;
import br.com.yahtosistemas.model.Mesa;
import br.com.yahtosistemas.model.PedidoVenda;
import br.com.yahtosistemas.model.StatusVenda;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.Venda;

public class VendaBuilder {

	private Venda instancia;
	
	public VendaBuilder() {
		instancia = new Venda();
	}
	
	public VendaBuilder comTenantId(String tenantId) {
		instancia.setTenantId(tenantId);
		return this;
	}
	
	public VendaBuilder comDataHoraCriacao(LocalDateTime dataHoraCriacao) {
		instancia.setDataHoraCriacao(dataHoraCriacao);
		return this;
	}
	
	public VendaBuilder comValorFrete(BigDecimal valorFrete) {
		instancia.setValorFrete(valorFrete);
		return this;
	}
	
	public VendaBuilder comDataHoraEntrega(LocalDateTime dataHoraEntrega) {
		instancia.setDataHoraEntrega(dataHoraEntrega);
		return this;
	}
	
	public VendaBuilder comValorDinheiro(BigDecimal valorDinheiro) {
		instancia.setValorDinheiro(valorDinheiro);
		return this;
	}
	
	public VendaBuilder comValorCartao(BigDecimal valorCartao) {
		instancia.setValorCartao(valorCartao);
		return this;
	}
	
	public VendaBuilder comValorTotal(BigDecimal valorTotal) {
		instancia.setValorTotal(valorTotal);
		return this;
	}
	
	public VendaBuilder comCliente(Cliente cliente) {
		instancia.setCliente(cliente); 
		return this;
	}
	
	public VendaBuilder comMesa(Mesa mesa) {
		instancia.setMesa(mesa);
		return this;
	}
	
	public VendaBuilder comUsuario(Usuario usuario) {
		instancia.setUsuario(usuario);
		return this;
	}
	
	public VendaBuilder comStatus(StatusVenda status) {
		instancia.setStatus(status);
		return this;
	}
	
	public VendaBuilder comObservacao(String observacao) {
		instancia.setObservacao(observacao);
		return this;
	}
	
	public VendaBuilder comPedidoVenda(PedidoVenda pedidoVenda) {
		instancia.setPedidoVenda(pedidoVenda);
		return this;
	}
	
	public VendaBuilder comValorAcrescimo(BigDecimal valorAcrescimo) {
		instancia.setValorAcrescimo(valorAcrescimo);
		return this;
	}
	
	public VendaBuilder comValorDesconto(BigDecimal valorDesconto) {
		instancia.setValorDesconto(valorDesconto);
		return this;
	}
	
	public VendaBuilder comItens(List<ItemVenda> itens) {
		instancia.setItens(itens);
		return this;
	}
	
	public VendaBuilderValido comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		
		return new VendaBuilderValido(instancia);
	}
	
}
