package br.com.yahtosistemas.model.builder;

import br.com.yahtosistemas.model.MovimentoEstoque;

public class MovimentoEstoqueBuilderValido {

	private MovimentoEstoque instancia;

	public MovimentoEstoqueBuilderValido(MovimentoEstoque instancia) {
		this.instancia = instancia;
	}
	
	public MovimentoEstoque construir() {
		return instancia;
	}
}
