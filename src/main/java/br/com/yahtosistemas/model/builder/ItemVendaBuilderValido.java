package br.com.yahtosistemas.model.builder;

import br.com.yahtosistemas.model.ItemVenda;

public class ItemVendaBuilderValido {

	private ItemVenda instancia;

	public ItemVendaBuilderValido(ItemVenda instancia) {
		this.instancia = instancia;
	}
	
	public ItemVenda construir() {
		return instancia;
	}
}
