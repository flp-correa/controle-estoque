package br.com.yahtosistemas.model.builder;

import br.com.yahtosistemas.model.Venda;

public class VendaBuilderValido {

	private Venda instancia;

	public VendaBuilderValido(Venda instancia) {
		this.instancia = instancia;
	}
	
	public Venda construir() {
		return instancia;
	}
}
