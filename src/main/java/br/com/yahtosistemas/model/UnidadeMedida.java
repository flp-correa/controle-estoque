package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;

import br.com.yahtosistemas.service.unidadeMedida.FamiliaUnidadeMedida;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@Entity
@Table(name = "unidade_medida")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class UnidadeMedida implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;
	
	@Column(name = "familia")
	@Enumerated(EnumType.STRING)
	private FamiliaUnidadeMedida familia;	

	@NotNull(message = "Unidade de medida é obrigatória")
	@Column(name = "unidade_medida")
	@Enumerated(EnumType.STRING)
	private TipoUnidadeMedida tipoUnidadeMedida;
	
	private BigDecimal quantidade;
	
	@Length(max = 200, message = "Observação deve conter no máximo 200 caracteres")
	private String observacao;
	
	private Boolean situacao = Boolean.TRUE;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();

	@Transient
	private boolean detalhes;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public FamiliaUnidadeMedida getFamilia() {
		return familia;
	}

	public void setFamilia(FamiliaUnidadeMedida familia) {
		this.familia = familia;
	}

	public TipoUnidadeMedida getTipoUnidadeMedida() {
		return tipoUnidadeMedida;
	}

	public void setTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida) {
		this.tipoUnidadeMedida = tipoUnidadeMedida;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public Boolean getSituacao() {
		return situacao;
	}
	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public boolean isDetalhes() {
		return detalhes;
	}

	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}

	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getDescricaoComQuantidade() {
		return this.tipoUnidadeMedida != null ? this.tipoUnidadeMedida.obterUnidadeMedida().getDescricaoComQuantidade(quantidade) : "";
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes da unidade de medida- %s", this.tipoUnidadeMedida.getDescricao());
		} else {
			return isNovo() ? "Cadastro de unidade de medida" : 
				String.format("Edição da unidade de medida - %s", this.tipoUnidadeMedida.getDescricao());
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeMedida other = (UnidadeMedida) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
