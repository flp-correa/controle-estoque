package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;

import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;
import br.com.yahtosistemas.validation.ValidarEnderecoLocalizacao;
import br.com.yahtosistemas.validation.ValidarRelacionamentoManyToOne;

@ValidarEnderecoLocalizacao(atributoBlocado = "blocado", atributoEndereco = "endereco")
@Entity
@Table(name = "localizacao")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Localizacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;

	@ValidarRelacionamentoManyToOne(message = "Depósito deve ser informado", buscarNoPai = false)
	@ManyToOne
	@JoinColumn(name = "codigo_deposito")
	private Deposito deposito;
	
	@ManyToOne
	@JoinColumn(name = "codigo_produto")
	private Produto produto;
	
	@NotNull(message = "Unidade de medida é obrigatória")
	@Column(name = "unidade_medida")
	@Enumerated(EnumType.STRING)
	private TipoUnidadeMedida tipoUnidadeMedida;
	
	@NotNull(message = "Estoque mínimo é obrigatório")
	@Min(value = 0)
	@Column(name = "estoque_minimo")
	private Integer estoqueMinimo;
	
	@NotNull(message = "Estoque máximo é obrigatório")
	@DecimalMin(value = "0.001")
	@Column(name = "estoque_maximo")
	private Integer estoqueMaximo;
	
	private Boolean blocado = Boolean.TRUE;
	
	private String endereco;

	@NotBlank(message = "Descrição deve ser informada")
	@Length(max = 100, message = "Descrição deve ter no máximo 100 caracteres")
	private String descricao;
	
	@Length(max = 200, message = "Observação deve ter no máximo 200 caracteres")
	private String observacao;
	
	private Boolean situacao = Boolean.TRUE;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();
	
	@Transient
	private BigDecimal saldoEstoque;
	
	@Transient
	private boolean detalhes;
	
	@PrePersist @PreUpdate
	private void prePersistPreUpdate() {
		if (this.endereco != null) {
			this.endereco = getEnderecoSemFormatacao();
		}
	}
	
	@PostLoad
	private void postLoad() {
		if (this.endereco != null) {
			this.endereco = getEnderecoComFormatacao();
		}
		if (this.tipoUnidadeMedida.equals(TipoUnidadeMedida.KG)) {
			this.estoqueMinimo = this.estoqueMinimo / 1000;
			this.estoqueMaximo = this.estoqueMaximo / 1000;
		}
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Deposito getDeposito() {
		return deposito;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public TipoUnidadeMedida getTipoUnidadeMedida() {
		return tipoUnidadeMedida;
	}

	public void setTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida) {
		this.tipoUnidadeMedida = tipoUnidadeMedida;
	}
	
	public Integer getEstoqueMinimo() {
		return estoqueMinimo;
	}

	public void setEstoqueMinimo(Integer estoqueMinimo) {
		this.estoqueMinimo = estoqueMinimo;
	}
	
	public Integer getEstoqueMaximo() {
		return estoqueMaximo;
	}

	public void setEstoqueMaximo(Integer estoqueMaximo) {
		this.estoqueMaximo = estoqueMaximo;
	}

	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}

	public Boolean getBlocado() {
		return blocado;
	}

	public void setBlocado(Boolean blocado) {
		this.blocado = blocado;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getSituacao() {
		return situacao;
	}

	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public BigDecimal getSaldoEstoque() {
		return saldoEstoque;
	}

	public void setSaldoEstoque(BigDecimal saldoEstoque) {
		this.saldoEstoque = saldoEstoque;
		if (this.saldoEstoque.compareTo(BigDecimal.ZERO) > 0 && this.produto.getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {
			this.saldoEstoque = this.saldoEstoque.divide(new BigDecimal(1000));
		}
	}

	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes da localização - %s", this.getDescricao());
		} else {
			return isNovo() ? "Cadastro de localização" : String.format("Edição da localização - %s", this.getDescricao());
		}
	}
	
	public String getEnderecoSemFormatacao() {
		return this.endereco.replaceAll("-", "");
	}
	
	public String getEnderecoComFormatacao() {
		return this.blocado ? this.endereco.replaceAll("(\\d{2})(\\d{2})", "$1-$2-") : 
			this.endereco.replaceAll("(\\d{2})(\\d{2})(\\d{3})(\\d{2})(\\d{1})", "$1-$2-$3-$4-$5");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localizacao other = (Localizacao) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
