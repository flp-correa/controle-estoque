package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;

import br.com.yahtosistemas.service.movimentoEstoque.TipoMovimentoEstoque;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;
import br.com.yahtosistemas.validation.ValidarDestinoSaidaEstoque;

@ValidarDestinoSaidaEstoque(atributoDestinoSaidaEstoque = "destinoSaidaEstoque", atributoEmpresaDestino = "empresaDestino", atributoVenda = "venda")
@Entity
@Table(name = "movimento_estoque")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class MovimentoEstoque implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;
	
	@ManyToOne
	@JoinColumn(name = "codigo_produto")
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name = "codigo_fornecedor")
	private Fornecedor fornecedor;
	
	@ManyToOne
	@JoinColumn(name = "codigo_venda")
	private Venda venda;
	
	@ManyToOne
	@JoinColumn(name = "codigo_empresa")
	private Empresa empresaDestino;
	
	@ManyToOne
	@JoinColumn(name = "codigo_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "codigo_unidade_medida")
	private UnidadeMedida unidadeMedida;
	
	@NotNull(message = "Tipo de movimento é obrigatório")
	@Column(name = "tipo_movimento")
	@Enumerated(EnumType.STRING)
	private TipoMovimentoEstoque tipoMovimento;
	
	@NotNull(message = "Quantidade é obrigatória")
	@Min(value = 1, message = "Quantidade deve ser maior ou igual a 1")
	private BigDecimal quantidade;
	
	@Column(name = "valor_unitario")
	private BigDecimal valorUnitario;
	
	@Length(max = 15, message = "Lote deve conter no máximo 15 caracteres")
	private String lote;
	
	@Column(name = "validade_lote")
	private LocalDate validadeLote;
	
	@Length(max = 200, message = "Observação deve ter no máximo 200 caracteres")
	private String observacao;
	
	@Column(name = "data_hora_cadastro")
	private LocalDateTime dataHoraCadastro = LocalDateTime.now();
	
	@OneToMany(mappedBy = "movimentoEstoque", cascade = CascadeType.PERSIST)
	private List<SaldoEstoque> saldosEstoque = new ArrayList<>();

	@Transient
	private BigDecimal quilograma;
	
	@Transient
	private BigDecimal valorTotal;
	
	@Transient
	private DestinoSaidaEstoque destinoSaidaEstoque;
	
	@Transient
	private Localizacao localizacaoTransferencia; 
	
	@Transient
	private boolean devolucaoVenda = false;
	
	@Transient
	private boolean detalhes;
	
	@PrePersist @PreUpdate
	private void prePersistUpdate() {
		if (this.unidadeMedida.getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {
			this.setQuantidade(this.quantidade.multiply(new BigDecimal(1000)));
		}
	}
	
	@PostLoad
	private void postLoad() {
		if (!this.tipoMovimento.equals(TipoMovimentoEstoque.TRANSFERENCIA)) {			
			if (this.unidadeMedida.getTipoUnidadeMedida().equals(TipoUnidadeMedida.KG)) {
				this.quantidade = this.quantidade.divide(new BigDecimal(1000));
				this.quilograma = this.quantidade;
			}
			setValorTotal(this.quantidade.multiply(this.valorUnitario));
		}
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public Empresa getEmpresaDestino() {
		return empresaDestino;
	}

	public void setEmpresaDestino(Empresa empresaDestino) {
		this.empresaDestino = empresaDestino;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoMovimentoEstoque getTipoMovimento() {
		return tipoMovimento;
	}

	public void setTipoMovimento(TipoMovimentoEstoque tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public BigDecimal getQuilograma() {
		return quilograma;
	}

	public void setQuilograma(BigDecimal quilograma) {
		this.quilograma = quilograma;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public LocalDate getValidadeLote() {
		return validadeLote;
	}

	public void setValidadeLote(LocalDate validadeLote) {
		this.validadeLote = validadeLote;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LocalDateTime getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(LocalDateTime dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public List<SaldoEstoque> getSaldosEstoque() {
		return saldosEstoque;
	}

	public void setSaldosEstoque(List<SaldoEstoque> saldosEstoque) {
		this.saldosEstoque = saldosEstoque;
	}
	
	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public DestinoSaidaEstoque getDestinoSaidaEstoque() {
		return destinoSaidaEstoque;
	}

	public void setDestinoSaidaEstoque(DestinoSaidaEstoque destinoSaidaEstoque) {
		this.destinoSaidaEstoque = destinoSaidaEstoque;
	}
	
	public Localizacao getLocalizacaoTransferencia() {
		return localizacaoTransferencia;
	}

	public void setLocalizacaoTransferencia(Localizacao localizacaoTransferencia) {
		this.localizacaoTransferencia = localizacaoTransferencia;
	}
	
	public boolean isDevolucaoVenda() {
		return devolucaoVenda;
	}

	public void setDevolucaoVenda(boolean devolucaoVenda) {
		this.devolucaoVenda = devolucaoVenda;
	}

	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public boolean produtoInformado() {
		return this.produto != null && this.produto.getCodigo() != null;
	}
	
	public void adicionarSaldoEstoque(List<SaldoEstoque> saldosEstoque) {
		this.saldosEstoque = saldosEstoque;
		this.saldosEstoque.forEach(s -> s.setMovimentoEstoque(this));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovimentoEstoque other = (MovimentoEstoque) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
