package br.com.yahtosistemas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "cliente")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Cliente extends Estabelecimento {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "permite_fiado")
	private Boolean permiteFiado = Boolean.FALSE;
	
	public Boolean getPermiteFiado() {
		return permiteFiado;
	}

	public void setPermiteFiado(Boolean permiteFiado) {
		this.permiteFiado = permiteFiado;
	}

	@NotBlank(message= "Celular é obrigatório")
	@Override
	public String getCelular() {
		return super.getCelular();
	}
	
	@Override
	public String getTituloCadastro() {
		if (isDetalhes()) {
			return String.format("Detalhes do cliente - %s", this.getNomeEmpresarial());
		} else {
			return isNovo() ? "Cadastro de cliente" : String.format("Edição do cliente - %s", this.getNomeEmpresarial());
		}
	}
}
