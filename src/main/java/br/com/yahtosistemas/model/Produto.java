package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.yahtosistemas.repository.listener.ProdutoEntityListener;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@EntityListeners(ProdutoEntityListener.class)
@Entity
@Table(name = "produto")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;
	
	@NotNull(message = "Unidade de medida é obrigatória")
	@Column(name = "unidade_medida")
	@Enumerated(EnumType.STRING)
	private TipoUnidadeMedida tipoUnidadeMedida;

	@Length(max = 100, message = "Descrição deve ter no máximo 100 caracteres")
	@NotBlank(message = "Descrição do produto é obrigatória")
	private String descricao;
	
	private Boolean vende = Boolean.TRUE;
	
	@Column(name = "lanche_prato")
	private Boolean lancheOuPrato = Boolean.FALSE;
	
	@Column(name = "por_quilo")
	private Boolean porQuilo = Boolean.FALSE;
	
	@Column(name = "enviar_cozinha")
	private Boolean enviarCozinha = Boolean.FALSE;
	
	@Column(name = "enviar_compra")
	private Boolean enviarCompra = Boolean.FALSE;

	@Column(name = "possui_estoque")
	private Boolean possuiEstoque = Boolean.TRUE;
	
	@Column(name = "local_fixo")
	private Boolean localFixo = Boolean.TRUE;

	@NotNull(message = "Valor unitário é obrigatório")
	@Min(value = 0)
	@Column(name = "valor_unitario")
	private BigDecimal valorUnitario = BigDecimal.ZERO;
	
	@Length(max = 200, message = "Observação deve ter no máximo 200 caracteres")
	private String observacao;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();
	
	@JsonIgnore
	@OneToMany(mappedBy = "produto")
	private List<Localizacao> localizacoes;

	@JsonIgnore
	@OneToMany(mappedBy = "produto")
	private List<MovimentoEstoque> movimentacoesEstoque;
	
	private String foto;

	@Column(name = "content_type")
	private String contentType;
	
	@ManyToMany
	@JoinTable(name = "produto_ingrediente", joinColumns = @JoinColumn(name = "codigo_produto")
	, inverseJoinColumns = @JoinColumn(name = "codigo_ingrediente"))
	private List<Ingrediente> ingredientes;

	@Transient
	private boolean novaFoto;
	
	@Transient
	private String urlFoto;

	@Transient
	private String urlThumbnailFoto;
	
	@Transient
	private String uuid;
	
	@Transient
	private boolean detalhes;
	
	@Transient
	private List<Fornecedor> fornecedores;
	
	@Transient
	private Localizacao localizacao;
	
	@Transient
	private BigDecimal quantidadeEstoque = BigDecimal.ZERO;
	
	@Transient
	private BigDecimal valorTotalEstoque = BigDecimal.ZERO;
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public Boolean getPossuiEstoque() {
		return possuiEstoque;
	}

	public void setPossuiEstoque(Boolean possuiEstoque) {
		this.possuiEstoque = possuiEstoque;
	}
	
	public Boolean getVende() {
		return vende;
	}

	public void setVende(Boolean vende) {
		this.vende = vende;
	}

	public Boolean getLancheOuPrato() {
		return lancheOuPrato;
	}

	public void setLancheOuPrato(Boolean lancheOuPrato) {
		this.lancheOuPrato = lancheOuPrato;
	}
	
	public Boolean getPorQuilo() {
		return porQuilo;
	}

	public void setPorQuilo(Boolean porQuilo) {
		this.porQuilo = porQuilo;
	}
	
	public Boolean getEnviarCozinha() {
		return enviarCozinha;
	}

	public void setEnviarCozinha(Boolean enviarCozinha) {
		this.enviarCozinha = enviarCozinha;
	}

	public Boolean getEnviarCompra() {
		return enviarCompra;
	}

	public void setEnviarCompra(Boolean enviarCompra) {
		this.enviarCompra = enviarCompra;
	}

	public Boolean getLocalFixo() {
		return localFixo;
	}

	public void setLocalFixo(Boolean localFixo) {
		this.localFixo = localFixo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public TipoUnidadeMedida getTipoUnidadeMedida() {
		return tipoUnidadeMedida;
	}

	public void setTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida) {
		this.tipoUnidadeMedida = tipoUnidadeMedida;
	}

	public BigDecimal getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(BigDecimal quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
		if (this.quantidadeEstoque.compareTo(BigDecimal.ZERO) > 0 && this.tipoUnidadeMedida != null && this.tipoUnidadeMedida.equals(TipoUnidadeMedida.KG)) {
			this.quantidadeEstoque = this.quantidadeEstoque.divide(new BigDecimal(1000));
		}
	}
	
	public BigDecimal getValorTotalEstoque() {
		return valorTotalEstoque;
	}

	public void setValorTotalEstoque(BigDecimal valorTotalEstoque) {
		this.valorTotalEstoque = valorTotalEstoque;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public List<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	public List<MovimentoEstoque> getMovimentacoesEstoque() {
		return movimentacoesEstoque;
	}

	public void setMovimentacoesEstoque(List<MovimentoEstoque> movimentacoesEstoque) {
		this.movimentacoesEstoque = movimentacoesEstoque;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public boolean isNovaFoto() {
		return novaFoto;
	}

	public void setNovaFoto(boolean novaFoto) {
		this.novaFoto = novaFoto;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public String getUrlThumbnailFoto() {
		return urlThumbnailFoto;
	}

	public void setUrlThumbnailFoto(String urlThumbnailFoto) {
		this.urlThumbnailFoto = urlThumbnailFoto;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	public List<Localizacao> getLocalizacoes() {
		return localizacoes;
	}

	public void setLocalizacoes(List<Localizacao> localizacoes) {
		this.localizacoes = localizacoes;
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getFotoOuMock() {
		return !StringUtils.isEmpty(foto) ? foto : "cerveja-mock.png";
	}
	public boolean temFoto() {
		return !StringUtils.isEmpty(this.foto);
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes do produto - %s", this.getDescricao());
		} else {
			return isNovo() ? "Cadastro de produto" : String.format("Edição do produto - %s", this.getDescricao());
		}
	}

	public void adicionarLocalizacao(List<Localizacao> localizadoes) {
		this.localizacoes = localizadoes;
		this.localizacoes.forEach(l -> l.setProduto(this));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
