package br.com.yahtosistemas.model;

import java.math.BigDecimal;

import br.com.yahtosistemas.repository.Vendas;
import br.com.yahtosistemas.service.exception.ValorDinheiroEValorCartaoIncorretosException;

public enum TipoPagamento {

	DINHEIRO("Dinheiro") {
		@Override
		public void validarVenda(Venda venda) {
			venda.setValorDinheiro(venda.getValorTotal());
			venda.setValorCartao(BigDecimal.ZERO);
		}

		@Override
		public void finalizarPendentes(Long[] codigos, Vendas vendas) {
			vendas.findByCodigoIn(codigos).forEach(v -> {
				v.setValorDinheiro(v.getValorTotal());
				v.setStatus(StatusVenda.FINALIZADA);
			});
		}
	},
	CARTAO("Cartão") {
		@Override
		public void validarVenda(Venda venda) {
			venda.setValorCartao(venda.getValorTotal());
			venda.setValorDinheiro(BigDecimal.ZERO);
		}
		
		@Override
		public void finalizarPendentes(Long[] codigos, Vendas vendas) {
			vendas.findByCodigoIn(codigos).forEach(v -> {
				v.setValorCartao(v.getValorTotal());
				v.setStatus(StatusVenda.FINALIZADA);
			});
		}
	},
	DINHEIRO_E_CARTAO("Dinheiro e cartão") {
		@Override
		public void validarVenda(Venda venda) {
			if (venda.getValorCartao().add(venda.getValorDinheiro()).doubleValue() != venda.getValorTotal().doubleValue()) {
				throw new ValorDinheiroEValorCartaoIncorretosException("A soma entre o valor em dinheiro e o valor em cartão deve ser igual ao valor total");
			}
		}
		
		@Override
		public void finalizarPendentes(Long[] codigos, Vendas vendas) {	}
	};
	
	private String descricao;
	
	private TipoPagamento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public abstract void validarVenda(Venda venda);
	public abstract void finalizarPendentes(Long[] codigos, Vendas vendas);
}
