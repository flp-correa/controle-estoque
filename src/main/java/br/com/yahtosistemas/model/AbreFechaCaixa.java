package br.com.yahtosistemas.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "abre_fecha_caixa")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class AbreFechaCaixa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@Column(name = "tenant_id")
	private String tenantId;

	@Column(name = "data_hora_inicio")
	private LocalDateTime dataHoraInicio = LocalDateTime.now();
	
	@Column(name = "data_hora_fim")
	private LocalDateTime dataHoraFim;
	
	@NotNull
	@Column(name = "valor_inicio_din")
	private BigDecimal valorInicioDinheiro;
	
	@NotNull
	@Column(name = "valor_inicio_car")
	private BigDecimal valorInicioCartao;
	
	@Column(name = "total_dinheiro")
	private BigDecimal totalDinheiro;
	
	@Column(name = "total_cartao")
	private BigDecimal totalCartao;
	
	@Column(name = "valor_total")
	private BigDecimal valorTotal;
	
	@Column(name = "valor_sangria")
	private BigDecimal valorSangria;
	
	@Column(name = "valor_entrada")
	private BigDecimal valorEntrada;
	
	private String observacao;
	
	@Transient
	private LocalDate dataInicio = LocalDate.now();

	@Transient
	private LocalTime horaInicio = LocalTime.now();
	
	@Transient
	private LocalDate dataFim = LocalDate.now();

	@Transient
	private LocalTime horaFim = LocalTime.now();
	
	@Transient
	private BigDecimal valorSangriaOriginal;
	
	@Transient
	private BigDecimal valorEntradaOriginal;
	
	@Transient
	private OpcaoAbreFechaCaixa opcaoAbreFechaCaixa;
	
	@Transient
	private Usuario usuario;
	
	@Transient
	private boolean detalhes;

	@PostLoad
	private void postLoad() {
		this.setDataInicio(this.dataHoraInicio.toLocalDate());
		this.setHoraInicio(this.dataHoraInicio.toLocalTime());
		this.setValorSangriaOriginal(this.valorSangria);
		this.setValorEntradaOriginal(this.valorEntrada);
		
		if (this.dataHoraFim != null) {
			this.setDataFim(this.dataHoraFim.toLocalDate());
			this.setHoraFim(this.dataHoraFim.toLocalTime());
		}
	}
	
	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public LocalDateTime getDataHoraInicio() {
		return dataHoraInicio;
	}

	public void setDataHoraInicio(LocalDateTime dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public LocalDateTime getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(LocalDateTime dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public BigDecimal getValorInicioDinheiro() {
		return valorInicioDinheiro;
	}

	public void setValorInicioDinheiro(BigDecimal valorInicioDinheiro) {
		this.valorInicioDinheiro = valorInicioDinheiro;
	}

	public BigDecimal getValorInicioCartao() {
		return valorInicioCartao;
	}

	public void setValorInicioCartao(BigDecimal valorInicioCartao) {
		this.valorInicioCartao = valorInicioCartao;
	}

	public BigDecimal getValorSangria() {
		return valorSangria;
	}

	public void setValorSangria(BigDecimal valorSangria) {
		this.valorSangria = valorSangria;
	}

	public BigDecimal getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(BigDecimal valorEntrada) {
		this.valorEntrada = valorEntrada;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getTotalDinheiro() {
		return totalDinheiro;
	}

	public void setTotalDinheiro(BigDecimal totalDinheiro) {
		this.totalDinheiro = totalDinheiro;
	}

	public BigDecimal getTotalCartao() {
		return totalCartao;
	}

	public void setTotalCartao(BigDecimal totalCartao) {
		this.totalCartao = totalCartao;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(LocalTime horaFim) {
		this.horaFim = horaFim;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}
	
	public BigDecimal getValorSangriaOriginal() {
		return valorSangriaOriginal;
	}

	public void setValorSangriaOriginal(BigDecimal valorSangriaOriginal) {
		this.valorSangriaOriginal = valorSangriaOriginal;
	}

	public BigDecimal getValorEntradaOriginal() {
		return valorEntradaOriginal;
	}

	public void setValorEntradaOriginal(BigDecimal valorEntradaOriginal) {
		this.valorEntradaOriginal = valorEntradaOriginal;
	}

	public OpcaoAbreFechaCaixa getOpcaoAbreFechaCaixa() {
		return opcaoAbreFechaCaixa;
	}

	public void setOpcaoAbreFechaCaixa(OpcaoAbreFechaCaixa opcaoAbreFechaCaixa) {
		this.opcaoAbreFechaCaixa = opcaoAbreFechaCaixa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes do fechamento de caixa - %s", this.codigo);
		} else {
			return isNovo() ? "Abrir caixa" : "Fechar caixa";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbreFechaCaixa other = (AbreFechaCaixa) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
