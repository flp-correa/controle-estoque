package br.com.yahtosistemas.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.yahtosistemas.model.builder.AdicionalBuilder;
import br.com.yahtosistemas.model.builder.IngredienteBuilder;

@Entity
@Table(name = "item_pedido_venda")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
public class ItemPedidoVenda extends ItemVendaPedidoVenda {

	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "codigo_pedido_venda")
	private PedidoVenda pedidoVenda;
	
	@Enumerated(EnumType.STRING)
	private StatusItemCozinha statusCozinha;
	
	@Column(name = "load_cozinha")
	private Boolean loadCozinha;
	
	@ManyToMany
	@JoinTable(name = "item_ped_venda_adic", joinColumns = @JoinColumn(name = "codigo_item_ped_venda")
				, inverseJoinColumns = @JoinColumn(name = "codigo_adicional"))
	private List<Adicional> adicionais = new ArrayList<>();
	
	@ManyToMany
	@JoinTable(name = "item_ped_venda_ingred", joinColumns = @JoinColumn(name = "codigo_item_ped_venda")
				, inverseJoinColumns = @JoinColumn(name = "codigo_ingrediente"))
	private List<Ingrediente> ingredientes = new ArrayList<>();
	
	@Override
	public BigDecimal getValorTotalAdicionais() {
		return adicionais.stream().filter(a -> a.getSelecionado())
				.map(Adicional::getValorUnitario)
				.reduce(BigDecimal::add)
				.orElse(BigDecimal.ZERO);
	}
	
	public PedidoVenda getPedidoVenda() {
		return pedidoVenda;
	}

	public void setPedidoVenda(PedidoVenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	
	public StatusItemCozinha getStatusCozinha() {
		return statusCozinha;
	}

	public void setStatusCozinha(StatusItemCozinha statusCozinha) {
		this.statusCozinha = statusCozinha;
	}

	public Boolean getLoadCozinha() {
		return loadCozinha;
	}

	public void setLoadCozinha(Boolean loadCozinha) {
		this.loadCozinha = loadCozinha;
	}

	public List<Adicional> getAdicionais() {
		return adicionais;
	}

	public void setAdicionais(List<Adicional> adicionais) {
		this.adicionais = adicionais;
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public Boolean getDesabledBtnStatusCozinha() {
		if (!this.getStatusCozinha().equals(StatusItemCozinha.PRONTO)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public Boolean getDesabledExcluirItem(boolean ehAdm) {
		if (!ehAdm && ( (this.getCodigo() > 0 && !this.getProduto().getEnviarCozinha()) || 
				(this.getProduto().getEnviarCozinha() && !this.getStatusCozinha().equals(StatusItemCozinha.NAO_INICIADO)) ) ) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public Boolean getDesabledQuantidade(boolean ehAdm) {
		if ((this.pedidoVenda != null && !this.pedidoVenda.getStatus().equals(StatusPedidoVenda.EM_ANDAMENTO)) ||
				this.getProduto().getPorQuilo() || this.getProduto().getEnviarCozinha()) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean getPodeEditarValorUnitario(boolean ehAdm) {
		if (this.getProduto().getPorQuilo() && (this.getCodigo() < 0 || 
				(ehAdm && this.pedidoVenda != null && this.pedidoVenda.getStatus().equals(StatusPedidoVenda.EM_ANDAMENTO))
				)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public void restaurarIngredientesOriginal() {
		this.getProduto()
			.getIngredientes()
			.stream().map(temp -> {
				return new IngredienteBuilder()
					.comCodigo(temp.getCodigo())
					.comNome(temp.getNome())
					.construir();
		}).collect(Collectors.toList())
		.forEach((i) -> {
			if (!this.getIngredientes().contains(i)) {
				i.setSelecionado(Boolean.FALSE);
				this.getIngredientes().add(i);
			}
		});
	}
	
	public void restaurarAdicionaisOriginal(List<Adicional> adicionais) {
		this.getAdicionais().forEach(i -> i.setSelecionado(Boolean.TRUE));
		
		adicionais.stream().map(temp -> {
			return new AdicionalBuilder()
				.comCodigo(temp.getCodigo())
				.comNome(temp.getNome())
				.comValorUnitario(temp.getValorUnitario())
				.construir();
		}).collect(Collectors.toList())
		.forEach((a) -> {
			if (!this.getAdicionais().contains(a)) {
				a.setSelecionado(Boolean.FALSE);
				this.getAdicionais().add(a);
			}
		});
	}

	public void ordenarIngredientesItemPorNome() {
		Comparator<Ingrediente> comparator = Comparator.comparing(Ingrediente::getNome);
		this.setIngredientes(this.getIngredientes().stream().sorted(comparator).collect(Collectors.toList()));
	}
	
	public void ordenarAdicionaisItemPorNome() {
		Comparator<Adicional> comparator = Comparator.comparing(Adicional::getNome);
		this.setAdicionais(this.getAdicionais().stream().sorted(comparator).collect(Collectors.toList()));
	}
}
