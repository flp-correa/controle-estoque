package br.com.yahtosistemas.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "pedido_venda")
@FilterDef(name = "tenant", parameters = { @ParamDef(name="id", type="string") })
@Filter(name = "tenant", condition = "tenant_id = :id")
@DynamicUpdate
public class PedidoVenda extends VendaPedidoVenda {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private StatusPedidoVenda status = StatusPedidoVenda.EM_ANDAMENTO;
	
	@Column(name = "data_hora_fim")
	private LocalDateTime dataHoraFim;
	
	@OneToMany(mappedBy = "pedidoVenda", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ItemPedidoVenda> itens = new ArrayList<>();
	
	@Enumerated(EnumType.STRING)
	@Transient
	private TipoPedidoVenda tipoPedidoVenda = TipoPedidoVenda.PEDIDO_MESA; 
	
	@Transient
	private StatusItemCozinha statusCozinha;
	
	@Transient
	private Boolean lancheOuPratoEntregue = Boolean.FALSE;
	
	public StatusItemCozinha getStatusCozinha() {
		if (this.itens.stream().filter(i -> i.getStatusCozinha().equals(StatusItemCozinha.NAO_INICIADO)).findFirst().isPresent()) {
			return StatusItemCozinha.NAO_INICIADO;
		} else {
			return StatusItemCozinha.INICIADO;
		}
	}

	public void setStatusCozinha(StatusItemCozinha statusCozinha) {
		this.statusCozinha = statusCozinha;
	}

	public StatusPedidoVenda getStatus() {
		return status;
	}

	public void setStatus(StatusPedidoVenda status) {
		this.status = status;
	}
	
	public TipoPedidoVenda getTipoPedidoVenda() {
		return tipoPedidoVenda;
	}

	public void setTipoPedidoVenda(TipoPedidoVenda tipoPedidoVenda) {
		this.tipoPedidoVenda = tipoPedidoVenda;
	}

	public LocalDateTime getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(LocalDateTime dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	
	public List<ItemPedidoVenda> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedidoVenda> itens) {
		this.itens = itens;
	}

	public Boolean getLancheOuPratoEntregue() {
		return lancheOuPratoEntregue;
	}
	
	public void setLancheOuPratoEntregue(Boolean lancheOuPratoEntregue) {
		this.lancheOuPratoEntregue = lancheOuPratoEntregue;
	}

	public void adicionarItens(List<ItemVendaPedidoVenda> itens) {
		this.itens = itens.stream()
			    .filter(e-> e instanceof ItemPedidoVenda)
			    .map(e->(ItemPedidoVenda) e)
			    .collect(Collectors.toList());
	}
	
	public BigDecimal getValorTotalItens() {
		return getItens().stream()
				.map(ItemPedidoVenda::getValorTotal)
				.reduce(BigDecimal::add)
				.orElse(BigDecimal.ZERO);
	}
	
	public void calcularValorTotal() {
		setValorTotal(calcularValorTotal(getValorTotalItens(), getValorFrete()));
	}
	
	public boolean isCancelarPermitido() {
		return !getStatus().equals(StatusPedidoVenda.CANCELADO) && !isNovo();
	}
	
	public boolean isSalvarProibido() {
		return !getStatus().equals(StatusPedidoVenda.EM_ANDAMENTO);
	}
	
	public String getTituloCadastro() {
		if (isDetalhes()) {
			return String.format("Detalhes pedido de venda %s", this.getCodigo());
		} else {
			return isNovo() ? "Cadastro pedido de venda" : String.format("Pedido Venda de código %s", this.getCodigo());
		}
	}
	
	private BigDecimal calcularValorTotal(BigDecimal valorTotalItens, BigDecimal valorFrete) {
		BigDecimal valorTotal = valorTotalItens
				.add(Optional.ofNullable(valorFrete).orElse(BigDecimal.ZERO));
		return valorTotal;
	}

}