package br.com.yahtosistemas.model;

import java.util.List;

import br.com.yahtosistemas.repository.ItensPedidosVendas;
import br.com.yahtosistemas.repository.PedidosVendas;
import br.com.yahtosistemas.service.exception.ErroSalvarPedidoVendaException;
import br.com.yahtosistemas.service.exception.ItemLancheOuPratoEntregueObrigatorioException;

public enum StatusPedidoVenda {

	EM_ANDAMENTO("Em andamento") {
		@Override
		public void validarPedidoVenda(PedidoVenda pedidoVenda, PedidosVendas pedidosVendas, ItensPedidosVendas itensPedidosVendas) {
			if (!pedidoVenda.isNovo()) {				
				StatusPedidoVenda statusSalvo = pedidosVendas.buscarStatusPedidoVenda(pedidoVenda.getCodigo()).getStatus();
				if (!statusSalvo.equals(EM_ANDAMENTO)) {
					pedidoVenda.setStatus(statusSalvo);
					pedidoVenda.setDetalhes(true);
					throw new ErroSalvarPedidoVendaException("Pedido de venda já esta encerrado e não pode ser alterado");
				} else {
					List<ItemPedidoVenda> itensSalvos = itensPedidosVendas.findByPedidoVenda(pedidoVenda);
					itensSalvos.forEach(i -> {
						if ( i.getProduto().getEnviarCozinha() && 
								(i.getStatusCozinha().equals(StatusItemCozinha.INICIADO) || i.getStatusCozinha().equals(StatusItemCozinha.PRONTO)) &&
								!pedidoVenda.getItens().contains(i)) {
							
							throw new ErroSalvarPedidoVendaException(String.format("%s não pode ser excluído, lanche está %s",
									i.getProduto().getDescricao(), i.getStatusCozinha().getDescricao()));
						}
					});
				}
			}
		}
	},
	FINALIZADO("Finalizado") {
		@Override
		public void validarPedidoVenda(PedidoVenda pedidoVenda, PedidosVendas pedidosVendas, ItensPedidosVendas itensPedidosVendas) {
			if (pedidoVenda.getItens().stream().filter(i -> 
				i.getProduto().getEnviarCozinha() && !i.getStatusCozinha().equals(StatusItemCozinha.ENTREGUE)).findFirst().isPresent()) {
			
				throw new ItemLancheOuPratoEntregueObrigatorioException("Lanche/prato deve estar marcado como entregue");
			}
		}
	}, 
	CANCELADO("Cancelado") {
		@Override
		public void validarPedidoVenda(PedidoVenda pedidoVenda, PedidosVendas pedidosVendas, ItensPedidosVendas itensPedidosVendas) {
			
		}
	};

	private String descricao;

	StatusPedidoVenda(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public abstract void validarPedidoVenda(PedidoVenda pedidoVenda, PedidosVendas pedidosVendas, ItensPedidosVendas itensPedidosVendas);
}
