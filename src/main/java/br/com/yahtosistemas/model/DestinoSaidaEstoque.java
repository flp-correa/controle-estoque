package br.com.yahtosistemas.model;

public enum DestinoSaidaEstoque {

	UNIDADE_PROPRIA("Matriz/Filial"),
	VENDA("Venda");
	
	private String descricao;
	
	private DestinoSaidaEstoque(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
