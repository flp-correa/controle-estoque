package br.com.yahtosistemas.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.EmpresaBuilder;
import br.com.yahtosistemas.model.Fornecedor;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.Fornecedores;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControleEstoqueApplication.class)
public class CadastroFornecedorServiceTest {

	@Autowired
	private CadastroFornecedorService cadastroFornecedorService;
	
	@Mock
	@Autowired
	private Fornecedores fornecedoresComMock;
	
	@Autowired
	private Fornecedores fornecedoresSemMock;
	
	private EmpresaBuilder empresa;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		//empresa = new EstabelecimentoBuilder(new Fornecedor());
	}
	
	private void criaFornecedor(UnidadeEstabelecimento unidade, String cpfOuCnpj) {
		empresa.comUnidadeEmpresa(unidade)
			.comCpfOuCnpj(cpfOuCnpj);
	}
	
	/*@Test
	public void deveSalvarFornecedorMatriz() throws Exception {
		criaFornecedor(UnidadeEstabelecimento.MATRIZ, "00.063.960/0027-30");
		cadastroFornecedorService.salvar((Fornecedor) empresa.construir(), fornecedoresComMock);
		Mockito.verify(fornecedoresComMock).save((Fornecedor) empresa.construir());
	}
	
	@Test
	public void deveChamarMetodoSeJaExisteMatrizCadastrada() throws Exception {
		criaFornecedor(UnidadeEstabelecimento.MATRIZ, "00.063.960/0027-30");
		cadastroFornecedorService.salvar((Fornecedor) empresa.construir(), fornecedoresComMock);
		Mockito.verify(fornecedoresComMock).seJaExisteMatrizCadastrada(empresa.construir().getCpfOuCnpjSemFormatacao().substring(0, 8));
	}
	
	@Test
	public void deveChamarMetodoSeJaExisteCnpjCadastrado() throws Exception {
		criaFornecedor(UnidadeEstabelecimento.MATRIZ, "00.063.960/0027-30");
		cadastroFornecedorService.salvar((Fornecedor) empresa.construir(), fornecedoresComMock);
		Mockito.verify(fornecedoresComMock).seJaExisteCnpjCadastrado(empresa.construir().getCpfOuCnpjSemFormatacao());
	}
	
	@Test(expected = EstabelecimentoJaCadastradaUnidadeMatrizException.class)
	public void deveGerarErroDeMatrizJaCadastrada() throws Exception {
		criaFornecedor(UnidadeEstabelecimento.MATRIZ, "00.063.960/0071-03");
		cadastroFornecedorService.salvar((Fornecedor) empresa.construir(), fornecedoresSemMock);
	}*/
}
