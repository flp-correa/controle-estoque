package br.com.yahtosistemas.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.Empresa;
import br.com.yahtosistemas.model.EmpresaBuilder;
import br.com.yahtosistemas.model.TipoPessoa;
import br.com.yahtosistemas.model.UnidadeEstabelecimento;
import br.com.yahtosistemas.repository.Empresas;
import br.com.yahtosistemas.service.exception.EstabelecimentoJaCadastradaUnidadeMatrizException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControleEstoqueApplication.class)
public class CadastroEmpresaServiceTest {

	@Autowired
	private CadastroEmpresaService cadastroEmpresaService;
	
	@Mock
	@Autowired
	private Empresas empresasComMock;
	
	@Autowired
	private Empresas empresasSemMock;
	
	private EmpresaBuilder empresa;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		empresa = new EmpresaBuilder(new Empresa());
	}
	
	private void criaEmpresa(String cep, String cpfOuCnpj, String email, String nomeEmpresarial, 
			Boolean possuiEstoque, TipoPessoa tipoPessoa, UnidadeEstabelecimento unidade, Long codigo) {
		empresa.comCep(cep)
				.comCpfOuCnpj(cpfOuCnpj)
				.comEmail(email)
				.comNomeEmpresarial(nomeEmpresarial)
				.comPossuiEstoque(possuiEstoque)
				.comTipoPessoa(tipoPessoa)
				.comUnidadeEmpresa(unidade)
				.comCodigo(codigo);
	}
	
	@Test
	public void deveChamarMetodoSeJaExisteMatrizCadastrada() throws Exception {
		criaEmpresa("", "00.063.960/0027-30", "empresa1@gmail.com", "Empresa 1", Boolean.TRUE, TipoPessoa.JURIDICA, UnidadeEstabelecimento.MATRIZ, null);
		cadastroEmpresaService.salvar((Empresa) empresa.comTenantId("yahto-controle-estoque").construir(), empresasComMock);
		Mockito.verify(empresasComMock).seJaExisteMatrizCadastrada(empresa.comTenantId("yahto-controle-estoque").construir().getCpfOuCnpjSemFormatacao().substring(0, 8));
	}
	
	@Test
	public void deveChamarMetodoSeJaExisteCnpjCadastrado() throws Exception {
		criaEmpresa("", "00.063.960/0027-30", "empresa1@gmail.com", "Empresa 1", Boolean.TRUE, TipoPessoa.JURIDICA, UnidadeEstabelecimento.MATRIZ, null);
		cadastroEmpresaService.salvar((Empresa) empresa.comTenantId("yahto-controle-estoque").construir(), empresasComMock);
		Mockito.verify(empresasComMock).seJaExisteCnpjCadastrado(empresa.comTenantId("yahto-controle-estoque").construir().getCpfOuCnpjSemFormatacao());
	}
	
	@Test(expected = EstabelecimentoJaCadastradaUnidadeMatrizException.class)
	public void deveGerarErroDeMatrizJaCadastrada() throws Exception {
		criaEmpresa("", "00.063.960/0071-03", "empresa1@gmail.com", "Empresa 1", Boolean.TRUE, TipoPessoa.JURIDICA, UnidadeEstabelecimento.MATRIZ, null);
		cadastroEmpresaService.salvar((Empresa) empresa.comTenantId("yahto-controle-estoque").construir(), empresasSemMock);
	}
}
