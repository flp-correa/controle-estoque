package br.com.yahtosistemas.service;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.Produto;
import br.com.yahtosistemas.model.Usuario;
import br.com.yahtosistemas.model.builder.MovimentoEstoqueBuilder;
import br.com.yahtosistemas.repository.MovimentacoesEstoque;
import br.com.yahtosistemas.service.exception.EntidadeObrigatoriaException;
import br.com.yahtosistemas.service.exception.ProdutoObrigatorioException;
import br.com.yahtosistemas.service.exception.ValorUnitarioObrigatorioException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControleEstoqueApplication.class)
public class CadastroMovimentoEstoqueServiceTest {

	@Autowired
	private CadastroMovimentoEstoqueService cadastroMovimentoEstoqueService;
	
	@Mock
	@Autowired
	private MovimentacoesEstoque movimentacoesEstoque;
	
	private MovimentoEstoqueBuilder movimentoEstoque;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		movimentoEstoque = new MovimentoEstoqueBuilder();
	}
	
	private void criaMovimentoEstoque(Long codFornec, Long codProduto, BigDecimal quantidade, BigDecimal valorUnitario) {
		movimentoEstoque.comFornecedor(codFornec)
			.comProduto(new Produto())
			//.comTipoMovimentoEntradaEstoque()
			.comQuantidade(quantidade)
			.comValorUnitario(valorUnitario);
	}
	
	@Test
	public void deveSalvarMovimentoEstoque() throws Exception {
		criaMovimentoEstoque(1L, 1L, new BigDecimal(10.0), new BigDecimal(3.5));
		cadastroMovimentoEstoqueService.salvar(movimentoEstoque.comCodigo(null).construir(), movimentacoesEstoque, new Usuario());
		Mockito.verify(movimentacoesEstoque).save(movimentoEstoque.comCodigo(null).construir());
	}
	
	@Test
	public void deveChamarMetodobuscarUltimaMovimentacaoPorCodigoProduto() throws Exception {
		criaMovimentoEstoque(1L, 1L, new BigDecimal(10.0), new BigDecimal(3.5));
		cadastroMovimentoEstoqueService.salvar(movimentoEstoque.comCodigo(null).construir(), movimentacoesEstoque, new Usuario());
		Mockito.verify(movimentacoesEstoque).buscarUltimaMovimentacaoPorProduto(movimentoEstoque.comCodigo(null).construir().getProduto());
	}

	@Test(expected = ProdutoObrigatorioException.class)
	public void deveGerarErrodeProdutoObrigatorio() throws Exception {
		criaMovimentoEstoque(1L, null, new BigDecimal(10.0), new BigDecimal(3.5));
		cadastroMovimentoEstoqueService.salvar(movimentoEstoque.comCodigo(null).construir(), movimentacoesEstoque, new Usuario());
	}
	
	@Test(expected = EntidadeObrigatoriaException.class)
	public void deveGerarErrodeFornecedorObrigatorio() throws Exception {
		criaMovimentoEstoque(null, 1L, new BigDecimal(10.0), new BigDecimal(3.5));
		cadastroMovimentoEstoqueService.salvar(movimentoEstoque.comCodigo(null).construir(), movimentacoesEstoque, new Usuario());
	}
	
	@Test(expected = ValorUnitarioObrigatorioException.class)
	public void deveGerarErrodeValorObrigatorioObrigatorio() throws Exception {
		criaMovimentoEstoque(1L, 1L, new BigDecimal(10.0), null);
		cadastroMovimentoEstoqueService.salvar(movimentoEstoque.comCodigo(null).construir(), movimentacoesEstoque, new Usuario());
	}
	
	
}
