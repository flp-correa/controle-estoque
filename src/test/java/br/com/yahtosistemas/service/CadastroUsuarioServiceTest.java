package br.com.yahtosistemas.service;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.yahtosistemas.ControleEstoqueApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControleEstoqueApplication.class)
public class CadastroUsuarioServiceTest {

	/*@Autowired
	private CadastroUsuarioService cadastroUsuarioService;
	
	@Mock
	@Autowired
	private Usuarios usuariosComMock;
	
	@Autowired
	private Usuarios usuariosSemMock;
	
	private UsuarioBuilder usuario;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		usuario = new UsuarioBuilder();
	}
	
	private Usuario criaUsuario(String tenantId, String email, String cpf, String nome, String senha, String confirmacaoSenha, Long codigo) {
		return usuario.comEmail(email)
				.comDataCadastro(LocalDate.now())
				.comDeposito(deposito)
			.comCpf(cpf)
			.comNome(nome)
			.comSenha(senha)
			.comConfirmacaoSenha(confirmacaoSenha)
			.comCodigo(codigo).construir();
	}
	
	@Test
	public void deveSalvarUsuarioSupervisor() throws Exception {
		Usuario usuarioSupervisor = criaUsuario("yahto-controle-estoque","supervisor@gmail.com", "66738493045", "Operacional", "sup", "sup", null);
		cadastroUsuarioService.salvar(usuarioSupervisor, usuariosSemMock);
		Mockito.verify(usuariosSemMock).save(usuarioSupervisor);
	}
	
	@Test(expected = EmailUsuarioJaCadastradoException.class)
	public void deveGerarErroEmailUsuarioJaCadastradoExceptionComEmailDeUmUsuarioESenhaDeOutro() throws Exception {
		criaUsuario("felipe.yahtosistemas@gmail.com", "00713690909'", "Felipe Correa", "1234", "1234", 2L);
		Usuario usuarioTeste = usuario.construir();
		cadastroUsuarioService.salvar(usuarioTeste, usuariosSemMock);
	}
	
	@Test(expected = EmailUsuarioJaCadastradoException.class)
	public void deveGerarErroEmailUsuarioJaCadastradoExceptionNovoUsuario() throws Exception {
		criaUsuario("felipe.yahtosistemas@gmail.com", "00713690909'", "Felipe Correa", "1234", "1234", null);
		Usuario usuarioTeste = usuario.construir();
		cadastroUsuarioService.salvar(usuarioTeste, usuariosSemMock);
	}
	
	@Test(expected = SenhaObrigatoriaUsuarioException.class)
	public void deveGerarErroSenhaObrigatoriaExceptionNovoUsuario() throws Exception {
		criaUsuario("joao@gmail.com", "00713690909'", "Felipe Correa", "", "", null);
		Usuario usuarioTeste = usuario.construir();
		cadastroUsuarioService.salvar(usuarioTeste, usuariosSemMock);
	}*/
}
