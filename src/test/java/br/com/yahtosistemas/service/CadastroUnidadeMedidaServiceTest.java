package br.com.yahtosistemas.service;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.yahtosistemas.ControleEstoqueApplication;
import br.com.yahtosistemas.model.UnidadesMedidaBuilder;
import br.com.yahtosistemas.repository.UnidadesMedida;
import br.com.yahtosistemas.service.exception.EntidadeJaCadastradaException;
import br.com.yahtosistemas.service.exception.QuantidadeDoTipoDeUnidadeInvalidaException;
import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ControleEstoqueApplication.class)
public class CadastroUnidadeMedidaServiceTest {

	@Autowired
	private CadastroUnidadeMedidaService cadastroUnidadeMedidaService;
	
	@Mock
	@Autowired
	private UnidadesMedida unidadesMedidaComMock;
	
	@Autowired
	private UnidadesMedida unidadesMedidaSemMock;
	
	private UnidadesMedidaBuilder unidadeMedida;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		unidadeMedida = new UnidadesMedidaBuilder();
	}
	
	private void criaUnidadesMedida(Long codigo, TipoUnidadeMedida tipoUnidadeMedida, BigDecimal quantidade) {
		unidadeMedida.comCodigo(codigo)
			.comTipoUnidadeMedida(tipoUnidadeMedida)
			.comQuantidade(quantidade);
	}
	
	@Test
	public void deveSalvarUnidadeMedida() throws Exception {
		criaUnidadesMedida(null, TipoUnidadeMedida.CX, new BigDecimal("100.00"));
		cadastroUnidadeMedidaService.salvar(unidadeMedida.construir(), unidadesMedidaComMock);
		Mockito.verify(unidadesMedidaComMock).save(unidadeMedida.construir());
	}

	@Test
	public void deveSalvarAlterarSituacaoUnidadeMedida() throws Exception {
		cadastroUnidadeMedidaService.alterarSituacao(new Long[1], SituacaoUnidadeMedida.ATIVAR, unidadesMedidaComMock);
		Mockito.verify(unidadesMedidaComMock).findByCodigoIn(new Long[1]);
	}
	
	@Test(expected = QuantidadeDoTipoDeUnidadeInvalidaException.class)
	public void deveGerarErroQuantidadeDoTipoDeUnidadeInvalidaException() throws Exception {
		criaUnidadesMedida(null, TipoUnidadeMedida.CX, BigDecimal.ZERO);
		cadastroUnidadeMedidaService.salvar(unidadeMedida.construir(), unidadesMedidaSemMock);
	}
	
	@Test(expected = EntidadeJaCadastradaException.class)
	public void deveGerarErroEntidadeJaCadastradaException() throws Exception {
		criaUnidadesMedida(null, TipoUnidadeMedida.UNID, BigDecimal.ONE);
		cadastroUnidadeMedidaService.salvar(unidadeMedida.construir(), unidadesMedidaSemMock);
	}
}