package br.com.yahtosistemas.model;

public class EmpresaBuilder {

	private Empresa instancia;
	
	public EmpresaBuilder(Empresa instancia) {
		this.instancia = instancia;
	}
	
	public EmpresaBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}
	
	public EmpresaBuilder comUnidadeEmpresa(UnidadeEstabelecimento unidade) {
		instancia.setUnidadeEmpresa(unidade);
		return this;
	}
	
	public EmpresaBuilder comTipoPessoa(TipoPessoa tipoPessoa) {
		instancia.setTipoPessoa(tipoPessoa);
		return this;
	}
	
	public EmpresaBuilder comCpfOuCnpj(String cpfOuCnpj) {
		instancia.setCpfOuCnpj(cpfOuCnpj);
		return this;
	}

	public EmpresaBuilder comNomeEmpresarial(String nomeEmpresarial) {
		instancia.setNomeEmpresarial(nomeEmpresarial);
		return this;
	}
	
	public EmpresaBuilder comEmail(String email) {
		instancia.setEmail(email);
		return this;
	}
	
	
	public EmpresaBuilder comCep(String cep) {
		instancia.setEndereco(new Endereco());
		instancia.getEndereco().setCep("");
		return this;
	}
	
	public EmpresaBuilder comPossuiEstoque(Boolean possuiEstoque) {
		instancia.setPossuiEstoque(possuiEstoque);
		return this;
	}
	
	public EmpresaBuilderValido comTenantId(String tenantId) {
		instancia.setTenantId(tenantId);
		return new EmpresaBuilderValido(instancia);
	}
}
