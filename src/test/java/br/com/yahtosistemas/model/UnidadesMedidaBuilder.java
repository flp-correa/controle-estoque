package br.com.yahtosistemas.model;

import java.math.BigDecimal;

import br.com.yahtosistemas.service.unidadeMedida.TipoUnidadeMedida;

public class UnidadesMedidaBuilder {

	private UnidadeMedida instancia;
	
	public UnidadesMedidaBuilder() {
		instancia = new UnidadeMedida();
	}
	
	public UnidadesMedidaBuilder comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return this;
	}
	
	public UnidadesMedidaBuilder comTipoUnidadeMedida(TipoUnidadeMedida tipoUnidadeMedida) {
		instancia.setTipoUnidadeMedida(tipoUnidadeMedida);
		return this;
	}
	
	public UnidadesMedidaBuilder comQuantidade(BigDecimal quantidade) {
		instancia.setQuantidade(quantidade);
		return this;
	}
	
	public UnidadeMedida construir() {
		return instancia;
	}
}
