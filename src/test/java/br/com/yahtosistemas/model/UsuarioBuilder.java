package br.com.yahtosistemas.model;

import java.time.LocalDate;
import java.util.List;

public class UsuarioBuilder {

	private Usuario instancia;
	
	public UsuarioBuilder() {
		instancia = new Usuario();
	}
	
	public UsuarioBuilder comTenantId(String tenantId) {
		instancia.setTenantId(tenantId);
		return this;
	}
	
	public UsuarioBuilder comEmail(String email) {
		instancia.setEmail(email);
		return this;
	}
	
	public UsuarioBuilder comCpf(String cpf) {
		instancia.setCpf(cpf);
		return this;
	}
	
	public UsuarioBuilder comNome(String nome) {
		instancia.setNome(nome);
		return this;
	}
	
	public UsuarioBuilder comSenha(String senha) {
		instancia.setSenha(senha);
		return this;
	}
	
	public UsuarioBuilder comConfirmacaoSenha(String confirmacaoSenha) {
		instancia.setConfirmacaoSenha(confirmacaoSenha);
		return this;
	}
	
	public UsuarioBuilder comTelefone(String telefone) {
		instancia.setTelefone(telefone);
		return this;
	}
	
	public UsuarioBuilder comNascimento(LocalDate dataNascimento) {
		instancia.setDataNascimento(dataNascimento);
		return this;
	}
	
	public UsuarioBuilder comDataCadastro(LocalDate dataCadastro) {
		instancia.setDataCadastro(dataCadastro);
		return this;
	}
	
	public UsuarioBuilder comDeposito(Deposito deposito) {
		instancia.setDeposito(deposito);
		return this;
	}
	
	public UsuarioBuilder comGrupos(List<Grupo> grupos) {
		instancia.setGrupos(grupos);
		return this;
	}
	
	public UsuarioBuilderValido comCodigo(Long codigo) {
		instancia.setCodigo(codigo);
		return new UsuarioBuilderValido(instancia);
	}
}
