package br.com.yahtosistemas.model;

public class EmpresaBuilderValido {

	private Empresa empresa;
	
	public EmpresaBuilderValido(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Empresa construir() {
		return empresa;
	}
}
