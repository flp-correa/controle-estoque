package br.com.yahtosistemas.model;

public class UsuarioBuilderValido {

	private Usuario usuario;
	
	public UsuarioBuilderValido(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Usuario construir() {
		return usuario;
	}
}
